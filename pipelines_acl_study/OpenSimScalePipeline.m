function OpenSimScalePipeline(rootFolder, genericModelFilename,...
    markersetFilename, scaleSetupFilename, subjectsPrefix, conditions,...
    staticTrialsRegExp, varargin)
% Scale the generic OpenSim model using a static trial as reference. A
% scaled .osim model is saved in each subject's folder.
%
% NOTE: there should be only a SINGLE STATIC TRIAL in each subject folder,
% otherwise the scaled model generated using the latest static trial will
% overwrite the scaled models generated using previous static trials.
%
% Input:
%   rootFolder = folder containing all subjects' subfolders.
%   genericModelFilename = filename of the generic .osim model to be scaled.
%   markersetFilename = filename of the .xml file containing the markerset.
%   scaleSetupFilename = filename of the .xml scale setup file.
%   subjectsPrefix = case insensitive string containing the prefix of the
%                 subjects' folders names. Generally a 3 characters string,
%                 e.g. 'DEV'.
%   conditions = cell array of strings containing the names of the
%               subfolders representing different capture conditionts,
%                e.g. {'Barefoot', 'Kayano'}.
%   staticTrialsRegExp = string containing a regular expression matching
%               the names of the static trials name used for static
%               trials, e.g. Cal\s\d+.trc' for Vicon or
%               'static_\d+_pre-processed.trc' for KinEdge.
%
% � July 2016 Alessandro Timmi

% TODO:
% - check when more than one static trial are available for the same
%   subject. Ideally, we should pick the one that returns the lowest RMS
%   error after scaling.


%% Default input values:
default.debugMode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'rootFolder',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'genericModelFilename',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'markersetFilename',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'scaleSetupFilename',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'subjectsPrefix',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'conditions',...
    @(x) validateattributes(x, {'cell'}, {'nonempty'}));
addRequired(p, 'staticTrialsRegExp',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));


% Parse input arguments:
parse(p, rootFolder, genericModelFilename, markersetFilename,...
    scaleSetupFilename, subjectsPrefix, conditions, staticTrialsRegExp,...
    varargin{:});

% Copy input arguments into more memorable variables:
rootFolder = p.Results.rootFolder;
genericModelFilename = p.Results.genericModelFilename;
markersetFilename = p.Results.markersetFilename;
scaleSetupFilename = p.Results.scaleSetupFilename;
subjectsPrefix = p.Results.subjectsPrefix;
conditions = p.Results.conditions;
staticTrialsRegExp = p.Results.staticTrialsRegExp;

clear varargin default p


%% Import OpenSim modelling classes:
import org.opensim.modeling.*

fprintf('OpenSim Scale pipeline\n')


%% Display input arguments:
fprintf('Root folder:\n\t%s\n', rootFolder)
fprintf('Model filename:\n\t%s\n', genericModelFilename)
[~, genericModelName, genericModelExt] = fileparts(genericModelFilename);

fprintf('Markerset filename:\n\t%s\n', markersetFilename)
fprintf('Scale setup filename:\n\t%s\n', scaleSetupFilename)
fprintf('Subjects'' prefix:\n\t%s\n', subjectsPrefix)
fprintf('Conditions:\n\t%s\n', conditions{:})
fprintf('Static trials regular expression:\n\t%s\n', staticTrialsRegExp)



%% List all trials in the root folder:
allTrials = list_trials_in_all_subjects(rootFolder,...
    subjectsPrefix,...
    conditions,...
    '',...
    '.trc');

% Select only static trials, using regular expressions:
startIndices = regexpi(allTrials, staticTrialsRegExp, 'once');
staticTrialsLogical = ~cellfun(@isempty, startIndices);
staticTrialsFilenames = allTrials(staticTrialsLogical);

if isempty(staticTrialsFilenames)
    fprintf('No static trials found.\n')
else
    fprintf('Static trials found:\n')
    fprintf('\t%s\n', staticTrialsFilenames{:})
end


% Obtain a scaled model for each static trial:
for f = 1:length(staticTrialsFilenames)
    fprintf('Scaling subject %d of %d.\n', f, length(staticTrialsFilenames))
    
    %% Open model
    % Load model from file:
    % (this could be probably done outside the for loop)
    genericModel = Model(genericModelFilename);
    
    % Initialize the model:
    myState = genericModel.initSystem();
    
    % Load markerset:
    markerset = MarkerSet(markersetFilename);
    
    % Attach markerset to model:
    genericModel.replaceMarkerSet(myState, markerset);
    
    
    %% Load static trial, to be used for scaling:
    staticData = MarkerData(staticTrialsFilenames{f});
    
    % Set time range for scaling:
    initialTime = staticData.getStartFrameTime();
    finalTime = staticData.getLastFrameTime();
    timeArray = ArrayDouble();
    timeArray.set(0, initialTime);
    timeArray.set(1, finalTime);
    
    % The scaled model will be stored in the current subject's folder:
    staticTrialPath = fileparts(staticTrialsFilenames{f});
    % Get current subject name (case insensitive):
    subjectName = regexpi(staticTrialsFilenames{f},...
        [subjectsPrefix, '\d+'], 'match', 'once');
    % Assemble filename of the scaled model:
    scaledModelName = [subjectName, '_scaled_', genericModelName];
    scaledModelFilename = fullfile(staticTrialPath, [scaledModelName,...
        genericModelExt]);
    % Rename the model object too:
    genericModel.setName(scaledModelName)
    
    %% Scaling
    % New Scale Tool object based on ScaleSetup.xml file:
    scaleTool = ScaleTool(scaleSetupFilename);
    
    % Scaling is composed of 2 steps: 1) model scaler, 2) marker placer.
    % 1) Model Scaler
    % Set marker file (.trc) to be used for scaling (usually a static trial):
    scaleTool.getModelScaler().setMarkerFileName(staticTrialsFilenames{f});
    % Set the time range:
    scaleTool.getModelScaler().setTimeRange(timeArray);
    % Set whether or not to preserve relative mass between segments:
    scaleTool.getModelScaler().setPreserveMassDist(1);
    % Don't export any file at this stage, because we still need to run the
    % marker placer:
    scaleTool.getModelScaler().setPrintResultFiles(0);    
    % Get current subject's mass from spreadsheet containing subject's
    % info for the ACL study:
    subjectMass = GetSubjectMass(subjectName);
    % Run Model Scaler, also setting the total mass of the current subject:
    scaleTool.getModelScaler().processModel(myState, genericModel, '', subjectMass);
    
    % 2) Marker Placer
    % Set marker file (.trc) containing the static pose:
    scaleTool.getMarkerPlacer().setStaticPoseFileName(staticTrialsFilenames{f});
    % Set the time range:
    scaleTool.getMarkerPlacer().setTimeRange(timeArray);
    % Set output OpenSim model file (.osim) to write when done placing markers:
    scaleTool.getMarkerPlacer().setOutputModelFileName(scaledModelFilename);
    % Now we need to export the scaled .osim model:
    scaleTool.getMarkerPlacer().setPrintResultFiles(1);
    % Run Marker Placer:
    scaleTool.getMarkerPlacer().processModel(myState, genericModel, '');
    
    
end

fprintf('End of OpenSim Scale pipeline.\n\n')
end

