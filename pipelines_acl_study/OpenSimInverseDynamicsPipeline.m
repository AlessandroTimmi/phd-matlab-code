function OpenSimInverseDynamicsPipeline(rootFolder, ikRegExp,...
    subjectsPrefix, conditions, varargin)
% Run inverse dynamics (ID) using OpenSim API.
%
% Input:
%   rootFolder = folder containing all subjects' subfolders.
%   ikRegExp = string containing a regular expression matching
%               the names of the IK results.
%   subjectsPrefix = case insensitive string containing the prefix of the
%               subjects' folders names. Generally a 3 characters string,
%               e.g. 'DEV'.
%   conditions = cell array of strings containing the names of the
%               subfolders representing different capture conditionts,
%                e.g. {'Barefoot', 'Kayano'}.
%
%   resultsSubfolder = (optional parameter, default = 'ID results')
%               subfolder in which ID results will be stored.
%
% NOTE: due to some bugs in the OpenSim 3.3 API, the Inverse Dynamics tool
% causes matlab to crash. From what I could understand on their GitHub and
% from some testing, it could be due to issues when the model is loaded,
% causing some memory leak in the Java Virtual Machine. To temporarily make
% the tool work, I increased the Java memory heap from 384 MB to 4,061 MB
% in Matlab: Preferences > General > Java Heap Memory.
%
% � October 2016 Alessandro Timmi

% TODO:
% - allow for multiple trial types to be selected and loaded.
% - replace errors with "return" in the parfor loop, using function
%   evaluation.


%% Default input values:
default.resultsSubfolder = 'ID results';

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'rootFolder',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'ikRegExp',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'subjectsPrefix',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'conditions',...
    @(x) validateattributes(x, {'cell'}, {'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'resultsSubfolder', default.resultsSubfolder, @ischar);

% Parse input arguments:
parse(p, rootFolder, ikRegExp, subjectsPrefix, conditions, varargin{:});

% Copy input arguments into more memorable variables:
rootFolder = p.Results.rootFolder;
ikRegExp = p.Results.ikRegExp;
subjectsPrefix = p.Results.subjectsPrefix;
conditions = p.Results.conditions;
resultsSubfolder = p.Results.resultsSubfolder;

clear varargin default p


%% Import OpenSim modelling classes:
import org.opensim.modeling.*

fprintf('OpenSim Inverse Dynamics (ID) pipeline\n')


%% Display input arguments:
fprintf('Root folder:\n\t%s\n', rootFolder)
fprintf('Regular expression for IK results:\n\t%s\n', ikRegExp)
fprintf('Subjects'' prefix: %s.\n', subjectsPrefix)
fprintf('Conditions:\n\t%s\n', conditions{:})


%% List all .mot trials in the root folder:
allMotFiles = list_trials_in_all_subjects(rootFolder,...
    subjectsPrefix,...
    conditions,...
    '',...
    '.mot');

% Select only trials of the desired type, using regular expressions:
ikStartIndices = regexpi(allMotFiles, ikRegExp, 'once');
ikLogical = ~cellfun(@isempty, ikStartIndices);
ikFilenames = allMotFiles(ikLogical);

fprintf('Found %d "%s" trials:\n', length(ikFilenames), ikRegExp)
fprintf('\t%s\n', ikFilenames{:})


%% Run ID for each trial.
% Set parameter "forces_to_exclude", the same for all trials:
excludedForces = ArrayStr();
excludedForces.append('Muscles');

% Start timer:
tic
% Not sure why, but within a parfor loop the OpenSim classes must be
% called using their full names, even if they are imported on top of this
% function.
for f = 1:length(ikFilenames)
    
    fprintf('Trial %d of %d.\n', f, length(ikFilenames))
    
    % Get filename of the scaled model for current subject:
    [subjectFolder, ikName] = fileparts(ikFilenames{f});
    scaledModelFilename = FindScaledOpenSimModel(subjectFolder, subjectsPrefix);
    
    % Make results folder if it doesn't exist:
    resultsFolder = fullfile(subjectFolder, resultsSubfolder);
    if ~isequal(exist(resultsFolder, 'dir'), 7)
        mkdir(resultsFolder);
    end
    
    % Check if the output file already exists:
    idName = [ikName '_id.sto'];
    if isequal(exist(idName, 'file'), 2)
        % Skip this trial. We cannot make this part more user-interactive,
        % due to the parallel nature of this loop.
        warning('This file already exists:\n\t%s.\nSkipping...\n', idName)
    else
        
        % Find GRF file corresponding to current IK file:
        viconGrfFilename = FindGrfCorrespondingToIkFilename(ikFilenames{f}, '_sync');
        externalLoadsFilename = fullfile(subjectFolder, 'ExternalLoads.xml');
        
        % Create External Loads object and print it to an .xml file:
        fprintf('� Setting up External Loads and printing .xml file...\n')
        SetExternalLoads(externalLoadsFilename, 2, 'calcn_r', viconGrfFilename, ikFilenames{f});
        
        % Get time range from current trial:
        fprintf('� Getting start and end time from IK file...\n')
        ikData = org.opensim.modeling.Storage(ikFilenames{f});
        initialTime = ikData.getFirstTime();
        finalTime = ikData.getLastTime();
        
        % Setup Inverse Dynamics tool for current trial.
        fprintf('� Setting up ID tool...\n')
        % The information required to setup ID is so little (and trial-specific)
        % that we don't need a generic ID setup .xml file for the constructor.
        % We can simply pass the required information via API (below) and leave
        % the parentheses empty:
        idTool = org.opensim.modeling.InverseDynamicsTool();
        
        idTool.setName('InverseDynamicsSettingsAle')
        idTool.setResultsDir(resultsFolder);
        idTool.setModelFileName(scaledModelFilename);
        % NOTE: in alternative to setModelFileName, we could pass a model object,
        % but first we would need to initialize it using:
%         scaledModel = org.opensim.modeling.Model(scaledModelFilename);
%         scaledModel.initSystem()
%         idTool.setModel(scaledModel)
        
        idTool.setStartTime(initialTime);
        idTool.setEndTime(finalTime);
        idTool.setExcludedForces(excludedForces);
        % Use the External Loads .xml file we have just created:
        idTool.setExternalLoadsFileName(externalLoadsFilename);
        idTool.setCoordinatesFileName(ikFilenames{f});
        idTool.setLowpassCutoffFrequency(-1);
        % Important: this must be just the file name, without path:
        idTool.setOutputGenForceFileName(idName);
        
        
        % Run Inverse Kinematics tool on current trial:
        fprintf('� Running ID tool...\n')
        idTool.run();
        
        fprintf('� Printing ID setup .xml file...\n')
        idTool.print(fullfile(subjectFolder, 'SetupID.xml'));
        
    end
    
end

% End timer:
toc

fprintf('End of OpenSim ID pipeline.\n\n')

end

