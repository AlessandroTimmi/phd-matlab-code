function PlotJointMomentsMeanSd(kinectArrayOfStructs, viconArrayOfStructs,...
    moments, normalize)
% Plot mean and standard deviation of joint moments obtained from Inverse
% Dynamics (ID), using Kinect and Vicon marker trajectories as input.
%
% NOTE: in contrast with DVJ tasks, SLS trials don't have events determined
% by force platform vertical load, because this variable is almost
% constant during the task. For this reason, trials were not cropped.
% This causes a large variability in the calculation of the standard
% deviation between joint moments within each motion capture system.
% This problem affects Kinect and Vicon data in the same way.
%
% kinectArrayOfStructs = array of structs containing joint moments
%       calculated from Kinect data.
% viconArrayOfStructs = array of structs containing joint moments
%       calculated from Vicon data.
% moments = row cell array of strings containing names of the moments to
%       be plotted.
% normalize = if true, normalize the moments by the subject's mass.
%
% � October 2016 Alessandro Timmi


%%
if normalize
    units = 'N m / kg';
    % Subject's mass will be calculated below, for each subject.
    titleString = ', normalized by BM';
else
    units = 'N m';
    subjectMass = 1;
    titleString = '';
end


% Number of datapoints to be used for resampling:
resamplingLength = 30;

% Preallocate a 3D array for the resampled data, with the following
% dimensions:
% 1) resampling length;
% 2) number of trials for each device;
% 3) number of moments to be analysed.
resampledMomentsKinect = zeros(resamplingLength, length(kinectArrayOfStructs), length(moments));
resampledMomentsVicon = zeros(size(resampledMomentsKinect));

for s = 1:length(kinectArrayOfStructs)
    for m = 1:length(moments)
        
        if normalize
            % Get current subject's mass:
            ksn = kinectArrayOfStructs(s).subjectNumber;
            vsn = viconArrayOfStructs(s).subjectNumber;
            % Just an extra check, not really required because we already
            % compared arrays of structs before.
            assert(isequal(ksn, vsn),...
                'Subject''s number mismatch between Kinect (%d) and Vicon (%d)',...
                ksn, vsn)
            subjectMass = GetSubjectMass(sprintf('dev%s', ksn));
        else

        end
        
        % Normalize moment arrays by subject's mass, to remove unwanted variability:
        kinectMoment = kinectArrayOfStructs(s).coords.(moments{m}) / subjectMass;
        viconMoment = viconArrayOfStructs(s).coords.(moments{m}) / subjectMass;
        
        % Array of query points, on which the moments will be interpolated:
        resampledFramesKinect = linspace(0, length(kinectMoment), resamplingLength)';
        resampledFramesVicon = linspace(0, length(viconMoment), resamplingLength)';
        
        % Resample moments, using spline interpolation in correspondence of the query points:
        resampledMomentsKinect(:, s, m) = interp1(kinectMoment, resampledFramesKinect, 'spline');
        resampledMomentsVicon(:, s, m)  = interp1(viconMoment, resampledFramesVicon, 'spline');
                
    end
end

% Calculate mean and standard deviation of resampled moments:
meanKinect = zeros(resamplingLength, length(moments));
meanVicon = zeros(size(meanKinect));
sdKinect = zeros(size(meanKinect));
sdVicon = zeros(size(meanKinect));

for m = 1:length(moments)
    for f = 1:resamplingLength
        meanKinect(f,m) = mean(resampledMomentsKinect(f, :, m));
        meanVicon(f,m) = mean(resampledMomentsVicon(f, :, m));
        
        sdKinect(f,m) = std(resampledMomentsKinect(f, :, m));
        sdVicon(f,m) = std(resampledMomentsVicon(f, :, m));
    end
end

% Get edges of the shaded areas:
uppKinect = meanKinect + sdKinect;
lowKinect = meanKinect - sdKinect;
uppVicon = meanVicon + sdVicon;
lowVicon = meanVicon - sdVicon;


%% Get values of mean and SD for Kinect and Vicon at peak:
idPeakKinect = zeros(1, length(moments));
idPeakVicon = zeros(size(idPeakKinect));
peakMeanKinect = zeros(size(idPeakKinect));
peakMeanVicon = zeros(size(idPeakKinect));
peakSdKinect = zeros(size(idPeakKinect));
peakSdVicon = zeros(size(idPeakKinect));

for m = 1:length(moments)
    % We don't know the sign of the signal, so we use its absolute value
    % to find its peak:
    [~, idPeakKinect(1,m)] = max(abs(meanKinect(:,m)));
    [~, idPeakVicon(1,m)] = max(abs(meanVicon(:,m)));
    
    % Then we calculate the values in correspondence of the peak:
    peakMeanKinect(1,m) = meanKinect(idPeakKinect(1,m), m);
    peakMeanVicon(1,m) = meanVicon(idPeakVicon(1,m), m);
    peakSdKinect(1,m) = sdKinect(idPeakKinect(1,m), m);
    peakSdVicon(1,m) = sdVicon(idPeakVicon(1,m), m);
    
    fprintf('Mean (SD) at peak %s (Kinect): %0.1f (%0.1f) %s.\n',...
        moments{m}, peakMeanKinect(1,m), peakSdKinect(1,m), units);
    fprintf('Mean (SD) at peak %s (Vicon): %0.1f (%0.1f) %s.\n',...
        moments{m}, peakMeanVicon(1,m), peakSdVicon(1,m), units);
end


%% Plot mean and standard deviation of joint moments:
taskPercent = linspace(0, 100, resamplingLength);

% Plot Y limits:
lowerBound = min(lowKinect(:));
upperBound = max(uppKinect(:));

colorKinect = [0, 0, 0.7];
colorVicon = [0, 0.7, 0];

myTitle = {sprintf('%s joint moments%s', kinectArrayOfStructs(1).trialType, titleString);...
    sprintf('Mean and SD across %d trials', length(kinectArrayOfStructs))};

    
figure('Name', myTitle{1}, 'Position', [100, 100, 1300, 500])

for m = 1:length(moments)
    
    hAx = subplot(1, length(moments), m);
    hold on
    
    plot(taskPercent, meanKinect(:, m), '-', 'Color', colorKinect, 'linewidth', 0.5);
    plot(taskPercent, meanVicon(:, m), '-', 'Color', colorVicon, 'linewidth', 0.5);
        
    hKinect = fill([taskPercent, fliplr(taskPercent)], [uppKinect(:,m)', fliplr(lowKinect(:,m)')],...
        colorKinect, 'FaceAlpha', 0.1, 'EdgeColor', 'None');
    hVicon = fill([taskPercent, fliplr(taskPercent)], [uppVicon(:,m)', fliplr(lowVicon(:,m)')],...
        colorVicon, 'FaceAlpha', 0.1, 'EdgeColor', 'None');
    
    xlabel('Task (%)')
    ylabel(sprintf('%s (%s)', moments{m}, units), 'Interpreter', 'None')    
    hAx.YGrid = 'on';
    hAx.YLim = custom_axis_margins([lowerBound, upperBound], 0.03);
    legend([hKinect, hVicon], {'Kinect (mean\pmSD)', 'Vicon (mean\pmSD)'},...
        'Location', 'Best');
    if m == 2
        title(myTitle)
    end
    
    
    % Plot peak values:
    plot(taskPercent(idPeakKinect(1,m)), peakMeanKinect(1,m),...
        'Marker', '.', 'MarkerEdgeColor', colorKinect, 'MarkerSize', 18)
    plot(taskPercent(idPeakVicon(1,m)), peakMeanVicon(1,m),...
        'Marker', '.', 'MarkerEdgeColor', colorVicon, 'MarkerSize', 18)
end


