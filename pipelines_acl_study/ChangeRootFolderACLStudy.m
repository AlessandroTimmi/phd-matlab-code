function newPath = ChangeRootFolderACLStudy(oldPath, newRootFolder)
% Modify the root folder of ACL study trials.
%
% Input:
%   oldPath = path of current trial.
%   newRootFolder = new root folder, which will replace the one in the
%           current trial path.
% Output:
%   newPath = path with updated root folder.
%   
% � July 2016 Alessandro Timmi

%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'oldPath',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'newRootFolder',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Parse input arguments:
parse(p, oldPath, newRootFolder);

% Copy input arguments into more memorable variables:
oldPath = p.Results.oldPath;
newRootFolder = p.Results.newRootFolder;


%%
fprintf('New root folder: %s\n', newRootFolder)

% Expression to be matched. Example: \Dev77\
expression = '\\Dev\d+\\';
splitPosition = regexp(oldPath, expression, 'start');

% Replace old root folder with new one:
newPath = fullfile(newRootFolder, oldPath(splitPosition(1) : end));

fprintf('Old path: %s\n', oldPath)
fprintf('New path: %s.\n', newPath)






