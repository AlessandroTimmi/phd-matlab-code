function mass = GetSubjectMass(subjectName)
% Get mass corresponding to subject's name. Subjects' info are read
% from a spreadsheet of the ACL Study.
%
% Input:
%   subjectName = case insensitive subject's name (e.g. 'dev101').
%
% Output:
%   mass = mass of the subject in kg.
%
% � October 2016 Alessandro Timmi


%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addRequired(p, 'subjectName', @ischar);

% Parse input arguments:
parse(p, subjectName);

% Copy input arguments into more memorable variables:
subjectName = p.Results.subjectName;

clear p


%% Spreadsheet containing subjects' info for the ACL Study:
spreadsheet = 'C:\TEST_FOLDER\ACL study - Kinect Vicon agreement - latest 14 subjects\ACL study - subjects'' info Ale.xlsx';

% Load spreadsheet (raw data only):
[~, ~, rawData] = xlsread(spreadsheet);

% Compare first column of the spreadsheet and subject name (case
% insensitive), to get an array of logicals:
subjectsLogical = strcmpi(rawData(:,1), subjectName);

% Find row corresponding to subject of interest:
row = find(subjectsLogical);

% Check that only a single match is found:
assert(isequal(length(row), 1),...
    'Multiple subjects found with name: "%s".', subjectName)

% Get mass at that row from spreadsheet raw data:
mass = rawData{row, 5};


end