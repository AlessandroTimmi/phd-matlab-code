function PreprocessKinectDataAclStudy(varargin)
%PreprocessKinectDataAclStudy Preprocess Kinect .trc files for the ACL study
% Process Kinect data for the ACL study, preparing them to be analysed in
% OpenSim. Only a single condition was used for Kinect (barefoot), hence 
% there are not "conditions" subfolders.
%
% Input:
%   kinectFolder = (optional parameter, default = '') path to the
%                 folder containing all Kinect subjects' subfolders.
%                 If empty, a dialog will open to select one.
%   filterType  = (optional, default = 'none') one of the following strings:
%                   'none', 'butter', 'sgolay', 'median'.
%   filterType2  = (optional, default = 'none') second pass of filtering,
%                   see above. 
%   butterOrder  = (optional, default = 0) order of Butterworth filter.
%   butterCutoff = (optional, default = 0) cutoff frequency for
%                   Butterworth filter [Hz].
%   medianSpan   = (optional, default = 0) span of median filter [samples].
%   sgolayOrder  = (optional, default = 0) order of Savitzky-Golay filter.
%   sgolaySpan   = (optional, default = 0) span of Savitzky-Golay filter [samples].
%
% Folders must be structured in the following way:
%
% kinectFolder/
%       Dev01/
%           calibration_01.trc
%           trial_01.trc
%           trial_02.trc
%               ...
%           trial_M.trc
%       Dev02/
%           calibration_01.trc
%           trial_01.trc
%           trial_02.trc
%               ...
%           trial_N.trc
%       DevXX/
%           calibration_01.trc
%           trial_01.trc
%           trial_02.trc
%               ...
%           trial_X.trc
%
% Each subject subfolder can contain any number of trials.
%
% � July 2016 Alessandro Timmi

% TODO:
% - log file is fragmented between root folder and subjects' folders.
%   We need to find a way to unify it.


%% Default input values:
default.kinectFolder = '';
expectedFilters = {'none', 'butter', 'sgolay', 'median'};
default.filterType = 'none';
default.filterType2 = 'none';
default.butterOrder = 0;
default.butterCutoff = 0;
default.medianSpan = 0;
default.sgolayOrder = 0;
default.sgolaySpan = 0;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'kinectFolder', default.kinectFolder,...
    @(x)validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'filterType', default.filterType,...
    @(x) any(validatestring(x, expectedFilters)));
addParameter(p, 'filterType2', default.filterType2,...
    @(x) any(validatestring(x, expectedFilters)));
addParameter(p, 'butterOrder', default.butterOrder,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));
addParameter(p, 'butterCutoff', default.butterCutoff,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));
addParameter(p, 'medianSpan', default.medianSpan,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));
addParameter(p, 'sgolayOrder', default.sgolayOrder,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));
addParameter(p, 'sgolaySpan', default.sgolaySpan,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
kinectFolder = p.Results.kinectFolder;
filterType = p.Results.filterType;
filterType2 = p.Results.filterType2;
butterOrder = p.Results.butterOrder;
butterCutoff = p.Results.butterCutoff;
medianSpan = p.Results.medianSpan;
sgolayOrder = p.Results.sgolayOrder;
sgolaySpan = p.Results.sgolaySpan;

clear varargin default p


%% Select root folder, if not passed as input argument:
if isempty(kinectFolder)
    kinectFolder = uigetdir('', 'Select root folder containing Kinect data to be pre-processed');
    if isequal(kinectFolder, 0)
        fprintf('No root folder selected for Kinect data.\n')
        return
    end
end


%% Read filenames of all trials contained in subjects' folders within
% Kinect root folder:
trialsKinectFilenames = list_trials_in_all_subjects(...
    kinectFolder,...
    'DEV',...
    {''},...
    '',...
    '.trc');

fprintf('Found %d Kinect trials:\n', length(trialsKinectFilenames))
fprintf('\t%s\n', trialsKinectFilenames{:})


%% Get filenames of calibration trials only (those containing coordinates
% of the L-frame in Kinect space):
calibrationStartIndices = regexpi(trialsKinectFilenames,...
    'calibration_\d+.trc', 'once');
calibrationLogicals = ~cellfun(@isempty, calibrationStartIndices);
calibrationTrialsFilenames = trialsKinectFilenames(calibrationLogicals);
fprintf('Found %d calibration trials:\n', length(calibrationTrialsFilenames))
fprintf('\t%s\n', calibrationTrialsFilenames{:})

% Pre-process Kinect calibration trials:
preprocess_kinect_data_pipeline(...
    'trc_cal_filenames', calibrationTrialsFilenames);


%% Get filenames of all the remaining trials:
nonCalibrationTrialsFilenames = trialsKinectFilenames(~calibrationLogicals);
fprintf('Found %d non-calibration trials:\n', length(nonCalibrationTrialsFilenames))
fprintf('\t%s\n', nonCalibrationTrialsFilenames{:})

% Preallocate cell array with filename of pre-processed non-calibration
% trials:
nonCalibrationTrialsPreprocessedFilenames  = cell(nonCalibrationTrialsFilenames);

% Pre-process them, using the proper calibration trial:
for f = 1:length(nonCalibrationTrialsFilenames)
    fprintf('Pre-processing trial %d of %d.\n', f, length(nonCalibrationTrialsFilenames))
    
    % Find the subject corresponding to the current trial:
    subjectName = regexpi(nonCalibrationTrialsFilenames{f},...
        '\\dev\d+\\', 'match', 'once');
    
    % Find the pre-processed calibration trial corresponding to current trial:
    listing = dir(fullfile(kinectFolder, subjectName, 'calibration_*_pre-processed.trc'));
    currentCalibrationTrial = fullfile(kinectFolder, subjectName, listing(1).name);
    
    % Pre-process current trial using proper calibration trial:
    [~, ~, nonCalibrationTrialsPreprocessedFilenames{f}] =...
        preprocess_kinect_data_pipeline(...
        'trc_trial_filenames', nonCalibrationTrialsFilenames(f),...
        'trial_markerset_name', 'CustomMarkers_single_leg_v1.6_skeleton',...
        'sph_trial_markerset_name', 'CustomMarkers_single_leg_v1.6',...
        'filterType', filterType,...
        'filterType2', filterType2,...
        'butterOrder', butterOrder,...
        'butterCutoff', butterCutoff,...
        'medianSpan', medianSpan,...
        'sgolayOrder', sgolayOrder,...
        'sgolaySpan', sgolaySpan,...
        'transform_kinect_to_vicon', true,...
        'trcReferenceFrame', currentCalibrationTrial,...
        'rotate_vicon_to_opensim', true);
end

end

