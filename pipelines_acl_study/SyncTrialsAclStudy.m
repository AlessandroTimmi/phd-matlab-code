function [kinectDownFilenames, viconDownFilenames, viconGrfSyncFilenames] =...
    SyncTrialsAclStudy(kinectFolder, viconFolder)
% Interp, sync and downsample joint angles (IK results stored in .mot
% files) from the ACL study. Then shift the Vicon .mot file containing GRF
% by the same amount of time used for synchronization and trim it to match
% the corresponding .trc file.
%
% Input:
%   kinectFolder = root folder where all Kinect data are stored.
%   viconFolder = root folder where all Vicon data are stored.
%
% Output:
%   kinectDownFilenames = row cell array containing full filenames of
%       synced and downsampled Kinect IK results.
%   viconDownFilenames = row cell array containing full filenames of
%       synced and downsampled Vicon IK results.
%   viconGrfSyncFilenames = row cell array containing full filenames of
%       synced Vicon GRF data.
%
% � August 2016 Alessandro Timmi


%% Subjects prefix (case insensitive):
subjectsPrefix = 'Dev';

% IK trials suffix and extension:
ikTrialsSuffix = 'ik.mot';

% Joint angles to be used for synchronization:
jointAnglesToSync = {'hip_flexion_r', 'knee_angle_r', 'ankle_angle_r'};

fprintf('Syncing Kinect and Vicon IK results files (.mot).\n')


%% Load Vicon IK results:
fprintf('Loading Vicon IK results files from:\n\t%s\n', viconFolder)
viconFilenames = list_trials_in_all_subjects(viconFolder, subjectsPrefix,...
    {'Barefoot'}, '', ikTrialsSuffix);
fprintf('Found %d files:\n', length(viconFilenames))
fprintf('\t%s\n', viconFilenames{:})

fprintf('\n')


%% Load Kinect IK trials:
fprintf('Loading Kinect IK results files from:\n\t%s\n', kinectFolder)
kinectFilenames = list_trials_in_all_subjects(kinectFolder, subjectsPrefix,...
    {''}, '', ikTrialsSuffix);
fprintf('Found %d files:\n', length(kinectFilenames))
fprintf('\t%s\n', kinectFilenames{:})


%% Sync IK results between Vicon (including GRF) and Kinect.
% This regular expression will be used to compare trials, to see if
% subject number, trial type and trial number match. The OR symbol (|) is
% used to allow for two different expressions, one for Vicon trials, the
% other for Kinect trials:
trialRegularExpression = [...
    '\\Dev(?<subjectNumber>\d+)\s(?<trialType>\w+)\s(?<trialNumber>\d+)_ik.mot|',...
    '\\Dev(?<subjectNumber>\d+)\\(?<trialType>\w+)_(?<trialNumber>\d+)_pre-processed_ik.mot'];

[viconSyncFilenames, kinectSyncFilenames, meanSdStorage,...
    viconGrfSyncFilenames] = SyncMotFilesPipeline(...
    viconFilenames, kinectFilenames, jointAnglesToSync, trialRegularExpression);

% Plot the mean SD of the differences between paired signals (which is the
% objective function used for synchronization) to spot problematic trials
% which might haven't been synced properly:
sdTitle = 'Mean SD of differences between paired signals';
figure('Name', sdTitle)
plot(meanSdStorage, 'linestyle', '-', 'Marker', '.')
xlabel('Trial pair number [#]')
ylabel('SD [�]')
title(sdTitle)


%% Downsample IK results to match Kinect original framerate. We don't need
% to downsample GRF data too, as OpenSim Inverse Dynamics results are
% automatically sampled at the same frequency of IK data.
viconDownFilenames = DownsampleMotTrials(viconSyncFilenames);
kinectDownFilenames = DownsampleMotTrials(kinectSyncFilenames);


end

