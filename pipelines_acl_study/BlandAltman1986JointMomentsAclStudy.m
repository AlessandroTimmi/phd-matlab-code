function BlandAltman1986JointMomentsAclStudy(varargin)
% Bland-Altman (The Lancet - 1986 version) analysis of agreement
% on the dataset of the ACL study. The agreement of the joint moments
% is assessed along the entire trial. Kinect and Vicon datasets must be
% already synced and downsampled to Kinect original framerate.
%
% Our study design doesn't match the one described in Bland and Altman's
% 1986 paper. Performing the analysis of agreement without taking into
% account the structure of the data is incorrect. However, it is a very
% good "benchmark" to have a first impression on the agreement between
% Kinect and Vicon.
%
% Input:
%   rootFolder = (optional parameter, default = '') folder containing all
%                trials to be loaded. The expected folder structure is
%                shown in list_trials_in_all_subjects.m. If empty, a dialog
%                window will open to select a root folder.
%
% � October 2016 Alessandro Timmi


%% Default input values:
default.rootFolder = 'C:\TEST_FOLDER\ACL study - Kinect Vicon agreement - latest 14 subjects';

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'rootFolder', default.rootFolder,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
rootFolder = p.Results.rootFolder;

clear varargin default p


%% Select root folder
if isempty(rootFolder)
    rootFolder = uigetdir('', 'Select root folder containing Kinect and Vicon data');
    if isequal(rootFolder, 0)
        return
    end
end

%% Other input data:
resultsSubfolder = '2 - Results';
viconInputFolder = fullfile(rootFolder, 'Vicon', resultsSubfolder);
kinectInputFolder = fullfile(rootFolder, 'Kinect', resultsSubfolder);
subjectsPrefix = 'Dev';
downsampledTrialsSuffix = 'ik_sync_downsampled_id.sto';


% Preallocate storage arrays for biases and limits of agreement:
moments = {'hip_flexion_r_moment', 'knee_angle_r_moment', 'ankle_angle_r_moment'};
biasStorage = zeros(1, length(moments));
lloaStorage = zeros(size(biasStorage));
uloaStorage = zeros(size(biasStorage));


% Enable logging:
LogFile(fullfile(rootFolder, 'Bland-Altman1986JointMomentsAclStudy.log'));

fprintf('Bland-Altman analysis of agreement (1986 version) of the joint moments from the ACL Study\n')


%% List Vicon ID files:
fprintf('Loading Vicon ID results from:\n\t%s\n', viconInputFolder)
viconFilenames = list_trials_in_all_subjects(viconInputFolder, subjectsPrefix,...
    {'Barefoot'}, '', downsampledTrialsSuffix);
fprintf('Found %d files:\n', length(viconFilenames))
fprintf('\t%s\n', viconFilenames{:})
fprintf('\n')


%% List Kinect ID files:
fprintf('Loading Kinect ID results from:\n\t%s\n', kinectInputFolder)
kinectFilenames = list_trials_in_all_subjects(kinectInputFolder, subjectsPrefix,...
    {''}, '', downsampledTrialsSuffix);
fprintf('Found %d files:\n', length(kinectFilenames))
fprintf('\t%s\n', kinectFilenames{:})


% This regular expression will be used to extract subject number, trial
% type and trial number from filenames. The OR symbol (|) is used to
% support two different expressions, one for Vicon and the other for
% Kinect trials:
trialRegularExpression = [...
    '\\Dev(?<subjectNumber>\d+)\s(?<trialType>\w+)\s(?<trialNumber>\d+)|',...
    '\\Dev(?<subjectNumber>\d+)\\(?<trialType>\w+)_(?<trialNumber>\d+)'];

% Load trials into arrays of structs:
kinectArrayOfStructs = LoadMotToArrayOfStructs(kinectFilenames, trialRegularExpression);
viconArrayOfStructs = LoadMotToArrayOfStructs(viconFilenames, trialRegularExpression);

% Check correspondence of structs between the two devices:
CompareArraysOfStructsAclStudy(kinectArrayOfStructs, viconArrayOfStructs);

% Concatenate each array of structs into a single struct:
kinectConcatenated = ConcatenateArrayOfMotStructs(kinectArrayOfStructs);
viconConcatenated = ConcatenateArrayOfMotStructs(viconArrayOfStructs);

% Check that both concatenated structs have same length:
CompareLengthConcatenatedMotStructs(kinectConcatenated,...
    viconConcatenated, moments);


%% Figures handles for Bland-Altman analysis:
hfigHist = figure('Name', 'Histograms of the differences');
hfigBa = figure('Name', 'Bland-Altman (1986) plots', 'Position', [100, 100, 1300, 500]);

for c = 1:numel(moments)
    % Make histogram figure current:
    figure(hfigHist)
    % Get the handle of the current subplot position:
    haxHist = subplot(1, length(moments), c);
    % Make the Bland-Altman figure current:
    figure(hfigBa)
    % Get the handle of the current subplot position:
    haxBa = subplot(1, length(moments), c);
        
    % Perform Bland-Altman analysis of agreement and plot the results:
    [biasStorage(c), lloaStorage(c), uloaStorage(c)] =...
        bland_altman_1986(...
        viconConcatenated.coords.(moments{c}),...
        kinectConcatenated.coords.(moments{c}),...
        moments{c},...
        moments{c},...
        'N m',...
        'hax_hist', haxHist,...
        'hax_ba', haxBa,...
        'yDecimals', 0);
end


% Plot mean and standard deviation of joint moments for Kinect and Vicon:
PlotJointMomentsMeanSd(kinectArrayOfStructs, viconArrayOfStructs, moments, false);

% Disable logging:
LogFile('off');

end

