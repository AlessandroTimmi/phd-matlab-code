function OpenSimInverseKinematicsPipeline(rootFolder, trialsRegExp,...
    ikSetupFilename, subjectsPrefix, conditions, varargin)
% Run inverse kinematics (IK) using OpenSim API.
%
% Input:
%   rootFolder = folder containing all subjects' subfolders.
%   trialsRegExp = string containing a regular expression matching
%               the names of the trials, e.g. Drop Landing\s\d+.trc' for
%               Vicon or 'SLS_\d+_pre-processed.trc' for KinEdge.
%   ikSetupFilename = full filename of the inverse kinematics .xml setup
%               file. NOTE: the following parameters need to be filled in
%               the IK setup file even if they will be replaced by this
%               function:
%   subjectsPrefix = case insensitive string containing the prefix of the
%               subjects' folders names. Generally a 3 characters string,
%               e.g. 'DEV'.
%   conditions = cell array of strings containing the names of the
%               subfolders representing different capture conditionts,
%                e.g. {'Barefoot', 'Kayano'}.
%
%   resultsSubfolder = (optional parameter, default = 'IK results')
%               subfolder in which IK results will be stored.
%
% � July 2016 Alessandro Timmi

% TODO:
% - allow for multiple trial types to be selected and loaded.
% - replace errors with "return" in the parfor loop, using function
%   evaluation.


%% Default input values:
default.resultsSubfolder = 'IK results';

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'rootFolder',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'trialsRegExp',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'ikSetupFilename',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'subjectsPrefix',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'conditions',...
    @(x) validateattributes(x, {'cell'}, {'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'resultsSubfolder', default.resultsSubfolder, @ischar);

% Parse input arguments:
parse(p, rootFolder, trialsRegExp, ikSetupFilename, subjectsPrefix,...
    conditions, varargin{:});

% Copy input arguments into more memorable variables:
rootFolder = p.Results.rootFolder;
trialsRegExp = p.Results.trialsRegExp;
ikSetupFilename = p.Results.ikSetupFilename;
subjectsPrefix = p.Results.subjectsPrefix;
conditions = p.Results.conditions;
resultsSubfolder = p.Results.resultsSubfolder;

clear varargin default p


%% Import OpenSim modelling classes:
import org.opensim.modeling.*

fprintf('OpenSim Inverse Kinematics (IK) pipeline\n')


%% Display input arguments:
fprintf('Root folder:\n\t%s\n', rootFolder)
fprintf('Trials regular expression:\n\t%s\n', trialsRegExp)
fprintf('Inverse Kinematics setup filename:\n\t%s\n', ikSetupFilename)
fprintf('Subjects'' prefix: %s.\n', subjectsPrefix)
fprintf('Conditions:\n\t%s\n', conditions{:})


%% List all trials in the root folder:
allTrials = list_trials_in_all_subjects(rootFolder,...
    subjectsPrefix,...
    conditions,...
    '',...
    '.trc');

% Select only trials of the specified type, using regular expressions:
startIndices = regexpi(allTrials, trialsRegExp, 'once');
trialsLogical = ~cellfun(@isempty, startIndices);
trialsFilenames = allTrials(trialsLogical);

fprintf('Found %d "%s" trials:\n', length(trialsFilenames), trialsRegExp)
fprintf('\t%s\n', trialsFilenames{:})


%% Run IK for each trial.
% Start timer:
tic
% Not sure why, but within a parfor loop the OpenSim classes must be
% called using their full names, even if they are imported on top of this
% function.
parfor f = 1:length(trialsFilenames)
    fprintf('Trial %d of %d.\n', f, length(trialsFilenames))

    % Get filename of the scaled model for current subject:
    [subjectPath, trialName] = fileparts(trialsFilenames{f});
    scaledModelFilename = FindScaledOpenSimModel(subjectPath, subjectsPrefix);
    
    % Make results folder if it doesn't exist:
    resultsFolder = fullfile(subjectPath, resultsSubfolder);
    if ~isequal(exist(resultsFolder, 'dir'), 7)
        mkdir(resultsFolder);
    end
    
    % Check if the output file already exists:
    outputFilename = fullfile(resultsFolder, [trialName '_ik.mot']);
    if isequal(exist(outputFilename, 'file'), 2)
        % Skip this trial. We cannot make this part more user-interactive,
        % due to the parallel nature of this loop.
        warning('This file already exists:\n\t%s.\nSkipping...\n', outputFilename)
    else
        % Load model from file (it must already have the markerset attached):
        scaledModel = org.opensim.modeling.Model(scaledModelFilename);
        
        % Initialize the model:
        scaledModel.initSystem();
        
        %% Get time range from current trial:
        dynamicData = org.opensim.modeling.MarkerData(trialsFilenames{f});
        initialTime = dynamicData.getStartFrameTime();
        finalTime = dynamicData.getLastFrameTime();
        
        % Setup Inverse Kinematics tool for current trial:
        ikTool = org.opensim.modeling.InverseKinematicsTool(ikSetupFilename);
        ikTool.setModel(scaledModel);
        ikTool.setMarkerDataFileName(trialsFilenames{f});
        ikTool.setStartTime(initialTime);
        ikTool.setEndTime(finalTime);
        ikTool.setOutputMotionFileName(outputFilename);
                
        % Run Inverse Kinematics tool on current trial:
        ikTool.run();
    end
    
end

% End timer:
toc

fprintf('End of OpenSim IK pipeline.\n\n')

end

