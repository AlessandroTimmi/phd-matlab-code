% Bland-Altman (1986) analysis of agreement of peak knee flexion angles
% derived from Vicon and Kinect v2 markerless pose estimation algorithm.
% Data are reported in a study on overhead squat (McGroarty et al., 2016).
% That study only discussed absolute differences between Kinect and Vicon.
% However, using absolute values of the errors introduces a statistical
% artefact in the calculation of mean and standard deviation, because with
% this approach the distribution of the error between Kinect and Vicon is 
% considered to be one-sided. However, the angle measurements show that
% this distribution is 2-sided.
%
% For this reason, and to allow comparison with the results from the ACL
% study during the SLS task, raw data from McGroarty et al. (2016) were
% re-analysed using the Bland-Altman analysis of agreement (1986). 
%
% � March 2017 Alessandro Timmi

clc
clear
close all

% Data from McGroarty et al. 2016:
kinectLeft = [106.7, 99.7, 100.1, 102.2, 83.7, 83.2, 87.2, 81.8, 90.3,...
    85.8, 86.7, 90.4, 92.7, 90.2, 81.4, 75.1, 77.7, 83.7, 96.6, 81.9]';

viconLeft = [111, 107.2, 108, 107, 89.1, 78.1, 79.3, 70.7, 99.8, 96.4,...
    95.5, 98, 101, 99.8, 79, 77.5, 80, 88.5, 103, 90]';

kinectRight = [106.6, 101.3, 99, 100.6, 79.9, 82.3, 84.5, 81.3, 87.5,...
    85.6, 86.7, 92.3, 95.8, 93.5, 82.5, 75.8, 80.3, 82.8, 96.2, 80.7]';

viconRight = [105, 104.2, 109, 106.8, 86.3, 87.7, 89.5, 82, 103, 98.3,...
    97.7, 102, 104, 103, 75.6, 72, 76, 90.2, 105, 89.6]';


% Perform Bland-Altman analysis of agreement and plot the results:
[biasLeft, lloaLeft, uloaLeft] =...
    bland_altman_1986(...
    viconLeft,...
    kinectLeft,...
    'Left knee flexion at peak of overhead squat',...
    'Peak left knee flexion',...
    '�',...
    'yDecimals', 0);

[biasRight, lloaRight, uloaRight] =...
    bland_altman_1986(...
    viconRight,...
    kinectRight,...
    'Right knee flexion at peak of overhead squat',...
    'Peak right knee flexion',...
    '�',...
    'yDecimals', 0);


%% Calculate mean and standard deviation of the differences:
differencesLeft = kinectLeft - viconLeft;
differencesRight = kinectRight - viconRight;

avgDiffLeft = mean(differencesLeft);
avgDiffRight = mean(differencesRight);

sdDiffLeft = std(differencesLeft);
sdDiffRight = std(differencesRight);

% Print results:
fprintf('Mean +/- SD of the differences between Kinect v2 and Vicon:\n')
fprintf('Left: %0.1f� +/- %0.1f�\n', avgDiffLeft, sdDiffLeft)
fprintf('Right: %0.1f� +/- %0.1f�\n', avgDiffRight, sdDiffRight)


% For comparison, use the same approach adopted by McGroarty et al., i.e.
% calculate mean and SD of the absolute values of the differences:
avgAbsoluteDiffLeft = mean(abs(differencesLeft));
avgAbsoluteDiffRight = mean(abs(differencesRight));

sdAbsoluteDiffLeft = std(abs(differencesLeft));
sdAbsoluteDiffRight = std(abs(differencesRight));

% Print results:
fprintf('\nMean +/- SD of the ABSOLUTE differences between Kinect v2 and Vicon:\n')
fprintf('Left: %0.1f� +/- %0.1f�\n', avgAbsoluteDiffLeft, sdAbsoluteDiffLeft)
fprintf('Right: %0.1f� +/- %0.1f�\n', avgAbsoluteDiffRight, sdAbsoluteDiffRight)





