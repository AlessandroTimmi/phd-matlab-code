function PreprocessViconDataAclStudy(varargin)
% Process Vicon data for the ACL study, preparing them to be analysed in
% OpenSim. Exported data (.trc and .mot files) will be stored in the input
% folder.
%
% Input:
%   viconFolder = (optional parameter, default = '') path to the 
%                 folder containing all Vicon subjects' subfolders.
%                 If empty, a dialog will open to select one.
%   filterType  = (optional, default = 'none') one of the following strings:
%                   'none', 'butter', 'sgolay', 'median'.
%   butterOrder  = (optional, default = 0) order of Butterworth filter.
%   butterCutoff = (optional, default = 0) cutoff frequency for
%                   Butterworth filter [Hz].
%   medianSpan   = (optional, default = 0) span of median filter [samples].
%   sgolayOrder  = (optional, default = 0) order of Savitzky-Golay filter.
%   sgolaySpan   = (optional, default = 0) span of Savitzky-Golay filter [samples].
%
%
% Folders must be structured in the following way:
%
% viconFolder/
%       Dev01/
%           Condition_A/
%           Condition_B/
%           Condition_N/
%       Dev02/
%           Condition_A/
%           Condition_B/
%           Condition_N/
%     ...
%       DevXX/
%           Condition_A/
%           Condition_B/
%           Condition_N/
%
% Each condition can contain any number of trials.
%
% � April 2016 Alessandro Timmi


%% Default input values:
default.viconFolder = '';
expectedFilters = {'none', 'butter', 'sgolay', 'median'};
default.filterType = 'none';
default.butterOrder = 0;
default.butterCutoff = 0;
default.medianSpan = 0;
default.sgolayOrder = 0;
default.sgolaySpan = 0;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'viconFolder', default.viconFolder,...
    @(x)validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'filterType', default.filterType,...
    @(x) any(validatestring(x, expectedFilters)));
addParameter(p, 'butterOrder', default.butterOrder,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));
addParameter(p, 'butterCutoff', default.butterCutoff,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));
addParameter(p, 'medianSpan', default.medianSpan,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));
addParameter(p, 'sgolayOrder', default.sgolayOrder,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));
addParameter(p, 'sgolaySpan', default.sgolaySpan,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
viconFolder = p.Results.viconFolder;
filterType = p.Results.filterType;
butterOrder = p.Results.butterOrder;
butterCutoff = p.Results.butterCutoff;
medianSpan = p.Results.medianSpan;
sgolayOrder = p.Results.sgolayOrder;
sgolaySpan = p.Results.sgolaySpan;

clear varargin default p


%% Select root folder, if not passed as input argument:
if isempty(viconFolder)
    viconFolder = uigetdir('', 'Select root folder containing Vicon data to be pre-processed');
    if isequal(viconFolder, 0)
        return
    end
end


%% Read all trial filenames from the root folder containing all Vicon
% data for multiple subjects and multiple conditions:
trialsViconFilenamesC3d = list_trials_in_all_subjects(...
    viconFolder,...
    'Dev',...
    {'Barefoot'},...
    '',...
    '.c3d');

% Extract Vicon trials in a new root folder:
[~, trcFilenames, motFilenames] = ExtractC3D(trialsViconFilenamesC3d,...
    'markersetName', 'Schache_CHESM_v092F',...
    'trimTrial', true,...
    'rotateViconToOpenSim', true);


if strcmpi(filterType, 'none')
    fprintf('Filtering not requested.\n')
    
else
    % Filter trials data:
    for i = 1:length(trcFilenames)
        % Load current .trc trial:
        sTrc = LoadTrcFile('filename', trcFilenames{i});
        
        % Filter .trc data:
        sTrcFiltered = FilterTrc(sTrc, filterType,...
            'butterOrder', butterOrder,...
            'butterCutoff', butterCutoff,...
            'medianSpan', medianSpan,...
            'sgolayOrder', sgolayOrder,...
            'sgolaySpan', sgolaySpan);
        
        % Overwrite .trc file:
        WriteTrcFile(sTrcFiltered, 'destinationFilename', trcFilenames{i}, 'force', true);
        
        
        if ~isempty(motFilenames{i})
            % Load current .mot file, if available:
            sMot = LoadMotFile('filename', motFilenames{i});
            
            % Filter .mot data:
            sMotFiltered = FilterMot(sMot, filterType,...
                'butterOrder', butterOrder,...
                'butterCutoff', butterCutoff,...
                'medianSpan', medianSpan,...
                'sgolayOrder', sgolayOrder,...
                'sgolaySpan', sgolaySpan);
            
            % Overwrite .mot file:
            WriteMotFile(sMotFiltered, 'destinationFilename', motFilenames{i}, 'force', true);
        end
        
    end
end

end
