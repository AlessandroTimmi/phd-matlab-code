% Plot joint angles obtained from Inverse Kinematics (IK) applied on
% Kinect and Vicon data.
% � August 2016 Alessandro Timmi

clear
clc

% Input arguments:
subjectNumber = 90;
trialNumber = 01;
myCoords = {'hip_flexion_r', 'knee_angle_r', 'ankle_angle_r'};


%% Load .mot files:
rootFolder = 'C:\TEST_FOLDER\ACL study - joint angles agreement - latest 14 subjects';
resultsSubfolder = '2 - Results';
viconInputFolder = fullfile(rootFolder, 'Vicon', resultsSubfolder);
kinectInputFolder = fullfile(rootFolder, 'Kinect', resultsSubfolder);

filenameVicon = fullfile(viconInputFolder,...
    sprintf('Dev%d\\Dev%d SLS 0%d_ik_sync_downsampled.mot', subjectNumber, subjectNumber, trialNumber));

filenameKinect = fullfile(kinectInputFolder,...
    sprintf('Dev%d\\SLS_0%d_pre-processed_ik_sync_downsampled.mot', subjectNumber, trialNumber));

vicon = LoadMotFile('filename', filenameVicon);
kinect = LoadMotFile('filename', filenameKinect);



%% Plot joint angles:
legendEntries = cell(1, 2 * length(myCoords));

myTitle = 'Downsampled data';
figure('Name', myTitle)

myColors = {'r', [0, 0.7, 0], 'b'};
hold on
for i=1:length(myCoords)
    plot(vicon.coords.(myCoords{i}), '--', 'Color', myColors{i})
    plot(kinect.coords.(myCoords{i}), '-', 'Color', myColors{i})
    
    legendEntries{2*i-1} = sprintf('Vicon %s', myCoords{i});
    legendEntries{2*i} = sprintf('Kinect %s', myCoords{i});
end

title(myTitle)
xlabel('Frames (#)')
ylabel('Joint angle (�)')
legend(legendEntries, 'Interpreter', 'None')



