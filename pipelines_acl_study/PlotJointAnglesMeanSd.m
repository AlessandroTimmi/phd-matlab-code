function PlotJointAnglesMeanSd(kinectArrayOfStructs, viconArrayOfStructs,...
    angles)
% Plot mean and standard deviation of joint angles obtained from Inverse
% Kinematics (IK), using Kinect and Vicon marker trajectories as input.
%
% NOTE: in contrast with DVJ tasks, SLS trials don't have events determined
% by force platform vertical load, because this variable is almost
% constant during the task. For this reason, trials were not cropped.
% This causes a large variability in the calculation of the standard
% deviation between joint angles within each motion capture system.
% This problem affects Kinect and Vicon data in the same way.
%
% kinectArrayOfStructs = array of structs containing joint angles
%   calculated from Kinect data.
% viconArrayOfStructs = array of structs containing joint angles
%   calculated from Vicon data.
% angles = row cell array of strings containing names of the angles to
%   be plotted.
%
% � October 2016 Alessandro Timmi


%% Number of datapoints to be used for resampling:
resamplingLength = 30;

% Preallocate a 3D array for the resampled data, with the following
% dimensions:
% 1) resampling length;
% 2) number of trials for each device;
% 3) number of angles to be analysed.
resampledAnglesKinect = zeros(resamplingLength, length(kinectArrayOfStructs), length(angles));
resampledAnglesVicon = zeros(size(resampledAnglesKinect));

for s = 1:length(kinectArrayOfStructs)
    for m = 1:length(angles)
        % Store current angles arrays in temporary variables:
        thisKinectAngle = kinectArrayOfStructs(s).coords.(angles{m});
        thisViconAngle = viconArrayOfStructs(s).coords.(angles{m});
        
        % Array of query points, on which the angles will be interpolated:
        resampledFramesKinect = linspace(0, length(thisKinectAngle), resamplingLength)';
        resampledFramesVicon = linspace(0, length(thisViconAngle), resamplingLength)';
        
        % Resample angles, using spline interpolation in correspondence of the query points:
        resampledAnglesKinect(:, s, m) = interp1(thisKinectAngle, resampledFramesKinect, 'spline');
        resampledAnglesVicon(:, s, m)  = interp1(thisViconAngle, resampledFramesVicon, 'spline');
        
    end
end

% Calculate mean and standard deviation of resampled angles:
meanKinect = zeros(resamplingLength, length(angles));
meanVicon = zeros(size(meanKinect));
sdKinect = zeros(size(meanKinect));
sdVicon = zeros(size(meanKinect));

for m = 1:length(angles)
    for f = 1:resamplingLength
        meanKinect(f,m) = mean(resampledAnglesKinect(f, :, m));
        meanVicon(f,m) = mean(resampledAnglesVicon(f, :, m));
        
        sdKinect(f,m) = std(resampledAnglesKinect(f, :, m));
        sdVicon(f,m) = std(resampledAnglesVicon(f, :, m));
    end
end

% Get edges of the shaded areas:
uppKinect = meanKinect + sdKinect;
lowKinect = meanKinect - sdKinect;
uppVicon = meanVicon + sdVicon;
lowVicon = meanVicon - sdVicon;


%% Get values of mean and SD for Kinect and Vicon at peak:
idPeakKinect = zeros(1, length(angles));
idPeakVicon = zeros(size(idPeakKinect));
peakMeanKinect = zeros(size(idPeakKinect));
peakMeanVicon = zeros(size(idPeakKinect));
peakSdKinect = zeros(size(idPeakKinect));
peakSdVicon = zeros(size(idPeakKinect));

for m = 1:length(angles)
    % We don't know the sign of the signal, so we use its absolute value
    % to find its peak:
    [~, idPeakKinect(1,m)] = max(abs(meanKinect(:,m)));
    [~, idPeakVicon(1,m)] = max(abs(meanVicon(:,m)));
    
    % Then we calculate the values in correspondence of the peak:
    peakMeanKinect(1,m) = meanKinect(idPeakKinect(1,m), m);
    peakMeanVicon(1,m) = meanVicon(idPeakVicon(1,m), m);
    peakSdKinect(1,m) = sdKinect(idPeakKinect(1,m), m);
    peakSdVicon(1,m) = sdVicon(idPeakVicon(1,m), m);
    
    fprintf('Mean (SD) at peak %s (Kinect): %0.1f (%0.1f) (�).\n',...
        angles{m}, peakMeanKinect(1,m), peakSdKinect(1,m));
    fprintf('Mean (SD) at peak %s (Vicon): %0.1f (%0.1f) (�).\n',...
        angles{m}, peakMeanVicon(1,m), peakSdVicon(1,m));
end


%% Plot mean and standard deviation of joint angles:
taskPercent = linspace(0, 100, resamplingLength);

% Plot Y limits:
lowerBound = min(lowKinect(:));
upperBound = max(uppKinect(:));

colorKinect = [0, 0, 0.7];
colorVicon = [0, 0.7, 0];

myTitle = {sprintf('%s joint angles', kinectArrayOfStructs(1).trialType);...
    sprintf('Mean and SD across %d trials', length(kinectArrayOfStructs))};

figure('Name', myTitle{1}, 'Position', [100, 100, 1300, 500])

for m = 1:length(angles)
    
    hAx = subplot(1, length(angles), m);
    hold on
    
    plot(taskPercent, meanKinect(:, m), '-', 'Color', colorKinect, 'linewidth', 0.5);
    plot(taskPercent, meanVicon(:, m), '-', 'Color', colorVicon, 'linewidth', 0.5);
    
    hKinect = fill([taskPercent, fliplr(taskPercent)], [uppKinect(:,m)', fliplr(lowKinect(:,m)')],...
        colorKinect, 'FaceAlpha', 0.1, 'EdgeColor', 'None');
    hVicon = fill([taskPercent, fliplr(taskPercent)], [uppVicon(:,m)', fliplr(lowVicon(:,m)')],...
        colorVicon, 'FaceAlpha', 0.1, 'EdgeColor', 'None');
    
    xlabel('Task (%)')
    ylabel(sprintf('%s (�)', angles{m}), 'Interpreter', 'None')
    hAx.YGrid = 'on';
    hAx.YLim = custom_axis_margins([lowerBound, upperBound], 0.03);
    legend([hKinect, hVicon], {'Kinect (mean\pmSD)', 'Vicon (mean\pmSD)'},...
        'Location', 'Best');
    if m == 2
        title(myTitle)
    end
    
    % Plot peak values:
    plot(taskPercent(idPeakKinect(1,m)), peakMeanKinect(1,m),...
        'Marker', '.', 'MarkerEdgeColor', colorKinect, 'MarkerSize', 18)
    plot(taskPercent(idPeakVicon(1,m)), peakMeanVicon(1,m),...
        'Marker', '.', 'MarkerEdgeColor', colorVicon, 'MarkerSize', 18)
end


end