function CheckTrialsAclStudy(varargin)
% Check correspondence between Vicon and Kinect trials, i.e. if the same
% trials have been recorded for each subject using both devices.
%
% Input:
%   rootFolderVicon = (optional parameter, default = '') path to the folder
%                 containing all Vicon data. If empty, a dialog will open
%                 to select one.
%   rootFolderKinect = (optional parameter, default = '') path to the
%                 folder containing all Kinect data. If empty, a dialog
%                 will open to select one.
%   debug_mode = (optional parameter, default = false). If true, displays
%                some extra info for debug purposes.
%
% Vicon folder must be structured in the following way:
%
% rootFolder/
%       Dev01/
%           ConditionA/
%           ConditionB/
%           ConditionN/
%       Dev02/
%           ConditionA/
%           ConditionB/
%           ConditionN/
%     ...
%       DevXX/
%           ConditionA/
%           ConditionB/
%           ConditionN/
%
% Kinect folder must be structure in the same way, but without the
% conditions subfolders, because Kinect data were collected only in
% barefoot condition.
%
% Each condition subfolder can contain any number of trials.
%
% � July 2016 Alessandro Timmi


%% Default input values:
default.rootFolderVicon = '';
default.rootFolderKinect = '';
default.debugMode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'rootFolderVicon', default.rootFolderVicon,...
    @(x)validateattributes(x, {'char'}, {'nonempty'}))
addParameter(p, 'rootFolderKinect', default.rootFolderKinect,...
    @(x)validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'debugMode', default.debugMode, @islogical);

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
rootFolderVicon = p.Results.rootFolderVicon;
rootFolderKinect = p.Results.rootFolderKinect;
debugMode = p.Results.debugMode;

clear varargin default p


%%
clc
fprintf('Checking correspondence between Vicon and Kinect trial files...\n')


%% Select Vicon root folder, if not passed as input argument:
if isempty(rootFolderVicon)
    rootFolderVicon = uigetdir('', 'Select root folder containing Vicon data');
    if isequal(rootFolderVicon, 0)
        fprintf('No root folder selected for Vicon data.\n')
        return
    end
end


%% Read all trial filenames from Vicon root folder, for all subjects and
% selected conditions:
viconFilenames = list_trials_in_all_subjects(...
    rootFolderVicon,...
    'Dev',...
    {'Barefoot'},...
    '',...
    '.trc');


if debugMode
    fprintf('Found %d trials in Vicon root folder:\n',...
        length(viconFilenames))
    fprintf('\t%s\n', viconFilenames{:})
end


%% Parse Vicon filenames:
expressionVicon = '\\Dev(?<Dev>\d+)\s(?<trialType>\w+\s*\w*)\s(?<trialNumber>\d+).trc';

% Get a cell array of strings, each one containing subject number, trial
% type and trial number. Trials of type 'Cal' will be skipped:
cellVicon = ParseTrialFilenames(viconFilenames, expressionVicon,...
    {'Cal'},...
    'debugMode', debugMode);

fprintf('Parsed %d trials in Vicon folder.\n', length(cellVicon))


%% Select Kinect root folder, if not passed as input argument:
if isempty(rootFolderKinect)
    rootFolderKinect = uigetdir('', 'Select root folder containing Kinect data');
    if isequal(rootFolderKinect, 0)
        fprintf('No root folder selected for Kinect data.\n')
        return
    end
end


%% Read all trial filenames from Kinect root folder, for all subjects
% (there are no conditions in Kinect data, as all were collected in
% barefoot condition):
kinectFilenames = list_trials_in_all_subjects(...
    rootFolderKinect,...
    'Dev',...
    {''},...
    '',...
    '.trc');

if debugMode
    fprintf('Found %d trials in Kinect root folder:\n',...
        length(kinectFilenames))
    fprintf('\t%s\n', kinectFilenames{:})
end


%% Parse Kinect filenames:
expressionKinect = '\\DEV(?<Dev>\d+)\\(?<trialType>\w+)_(?<trialNumber>\d+).trc';

% Get a cell array of strings, each one containing subject number, trial
% type and trial number. Trials of type 'calibration' and 'static' will be
% skipped:
cellKinect = ParseTrialFilenames(kinectFilenames, expressionKinect,...
    {'calibration', 'static'},...
    'debugMode', debugMode);

fprintf('Parsed %d trials in Kinect folder.\n', length(cellKinect))


%% Compare Vicon and Kinect trials:
viconUnmatched = setdiff(cellVicon, cellKinect);
kinectUnmatched = setdiff(cellKinect, cellVicon);

fprintf('Unmatched Vicon trials: %d.\n', length(viconUnmatched))
fprintf('\t%s\n', viconUnmatched{:})
fprintf('\n')

fprintf('Unmatched Kinect trials: %d.\n', length(kinectUnmatched))
fprintf('\t%s\n', kinectUnmatched{:})


if ~isempty(viconUnmatched) || ~isempty(kinectUnmatched)
    errordlg(sprintf('There are unmatched trials. Vicon: %d, Kinect: %d.\nCheck the log for more info.',...
        length(viconUnmatched), length(kinectUnmatched)), 'Unmatched trials found')
end

end



function cellParsed = ParseTrialFilenames(filenames, expression,...
    ignoredTrialTypes, varargin)
% Parse trials filenames to extract informatin on subject number, trial
% name and trial number.
%
% Input:
%   filenames = full filenames of trials containing info to be extracted.
%   expression = regular expression to be matched. Info will be extracted
%               using tokens.
%   ignoredTrialTypes = row cell array of strings containing types of
%               trials to be skipped, e.g. {'calibration', 'static'}.
%   debugMode = (optional parameter, default = false). If true, displays
%                some extra info for debug purposes.
%
% Output:
%   cellParsed = cell array of strings, containing parsed info with the
%       following structure:
%           DevNNtrialtypeMM
%       where NN is the subject number and MM is the trial number. For
%       example:
%           Dev01droplanding01
%           Dev102sls05
%       Trial types are converted in lower case and spaces are removed
%       before storing info in the cell array.
%
% � July 2016 Alessandro Timmi

%% Default input values:
default.debugMode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input argument
addRequired(p, 'filenames',...
    @(x)validateattributes(x, {'cell'}, {'row', 'nonempty'}))
addRequired(p, 'expression',...
    @(x)validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'ignoredTrialTypes',...
    @(x)validateattributes(x, {'cell'}, {'row', 'nonempty'}))

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'debugMode', default.debugMode, @islogical);

% Parse input arguments:
parse(p, filenames, expression, ignoredTrialTypes, varargin{:});

% Copy input arguments into more memorable variables:
filenames = p.Results.filenames;
expression = p.Results.expression;
ignoredTrialTypes = p.Results.ignoredTrialTypes;
debugMode = p.Results.debugMode;

clear varargin default p


%% Parse trials filenames using regular expression and store tokens:
parsed = regexp(filenames, expression, 'names')';

cellParsed = cell(length(parsed), 1);

fprintf('Ignoring trial type: ''%s''...\n', ignoredTrialTypes{:})

if debugMode
    fprintf('Data parsed from filenames: \n')
end

for i=1:length(parsed)    
    
    if ~any(size(parsed{i}))
        warning('This trial could not be parsed:\n%s', filenames{i})        
    else
        if debugMode
            % Print data parsed from current filename:
            fprintf('\tDev%s %s %s\n', parsed{i}.Dev,...
                parsed{i}.trialType, parsed{i}.trialNumber)
        end   
        
        % Store current trial info in a string, skipping Kinect calibration
        % and static trials:
        if ~any(strcmpi(parsed{i}.trialType, ignoredTrialTypes))
            cellParsed{i} = ['Dev', parsed{i}.Dev,...
                lower(parsed{i}.trialType(~isspace(parsed{i}.trialType))),...
                parsed{i}.trialNumber];
        end            
    end
end

% Remove empty cells, if any:
cellParsed = cellParsed(~cellfun('isempty', cellParsed));

end
