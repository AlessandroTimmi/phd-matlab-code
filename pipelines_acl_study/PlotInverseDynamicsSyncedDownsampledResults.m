% Plot joint moments obtained from Inverse Dynamics (ID) applied on
% Kinect and Vicon data.
%
% � August 2016 Alessandro Timmi

clear
clc

% Input arguments:
rootFolder = 'C:\TEST_FOLDER\ACL study - Kinect Vicon agreement - latest 14 subjects';
resultsSubfolder = '2 - Results';

subjectNumber = 90;
trialNumber = 01;
myCoords = {'hip_flexion_r_moment', 'knee_angle_r_moment', 'ankle_angle_r_moment'};


%% Load .sto files:
viconInputFolder = fullfile(rootFolder, 'Vicon', resultsSubfolder);
kinectInputFolder = fullfile(rootFolder, 'Kinect', resultsSubfolder);

viconFilename = fullfile(viconInputFolder,...
    sprintf('Dev%d\\Barefoot\\Dev%d SLS 0%d_ik_sync_downsampled_id.sto', subjectNumber, subjectNumber, trialNumber));

kinectFilename = fullfile(kinectInputFolder,...
    sprintf('Dev%d\\SLS_0%d_pre-processed_ik_sync_downsampled_id.sto', subjectNumber, trialNumber));

vicon = LoadMotFile('filename', viconFilename);
kinect = LoadMotFile('filename', kinectFilename);


%% Plot joint moments:
legendEntries = cell(1, 2 * length(myCoords));

myTitle = 'Downsampled data';
figure('Name', myTitle)

myColors = {'r', [0, 0.7, 0], 'b'};
hold on
for i=1:length(myCoords)
    plot(vicon.coords.(myCoords{i}), '--', 'Color', myColors{i})
    plot(kinect.coords.(myCoords{i}), '-', 'Color', myColors{i})
    
    legendEntries{2*i-1} = sprintf('Vicon %s', myCoords{i});
    legendEntries{2*i} = sprintf('Kinect %s', myCoords{i});
end

title(myTitle)
xlabel('Frames (#)')
ylabel('Joint moment (Nm)')
legend(legendEntries, 'Interpreter', 'None')



end