function CompareArraysOfStructsAclStudy(kinectArrayOfStructs, viconArrayOfStructs)
% Perform some correspondence checks on two arrays of structs for the
% ACL Study.
%
% Input:
%   kinectArrayOfStructs = array of structs containing .mot data derived
%               from Kinect.
%   viconArrayOfStructs = array of structs containing .mot data derived
%               from Vicon.
%
% � October 2016 Alessandro Timmi


% Check that both arrays of structs have same NUMBER OF TRIALS:
assert(isequal(length(kinectArrayOfStructs), length(viconArrayOfStructs) ),...
    'The two arrays of structs have different lengths')

for f = 1:length(kinectArrayOfStructs)
    
    % Check correspondence of SUBJECT NUMBER between both arrays of structs:    
    assert(isequal(...
        kinectArrayOfStructs(f).subjectNumber,...
        viconArrayOfStructs(f).subjectNumber),...
        'Trials %d of %d have different subject number', f, length(kinectArrayOfStructs))
    
    % Check correspondence of TRIAL TYPE between both arrays of structs:    
    assert(isequal(...
        kinectArrayOfStructs(f).trialType,...
        viconArrayOfStructs(f).trialType),...
        'Trials %d of %d have different trial type', f, length(kinectArrayOfStructs))
    
    % Check correspondence of TRIAL NUMBER between both arrays of structs:    
    assert(isequal(...
        kinectArrayOfStructs(f).trialNumber,...
        viconArrayOfStructs(f).trialNumber),...
        'Trials %d of %d have different trial number', f, length(kinectArrayOfStructs))
            
    % Check correspondence of FRAMES NUMBER:
    assert(isequal(...
        length(kinectArrayOfStructs(f).time), length(viconArrayOfStructs(f).time)),...
        'Trials %d of %d have different frames numbers', f, length(kinectArrayOfStructs))
end

% We cannot check that Kinect and Vicon arrays of structs have same
% COORDINATES NAMES, because they are determined by the different OpenSim
% model used for each device. Models can differ in terms of degrees of
% freedom and body segments available, causing different coordinate names.

end