function WriteMotFile(s, varargin)
% Write a .mot file from data contained into a struct. Both joint
% coordinates and ground reaction forces are supported, because their
% data structure is the same.
%
% Input:
%   s = the struct containing .mot data to be written.
%   destinationFilename = (optional, default = '') the path and filename
%       where data will be stored. If empty, a dialog will open to select
%       a destination filename.
%   force = (optional, default = false) if the file already exists,
%       overwrite it without asking for permission.
%
% � November 2015 Alessandro Timmi


%% Default input values:
default.destinationFilename = '';
default.force = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 's', @isstruct);

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'destinationFilename', default.destinationFilename,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'force', default.force, @islogical)

% Parse input arguments:
parse(p, s, varargin{:});

% Copy input arguments into more memorable variables:
s = p.Results.s;
destinationFilename = p.Results.destinationFilename;
force = p.Results.force;


%%
if isempty(destinationFilename)
    [fname, pname] = uiputfile('*.mot', 'Save .mot file as...');
    destinationFilename = fullfile(pname, fname);
end


%% If "force" is false:
if ~force
    % check if the destination file already exists:
    if exist(destinationFilename, 'file')
        fprintf('File %s already exists...\n', destinationFilename)
        button = questdlg(sprintf('File %s already exists: overwrite?', destinationFilename),...
            'Overwrite file?', 'Yes', 'No', 'Skip', 'No');
        switch button
            case 'Yes'
                fprintf('Overwriting existing file...\n')
            case 'No'
                [fname, pname] = uiputfile('*.mot', 'Select new .mot filename');
                destinationFilename = fullfile(pname, fname);
                fprintf('Selected a new .mot filename:\n')
                fprintf('\t%s\n', destinationFilename);
            case {'Skip', ''}
                fprintf('Skipping file %s...\n\n', destinationFilename);
                return
        end
    end
end


% Get the name and extension of the destination file, to be written in the
% .mot header:
[~, destinationName, destinationExtension] = fileparts(destinationFilename);

% Read coordinate names:
coordinatesNames = fieldnames(s.coords);

% Open the destination .mot file for writing:
fileID = fopen(destinationFilename, 'w');


%% Write the header, using the format compatible with both OpenSim and
% SIMM. For more info see:
% http://simtk-confluence.stanford.edu:8080/display/OpenSim/Motion+(.mot)+Files
% the sample file "subject01_walk1_grf.mot" provided with OpenSim and any
% other IK results .mot file produced by OpenSim.
% NOTE: the comment lines "Units are S.I. units..." which are added by
% OpenSim in its IK results files are not reported here, because they are
% not essential.

% Row 1:
fprintf(fileID, '%s\n', [destinationName, destinationExtension]);

% Row 2:
fprintf(fileID, 'version=1\n');

% Row 3
fprintf(fileID, 'nRows=%d\n', length(s.time));

% Row 4:
fprintf(fileID, 'nColumns=%d\n', 1 + length(coordinatesNames));

% Row 5:
fprintf(fileID, 'inDegrees=yes\n');

% Row 6:
fprintf(fileID, 'endheader\n');

% Row 7, write column headers:
fprintf(fileID, 'time');
for i=1:length(coordinatesNames)
    % If present, remove the 'N' char added during data loading, e.g.:
    % N1_ground_force_vx
    if strcmp(coordinatesNames{i}(1), 'N') && ~isnan(str2double(coordinatesNames{i}(2)))
        this_coord_name = coordinatesNames{i}(2:end);
    else
        this_coord_name = coordinatesNames{i};
    end
    
    fprintf(fileID, '\t%s', this_coord_name);
    
end
fprintf(fileID, '\n');

% Row 8 until end of file:
% Create a string which will contain the formatting operators for the data:
dataFormatters = repmat('\t%.8f', 1, length(coordinatesNames));
% Add a formatter for time and the newline formatter at the end:
dataFormatters = ['%.8f' dataFormatters '\n'];

% Preallocate an array to store coordinates from the struct:
data = zeros(length(s.time), length(coordinatesNames));
for i=1:length(coordinatesNames)
    % Populate the array:
    data(:, i) = s.coords.(coordinatesNames{i});
end

% Add the time array as first column:
data = [s.time, data];

% Write data into the destination .mot file.
% NOTE: since "fprintf" operates column-wise, we need to transpose the
% "data" array:
fprintf(fileID, dataFormatters, data');
fclose(fileID);

fprintf('Data stored in:\n\t%s.\n\n', destinationFilename);

end








