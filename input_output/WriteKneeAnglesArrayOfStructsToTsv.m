function WriteKneeAnglesArrayOfStructsToTsv(kinectArrayOfStructs,...
    viconArrayOfStructs, fieldsToWrite, varargin)
% Write a tab separated values (.tsv) file containing knee angle data from
% Kinect and Vicon, stored in arrays of structs. It is assumed that the two
% arrays of structs have been checked to have corresponding characteristics
% (length, marker names, etc.).
%
% Input:
%   kinectArrayOfStructs = array of structs containing Kinect knee angle data.
%   viconArrayOfStructs = array of structs containing Vicon knee angle data.
%   fieldsToWrite = cell array containing names of fields to write.
%   destFilename = (optional, default = '') the path and filename
%       where data will be stored. If empty, a dialog will open to select
%       a destination filename.
%   balanced = (optional parameter, default = false) if true, writes the
%       same number of samples for each trial, which is equal to the
%       shortest sample size among all trials in the array of structs.
%
% � September 2016 Alessandro Timmi


%% Default input values:
default.destFilename = '';
default.balanced = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'kinectArrayOfStructs', @isstruct);
addRequired(p, 'viconArrayOfStructs', @isstruct);
addRequired(p, 'fieldsToWrite', @iscell)

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'destFilename', default.destFilename,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'balanced', default.balanced, @islogical);

% Parse input arguments:
parse(p, kinectArrayOfStructs, viconArrayOfStructs, fieldsToWrite, varargin{:});

% Copy input arguments into more memorable variables:
kinectArrayOfStructs = p.Results.kinectArrayOfStructs;
viconArrayOfStructs = p.Results.viconArrayOfStructs;
fieldsToWrite = p.Results.fieldsToWrite;
destFilename = p.Results.destFilename;
balanced = p.Results.balanced;


%%
if isempty(destFilename)
    [fname, pname] = uiputfile('*.tsv', 'Save .tsv file as...');
    destFilename = fullfile(pname, fname);
end

% Check if the destination file already exists:
if exist(destFilename, 'file')
    fprintf('File %s already exists...\n', destFilename)
    button = questdlg(sprintf('File %s already exists: overwrite?', destFilename),...
        'Overwrite file?', 'Yes', 'No', 'Skip', 'No');
    switch button
        case 'Yes'
            fprintf('Overwriting existing file...\n')
        case 'No'
            [fname, pname] = uiputfile('*.tsv', 'Select new .tsv filename');
            destFilename = fullfile(pname, fname);
            fprintf('Selected a new .tsv filename:\n')
            fprintf('\t%s\n', destFilename);
        case {'Skip', ''}
            fprintf('Skipping file %s...\n\n', destFilename);
            return
    end
end


%% Open the destination file for writing:
fileID = fopen(destFilename, 'w');

% Number of fields:
nFields = length(fieldsToWrite);

% Write the header row. The resulting header line will follow this
% template:
%   ID Type Frame Time...
%   KinectField1 KinectField2 ... KinectFieldN...
%   ViconField1 ViconField2 ... ViconFieldN
%   (Kinect - Vicon)Field1 (Kinect - Vicon)Field2 (Kinect - Vicon)FieldN ...
fprintf(fileID, 'Subject ID\tType\tFrame\tTime');

% Write Kinect labels in header:
for i = 1:nFields
    fprintf(fileID, '\tK %s', fieldsToWrite{i});
end

% Write Vicon labels in header:
for i = 1:nFields
    fprintf(fileID, '\tV %s', fieldsToWrite{i});
end

% Write differences labels in the header:
for i = 1:nFields
    fprintf(fileID, '\tdiff %s (K-V)', fieldsToWrite{i});
end
fprintf(fileID, '\n');


%% Write the content of the selected fields from the arrays of structs into
% the file. Synchronized Kinect and Vicon data will be stored side by
% side, and data from consecutive pairs of trials will be appended one
% below the other.

% Formatting operator for each field:
dataFormatter = '\t%.8f';


if balanced
    % All trials will have same length (balanced data), equal to the
    % shortest one:
    nFramesArray = zeros(1, length(kinectArrayOfStructs));
    
    for s = 1:length(kinectArrayOfStructs)
        nFramesArray(1, s) = kinectArrayOfStructs(s).NumFrames;
    end
    
    nFramesBalanced = min(nFramesArray);
end


for s = 1:length(kinectArrayOfStructs)
    
    if balanced
        lastFrame = nFramesBalanced;
    else
        lastFrame = kinectArrayOfStructs(s).NumFrames;
    end
    
    % Since we have mixed data (double and char) we need to write the text
    % file line by line:
    for i = 1:lastFrame
        
        % Write initial columns (ID, Type, Frame, Time).
        % For the trial type, we use the trial name removing the trial
        % number from it:
        fprintf(fileID, '%d\t%s\t%d\t%.8f',...
            kinectArrayOfStructs(s).subjectNumber,...
            kinectArrayOfStructs(s).filename(1:end-7),...
            i, kinectArrayOfStructs(s).time(i));
        
        % Add Kinect data to this line:
        for m = 1:nFields
            fprintf(fileID, dataFormatter,...
                kinectArrayOfStructs(s).(fieldsToWrite{m})(i));
        end
        
        % Add Vicon data to this line:
        for m = 1:nFields
            fprintf(fileID, dataFormatter,...
                viconArrayOfStructs(s).(fieldsToWrite{m})(i));
        end
        
        % Add differences Kinect - Vicon:
        for m = 1:nFields
            fprintf(fileID, dataFormatter,...
                kinectArrayOfStructs(s).(fieldsToWrite{m})(i) -...
                viconArrayOfStructs(s).(fieldsToWrite{m})(i));
        end
        
        % End the line:
        fprintf(fileID, '\n');        
    end
    
end

fclose(fileID);

fprintf('Data stored in:\n\t%s.\n\n', destFilename);
end