function [rig_kinect_filename, kinect_cal_filename,...
    trials_kinect_filenames, trials_vicon_filenames, root_folder] =...
    root_folder_lister_agreement_study(subjects_prefix,...
    varargin)
% Obtain a list of all filenames required for the agreement study from
% a root folder.
%
% Input:
%   subjects_prefix = string containing the prefix of the subjects'
%                 folders names. Generally a 3 characters string.
%   root_folder = (optional parameter, default = '') string with the path
%                 to the folder containing all data for Kinect and Vicon.
%                 If empty, a dialog will open to select one.
%
% Output:
%   rig_kinect_filename = cell array with the full filename of the .trc
%                 trial containing coordinates of a 3D printed rig in
%                 Kinect space. This point will be used to determine the
%                 translation vector from Vicon to Kinect origin.
%   kinect_cal_filename = cell array with the full filename of the .c3d
%                 file containing coordinates of reflective markers
%                 attached on Kinect surface, used to estimate Kinect
%                 orientation in Vicon space. This trial also contains
%                 coordinates of the 3D printed rig in Vicon space. This
%                 point will be used to determine the translation vector
%                 from Vicon to Kinect origin.
%   trials_kinect_filename = row cell array containing full filenames of
%                 dynamic trials recorded using Kinect.
%   trials_vicon_filenames = row cell array containing full filenames of
%                 dynamic trials recorded using Vicon.
%   root_folder = string with the path to the folder containing all data
%                 for Kinect and Vicon.
%
%
% Folders must be structured in the following way:
%
% Root_folder/
%   Kinect
%       translation_rig_01.trc
%       TML01/
%           gait_slow_NN.trc
%           gait_fast_NN.trc
%           run_slow_NN.trc
%           run_fast_NN.trc
%       TML02/
%           gait_slow_NN.trc
%           gait_fast_NN.trc
%           run_slow_NN.trc
%           run_fast_NN.trc
%     ...
%       TMLXX/
%           gait_slow_NN.trc
%           gait_fast_NN.trc
%           run_slow_NN.trc
%           run_fast_NN.trc
%   Vicon
%       Kinect_calibration Cal 01.c3d
%       TML01/
%           gait_slow_NN.trc
%           gait_fast_NN.trc
%           run_slow_NN.trc
%           run_fast_NN.trc
%       TML02/
%           gait_slow_NN.trc
%           gait_fast_NN.trc
%           run_slow_NN.trc
%           run_fast_NN.trc
%     ...
%       TMLXX/
%           gait_slow_NN.trc
%           gait_fast_NN.trc
%           run_slow_NN.trc
%           run_fast_NN.trc
%
% Each subject folder can contain any number of trials.
%
% � April 2016 Alessandro Timmi.


%% Default input values:
default.root_folder = '';
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'subjects_prefix',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'root_folder', default.root_folder,...
    @(x)validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, subjects_prefix, varargin{:});

% Copy input arguments into more memorable variables:
subjects_prefix = p.Results.subjects_prefix;
root_folder = p.Results.root_folder;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
if isempty(root_folder)
    root_folder = uigetdir('', 'Select root folder containing Kinect and Vicon data');
    if isequal(root_folder, 0)
        return
    end
end


%% Filename of Kinect translation_rig trial:
rig_kinect_filename = {fullfile(root_folder, 'Kinect', 'translation_rig_01.trc')};
assert(isequal(exist(rig_kinect_filename{1}, 'file'), 2),...
    'Cannot find Kinect translation_rig trial.');


%% Filename of Kinect calibration trial in Vicon space:
kinect_cal_filename = {fullfile(root_folder, 'Vicon', 'Kinect_calibration Cal 01.c3d')};
assert(isequal(exist(kinect_cal_filename{1}, 'file'), 2),...
    'Cannot find Kinect calibration trial in Vicon space.');


%% Subfolders for different conditions are not required for
% this study, because trial names are already explicative of the treadmill
% speed, which is the only changing condition.
conditions_folders = {''};

% List all Kinect trials in all subjects subfolders.
trials_kinect_filenames = list_trials_in_all_subjects(root_folder,...
    subjects_prefix, conditions_folders, 'Kinect', '.trc');

% List all Vicon trials in all subjects subfolders:
trials_vicon_filenames = list_trials_in_all_subjects(root_folder,...
    subjects_prefix, conditions_folders, 'Vicon', '.c3d');

end

