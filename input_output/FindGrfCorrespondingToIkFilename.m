function viconGrfFilename = FindGrfCorrespondingToIkFilename(ikFilename, grfSuffix)
% Match a .mot file containing IK results with the corresponding .mot
% file containing GRF data from Vicon. This is used for Inverse Dynamics.
%
% Input:
% ikFilename = filename of the .mot file containing IK results.
% grfSuffix = suffix characterising the specific GRF file type we want to
%   find, e.g. '_sync'.
%
% Output:
% viconGrfFilename = filename of the .mot file containing Vicon GRF data.
%
% � October 2016 Alessandro Timmi

%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'ikFilename',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'grfSuffix',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Parse input arguments:
parse(p, ikFilename, grfSuffix);

% Copy input arguments into more memorable variables:
ikFilename = p.Results.ikFilename;
grfSuffix = p.Results.grfSuffix;

clear p


%% Regular expression to extract info from IK results filename. The OR
% allows extracting info from both Kinect (first string) and Vicon (second
% string) filenames.
regularExpression = ['\\dev(?<subjectNumber>\d+)\\(?<trialType>\w+)_(?<trialNumber>\d+)_|',...
    '\\dev(?<subjectNumber>\d+) (?<trialType>\w+) (?<trialNumber>\d+)_'];
tokens = regexpi(ikFilename, regularExpression, 'names');


if strfind(ikFilename, '\Kinect\')
    % Input is a Kinect IK file:
    kinectSubjectFolder = fileparts(ikFilename);
    viconSubjectFolder = fullfile(strrep(kinectSubjectFolder,...
        '\Kinect\', '\Vicon\'), 'Barefoot');
else
    % Input is a Vicon IK file:
    viconSubjectFolder = fileparts(ikFilename);
end

viconGrfFilename = fullfile(viconSubjectFolder,...
    sprintf('Dev%s %s %s_grf%s.mot',...
    tokens.subjectNumber, tokens.trialType, tokens.trialNumber, grfSuffix));

% Check if file exists:
assert(isequal(exist(viconGrfFilename, 'file'), 2), 'File not found:\n\t%s.', viconGrfFilename)

end