function folder = CheckFolderExistence(folder)
% Check if the folder already exist and, if so, ask the user what to do.
%
% Input:
%   folder = path to the folder to be checked for existence.
%
% Output:
%   folder = If the input folder does not exist or if it exists and the
%            user wants to add files to it, this is the same folder as in
%            input. Otherwise, this contains the new folder selected by the
%            user.
%
% � August 2016 Alessandro Timmi


if isequal(exist(folder, 'dir'),7)
    fprintf('Folder "%s" already exists.\n', folder)
    button = questdlg(...
        sprintf('Folder "%s" already exists: do you want to save files in it?', folder),...
        'Save files in existing folder?', 'Yes', 'No', 'Stop', 'No');
    
    switch button
        case 'Yes'
            % Do nothing:
            fprintf('Saving files in existing folder...\n')
            
        case 'No'
            % Ask the user to select another folder:
            folder = uigetdir(folder, 'Select destination folder:');
            if isequal(folder, 0)
                StopProgramExecution();
            end
            fprintf('Selected a new destination folder:\n')
            fprintf('\t%s\n', folder);
            
        case {'Stop', ''}            
            StopProgramExecution();
    end
end

end