function n_cols = number_of_columns_per_field(filetype)
% Expected number of columns (coordinates) per field in a trial file.
% The number of columns for each field depends on the file type.
% Input
%       filetype = a string containing the extension of the trial.
% Output
%       n_cols = the expected number of columns for the input filetype.
%
% � March 2016 Alessandro Timmi


switch filetype
    case '.trc'
        % In .trc files, each marker has 3 coordinates (X, Y, Z):
        n_cols = 3;
    case '.mot'
        % In .mot files each field represents 1 joint angle:
        n_cols = 1;
end

end
