function [s, filename] = LoadMotFile(varargin)
% This function loads a .mot file, (TODO) including data in the header.
%
% Input:
% filename    = (optional parameter, default = '') the path and
%               filename of the .mot file to beloaded. If empty, opens a
%               dialog to select a .mot file.
% logMessage = (optional parameter, default = 'Loading .mot file...\n')
%               custom message for the log.
% dialogTitle = (optional parameter, default = 'Select a .mot file')
%               custom title for the dialog.
%
% Output:
% s = the struct containing all the info and data from the .mot file.
% filename = the path and filename of the loaded .mot file. This is
%           especially useful when the file to be loaded is not passed as
%           input argument to this function: in this case, the filename is
%           obtained from a dialog window.
%
% � November 2015 Alessandro Timmi.


%% Default input values:
default.filename = '';
default.logMessage = sprintf('Loading .mot file...\n');
default.dialogTitle = 'Select a .mot file';

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'filename', default.filename, @ischar);
addParameter(p, 'logMessage', default.logMessage,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'dialogTitle', default.dialogTitle,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
filename = p.Results.filename;
logMessage = p.Results.logMessage;
dialogTitle = p.Results.dialogTitle;

clear varargin default p


%% If no input filename, select it using a dialog window:
fprintf('%s', logMessage);
if isempty(filename)
    [fname, pathname] = uigetfile('*.mot', dialogTitle);
    filename = fullfile(pathname, fname);
end
fprintf('\t%s\n', filename);

%% Read the files:
delimiterIn = '\t';
% The number of lines of the header is variable according to the kind of
% .mot file (e.g. ground reaction forces or inverse kinematics results).
% For this reason, we let MATLAB automatically determine it:
motRaw = importdata(filename, delimiterIn);

% Check that Matlab correctly determined the number of lines in the header.
% The second last row of the header should contain the word "endheader":
if ~strcmpi(motRaw.textdata{end-1, 1}, 'endheader')
    error(['The number of rows constituting the header of the .mot file\n',...
        'was not properly determined, or the word endheader is missing']);
end

% Cell array of coord names, read from column headers of the .mot file:
coordNames = motRaw.colheaders(2:end)';

% When the .mot file contains ground reaction forces (GRF), different
% force plates are identified by numbers positioned at the beginning of
% the coordinate names, e.g.:
% 1_ground_force_vx
% However, Matlab structs cannot have fields with names starting by number
% or symbols. For this reason, we need to add a letter (e.g. "N") in front
% of the affected labels. This approach makes it easy to identify
% later what names have been changed, in order to revert them to the
% original name.
for i=1:numel(coordNames)
    % If the first character of the current name is a number...
    if ~isnan(str2double(coordNames{i}(1)))
        % ... add "N" before it:
        coordNames{i} = ['N' coordNames{i}];
    end    
end

% Read coordinates from imported data, skipping the timestamps column:
coords = motRaw.data(:, 2:end);


%% Assemble the output struct.
% Timestamps:
s.time = motRaw.data(1:end, 1);

% Store the name and extension of the .mot file, but disregard the path:
[~, fname, ext] = fileparts(filename);
s.filename = [fname, ext];

% Copy coordinates into the struct:
for i=1:numel(coordNames)
    s.coords.(coordNames{i}) = coords(:, i);
end

end






