function arrayOfStructs = LoadMotToArrayOfStructs(filenames, trialRegularExpression)
% Load .mot files and store data into an array of structs. Extra fields
% are added into each struct, see below.
%
% Input:
%   filenames = row cell array containing .mot filenames.
%   trialRegularExpression = case insensitive string used to extract
%   subject number, trial type and trial number from the full filename of
%       the trial. These info will be added in the following fields of the
%       structs, in string format: 'subjectNumber', 'trialType', 'trialNumber'.
%
% Output:
%   arrayOfStructs = array of structs containing all loaded trials.
%
% � October 2016 Alessandro Timmi

%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'filenames',...
    @(x) validateattributes(x, {'cell'}, {'row', 'nonempty'}));
addRequired(p, 'trialRegularExpression',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}))

% Parse input arguments:
parse(p, filenames, trialRegularExpression);

% Copy input arguments into more memorable variables:
filenames = p.Results.filenames;
trialRegularExpression = p.Results.trialRegularExpression;

clear p


%% Sort trials in alphabetical order:
fprintf('Sorting filenames in alphabetical order...\n')
filenames = sort(filenames);
fprintf('Filenames:\n')
fprintf('\t%s.\n', filenames{:});

nTrials = length(filenames);

% Check that all files have the same extension (.mot or .sto):
ext = cell(1, length(filenames));
for f = 1:nTrials
    [~, ~, ext{f}] = fileparts(filenames{f});    
end
assert(...
    all(strcmpi(ext, '.sto')) || all(strcmpi(ext, '.mot')),...
    'Trials are not all .mot or .sto files.')
fprintf('All trials have same extension')


%% Load each trial:
for f = 1:nTrials
    fprintf('\nLoading trial number %d of %d.\n', f, nTrials)
    s = LoadMotFile('filename', filenames{f});
    
    
    % Check consistency of coordinates names across all trials:
    if f == 1
        % Store coordinates names from current trial, sorting them in alphabetical order:
        sortedCoordinatesNames = sort(fieldnames(s.coords));
    else
        % Compare coordinates names of current trial with those from first trial:
        assert(isequal(sort(fieldnames(s.coords)), sortedCoordinatesNames),...
            'Coordinates names from trial number %d don''t match those from previous trials.', f);
    end
    
    
    % Get subjectNumber, trialType and trialNumber from filenames and add
    % them to the structs:
    regExpResults = regexpi(filenames{f}, trialRegularExpression, 'names');
    s.subjectNumber = regExpResults.subjectNumber;
    s.trialType = regExpResults.trialType;
    s.trialNumber = regExpResults.trialNumber;
    
    
    % Store current struct in an array of structs.
    % No need to preallocate the structs in this case, as it would just
    % create an error when copying during the first iteration, without any
    % improvement in terms of performance.
    arrayOfStructs(f) = s; %#ok<AGROW>
   
end


end