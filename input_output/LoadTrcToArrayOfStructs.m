function arrayOfStructs = LoadTrcToArrayOfStructs(filenames, subjectIdString)
% Load .trc files and store data into an array of structs. An extra field
% ('subjectNumber') is added into each struct, containing the subject ID
% number (in double format).
%
% Input:
%   filenames = row cell array containing .trc filenames.
%   subjectIdString = string used to identify subjects, e.g. 'TML'.
%
% Output:
%   arrayOfStructs = array of structs containing all loaded trials.
%
% � September 2016 Alessandro Timmi

%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'filenames',...
    @(x) validateattributes(x, {'cell'}, {'row', 'nonempty'}));
addRequired(p, 'subjectIdString',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}))

% Parse input arguments:
parse(p, filenames, subjectIdString);

% Copy input arguments into more memorable variables:
filenames = p.Results.filenames;
subjectIdString = p.Results.subjectIdString;

clear p


%% Sort trials in alphabetical order:
fprintf('Sorting filenames in alphabetical order...\n')
filenames = sort(filenames);
fprintf('Filenames:\n')
fprintf('\t%s.\n', filenames{:});

nTrials = length(filenames);

% Check that all files have .trc extension:
for f = 1:nTrials
    [~, ~, ext] = fileparts(filenames{f});
    assert(strcmpi(ext, '.trc'), 'Trial %s is not a .trc file.', filenames{f})
end
fprintf('All trials have .trc extension')


%% Load each trial:
for f = 1:nTrials
    fprintf('\nLoading trial number %d of %d.\n', f, nTrials)
    s = LoadTrcFile('filename', filenames{f});
    
    
    % Check consistency of marker names across all trials:
    if f == 1
        % Store marker names from current trial, sorting them in alphabetical order:
        sortedMarkerNames = sort(fieldnames(s.markers));
    else
        % Compare marker names of current trial with those from first trial:
        assert(isequal(sort(fieldnames(s.markers)), sortedMarkerNames),...
            'Marker names from trial number %d don''t match those from previous trials.', f);
    end
    
    
    % Get subject's ID number from filenames and add it to the structs:
    subjectRegularExpression = [subjectIdString '(\d+)'];
    regExpResults = regexp(filenames{f}, subjectRegularExpression, 'tokens', 'once');
    s.subjectNumber = str2double(regExpResults{1});
    
    
    % Store current struct in an array of structs.
    % No need to preallocate the structs in this case, as it would just
    % create an error when copying during the first iteration, without any
    % improvement in terms of performance.
    arrayOfStructs(f) = s; %#ok<AGROW>
   
end


end