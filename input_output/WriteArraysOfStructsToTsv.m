function WriteArraysOfStructsToTsv(kinectArrayOfStructs,...
    viconArrayOfStructs, varargin)
% Write a tab separated values (.tsv) file containing trials data from
% Kinect and Vicon, stored in arrays of structs. It is assumed that the two
% arrays of structs have been checked to have corresponding characteristics
% (length, marker names, etc.).
% Data are converted from m to mm.
%
% Input:
%   kinectArrayOfStructs = array of structs containing Kinect data.
%   viconArrayOfStructs = array of structs containing Vicon data.
%   destFilename = (optional, default = '') the path and filename
%       where data will be stored. If empty, a dialog will open to select
%       a destination filename.
%   balanced = (optional parameter, default = false) if true, writes the
%       same number of samples for each trial, which is equal to the
%       shortest sample size among all trials in the array of structs.
%
% � September 2016 Alessandro Timmi


%% Default input values:
default.destFilename = '';
default.balanced = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'kinectArrayOfStructs', @isstruct);
addRequired(p, 'viconArrayOfStructs', @isstruct);

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'destFilename', default.destFilename,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'balanced', default.balanced, @islogical);

% Parse input arguments:
parse(p, kinectArrayOfStructs, viconArrayOfStructs, varargin{:});

% Copy input arguments into more memorable variables:
kinectArrayOfStructs = p.Results.kinectArrayOfStructs;
viconArrayOfStructs = p.Results.viconArrayOfStructs;
destFilename = p.Results.destFilename;
balanced = p.Results.balanced;


%%
if isempty(destFilename)
    [fname, pname] = uiputfile('*.tsv', 'Save .tsv file as...');
    destFilename = fullfile(pname, fname);
end

% Check if the destination file already exists:
if exist(destFilename, 'file')
    fprintf('File %s already exists...\n', destFilename)
    button = questdlg(sprintf('File %s already exists: overwrite?', destFilename),...
        'Overwrite file?', 'Yes', 'No', 'Skip', 'No');
    switch button
        case 'Yes'
            fprintf('Overwriting existing file...\n')
        case 'No'
            [fname, pname] = uiputfile('*.tsv', 'Select new .tsv filename');
            destFilename = fullfile(pname, fname);
            fprintf('Selected a new .tsv filename:\n')
            fprintf('\t%s\n', destFilename);
        case {'Skip', ''}
            fprintf('Skipping file %s...\n\n', destFilename);
            return
    end
end


%% Open the destination file for writing:
fileID = fopen(destFilename, 'w');

% Read marker names from the first struct in the array, under the
% assumption that all structs contain the same markers, both for Vicon
% and Kinect:
markerNames = fieldnames(kinectArrayOfStructs(1).markers);

% Number of markers:
nMarkers = length(markerNames);

% Write the header row. The resulting header line will follow this
% template:
%   ID Type Frame Time...
%   KinectMarker1x KinectMarker1y KinectMarker1z ...
%   KinectMarkerNx KinectMarkerNy KinectMarkerNz ...
%   ViconMarker1x ViconMarker1y ViconMarker1z ...
%   ViconMarkerNx ViconMarkerNy ViconMarkerNz ...
%   (Kinect - Vicon)Marker1x (Kinect - Vicon)Marker1y (Kinect - Vicon)Marker1z ...
%   (Kinect - Vicon)MarkerNx (Kinect - Vicon)MarkerNy (Kinect - Vicon)MarkerNz ...
fprintf(fileID, 'Subject ID\tType\tFrame\tTime');

% Write Kinect labels in header:
for i = 1:nMarkers
    for c = 1:3
        fprintf(fileID, '\tK %s %s', markerNames{i}, num2coord(c));
    end
end

% Write Vicon labels in header:
for i = 1:nMarkers
    for c = 1:3
        fprintf(fileID, '\tV %s %s', markerNames{i}, num2coord(c));
    end
end

% Write differences labels in the header:
for i = 1:nMarkers
    for c = 1:3
        fprintf(fileID, '\tdiff %s %s (K-V)', markerNames{i}, num2coord(c));
    end
end
fprintf(fileID, '\n');


%% Write the content of the arrays of structs into the file. Synchronized
% Kinect and Vicon trials will be stored side by side, and consecutive
% pairs of trials will be appended one below the other.

% Formatting operators for each marker: the number "3" represents the
% three coordinates for each marker (X, Y, Z).
% Since we are converting from m to mm, we use 5 decimal places instead of
% the usual 8:
markerDataFormatters = repmat('\t%.5f', 1, 3);


if balanced
    % All trials will have same length (balanced data), equal to the
    % shortest one:
    nFramesArray = zeros(1, length(kinectArrayOfStructs));
    
    for s = 1:length(kinectArrayOfStructs)
        nFramesArray(1, s) = kinectArrayOfStructs(s).NumFrames;
    end
    
    nFramesBalanced = min(nFramesArray);
end


for s = 1:length(kinectArrayOfStructs)
    
    if balanced
        lastFrame = nFramesBalanced;
    else
        lastFrame = kinectArrayOfStructs(s).NumFrames;
    end
    
    % Since we have mixed data (double and char) we need to write the text
    % file line by line:
    for i = 1:lastFrame
        
        % Write initial columns (ID, Type, Frame, Time).
        % For the trial type, we use the trial name removing the trial
        % number from it:
        fprintf(fileID, '%d\t%s\t%d\t%.8f',...
            kinectArrayOfStructs(s).subjectNumber,...
            kinectArrayOfStructs(s).filename(1:end-7),...
            i, kinectArrayOfStructs(s).time(i));
        
        % Add Kinect data to this line, converting them from m to mm:
        for m = 1:nMarkers
            fprintf(fileID, markerDataFormatters,...
                kinectArrayOfStructs(s).markers.(markerNames{m})(i,:) * 1000);
        end
        
        % Add Vicon data to this line, converting them from m to mm:
        for m = 1:nMarkers
            fprintf(fileID, markerDataFormatters,...
                viconArrayOfStructs(s).markers.(markerNames{m})(i,:) * 1000);
        end
        
        % Add differences Kinect - Vicon, converting them from m to mm:
        for m = 1:nMarkers
            fprintf(fileID, markerDataFormatters,...
                ( kinectArrayOfStructs(s).markers.(markerNames{m})(i,:) -...
                viconArrayOfStructs(s).markers.(markerNames{m})(i,:) ) * 1000);
        end
        
        % End the line:
        fprintf(fileID, '\n');
        
    end
    
end

fclose(fileID);

fprintf('Data stored in:\n\t%s.\n\n', destFilename);
end