function [s, filename] = LoadTrcFile(varargin)
% This function loads a .trc file, including data in the header.
%
% Input:
% filename    = (optional parameter, default = '') the path and
%               filename of the .trc file to beloaded. If empty, opens a
%               dialog to select a .trc file.
% logMessage = (optional parameter, default = 'Loading .trc file...\n')
%               custom message for the log.
% dialogTitle = (optional parameter, default = 'Select a .trc file')
%               custom title for the dialog.
%
% Output:
% s = the struct containing all the info and data from the .trc file.
%           NOTICE: The column "Frame#" containing the indices of the
%           frames is discarded on purpose. It will be re-generated when
%           writing the .trc data to a file.
% filename = the path and filename of the loaded .trc file. This is
%           especially useful when the file to be loaded is not passed as
%           input argument to this function: in this case, the filename is
%           obtained from a dialog window.
%
% � November 2014 Alessandro Timmi.


%% Default input values:
default.filename = '';
default.logMessage = sprintf('Loading .trc file...\n');
default.dialogTitle = 'Select a .trc file';

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'filename', default.filename, @ischar);
addParameter(p, 'logMessage', default.logMessage,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'dialogTitle', default.dialogTitle,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
filename = p.Results.filename;
logMessage = p.Results.logMessage;
dialogTitle = p.Results.dialogTitle;

clear varargin default p


%% If no input .trc filename, select it using a dialog window:
fprintf('%s', logMessage);

if isempty(filename)
    [fname, pathname] = uigetfile('*.trc', dialogTitle);
    filename = fullfile(pathname, fname);
end
fprintf('\t%s\n', filename);

%% Read the .trc files:
delimiterIn = '\t';
% Number of lines of the header:
headerlinesIn = 5;
trcRaw = importdata(filename, delimiterIn, headerlinesIn);

% Read data info from header:
dataInfo = regexp(trcRaw.textdata{3,1}, '\t', 'split');

% Read marker names:
labels = textscan(trcRaw.textdata{4, 1}, '%s');
labels = labels{1, 1};

% Cell array of markers names:
markerNamesOriginal = labels(3:end);

% Clean marker names, removing the underscore and the characters after it:
markerNames = clean_marker_names(markerNamesOriginal);

% Read marker coordinates from imported data (skip frame numbers and
% timestamps):
coordinates = trcRaw.data(:, 3:end);


%% Build the struct containing .trc data
% Timestamps:
s.time = trcRaw.data(1:end, 2);

% Store the name and extension of the .trc file, but disregard the path:
[~, fname, ext] = fileparts(filename);
s.filename = [fname, ext];

% Info from the .trc header:
s.DataRate = str2double(dataInfo{1});
s.CameraRate = str2double(dataInfo{2});
s.NumFrames = str2double(dataInfo{3});
s.NumMarkers = str2double(dataInfo{4});
s.Units = dataInfo{1,5};
s.OrigDataRate = str2double(dataInfo{6});
s.OrigDataStartFrame = str2double(dataInfo{7});
s.OrigNumFrames = str2double(dataInfo{8});

% Copy marker coordinates into the struct. Each marker array contains its
% own X, Y and Z coordinate (NumFrames x 3):
for i=1:numel(markerNames)
    s.markers.(markerNames{i}) = coordinates(:, (i-1)*3+(1:3));
end

% Perform some checks on the .trc header info and display warnings if
% they are different from those calculated from the data:
CheckTrcStructInfo(s);

end






