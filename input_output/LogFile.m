function timestampedFilename = LogFile(filename)
% Use Matlab diary function to store a log of the Command Window text to
% file. 
%
% LogFile('desiredFilename') will store the log file in the desired
% location, but the filename will include a timestamp too at the end.
%
% LogFile('off') will turn logging off.
% 
% Input:
%   filename = desired destination where the log file will be stored. The
%              current timestamp will be added to the end of the filename.
%              If equal to 'off', logging will be disabled.
%              
% Output:
%   timestampedFilename = actual filename (including current timestamp) 
%              where the log file will be saved.
%
% � August 2016 Alessandro Timmi

if strcmpi(filename, 'off')
    % Disable the diary and return:
    diary off
    fprintf('Logging disabled.\n')
    return
end

[path, name, ext] = fileparts(filename);
timestamp = datestr(now, 'dd-mmm-yyyy HH.MM.SS');
% Add the current timestamp to the end of the filename:
timestampedFilename = fullfile(path, sprintf('%s %s%s', name, timestamp, ext));

% Check if the file already exist:
if isequal(exist(timestampedFilename, 'file'), 2)
    fprintf('Log file already exists:\n\t%s.\n', timestampedFilename)
    fprintf('Appending output to the end of the file.\n')
end

% Enable the diary:
diary(timestampedFilename);
fprintf('Logging enabled.\n')