function WriteTrcFile(s, varargin)
% Write a .trc file from data contained into a struct.
%
% Input:
%   s = the struct containing .trc data to be written. NOTE: Frame numbers
%       are not included.
%   destinationFilename = (optional, default = '') the path and filename
%       where data will be stored. If empty, a dialog will open to select
%       a destination filename.
%   force = (optional, default = false) if the file already exists,
%       overwrite it without asking for permission.
%
% � November 2014 Alessandro Timmi


%% Default input values:
default.destinationFilename = '';
default.force = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 's', @isstruct);

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'destinationFilename', default.destinationFilename,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'force', default.force, @islogical)

% Parse input arguments:
parse(p, s, varargin{:});

% Copy input arguments into more memorable variables:
s = p.Results.s;
destinationFilename = p.Results.destinationFilename;
force = p.Results.force;


%%
if isempty(destinationFilename)
    [fname, pname] = uiputfile('*.trc', 'Save .trc file as...');
    destinationFilename = fullfile(pname, fname);
end


%% If "force" is false:
if ~force
    % check if the destination file already exists:
    if exist(destinationFilename, 'file')
        fprintf('File %s already exists...\n', destinationFilename)
        button = questdlg(sprintf('File %s already exists: overwrite?', destinationFilename),...
            'Overwrite file?', 'Yes', 'No', 'Skip', 'No');
        switch button
            case 'Yes'
                fprintf('Overwriting existing file...\n')
            case 'No'
                [fname, pname] = uiputfile('*.trc', 'Select new .trc filename');
                destinationFilename = fullfile(pname, fname);
                fprintf('Selected a new .trc filename:\n')
                fprintf('\t%s\n', destinationFilename);
            case {'Skip', ''}
                fprintf('Skipping file %s...\n\n', destinationFilename);
                return
        end
    end
end

% Perform some checks on the .trc header info:
CheckTrcStructInfo(s);

% Get the name and extension of the destination file, to be written in the
% .trc header:
[~, destinationName, destinationExtension] = fileparts(destinationFilename);

% Read marker names:
markerNames = fieldnames(s.markers);

% Open the destination .trc file for writing:
fileID = fopen(destinationFilename, 'w');


%% Write the header:
% Row 1:
fprintf(fileID, '%s\t%d\t%s\t%s\n',...
    'PathFileType', 4, '(X/Y/Z)', [destinationName, destinationExtension]);

% Row 2:
fprintf(fileID,...
    'DataRate\tCameraRate\tNumFrames\tNumMarkers\tUnits\tOrigDataRate\tOrigDataStartFrame\tOrigNumFrames\n');

% Row 3
fprintf(fileID, '%d\t%d\t%d\t%d\t%s\t%d\t%d\t%d\n',...
    s.DataRate, s.CameraRate, s.NumFrames, s.NumMarkers, s.Units,...
    s.OrigDataRate, s.OrigDataStartFrame, s.OrigNumFrames);

% Row 4:
fprintf(fileID, 'Frame#\tTime\t%s', markerNames{1});
for i=2:s.NumMarkers
    fprintf(fileID, '\t\t\t%s', markerNames{i});
end
fprintf(fileID, '\n');

% Row 5 and 6 (which is empty):
% Write the coordinate names, skipping the "Frame#" and "Time" columns:
fprintf(fileID, '\t');
for i=1:s.NumMarkers
    fprintf(fileID, '\tX%d\tY%d\tZ%d', i, i, i);
end
fprintf(fileID, '\n\n');

% Row 7 until end of file:
% Create a string which will contain the formatting operators for the data,
% including the time array:
dataFormatters = repmat('\t%.8f', 1, 1 + s.NumMarkers * 3);

% Frame numbers will be formatted as integers:
dataFormatters = ['%d', dataFormatters '\n'];

% Preallocate an array to store marker coordinates from the struct:
data = zeros(s.NumFrames, s.NumMarkers * 3);
for i=1:s.NumMarkers
    % Populate the array:
    data(:, (i-1)*3+(1:3)) = s.markers.(markerNames{i});
end

% The content of the "Frame#" column is generated here:
data = [(1:s.NumFrames)', s.time, data];

% Write data into the destination .trc file.
% NOTE: since "fprintf" operates column-wise, we need to transpose the
% "data" array:
fprintf(fileID, dataFormatters, data');
fclose(fileID);

fprintf('Data stored in:\n\t%s.\n\n', destinationFilename);
end

