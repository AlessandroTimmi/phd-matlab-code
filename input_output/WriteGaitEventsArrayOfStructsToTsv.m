function WriteGaitEventsArrayOfStructsToTsv(kinectArrayOfStructs,...
    viconArrayOfStructs, eventToWrite, eventLocations, varargin)
% Write a tab separated values (.tsv) file containing knee angle data in
% correspondence of specific gait events. Data are collected from Kinect
% and Vicon and stored in arrays of structs. It is assumed that the two
% arrays of structs have been checked to have corresponding characteristics
% (length, marker names, etc.).
%
% Input:
%   kinectArrayOfStructs = array of structs containing Kinect knee angle data.
%   viconArrayOfStructs = array of structs containing Vicon knee angle data.
%   eventToWrite = string containing the name of the event to write.
%   eventLocations = string containing the name of the field containing the
%       locations (frames numbers) of the event to write.
%   destFilename = (optional, default = '') the path and filename
%       where data will be stored. If empty, a dialog will open to select
%       a destination filename.
%   balanced = (optional parameter, default = false) if true, writes the
%       same number of samples for each trial, which is equal to the
%       shortest sample size among all trials in the array of structs.
%
% � September 2016 Alessandro Timmi


%% Default input values:
default.destFilename = '';
default.balanced = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'kinectArrayOfStructs', @isstruct);
addRequired(p, 'viconArrayOfStructs', @isstruct);
addRequired(p, 'eventToWrite',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'eventLocations',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'destFilename', default.destFilename,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'balanced', default.balanced, @islogical);

% Parse input arguments:
parse(p, kinectArrayOfStructs, viconArrayOfStructs, eventToWrite,...
    eventLocations, varargin{:});

% Copy input arguments into more memorable variables:
kinectArrayOfStructs = p.Results.kinectArrayOfStructs;
viconArrayOfStructs = p.Results.viconArrayOfStructs;
eventToWrite = p.Results.eventToWrite;
eventLocations = p.Results.eventLocations;
destFilename = p.Results.destFilename;
balanced = p.Results.balanced;


%%
if isempty(destFilename)
    [fname, pname] = uiputfile('*.tsv', 'Save .tsv file as...');
    destFilename = fullfile(pname, fname);
end

% Check if the destination file already exists:
if exist(destFilename, 'file')
    fprintf('File %s already exists...\n', destFilename)
    button = questdlg(sprintf('File %s already exists: overwrite?', destFilename),...
        'Overwrite file?', 'Yes', 'No', 'Skip', 'No');
    switch button
        case 'Yes'
            fprintf('Overwriting existing file...\n')
        case 'No'
            [fname, pname] = uiputfile('*.tsv', 'Select new .tsv filename');
            destFilename = fullfile(pname, fname);
            fprintf('Selected a new .tsv filename:\n')
            fprintf('\t%s\n', destFilename);
        case {'Skip', ''}
            fprintf('Skipping file %s...\n\n', destFilename);
            return
    end
end


%% Open the destination file for writing:
fileID = fopen(destFilename, 'w');

% Write the header row. The resulting header line will follow this
% template:
%   ID Type Frame Time...
%   KinectEvent ViconEvent (Kinect - Vicon)Event
fprintf(fileID, 'Subject ID\tType\tFrame\tTime');

% Write Kinect label in header:
fprintf(fileID, '\tK %s', eventToWrite);

% Write Vicon label in header:
fprintf(fileID, '\tV %s', eventToWrite);

% Write difference label in the header:
fprintf(fileID, '\tdiff %s (K-V)', eventToWrite);
fprintf(fileID, '\n');


%% Write the content of the selected fields from the arrays of structs into
% the file. Synchronized Kinect and Vicon data will be stored side by
% side, and data from consecutive pairs of trials will be appended one
% below the other.

% Formatting operator for each field:
dataFormatter = '\t%.8f';


if balanced
    % All trials will have same length (balanced data), equal to the
    % shortest one:
    nFramesArray = zeros(1, length(kinectArrayOfStructs));
    
    for s = 1:length(kinectArrayOfStructs)
        nFramesArray(1, s) = length(kinectArrayOfStructs(s).(eventToWrite));
    end
    
    nFramesBalanced = min(nFramesArray);
end


for s = 1:length(kinectArrayOfStructs)
    
    if balanced
        lastFrame = nFramesBalanced;
    else
        lastFrame = length(kinectArrayOfStructs(s).(eventToWrite));
    end
    
    % Since we have mixed data (double and char) we need to write the text
    % file line by line:
    for i = 1:lastFrame
        
        % Write initial columns (ID, Type, Frame, Time).
        % For the trial type, we use the trial name removing the trial
        % number from it:
        fprintf(fileID, '%d\t%s\t%d\t%.8f',...
            kinectArrayOfStructs(s).subjectNumber,...
            kinectArrayOfStructs(s).filename(1:end-7),...
            viconArrayOfStructs(s).(eventLocations)(i),...
            viconArrayOfStructs(s).time(viconArrayOfStructs(s).(eventLocations)(i)));
        
        % Add Kinect data to this line:
        fprintf(fileID, dataFormatter,...
            kinectArrayOfStructs(s).(eventToWrite)(i));
        
        % Add Vicon data to this line:
        fprintf(fileID, dataFormatter,...
            viconArrayOfStructs(s).(eventToWrite)(i));
        
        % Add difference Kinect - Vicon:
        fprintf(fileID, dataFormatter,...
            kinectArrayOfStructs(s).(eventToWrite)(i) -...
            viconArrayOfStructs(s).(eventToWrite)(i));
                
        % End the line:
        fprintf(fileID, '\n');        
    end
    
end

fclose(fileID);

fprintf('Data stored in:\n\t%s.\n\n', destFilename);
end