function [c3dFilenames, trcFilenames, motFilenames] =...
    ExtractC3D(c3dFilenames, varargin)
% Extract .c3d files to .trc and .mot files, compatible with OpenSim.
%
% Input:
%   c3dFilenames = cell array of strings containing full filenames of the
%                 .c3d file(s) to be extracted.
%   markersetName = (optional parameter, default = see below) string
%                 containing the name of the markerset to be used for
%                 extracting data from the .c3d file(s).
%   trimTrial    = (optional parameter, default = true) true if you want
%                 to trim the trial, based on vertical ground reaction
%                 forces. Useuful to cut the trial in correspondence of
%                 foot initial/final contact.
%   FvMin        = (optional parameter, default = 20 N) threshold on
%                 vertical force component (Fv [N]). Used in 3 instances:
%                 1) If the max Fv from a force platform is below or equal
%                 to this value, that force platform is completely
%                 ignored for trimming.
%                 2) Threshold used to detect contact events, defined as
%                 the first and last frames in which Fv > Fv_min. Force
%                 plates at CHESM lab sometimes have a random noise on the
%                 vertical axis ranging within +/- 10 N. For this reason,
%                 we set our default minimum threshold to 20 N, to ensure
%                 that random noise cannot generate false events.
%                 3) Used in btk_loadc3d() to set all points of wrench
%                 application computed with Fv <= Fv_min to the centre(?)
%                 of the force plate. See btkGroundReactionWrenches.m for
%                 further info.
%   rotateViconToOpensim = (optional parameter, default = true) if
%                 true, applies a rotation to imported data, in order to
%                 transform coordinates from Vicon to OpenSim reference
%                 frame.
%   newRootFolder = (optional parameter, default = '') new root folder in
%                 which trials will be stored. If the string is not empty,
%                 it will replace the one in the current trial filename.
%   destinationFilename = (optional parameter, default = '') name of the
%                 extracted trial. It must not contain any extension. Only
%                 usable when a single trial is selected.
%
% Output
%
%   c3dFilenames = see above.
%   trcFilenames = full filenames of the extracted .trc files.
%   motFilenames = full filenames of the extracted .mot files.
%
% Inspired by Matlab_OpenSim_Toolbox_v2, by Glen Lichtwark et al.
% � October 2014 Alessandro Timmi.

%% TODO:
% - add a check on missing markers, even if missing only in a part of the
%   trial.
% - there might be problems if a newRootFolder is specified, but the original
%   folder does not contain 'Dev' in the path.
% - add a list dialog for the markerset choice, when this_markerset is empty.
% - add debug plots and debug fprintf() where required.
% - move the variables for plotting (marker coord, GRF coord) to input
%   parser.
% - Add possibility to use different filters.
% - it looks like set_contact_events() doesn't work when the force plates
% where not actively used during the trial. The easy solution is to ensure
% that the "trim_trial" parameter is false, but it would be nice to make
% the function more robust in these cases.
% - Add a log reporting trials in which markers where missing.

%% Default input values:
default.markersetName = 'Schache_CHESM_v092F';
default.trimTrial = true;
default.FvMin = 20;
default.rotateViconToOpenSim = true;
default.newRootFolder = '';
default.destinationFilename = '';

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required arguments:
addRequired(p, 'c3dFilenames',...
    @(x) validateattributes(x, {'char', 'cell'}, {'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'markersetName', default.markersetName,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'trimTrial', default.trimTrial, @islogical);
addParameter(p, 'FvMin', default.FvMin, @isnumeric);
addParameter(p, 'rotateViconToOpenSim',...
    default.rotateViconToOpenSim, @islogical);
addParameter(p, 'newRootFolder', default.newRootFolder,...
    @(x) validateattributes(x, {'char'}, {}));
addParameter(p, 'destinationFilename', default.destinationFilename,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Parse input arguments:
parse(p, c3dFilenames, varargin{:});

% Copy input arguments into more memorable variables:
c3dFilenames = p.Results.c3dFilenames;
markersetName = p.Results.markersetName;
trimTrial = p.Results.trimTrial;
FvMin = p.Results.FvMin;
rotateViconToOpenSim = p.Results.rotateViconToOpenSim;
newRootFolder = p.Results.newRootFolder;
destinationFilename = p.Results.destinationFilename;


%% The destination filename can be passed as input argument only if a
% single trial is loaded:
if ~isempty(destinationFilename)
    assert(isequal(length(c3dFilenames), 1),...
        'The destination filename can be specified only if a single trial is selected.')
end


%% Preallocate cell arrays for extracted filenames:
trcFilenames = cell(size(c3dFilenames));
% Sometimes .c3d files don't have force plates data, hence some cells
% might be left empty at the end:
motFilenames = cell(size(c3dFilenames));


% Load the requested markerset:
[dynamicMarkersList, staticMarkersList] = markerset(markersetName);


% Suppress the warning from BTK library related to the centre of the force plates:
warning('off', 'btk:GetGroundReactionWrenches')


%% Loop on the trial names:
fprintf('Extracting .c3d files to .trc and .mot files...\n')
for f=1:numel(c3dFilenames)
    
    % Load the C3D file using BTK.
    % FvMin here is used to set all points of wrench application computed
    % with a Fv <= FvMin to the centre(?) of the force plate:
    data = btk_loadc3d(c3dFilenames{f}, FvMin);
    
    fprintf('Loaded file %d of %d:\n', f, numel(c3dFilenames))
    fprintf('\t%s\n', c3dFilenames{f})
    
    % Get path and file name of current trial:
    [destinationFolder, fname, ~] = fileparts(c3dFilenames{f});
    
    % Change root folder if requested:
    if ~isempty(newRootFolder)
        destinationFolder = ChangeRootFolderACLStudy(destinationFolder, newRootFolder);
        
        % Check for its existence:
        if ~isequal(exist(destinationFolder, 'dir'), 7)
            % Create the folder if required:
            mkdir(destinationFolder);
        end
    end
    
    % Marker names found in the loaded .c3d file:
    foundMarkers = fields(data.marker_data.Markers);
    
    % If current trial is NOT a static (a.k.a. calibration) one:
    if isempty(strfind(lower(fname), 'static')) &&...
            isempty(strfind(lower(fname), 'cal'))
        
        fprintf('This is a dynamic trial.\n')
        
        % Check for missing markers using the dynamic markerset:
        [~, extraMarkers] = CheckMissingExtraMarkers(dynamicMarkersList, foundMarkers);
        % Remove extra markers if found:
        if ~isempty(extraMarkers)
            for m = 1:length(extraMarkers)
                data.marker_data.Markers = rmfield(data.marker_data.Markers, extraMarkers{m});
            end
        end
        
        % Set the start and end frame based on vertical ground reaction
        % forces.
        if trimTrial
            data = set_contact_events(data, FvMin);
        end
        
        % Export to .trc and .mot files:
        data = btk_c3d2trc('data', data,...
            'rotate_vicon_to_opensim', rotateViconToOpenSim,...
            'dest_folder', destinationFolder,...
            'dest_fname', destinationFilename);
        
    else
        fprintf('This is a static (a.k.a. calibration) trial.\n')
        
        % The static markerset if composed of the dynamic markerset plus
        % some markers used only for static trials:
        staticMarkerset = [dynamicMarkersList;...
            staticMarkersList];
        
        % Check for missing markers using the static markersets:
        [~, extraMarkers] = CheckMissingExtraMarkers(staticMarkerset, foundMarkers);
        % Remove extra markers if found:
        if ~isempty(extraMarkers)
            for m = 1:length(extraMarkers)
                data.marker_data.Markers = rmfield(data.marker_data.Markers, extraMarkers{m});
            end
        end
        
        % Export to .trc and .mot files:
        data = btk_c3d2trc('data', data,...
            'rotate_vicon_to_opensim', rotateViconToOpenSim,...
            'dest_folder', destinationFolder,...
            'dest_fname', destinationFilename);
        
    end
    
    % Store full filenames of the extracted trials:
    trcFilenames{f} = data.TRC_Filename;
    if isfield(data,'GRF_Filename')
        motFilenames{f} = data.GRF_Filename;
    end
    
end

fprintf('\n')

end
