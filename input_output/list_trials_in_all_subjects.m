function trials_filenames = list_trials_in_all_subjects(root_folder,...
    subjects_prefix, conditions_folders, device_name, trials_ext)
% List all trials, searching through each subject and each condition
% folder.
%
% Input:
%   root_folder = string with the path to the folder containing all trials
%                 data.
%   subjects_prefix = case insensitive string containing the prefix of the
%                 subjects' folders names. Generally a 3 characters string,
%                 e.g. 'DEV'.
%   conditions_folders = cell array with names of subfolders contained in
%                 each subject folder. They represent different conditions
%                 in which trials were recorded (e.g. {'Barefoot',
%                 'Kayano', 'Zaraca'}).
%   device_name = name of the folder containing subject data collected
%                 using a specific motion capture device, e.g. 'Vicon' or
%                 'Kinect'. If only a single device is used, this parameter
%                 can be set to the empty string ('').
%   trials_ext =  extension of the trials files, e.g. '.c3d' or '.trc'.
%
% Output
%   trials_filenames = row cell array containing full filenames of the
%                 loaded trials contained in all subjects and conditions
%                 folders.
%
% This is the expected folder structure:
% rootFolder
%   deviceName\
%       sub01\
%           conditionA\
%               trial01.ext
%               trial02.ext
%               ...
%               trialM.ext
%           conditionB\
%               trial01.ext
%               trial02.ext
%               ...
%               trialN.ext
%       sub02\
%           conditionA\
%               trial01.ext
%               trial02.ext
%               ...
%               trialX.ext
%           conditionB\
%               trial01.ext
%               trial02.ext
%               ...
%               trialY.ext
%
% � April 2016 Alessandro Timmi


subjects = dir(fullfile(root_folder, device_name, [subjects_prefix '*']));
% Only pick the results which are folders:
subjects = subjects([subjects.isdir]);

conditions = struct;

for s=1:length(subjects)
    for c=1:length(conditions_folders)
        % List all files for current subject in current condition:
        this_folder = fullfile(root_folder, device_name, subjects(s).name,...
            conditions_folders{c});
        listing = dir(fullfile(this_folder, ['*', trials_ext]));
        listing_cell = struct2cell(listing);
        listing_fnames = listing_cell(1,:);
        % Assemble full filenames of files of current subject in current
        % condition. The function fullfile() preserves the shape of the
        % input cell array, hence the result will be a row cell array.
        conditions(c).filenames = fullfile(this_folder, listing_fnames);
    end
    % Horizontally concatenate all .c3d filenames for current subject:
    subjects(s).filenames = horzcat(conditions(:).filenames);
end

% Horizontally concatenate all .c3d filenames for all subjects:
trials_filenames = horzcat(subjects(:).filenames);


end

