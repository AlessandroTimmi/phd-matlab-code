function out = IsFiltrable(fieldName, y)
% Check if struct field is filtrable. Not filtrable fields are those:
% - containing timestamps (time array);
% - containing frame numbers (frame#);
% - having less than 2 samples.
% If the field is not filtrable, the output will be false.
% The functions filtfilt(), medfilt1() and sgolayfilt() operate along the
% first non-singleton dimension of the input array. Hence, the input "y"
% can have multiple columns.
%
% Input:
%   fieldName  = string with the name of the field of a struct.
%   y          = array representing a field of a struct.
%
% Output:
%   out        = (logical) true if field "field_name" is filtrable, false
%                otherwise.
% 
% � July 2015 Alessandro Timmi

%% TODO:
% - auto determine field name, without having it as input argument.
% - see if there are other criteria to make an array filtrable.


%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'fieldName', @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'y', @ismatrix);

% Parse input arguments:
parse(p, fieldName, y);

% Copy input arguments into more memorable variables:
fieldName = p.Results.fieldName;
y = p.Results.y;


%% Checks:
if strcmpi(fieldName, 'time') || strcmpi(fieldName, 'frame#')
    % Don't filter the time or frames array, just copy it:
    out = false;
    return
end            

if size(y(~isnan(y)), 1)<2
    % We don't wanna let the filter "invent" data when gaps are too big.
    % We expect gaps to be already filled at this stage.
    % If there is only one sample in the current field, skip
    % filtering and return the vector as it is:
    warning('Less than 2 samples in this coordinate array. Filtering not performed on this column.');
    out = false;
    return
end

% If previous checks are passed, then the field is filtrable:
out = true;