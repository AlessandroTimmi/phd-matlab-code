function [mean_thompson, outlier_indices, outlier_values] = remove_outliers_thompson(x)
% Remove outliers from a sample of a single variable, based on Modified
% Thompson tau.
% Reference document:
% "Outliers", Cimbala JM, Penn State University, Sept 2011.
%
% � May 2015 Alessandro Timmi

% Copy the input array, to be used later:
x_orig = x;

% Empty variables:
outlier_values = [];
outlier_indices = [];

% Status variable:
outliers_still_present = true;

while outliers_still_present == true
    mean_x = mean(x);
    sd_x = std(x);

    % Absolute values of the deviations:
    d_abs = abs(x - mean_x);
    
    % Max deviation and its index:
    [max_d_abs, max_d_abs_id] = max(d_abs);
    
    % Calculate the modified Thompson tau for this sample size (n):
    n = length(x);
    tau = thompson_mod_tau(n);

    % We consider only one suspected outlier at a time, the one with
    % largest absolute value of the deviation:
    if max_d_abs > tau * sd_x
        % Store the outlier value:
        outlier_values = [outlier_values, x(max_d_abs_id)];
        % Find outlier index:
        outlier_indices = [outlier_indices, find(x_orig == x(max_d_abs_id),1,'first')];
        % Remove the outlier:
        x(max_d_abs_id) = [];
    else
        outliers_still_present = false;
    end   
end

mean_thompson = mean(x);
fprintf('Outlier(s) found using MODIFIED THOMPSON TAU: %d\n', length(outlier_values));
fprintf('Mean after removing outlier(s):\t %0.3f m\n', mean_thompson);

end



function tau = thompson_mod_tau(n)
% Modified Thompson tau, used for outliers removal.

if n<3
    error('Error: sample size n must be >= 3.')
end

% Level of significance, usually 0.05:
alpha = 0.05;    
% Degrees of freedom:
df = n - 2;

% Student's t value:
t = tinv(1 - alpha / 2, df);

% Modified Thompson tau:
tau = (t*(n-1))/(sqrt(n)*sqrt(n-2+t^2));
end