% Study noise in marker data
% � 2015 Alessandro Timmi
clear
close all
clc

[s, filename] = LoadTrcFile();

marker_names = fieldnames(s.markers);

marker1 = marker_names{27};
marker2 = marker_names{28};

fig_title = sprintf('%s coordinates', marker1);
figure('Name', fig_title)
plot(s.time, s.markers.(marker1)(:,1), 'r')
title(fig_title)
hold on
plot(s.time, s.markers.(marker1)(:,2), 'g')
plot(s.time, s.markers.(marker1)(:,3), 'b')
xlabel('Time (s)')
ylabel('Coordinates (m)')
legend('X', 'Y', 'Z')

fig_title = sprintf('%s coordinates', marker2);
figure('Name', fig_title)
plot(s.time, s.markers.(marker2)(:,1), 'r')
title(fig_title)
hold on
plot(s.time, s.markers.(marker2)(:,2), 'g')
plot(s.time, s.markers.(marker2)(:,3), 'b')
xlabel('Time (s)')
ylabel('Coordinates (m)')
legend('X', 'Y', 'Z')


[diff_t, mean_diff_t, std_dev_diff_t, avg_freq_t]= ...
    check_sampling_freq(s.time, 'debug_mode', true);

Fs = 30;
t = s.time;
x = s.markers.(marker2)(:,1);
xdft = (1/length(x))*fft(x);
freq = -Fs/2:(Fs/length(x)):Fs/2-(Fs/length(x));
title_freq = sprintf('(DOUBLE CHECK FREQ ANALYSIS) %s', marker2);
figure('Name', title_freq)
plot(freq, abs(fftshift(xdft)));
title(title_freq)