clear
clc
close all

a=importdata(...
'D:\Tesi\VirtualSenseiLite\Prove Filtro Butterworth\prova25Dic-zenku.csv',',',6);
HEAD=0;
NECK=1;
TORSO=2;
L_SHOULDER=3;
L_ELBOW=4;
L_HAND=5;
R_SHOULDER=6;
R_ELBOW=7;
R_HAND=8;
L_HIP=9;
L_KNEE=10;
L_FOOT=11;
R_HIP=12;
R_KNEE=13;
R_FOOT=14;

% Giunto scelto (1-based):
joint=HEAD;

% Coordinata scelta (X=1, Y=2, Z=3)
c=2;

% Dati grezzi:
raw_data=a.data(:,12*joint+c);

% Filtro median:
%median_filt_data=medfilt1(raw_data,5);

% Filtro Butter:
FRAMERATE_CAPTURE=30; % frequenza di campionamento = 30 fps
% Parametri del filtro Butterworth
% Frequenza di cutoff in Hz:
CUTOFF_FREQ=6;
% Ordine del filtro: un ordine minore segue meno il segnale in ingresso, ma credo sia meno pesante computazionalmente
FILTER_ORDER=5;
[b,a] = butter(FILTER_ORDER, CUTOFF_FREQ/(FRAMERATE_CAPTURE/2), 'low');
butter_filt_data = filtfilt(b, a, raw_data);

% Filtro Savitzky-Golay
sgolay7_filt_data=sgolayfilt(raw_data,3,7);

% Filtro median + sgolay
appo=medfilt1(raw_data,5);
median5_sgolay5_filt_data=sgolayfilt(appo,3,5);
median5_sgolay7_filt_data=sgolayfilt(appo,3,7);


% PLOT
lw=1.5; %linewidth
figure
plot(raw_data,'b','linewidth',lw)
hold on
%plot(median_filt_data,'r-','linewidth',lw)
plot(butter_filt_data,'c','linewidth',lw)
%plot(sgolay_filt_data,'g','linewidth',lw)

%plot(sgolay7_filt_data,'m','linewidth',lw)

plot(median5_sgolay5_filt_data,'color',[0.7,0,0.7],'linewidth',lw)
plot(median5_sgolay7_filt_data,'color',[0.5,0.8,0.5],'linewidth',lw)

legend('raw',...
    'median span5+sgolay 3rd span5',...
    'median span5+sgolay 3rd span7',...
    'location','best')
title('TSUKI - L\_HAND Z - Filters comparison','fontweight','bold')
ylabel('m')
xlabel('1/30 s')

% Attualmente il miglior compromesso sembra rappresentato dalla
% sovrapposizione di:
% un filtro median con span=5 (per rimuovere gli spike);
% un filtro savitzky-golay di 3 ordine e span=7 (per lo smoothing).



