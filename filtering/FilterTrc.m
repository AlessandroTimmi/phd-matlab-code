function s = FilterTrc(s, filterType, varargin)
% Filter markers coordinates from a struct containing .trc data, using one
% among several possible filtering techniques.
%
% NOTE: all filtering functions used below operate along the first
% nonsingleton dimension of the input matrix.
%
% Input:
%   s             = struct containing .trc markers coordinates to be
%                   filtered.
%   filterType    = one of the following strings: 'butter', 'sgolay',
%                  'median'.
%   butterOrder  = (optional parameter, default = 0) order of
%                  Butterworth filter.
%   butterCutoff = (optional parameter, default = 0) cutoff frequency
%                  for Butterworth filter. As suggested by Matlab Help for
%                  butter filter, the normalized cutoff frequency used by
%                  the filter will be calculated as:
%                  Wn = cutoff_frequency / Nyquist_frequency
%                  where Nyquist_frequency is half the sampling rate.
%   medianSpan   = (optional parameter, default = 0) span of median
%                  filter. Even if Matlab documentation is not clear on
%                  this aspect, the median filter order represents the
%                  number of samples the median is calculated on, so it is
%                  actually a span (or window), as in Python Scipy
%                  implementation.
%   sgolayOrder  = (optional parameter, default = 0) order of
%                  Savitzky-Golay filter.
%   sgolaySpan   = (optional parameter, default = 0) span of
%                  Savitzky-Golay filter.
%
% Output:
%   s           = struct containing filtered .trc markers coordinates.
%
% � November 2014 Alessandro Timmi

% TODO:
% - When there are gaps in data, filter only the good data instead of
%   returning the vector as it is (see function IsFiltrable() below)
% - filtfilt() (used to apply Butterworth filter) is a zero-phase shifting
%   filtering techniques, but it doubles the order of the filter. Maybe
%   we should halve the order of the filter at design time (see: Winter 2009).
% - look at zp2sos() function and try to implement the Butterworth filter
%   using the z,p,k method instead of a,b for improved numerical stability.


%% Default input values:
expectedFilters = {'butter', 'sgolay', 'median'};
default.butterOrder = 0; % Butterworth filter order
default.butterCutoff = 0; % Cutoff frequency [Hz] for lowpass Butterworth filter
default.medianSpan = 0; % Span size [samples] of the median filter
default.sgolayOrder = 0; % Savitzky-Golay filter order
default.sgolaySpan = 0; % Span size [samples] of the Savitzky-Golay filter

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 's', @isstruct);
addRequired(p, 'filterType', @(x) any(validatestring(x, expectedFilters)));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'butterOrder', default.butterOrder,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'butterCutoff', default.butterCutoff,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'medianSpan', default.medianSpan,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'sgolayOrder', default.sgolayOrder,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'sgolaySpan', default.sgolaySpan,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));

% Parse input arguments:
parse(p, s, filterType, varargin{:});

% Copy input arguments into more memorable variables:
s = p.Results.s;
filterType = p.Results.filterType;

butterOrder = p.Results.butterOrder;
butterCutoff = p.Results.butterCutoff;
medianSpan = p.Results.medianSpan;
sgolayOrder = p.Results.sgolayOrder;
sgolaySpan = p.Results.sgolaySpan;


%%
fprintf('Filtering data...\n')

% Check data sampling frequency:
[~, ~, ~, samplingFreq] = check_sampling_freq(s.time);

% Get the names of the fields in the input struct:
markerNames = fieldnames(s.markers);


% Filter coordinates using the selected method:
fprintf('Selected filter: ')
switch filterType
    
    case 'butter'
        %% Butterworth filter:
        fprintf('Butterworth.\n')
        
        % Check Butterworth filter settings:
        if ~butterOrder
            error('Butterworth order not selected.')
        end
        if ~butterCutoff
            error('Butterworth cut-off frequency not selected.')
        end
        
        fprintf('Filter design:\n');
        fprintf('Order = %d;\n', butterOrder);
        fprintf('Cutoff frequency = %.1f Hz;\n', butterCutoff);
        
        nyquistFrequency = samplingFreq / 2;
        fprintf('Nyquist frequency (1/2 sampling frequency of current trial) = %.1f Hz.\n',...
            nyquistFrequency)
        
        [b, a] = butter(butterOrder, butterCutoff / nyquistFrequency, 'low');
        
        % Apply Butterworth filter to each field of the input struct:
        for i = 1:numel(markerNames)
            
            % Store current unfiltered field into a temporary array,
            % just because it has a shorter name:
            y = s.markers.(markerNames{i});
            
            if ~IsFiltrable(markerNames{i}, y)                
                continue
            end
            
            % Apply Butterworth filter to current field, using zero-phase
            % shift technique:
            s.markers.(markerNames{i}) = filtfilt(b, a, y);
        end
        
        
    case 'median'
        %% Median filter:
        fprintf('Median.\n')
        
        % Check median filter settings:
        if ~medianSpan
            error('Median filter span not selected')
        end
        
        fprintf('Filter design:\n');
        fprintf('Median span = %d frames.\n', medianSpan);
        
        % Apply median filter to each field of the input struct:
        for i=1:numel(markerNames)
            
            % Store current unfiltered field into a temporary array,
            % just because it has a shorter name:
            y = s.markers.(markerNames{i});
            
            if ~IsFiltrable(markerNames{i}, y)                
                continue
            end
            
            % Apply median filter to current field:
            s.markers.(markerNames{i}) = medfilt1(y, medianSpan);            
        end
        
        
    case 'sgolay'
        %% Savitzky-Golay filter:
        fprintf('Sgolay.\n')
        
        % Check Savitzky-Golay filter settings:
        if ~sgolayOrder
            error('Savitzky-Golay filter order not selected')
        end
        if ~sgolaySpan
            error('Savitzky-Golay filter span not selected')
        end
        
        fprintf('Filter design:\n');
        fprintf('Sgolay order = %d;\n', sgolayOrder);
        fprintf('Sgolay span = %d frames.\n', sgolaySpan);
        
        % Apply filter to data:
        for i = 1:numel(markerNames)
            
            % Store current unfiltered field into a temporary array,
            % just because it has a shorter name:
            y = s.markers.(markerNames{i});
            
            if ~IsFiltrable(markerNames{i}, y)               
                continue
            end
            
            % Apply Sgolay filter to each field of the input struct:
            s.markers.(markerNames{i}) = sgolayfilt(y, sgolayOrder,...
                sgolaySpan);
        end       
        
end

end
