%% Kinect 2 filter test tool
% This is a test tool to evaluate the performances of different filters
% to be applied on raw Kinect v2 joint data, in order to reduce noise due
% to inaccuracies and occlusions.

% � August 2014 Alessandro Timmi
% Latest update: June 2015

clear
clc
close all

% Select the motion data file to be imported:
a = importdata(...
'X:\PhD\Kinect\Validation Kinect2\Mocap Trials\Day 1 (preliminary)\Kinect2 CSV\Alex_StaticTrial_LowerCamera_Wave1.csv',',',6);

% Joint numbers:
SpineBase       =	1;
SpineMid        =	2;
Neck            =	3;
Head            =	4;
ShoulderLeft	=	5;
ElbowLeft       =	6;
WristLeft       =	7;
HandLeft        =	8;
ShoulderRight	=	9;
ElbowRight      =	10;
WristRight      =	11;
HandRight       =	12;
HipLeft         =	13;
KneeLeft        =	14;
AnkleLeft       =	15;
FootLeft        =	16;
HipRight        =	17;
KneeRight       =	18;
AnkleRight      =	19;
FootRight       =	20;
SpineShoulder	=	21;
HandTipLeft     =	22;
ThumbLeft       =	23;
HandTipRight	=	24;
ThumbRight      =	25;

% Joint labels:
joint_names = { 'SpineBase',...
                'SpineMid',...
                'Neck',...
                'Head',...
                'ShoulderLeft',...
                'ElbowLeft',...
                'WristLeft',...
                'HandLeft',...
                'ShoulderRight',...
                'ElbowRight',...
                'WristRight',...
                'HandRight',...
                'HipLeft',...
                'KneeLeft',...
                'AnkleLeft',...
                'FootLeft',...
                'HipRight',...
                'KneeRight',...
                'AnkleRight',...
                'FootRight',...
                'SpineShoulder',...
                'HandTipLeft',...
                'ThumbLeft',...
                'HandTipRight',...
                'ThumbRight'};

%Coordinate labels
coord_labels = {'X','Y','Z'};
 
% Joint to be filtered in this test program (index is 1-based):
joint = KneeRight;

% Coordinate to be filtered (X=1, Y=2, Z=3):
c = 2;

% Raw data for the selected joint coordinate:
raw_data = a.data(:,3*(WristRight-1)+c);


%%  Filters settings
% Median filter (good to remove spikes):
%median_filt_data=medfilt1(raw_data,5);

% Butterworth filter:
% Filter order: a lower order replicates the input with less fidelity?
but_ord = 5;
% desired cutoff frequency [Hz]:
cutoff = 6;
% sample frequency = Kinect 2 recording framerate = 30 fps
sample_fps = 30; 
% Normalized cutoff frequency: it is a number between 0 and 1, where 1
% corresponds to the Nyquist frequency (half of the sampling rate of a
% discrete signal processing system):
Wn = cutoff/(sample_fps/2);
% Designs a lowpass butterworth filter, returning the filter coefficients:
% TODO: use the [z,p,k] syntax instead of [b,a] (check matlab help) 
[b,a] = butter(but_ord, Wn, 'low');
% Filters data with zero phase shift (back and forth,
% does this double filtering affect the resulting cutoff frequency?):
butter_filt_data = filtfilt(b, a, raw_data);

% Savitzky-Golay filter:
% TODO: we can customize the weights over the frame size (see help).
poly_ord = 3; % as suggested in Mircrosoft white paper by M. Azimi
frame_size = 7;
sgolay_filt_data = sgolayfilt(raw_data,poly_ord,frame_size);

% Median + Savitzky-Golay filters:
median_order = 5;
temp_array = medfilt1(raw_data,median_order);
median_sgolay_filt_data = sgolayfilt(temp_array,poly_ord,frame_size);



%% Plot results
lw=1.5; % linewidth

purple = [0.6,0.5,1];
dark_green = [0.2,0.6,0];
orange = [1,0.5,0];
dark_blue = [0,0.2,1];

figure('Name','Filters comparison')
plot(raw_data,'color',[0,0,0],'linewidth',1.5,'linestyle','--')
hold on
%plot(median_filt_data,'b-','linewidth',lw)
plot(butter_filt_data,'color',orange,'linewidth',lw)
%plot(sgolay_filt_data,'g','linewidth',lw)
plot(median_sgolay_filt_data,'color',purple,'linewidth',lw)

legend('Raw joint data',...
    sprintf('BW. ord=%d, cutoff=%d Hz',but_ord, cutoff),...
    sprintf('Median ord=%d + SG ord=%d, f=%d',median_order, poly_ord, frame_size),...
    'location','NorthEast')

my_title = sprintf('%s','Kinect 2 ', joint_names{(joint)},' joint ',...
    coord_labels{(c)},' position - Filters comparison');
title(my_title,'fontweight','bold')

my_y_label = sprintf('%s', joint_names{(joint)},' ',coord_labels{(c)},' [m]');
ylabel(my_y_label);
xlabel('Frames [~1/30 s]')

% Attualmente il miglior compromesso sembra rappresentato dalla
% sovrapposizione di:
% un filtro median con span=5 (per rimuovere gli spike);
% un filtro savitzky-golay di 3 ordine e span=7 (per lo smoothing).



