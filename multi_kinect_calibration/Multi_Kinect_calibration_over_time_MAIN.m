% Calibration of 2 Kinect v2
% �2014 Alessandro Timmi

close all
clear
clc

format long

% RGB
gray = [0.7,0.7,0.7];

% .trc files to be read:
file_master = ['X:\PhD\Kinect\Validation Kinect2\Mocap Trials\CalibrationTrialsAleOffice_17Nov2014\master\K4WBodyData_t0002_master_20141117-04153405.trc'];
file_slave = ['X:\PhD\Kinect\Validation Kinect2\Mocap Trials\CalibrationTrialsAleOffice_17Nov2014\slave\K4WBodyData_t002_slave_20141117-04153086.trc'];

% TODO: use my new function to load the trc file.
% Read the .trc files:
delimiterIn = '\t';
% It looks like empty lines must not be taken into account as part of the header
headerlinesIn = 5;
master_trc = importdata(file_master, delimiterIn, headerlinesIn);
slave_trc = importdata(file_slave, delimiterIn, headerlinesIn);

master_labels = textscan(master_trc.textdata{4,1}, '%s');
slave_labels = textscan(slave_trc.textdata{4,1}, '%s');
% The actual labels are nested into the first cell of the cell array, we
% need to extract them, so we overwrite the cell array:
master_labels = master_labels{1,1};
slave_labels = slave_labels{1,1};

% If at least one of the labels is different, raise an error:
if prod(strcmp(master_labels,slave_labels))==0
    error('Different labels in the two mocap files.')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% JOINTS
% Cell array of joint names:
joints_names = master_labels(3:end);
% Total number of joints in Kinect v2 body algorithm:
joints_num = length(joints_names);
% A useful container that allows to recall the joints using either numbers
% or names.
% NOTICE: values become sorted in alphabetical order according to their keys!
joints_map = containers.Map(joints_names, 1:joints_num);

% Read joint coordinates from imported data (skip frame numbers and timestamps):
master_coords = master_trc.data(:,3:end);
slave_coords = slave_trc.data(:,3:end);

% Initialize structs copying the time array. They will contain all joints data:
master = struct('time',master_trc.data(1:end,2));
slave = struct('time',slave_trc.data(1:end,2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHECK SAMPLING FREQUENCY of the two files:
[diff_m, mean_diff_m, std_dev_diff_m] = check_sampling_freq(master.time);
[diff_s, mean_diff_s, std_dev_diff_s] = check_sampling_freq(slave.time);
% We take 0.0341 as a tolerance value, because usually the interval between
% adjacent frames varies between 0.033 and 0.034 (plus some noise):
lag_m = find(diff_m > 0.0341); 
lag_s = find(diff_s > 0.0341);

figure('name','Sampling intervals (Master and Slave)')
plot(diff_m,'b')
hold on
plot(diff_s,'r')
legend('Master Kinect','Slave Kinect')
xlabel('Frames [#]')
ylabel('Time [s]')
title('Differences between adjacent elements of the time arrays')

fprintf('[MASTER] Mean interval between adjacent elements and std dev: %4.4f (%4.4f)\n',...
    mean_diff_m, std_dev_diff_m)
fprintf('Number of frames with lag: %d out of %d.\n',length(lag_m), length(diff_m));
fprintf('[SLAVE] Mean interval between adjacent elements and std dev: %4.4f (%4.4f)\n',...
    mean_diff_s, std_dev_diff_s)
fprintf('Number of frames with lag: %d out of %d.\n',length(lag_s), length(diff_s));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cell array containg the names of the coordinates. They will be used to
% define the fields in the structs:
coords_labels = {'x','y','z'};

% TEST: with this block of code we set up structs having as fields the name
% of the joints and as subfields the X,Y,Z coords. This is the cleanest
% method but also difficult to manage because the actual values are at the
% second sublevel of the struct. For the moment, we stick with the .trc
% convention, in which the coordinates are numbered.
% for i=1:numel(joint_names)
%     for c=1:3
%         master.(joint_names{i}).(coords{c}) = master_data(:,(i-1)*3+c);
%         slave.(joint_names{i}).(coords{c}) = slave_data(:,(i-1)*3+c);
%     end
% end

% Fill the struct with the joints data. Each coordinate is labelled with a
% unique name:
% X1, Y1, Z1, X2, Y2, Z2,... Xn, Yn, Zn.
for i=1:numel(joints_names)
    for c=1:3
        coord_num = (i-1)*3+c;
        master.([coords_labels{c} num2str(i)]) = master_coords(1:end,coord_num);
        slave.([coords_labels{c} num2str(i)]) = slave_coords(1:end,coord_num);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FILTERING
master = filter_struct(master, 30/2);
slave = filter_struct(slave, 30/2);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTERPOLATION of Kinect data up to Vicon framerate. This is done in order
% to use a finer step when syncing the two mocap files.
kinect_fps = 30;
vicon_fps = 120;

% New time array for Kinect, interpolated at Vicon framerate.
% Kinect start time is always 0.0333 (which is 1/30), while Vicon start
% time is always 0.0083 (which is 1/120). Later we will extrapolate Kinect
% coordinates in the interval between 0 and 0.0333:
time_master_interp = master.time(1) : 1/vicon_fps : master.time(end);
time_slave_interp = slave.time(1) : 1/vicon_fps : slave.time(end);

% Interpolate each field of Kinect's struct at Vicon's framerate,
% using splines. Values before Kinect's first frame are extrapolated.
master_interp = structfun(@(y)...
    ( interp1(master.time, y, time_master_interp,'spline','extrap')' ),...
    master, 'UniformOutput', false);
slave_interp = structfun(@(y)...
    ( interp1(slave.time, y, time_slave_interp,'spline','extrap')' ),...
    slave, 'UniformOutput', false);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SYNC joints data:
[master_sync, slave_sync,shift] =...
    SyncStructs(master_interp,slave_interp,'y11');

% Plot raw and synced data:
figure('Name','Raw and synced data')
plot(master.time, master.y11,'linestyle','--','Color','k','linewidth',2)
hold on
plot(slave.time, slave.y11,'linestyle',':','Color','m','linewidth',2)
plot(master_sync.time, master_sync.y11,'linestyle','-','Color','b','linewidth',2)
plot(slave_sync.time, slave_sync.y11,'linestyle','-','Color','g','linewidth',2)
legend('Master (raw)','Slave (raw)','Master (sync)','Slave (sync)')
xlabel('Time [s]')
ylabel('Joint position [m]')
title('Raw and synced data: Y11')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Choose the FRAMES TO BE USED FOR CALIBRATION. We can choose multiple
% pairs of frames for calibration, but with these requirements:
% - frames in each pair must be synced
% - all frames must refer to the exactly same Kinect sensors setup.
cal_frames_indices = [420:2700]; % they can be more than one, comma separated
% Number of frames requested for calibration:
cal_frames_indices_lenght = length(cal_frames_indices);
fprintf('Number of frames used for calibration: %d.\n',cal_frames_indices_lenght)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Number of frames from the Slave file (the one that must be transformed):
frames_slave = size(slave_coords,1);

% Joints data used for calibration:
master_cal_rows = [master_sync.x11(cal_frames_indices),...
                    master_sync.y11(cal_frames_indices),...
                    master_sync.z11(cal_frames_indices)];
slave_cal_rows = [slave_sync.x11(cal_frames_indices),...
                    slave_sync.y11(cal_frames_indices),...
                    slave_sync.z11(cal_frames_indices)];

% Rehape calibration joints arrays in [3*?] format:
% [X1,X2,...,Xn;
%  Y1,Y2,...,Yn;
%  Z1,Z2,...,Zn;];
% TODO make it more robust calculating the number of markers.
% NOTICE: we transpose the rows to be reshaped because
% reshape works column-wise 
master_cal = reshape(master_cal_rows',3,[]);
slave_cal = reshape(slave_cal_rows',3,[]);

% Total number of joints imported for calibration:
replicated_joints_num = size(slave_cal,2);
fprintf('Number of joints used for calibration: %d.\n',replicated_joints_num)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run GUPTA-CHUTAKANONTA algorithm
% TODO: check if we need the econ or std version of SVD.
D = gupta_chuta_shiflett_laub_svd_std_Dmatrix(master_cal, slave_cal);

% Transform the coordinates of Slave into Master coordinate system:
slave_cal_transf = D * [slave_cal;    
                        ones(1,size(slave_cal,2))];

% Delete the last row (4th) of the matrix containing the transformed joints,
% which is made of ones. The resulting matrix is [3 x jointsNum]:
slave_cal_transf(4,:) = [];

figure
plot3(master_cal(1,:),master_cal(2,:),master_cal(3,:),'ko')
hold on
%plot3(slave_cal(1,:),slave_cal(2,:),slave_cal(3,:),'bo')
plot3(slave_cal_transf(1,:),slave_cal_transf(2,:),slave_cal_transf(3,:),'go')
legend('Master','Slave (transformed)')

figure
plot(master_cal(1,:))
hold on
plot(slave_cal(1,:),'r')

% Evaluate calibration results in terms of distances between corresponding
% joints from KMaster and KSlave_transformed:
[rms_dist,max_dist,max_index,dist_array] = calibration_error(master_cal,slave_cal_transf);
% Print summary results:
fprintf('RMS = %0.4f m\tMax error = %0.4f m on joint %s\n',...
    rms_dist, max_dist, replicated_jointsNames{max_index});
% Print specific results for each joint:
for j=1:replicated_joints_num
    fprintf('%s\t%0.4f', replicated_jointsNames{j}, dist_array(j))
    if any(replicated_ignoredJointsValues==j)
        fprintf(' (ignored for calibration)')
    end
    fprintf('\n')
end
fprintf('\n\n')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot KMaster and KSlave ORIGINAL coordinates:
figure('Name','Original data')
a = gca;
set(a,'DataAspectRatio',[1,1,1])
hold on
xlabel('X [m]');
ylabel('Y [m]');
zlabel('Z [m]');

% plot joints:
plot3(a,master_cal(1,:),master_cal(2,:),master_cal(3,:),'r.')
plot3(a,slave_cal(1,:),slave_cal(2,:),slave_cal(3,:),'bo','markersize',5,'linewidth',1)
legend('Kinect Master','Kinect Slave','location','southeast')
legend('boxon')

% plot coordinate system
xaxis=[0,0,0;0.2,0,0]';
yaxis=[0,0,0;0,0.2,0]';
zaxis=[0,0,0;0,0,0.2]';
plot3(a,xaxis(1,:),xaxis(2,:),xaxis(3,:),'linewidth',4,'color','r');
plot3(a,yaxis(1,:),yaxis(2,:),yaxis(3,:),'linewidth',4,'color','g');
plot3(a,zaxis(1,:),zaxis(2,:),zaxis(3,:),'linewidth',4,'color','b');
plot3(0,0,0,'ro')

% Set the camera:
view([0.9,0.1,0.1])
camup(a,[0,1,0])
%camroll(90)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot KMaster and transformed KSlave coordinates:
figure('Name','Transformed data')
a=gca;
set(a,'DataAspectRatio',[1,1,1])
hold on
xlabel('X [m]');
ylabel('Y [m]');
zlabel('Z [m]');

% plot joints:
plot3(a,master_cal(1,:),master_cal(2,:),master_cal(3,:),'r.')
plot3(a,slave_cal_transf(1,:),slave_cal_transf(2,:),slave_cal_transf(3,:),'bo','markersize',5,'linewidth',1)
legend('Kinect Master (reference)','Kinect Slave (transformed)','location','southeast')
legend('boxon')

% plot coordinate system
xaxis=[0,0,0;0.2,0,0]';
yaxis=[0,0,0;0,0.2,0]';
zaxis=[0,0,0;0,0,0.2]';
plot3(a,xaxis(1,:),xaxis(2,:),xaxis(3,:),'linewidth',4,'color','r');
plot3(a,yaxis(1,:),yaxis(2,:),yaxis(3,:),'linewidth',4,'color','g');
plot3(a,zaxis(1,:),zaxis(2,:),zaxis(3,:),'linewidth',4,'color','b');
plot3(0,0,0,'ro')

% Set the camera:
view([0.9,0.1,0.1])
camup(a,[0,1,0])
%camroll(90)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Validation of the algorithm error using a predetermined transformation
%calibration_validation(replicated_jointsNum,replicated_jointsNames,replicated_ignoredJointsValues,KSlave)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now we apply the calculated transformation to the actual Kinect Slave data:
%file1 = 'X:\PhD\ACL injury puberty and footwear\Trials12Sept2014\Calibration test\K4WBodyData_20140912-10493307.trc';
%file2 = 'X:\PhD\ACL injury puberty and footwear\Trials12Sept2014\Calibration test\Karine vertical jump 1.trc';

% Preallocate the array for the transformed joints coordinates:
slave_data_transf = zeros(frames_slave,3*jointsNum);
for i=1:frames_slave
    slave_row = slave_coords(i,:);
    slave_matrix = reshape(slave_row,[3,jointsNum]);
    % Calculate the coordinate of Kinect Slave in Kinect Master system:
    slave_matrix_transf = D * [slave_matrix; ones(1,jointsNum)];
    
    % Delete the last row (4th) of the matrix containing the transformed joints,
    % which is made of ones. The resulting matrix is [3 x jointsNum]:
    slave_matrix_transf(4,:) = [];
    slave_row_transf = reshape(slave_matrix_transf,[1,3*jointsNum]);
    slave_data_transf(i,:) = slave_row_transf;
end


slave_trc_transf_noHeader = [slave_trc.data(:,1:2),slave_data_transf(:,:)];

% Write the transformed .trc file
[path_slave,name_slave,ext_slave] = fileparts(file_slave);
file_slave_transf = [path_slave,filesep,name_slave,'_transformed',ext_slave];
file_slave_open_id = fopen(file_slave_transf,'w');
% Write the header copying the old one and editing only the path:
fprintf(file_slave_open_id,'%s\t%d\t%s\t%s\n','PathFileType',4,'(X/Y/Z)',file_slave_transf);
fprintf(file_slave_open_id,'%s\n',slave_trc.textdata{2,1});
fprintf(file_slave_open_id,'%s\n',slave_trc.textdata{3,1});
fprintf(file_slave_open_id,'%s\n',slave_trc.textdata{4,1});
fprintf(file_slave_open_id,'%s\n\n',slave_trc.textdata{5,1});
fclose(file_slave_open_id);

% Append the transformed coordinates below the header of the transformed .trc file:
dlmwrite(file_slave_transf,slave_trc_transf_noHeader,'-append','delimiter','\t');
