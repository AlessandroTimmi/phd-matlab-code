% Calibration of 2 Kinect v2 using points from rigid bodies in the depth
% map selected by the user.
% �2014 Alessandro Timmi

close all
clear all
clc

format long

% .trc files to be read
file_master = 'X:\PhD\Kinect\Validation Kinect2\Mocap Trials\Box Calibration\Calibration points_master4.csv';
file_slave = 'X:\PhD\Kinect\Validation Kinect2\Mocap Trials\Box Calibration\Calibration points_slave4.csv';

% Read the .trc files:
delimiterIn = '\t';
% It looks like empty lines must not be taken into account as part of the header
headerlinesIn = 1;
master_csv = importdata(file_master,delimiterIn,headerlinesIn);
slave_csv = importdata(file_slave,delimiterIn,headerlinesIn);

% Read joint coordinates from imported data:
master_data = master_csv.data(:,:)';
slave_data = slave_csv.data(:,:)';

% Number of frames from the Slave file (the one that must be transformed):
num_points_master = size(master_data,2);
num_points_slave = size(slave_data,2);

if num_points_master~=num_points_slave
    error('Error: different number of calibration points between devices.');
end

% Run Gupta-Chutakanonta algorithm
% TODO: check if we need the econ or std version of SVD.
D = gupta_chuta_shiflett_laub_svd_std_Dmatrix(master_data,slave_data);

% Now that we know the calibration matrix, we stop using the reduced set
% of joints and now we use the full one.
% Calculate the coordinates of KSlave in KMaster system:
slave_data_transf = D * [slave_data;    
                    ones(1,size(slave_data,2))];

% Delete the last row (4th) of the matrix containing the transformed joints,
% which is made of ones. The resulting matrix is [3 x jointsNum]:
slave_data_transf(4,:) = [];

% Evaluate calibration results in terms of distances between corresponding
% joints from KMaster and KSlave_transformed:
[rms_dist,max_dist,max_index,dist_array] = calibration_error(master_data,slave_data_transf);
% Print summary results:
fprintf('RMS = %0.4f m\tMax error = %0.4f m on point %d\n',...
    rms_dist, max_dist, max_index);
% Print specific results for each joint:
for j=1:num_points_master
    fprintf('point %d\t%0.4f', j, dist_array(j))
    fprintf('\n')
end
fprintf('\n\n')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot master and slave ORIGINAL coordinates:
figure('Name','Original data')
a = gca;
set(a,'DataAspectRatio',[1,1,1])
hold on
xlabel('X [m]');
ylabel('Y [m]');
zlabel('Z [m]');

% plot joints:
plot3(a,master_data(1,:),master_data(2,:),master_data(3,:),'r.')
plot3(a,slave_data(1,:),slave_data(2,:),slave_data(3,:),'bo','markersize',5,'linewidth',1)
legend('Kinect Master','Kinect Slave','location','southeast')
legend('boxon')

% plot coordinate system
xaxis=[0,0,0;0.2,0,0]';
yaxis=[0,0,0;0,0.2,0]';
zaxis=[0,0,0;0,0,0.2]';
plot3(a,xaxis(1,:),xaxis(2,:),xaxis(3,:),'linewidth',4,'color','r');
plot3(a,yaxis(1,:),yaxis(2,:),yaxis(3,:),'linewidth',4,'color','g');
plot3(a,zaxis(1,:),zaxis(2,:),zaxis(3,:),'linewidth',4,'color','b');
plot3(0,0,0,'kd')

% Set the camera:
view([0.9,0.1,0.1])
camup(a,[0,1,0])
%camroll(90)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot KMaster and transformed KSlave coordinates:
figure('Name','Transformed data')
a=gca;
set(a,'DataAspectRatio',[1,1,1])
hold on
xlabel('X [m]');
ylabel('Y [m]');
zlabel('Z [m]');

% plot joints:
plot3(a,master_data(1,:),master_data(2,:),master_data(3,:),'r.')
plot3(a,slave_data_transf(1,:),slave_data_transf(2,:),slave_data_transf(3,:),'bo','markersize',5,'linewidth',1)
legend('Kinect Master (reference)','Kinect Slave (transformed)','location','southeast')
legend('boxon')

% plot coordinate system
xaxis=[0,0,0;0.2,0,0]';
yaxis=[0,0,0;0,0.2,0]';
zaxis=[0,0,0;0,0,0.2]';
plot3(a,xaxis(1,:),xaxis(2,:),xaxis(3,:),'linewidth',4,'color','r');
plot3(a,yaxis(1,:),yaxis(2,:),yaxis(3,:),'linewidth',4,'color','g');
plot3(a,zaxis(1,:),zaxis(2,:),zaxis(3,:),'linewidth',4,'color','b');
plot3(0,0,0,'ro')

% Set the camera:
view([0.9,0.1,0.1])
camup(a,[0,1,0])
%camroll(90)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Validation of the algorithm error using a predetermined transformation
%calibration_validation(replicated_jointsNum,replicated_jointsNames,replicated_ignoredJointsValues,KSlave)



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Now we apply the calculated transformation to the actual Kinect Slave data:
% %file1 = 'X:\PhD\ACL injury puberty and footwear\Trials12Sept2014\Calibration test\K4WBodyData_20140912-10493307.trc';
% %file2 = 'X:\PhD\ACL injury puberty and footwear\Trials12Sept2014\Calibration test\Karine vertical jump 1.trc';
% 
% % Preallocate the array for the transformed joints coordinates:
% slave_data_transf = zeros(num_points_slave,3*jointsNum);
% for i=1:num_points_slave
%     slave_row = slave_data(i,:);
%     slave_matrix = reshape(slave_row,[3,jointsNum]);
%     % Calculate the coordinate of Kinect Slave in Kinect Master system:
%     slave_matrix_transf = D * [slave_matrix; ones(1,jointsNum)];
%     
%     % Delete the last row (4th) of the matrix containing the transformed joints,
%     % which is made of ones. The resulting matrix is [3 x jointsNum]:
%     slave_matrix_transf(4,:) = [];
%     slave_row_transf = reshape(slave_matrix_transf,[1,3*jointsNum]);
%     slave_data_transf(i,:) = slave_row_transf;
% end
% 
% 
% slave_trc_transf_noHeader = [slave_csv.data(:,1:2),slave_data_transf(:,:)];
% 
% % Write the transformed .trc file
% [path_slave,name_slave,ext_slave] = fileparts(file_slave);
% file_slave_transf = [path_slave,filesep,name_slave,'_transformed',ext_slave];
% file_slave_open_id = fopen(file_slave_transf,'w');
% % Write the header copying the old one and editing only the path:
% fprintf(file_slave_open_id,'%s\t%d\t%s\t%s\n','PathFileType',4,'(X/Y/Z)',file_slave_transf);
% fprintf(file_slave_open_id,'%s\n',slave_csv.textdata{2,1});
% fprintf(file_slave_open_id,'%s\n',slave_csv.textdata{3,1});
% fprintf(file_slave_open_id,'%s\n',slave_csv.textdata{4,1});
% fprintf(file_slave_open_id,'%s\n\n',slave_csv.textdata{5,1});
% fclose(file_slave_open_id);
% 
% % Append the transformed coordinates below the header of the transformed .trc file:
% dlmwrite(file_slave_transf,slave_trc_transf_noHeader,'-append','delimiter','\t');
