%GUPTA_CHUTA_SHIFLETT_LAUB_SVD_ECON    Calcola la matrice di rotazione a
%   partire dalle coordinate assolute e locali di un minimo di 3 marker
%   individuanti il corpo rigido.
%
%   La procedura (ideata da Gupta e Chutakanonta) scarta gli inevitabili
%   errori dovuti alla non perfetta rigidit� del corpo stesso e ad
%   imprecisioni connesse al sistema di acquisizione
%   (vedi libro CDC pag. 37).
%
%   L'errore � distribuito uniformemente su tutta la matrice di rotazione,
%   applicando la procedura alternativa proposta dagli stessi Gupta e
%   Chutakanonta che permette di avere una matrice di errore simmetrica.
%
%   QUESTO ALGORITMO CONTIENE UNA INCONGRUENZA: il metodo SVD/QS creato da
%   Gupta e Chutakanonta e qui implementato � pensato per un numero
%   ridondante di marker (n>=4). Tuttavia lo scheletro di Virtual Sensei
%   utilizza per tutti gli arti 3 marker (tranne per Hip che ne ha 4). In
%   questo caso la decomposizione SVD 'econ' di Matlab
%   restituisce una matrice V' rettangolare e quindi non pi� invertibile.
%   Questo fatto rende indimostrabile l'algoritmo di G&C. Tuttavia si fa notare
%   che i risultati ottenuti sia graficamente (animazione manichino) sia
%   numericamente (energia cinetica rotazionale totale del corpo umano) non
%   risentono minimamente di questo fatto. I valori ottenuti sono gli
%   stessi rispetto al caso di utilizzo della SVD standard.
%
%   L'eventualit� del det(A)=-1 � scongiurata applicando
%   l'accorgimento proposto da Shiflett e Laub (vedi libro CDC pag. 36)
%   alla matrice Ur ottenuta dalla decomposizione QR.
%
%   Input:
%   XYZ =   array le cui colonne costituiscono i vettori delle coordinate
%           assolute dei marker;
%   xyz =   array le cui colonne costituiscono i vettori delle coordinate
%           locali degli stessi marker, espresse nel riferimento del quale
%           si vuole calcolare la matrice di rotazione rispetto a quello
%           assoluto.
%
%   Output:
%   A   =   matrice [3x3] di rotazione del corpo rigido con errore
%           distribuito uniformemente su tutti gli elementi.
%
%
% Written by Alessandro Timmi


function A = gupta_chuta_shiflett_laub_svd_econ(XYZ,xyz)

% Valore di tolleranza: un numero n, tale che abs(n)<eps, viene
% approssimato a zero:
eps=1e-007;

% Numero di marker costituenti il corpo rigido in esame = n� colonne XYZ:
nMarker=size(XYZ,2);

% Se il corpo rigido � individuato da meno di 3 marker non � possibile
% determinarne l'orientamento:
if nMarker<3
    errordlg('Errore: corpo rigido costituito da meno di 3 marker','Rigid Body Error');
    return
end

% Costruisce le due matrici [4 x nMarker] necessarie; la loro ultima riga
% � costituita da 1:
P=[XYZ;ones(1,nMarker)];
p=[xyz;ones(1,nMarker)];

% Applica la decomposizione SVD economica alla matrice p trasposta: 
% essa permette di ottenere, nel caso di nMarker=3, la matrice S
% diagonale quadrata di dimensioni nMarker x nMarker (invece di 3x4),
% semplificando quindi la successiva operazione di inversione: non
% occorre cio� pi� eliminare l'ultima colonna di zeri.
% Nel caso di nMarker=4, SVD=SVD econ.
[U,S,V]=svd(p','econ');
% Le matrici ottenute dalla thin SVD sono
% p'[3x4]=U_eco(3x3) S_eco(3x3) V_eco'(3x4)
% p'[4x4]=U_eco(4x4) S_eco(4x4) V_eco'(4x4)

% La matrice S potrebbe essere singolare, pertanto, dovendo invertirla,
% occorre scrivere una pseudo-inversa positiva (Sp) che abbia come
% elementi della diagonale:
% -     1/S(j,j)    se S(j,j) � diverso da zero
% -     0           se S(j,j) � circa uguale a zero.
Sp=zeros(nMarker,nMarker);
for j=1:nMarker
    if abs(S(j,j))<eps     % considera S(j,j)=0
        Sp(j,j)=0;
    else                    % S(j,j) diverso da zero
        Sp(j,j)=1/S(j,j);
    end
end

% Calcola la matrice di spostamento ("displacement") D [4x4] incognita:
D=P*U*Sp*V';

% Il minore M [3x3], costituito dalle prime 3 righe e 3 colonne di D,
% rappresenta una prima stima della matrice di rotazione:
M=D(1:3,1:3);

% Metodo alternativo per la distribuzione uniforme dell'errore sulla
% matrice di rotazione, proposto dagli stessi Gupta e Chutakanonta
% (nel caso di matrice quadrata, svd economica=svd standard):
[Ur,~,Vr]=svd(M);

% Modifica sulla Ur proposta da Shiflett e Laub per ottenere il det=1:
% Calcola la matrice di rotazione provvisoria:
A=Ur*Vr';
% ed il suo determinante:
detA=det(A);
% Se esso risulta diverso da 1 (con tolleranza eps), applica
% l'accorgimento proposto da Shiflett e Laub:
if detA<=1-eps || detA>=1+eps
    % Definisce Urp=[u1 u2 +u3]:
    Urp=Ur;
    % Definisce Urm=[u1 u2 -u3]:
    Urm=[Ur(:,1:2),-Ur(:,3)];
    % Calcola le due matrici di rotazione:
    Ap=Urp*Vr';
    Am=Urm*Vr';
    % Verifico se una delle nuove matrici ha det=1 (con tolleranza eps):
    % solo in questo caso una delle due diventa quella definitiva,
    % altrimenti viene mantenuta quella originale:
    detAp=det(Ap);
    detAm=det(Am);
    if detAp>=1-eps && detAp<=1+eps
        A=Ap;
    elseif detAm>=1-eps && detAm<=1+eps
        A=Am;
    end
end
% mostra il determinante definitivo:
%disp(det(A));
