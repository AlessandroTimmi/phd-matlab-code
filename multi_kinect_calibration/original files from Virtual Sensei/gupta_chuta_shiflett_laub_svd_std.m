%GUPTA_CHUTA_SHIFLETT_LAUB_SVD_STD    Calcola la matrice di rotazione a
%   partire dalle coordinate assolute e locali di un minimo di 3 marker
%   individuanti il corpo rigido.
%
%   La procedura (ideata da Gupta e Chutakanonta) scarta gli inevitabili
%   errori dovuti alla non perfetta rigidit� del corpo stesso e ad
%   imprecisioni connesse al sistema di acquisizione
%   (vedi libro CDC pag. 37).
%
%   L'errore � distribuito uniformemente su tutta la matrice di rotazione,
%   applicando la procedura alternativa proposta dagli stessi Gupta e
%   Chutakanonta che permette di avere una matrice di errore simmetrica.
%
%   A differenza dell'algoritmo ideato da Gupta e Chutakanonta, il quale
%   utilizza la thin SVD ('econ' in Matlab), qui viene usata la SVD
%   standard. Il motivo � connesso al fatto che questo algoritmo nasce per
%   dati ridondanti, quindi per un numero di marker n>=4. Poich� in virtual
%   Sensei molti corpi rigidi hanno 3 soli marker, la matrice V perderebbe
%   la sua ortogonalit� usando la thin SVD. L'aspetto negativo di questa
%   modifica � che la matrice S � rettangolare per n=3 e quindi nel
%   processo di inversione occorre creare S+ con le stesse dimensioni di S
%   (avr� quindi l'ultima colonna di zeri nel caso di 3 marker) per
%   permettere di moltiplicare le 3 matrici U, Sp, V' invertite.
%
%   L'eventualit� del det(A)=-1 � scongiurata applicando
%   l'accorgimento proposto da Shiflett e Laub (vedi libro CDC pag. 36)
%   alla matrice Ur ottenuta dalla decomposizione QR.
%
%   Input:
%   XYZ =   array le cui colonne costituiscono i vettori delle coordinate
%           assolute dei marker;
%   xyz =   array le cui colonne costituiscono i vettori delle coordinate
%           locali degli stessi marker, espresse nel riferimento del quale
%           si vuole calcolare la matrice di rotazione rispetto a quello
%           assoluto.
%   
%   Output:
%   A   =   matrice [3x3] di rotazione del corpo rigido con errore
%           distribuito uniformemente su tutti gli elementi.
%
%
% Written by Alessandro Timmi


function A = gupta_chuta_shiflett_laub_svd_std(XYZ,xyz)

% Valore di tolleranza: un numero n, tale che abs(n)<eps, viene
% approssimato a zero:
eps=1e-007;

% Numero di marker costituenti il corpo rigido in esame = n� colonne XYZ:
nMarker=size(XYZ,2);

% Se il corpo rigido � individuato da meno di 3 marker non � possibile
% determinarne l'orientamento:
if nMarker<3
    errordlg('Errore: corpo rigido costituito da meno di 3 marker','Rigid Body Error');
    return
end

% Costruisce le due matrici [4 x nMarker] necessarie; la loro ultima riga
% � costituita da 1:
P=[XYZ;ones(1,nMarker)];
p=[xyz;ones(1,nMarker)];

% Applica la decomposizione SVD alla matrice p trasposta:
[U,S,V]=svd(p');
% Le matrici ottenute dalla SVD standard sono:
% p'[3x4]=U(3x3) S(3x4) V'(4x4)
% p'[4x4]=U(4x4) S(4x4) V'(4x4)


% S � una matrice diagonale nx4, quindi quadrata nel caso di n=4 marker.
% tuttavia nel caso di n=3, l'ultima colonna � costituita da zeri, mentre
% nel caso di n=5 (non capita nel nostro scheletro) � l'ultima riga ad
% essera costituita da zeri.
% Pertanto, nel ciclo for necessario a costruire la pseudo-inversa Sp di S,
% viene calcolata la dimensione minima di S e questo valore viene usato
% come numero di iterazioni del ciclo for stesso. Le dimensioni della
% matrice Sp vengono invece assunte uguali a quelle della matrice S.
% IN QUESTO MODO QUESTA FUNZIONE � RESA COMPATIBILE CON QUALUNQUE NUMERO
% DI MARKER COSTITUENTI UN CORPO RIGIDO (modifica al metodo di Gupta e
% Chutakanonta effettuata da ALE)

% La matrice S potrebbe essere singolare, pertanto, dovendo invertirla,
% occorre scrivere una pseudo-inversa positiva (Sp) che abbia come
% elementi della diagonale:
% -     1/S(j,j)    se S(j,j) � diverso da zero
% -     0           se S(j,j) � circa uguale a zero.
dimS=size(S);
Sp=zeros(dimS);
for j=1:min(dimS)
    if abs(S(j,j))<eps     % considera S(j,j)=0
        Sp(j,j)=0;
    else                    % S(j,j) diverso da zero
        Sp(j,j)=1/S(j,j);
    end
end


% Calcola la matrice D [4x4] incognita:
D=P*U*Sp*V';

% Il minore M [3x3], costituito dalle prime 3 righe e 3 colonne di D,
% rappresenta una prima stima della matrice di rotazione:
M=D(1:3,1:3);

% Metodo alternativo per la distribuzione uniforme dell'errore sulla
% matrice di rotazione, proposto dagli stessi Gupta e Chutakanonta:
[Ur,~,Vr]=svd(M);

% Modifica sulla Ur proposta da Shiflett e Laub per ottenere il det=1:
% Calcola la matrice di rotazione provvisoria:
A=Ur*Vr';
% ed il suo determinante:
detA=det(A);
% Se esso risulta diverso da 1 (con tolleranza eps), applica
% l'accorgimento proposto da Shiflett e Laub:
if detA<=1-eps || detA>=1+eps
    % Definisce Urp=[u1 u2 +u3]:
    Urp=Ur;
    % Definisce Urm=[u1 u2 -u3]:
    Urm=[Ur(:,1:2),-Ur(:,3)];
    % Calcola le due matrici di rotazione:
    Ap=Urp*Vr';
    Am=Urm*Vr';
    % Verifico se una delle nuove matrici ha det=1 (con tolleranza eps):
    % solo in questo caso una delle due diventa quella definitiva,
    % altrimenti viene mantenuta quella originale:
    detAp=det(Ap);
    detAm=det(Am);
    if detAp>=1-eps && detAp<=1+eps
        A=Ap;
    elseif detAm>=1-eps && detAm<=1+eps
        A=Am;
    end
end
% mostra il determinante definitivo:
%disp(det(A));
