% Calibration of 2 Kinect v2
% �2014 Alessandro Timmi

close all
clear all
clc

format long

% .trc files to be read:
file_master = ['X:\PhD\Kinect\Validation Kinect2\'...
    'Mocap Trials\CalibrationTrialsAleOffice_03Oct2014\master\'...
    'K4WBodyData_AleTrial11_master_jointFlexed_20141007-01380131.trc'];
file_slave = ['X:\PhD\Kinect\Validation Kinect2\'...
    'Mocap Trials\CalibrationTrialsAleOffice_03Oct2014\slave\'...
    'K4WBodyData_AleTrial11_slave_jointsFlexed_20141007-01375902.trc'];

% Choose the frames to be used for calibration. We can choose multiple
% pairs of frames for calibration, but with these requirements:
% - frames in each pair must be synced
% - all frames must refer to the exactly same Kinect sensors setup.
cal_frames_master_nums = [403,525]; % they can be more than one, comma separated
cal_frames_slave_nums = cal_frames_master_nums;
% Number of frames requested for calibration:
cal_frames_num = size(cal_frames_slave_nums,2);
fprintf('Number of frames used for calibration: %d.\n',cal_frames_num)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read the .trc files:
delimiterIn = '\t';
% It looks like empty lines must not be taken into account as part of the header
headerlinesIn = 5;
master_trc = importdata(file_master,delimiterIn,headerlinesIn);
slave_trc = importdata(file_slave,delimiterIn,headerlinesIn);

% Read joint coordinates from imported data (skip frame numbers and timestamps):
master_data = master_trc.data(:,3:end);
slave_data = slave_trc.data(:,3:end);

% Number of frames from the Slave file (the one that must be transformed):
frames_slave = size(slave_data,1);

% Joints data used for calibration:
master_cal_rows = master_data(cal_frames_master_nums,:);
slave_cal_rows = slave_data(cal_frames_master_nums,:);
%[KMaster_row,KSlave_row,file_master,file_slave] = calibration_frames_container();

if size(master_cal_rows)~=size(slave_cal_rows)
    error('Error: imported calibration data size mismatch!');
end

% Rehape calibration joints arrays in [3*?] format:
% [X1,X2,...,Xn;
%  Y1,Y2,...,Yn;
%  Z1,Z2,...,Zn;];
% TODO rendila piu robusta calcolando il numero di marker.
% NOTICE: we transpose the rows to be reshaped because
% reshape works column-wise 
master_cal = reshape(master_cal_rows',3,[]);
slave_cal = reshape(slave_cal_rows',3,[]);

% Total number of joints imported for calibration:
replicated_jointsNum = size(slave_cal,2);
fprintf('Number of joints used for calibration: %d.\n',replicated_jointsNum)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Joints list in Kinect v2 body algorithm:
jointsNames = {'SpineBase','SpineMid','Neck','Head',...
    'ShoulderLeft','ElbowLeft','WristLeft','HandLeft',...
    'ShoulderRight','ElbowRight','WristRight','HandRight',...
    'HipLeft','KneeLeft','AnkleLeft','FootLeft',...
    'HipRight','KneeRight','AnkleRight','FootRight',...
    'SpineShoulder','HandTipLeft','ThumbLeft','HandTipRight','ThumbRight'};

% Total number of joints in Kinect v2 body algorithm:
jointsNum = length(jointsNames);

% A useful container that allows to recall the joints using either numbers
% or names.
% NOTICE: values become sorted in alphabetical order according to their keys!
jointsMap = containers.Map(jointsNames,1:jointsNum);

% Joints to be ignored for calibration (cell array of strings):
ignoredJointsNames = {'Head'};
% ignoredJointsNames = {'Head',...
%     'WristLeft','HandLeft',...
%     'WristRight','HandRight',...
%     'HandTipLeft','ThumbLeft','HandTipRight','ThumbRight'};
    
     
% A numeric array with numbers corresponding to ignored joints:
ignoredJointsValues = cell2mat(values(jointsMap,ignoredJointsNames));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copy the joint arrays in those to be used only for calibration:
% some bad joints can be skipped for calibration.
master_cal_reduced = master_cal;
slave_cal_reduced = slave_cal;

% Replicate (and adjust) the ignoredJointsValues array as many times as
% the number of frames we imported for calibration:
replicated_ignoredJointsValues = [];
replicated_jointsNames = {};
for i=1:cal_frames_num
    % Concatenate the ignored joints, obtaining their final positions in the coordinate arrays:
    replicated_ignoredJointsValues = cat(2,replicated_ignoredJointsValues,(i-1)*25 + ignoredJointsValues);
    replicated_jointsNames = cat(2,replicated_jointsNames,jointsNames);
end

% For calibration only, delete the columns corresponding to ignored joints:
master_cal_reduced(:,replicated_ignoredJointsValues(:)) = [];
slave_cal_reduced(:,replicated_ignoredJointsValues(:)) = [];

% % Number of joints used for calibration:
% jointsNumCal = size(master_cal_reduced,2)

% Run Gupta-Chutakanonta algorithm
% TODO: check if we need the econ or std version of SVD.
D = gupta_chuta_shiflett_laub_svd_std_Dmatrix(master_cal_reduced,slave_cal_reduced);

% Now that we know the calibration matrix, we stop using the reduced set
% of joints and now we use the full one.
% Calculate the coordinates of KSlave in KMaster system:
slave_cal_transf = D * [slave_cal;    
                    ones(1,size(slave_cal,2))];

% Delete the last row (4th) of the matrix containing the transformed joints,
% which is made of ones. The resulting matrix is [3 x jointsNum]:
slave_cal_transf(4,:) = [];

% Evaluate calibration results in terms of distances between corresponding
% joints from KMaster and KSlave_transformed:
[rms_dist,max_dist,max_index,dist_array] = calibration_error(master_cal,slave_cal_transf);
% Print summary results:
fprintf('RMS = %0.4f m\tMax error = %0.4f m on joint %s\n',...
    rms_dist, max_dist, replicated_jointsNames{max_index});
% Print specific results for each joint:
for j=1:replicated_jointsNum
    fprintf('%s\t%0.4f', replicated_jointsNames{j}, dist_array(j))
    if any(replicated_ignoredJointsValues==j)
        fprintf(' (ignored for calibration)')
    end
    fprintf('\n')
end
fprintf('\n\n')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot KMaster and KSlave ORIGINAL coordinates:
figure('Name','Original data')
a = gca;
set(a,'DataAspectRatio',[1,1,1])
hold on
xlabel('X [m]');
ylabel('Y [m]');
zlabel('Z [m]');

% plot joints:
plot3(a,master_cal(1,:),master_cal(2,:),master_cal(3,:),'r.')
plot3(a,slave_cal(1,:),slave_cal(2,:),slave_cal(3,:),'bo','markersize',5,'linewidth',1)
legend('Kinect Master','Kinect Slave','location','southeast')
legend('boxon')

% plot coordinate system
xaxis=[0,0,0;0.2,0,0]';
yaxis=[0,0,0;0,0.2,0]';
zaxis=[0,0,0;0,0,0.2]';
plot3(a,xaxis(1,:),xaxis(2,:),xaxis(3,:),'linewidth',4,'color','r');
plot3(a,yaxis(1,:),yaxis(2,:),yaxis(3,:),'linewidth',4,'color','g');
plot3(a,zaxis(1,:),zaxis(2,:),zaxis(3,:),'linewidth',4,'color','b');
plot3(0,0,0,'ro')

% Set the camera:
view([0.9,0.1,0.1])
camup(a,[0,1,0])
%camroll(90)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot KMaster and transformed KSlave coordinates:
figure('Name','Transformed data')
a=gca;
set(a,'DataAspectRatio',[1,1,1])
hold on
xlabel('X [m]');
ylabel('Y [m]');
zlabel('Z [m]');

% plot joints:
plot3(a,master_cal(1,:),master_cal(2,:),master_cal(3,:),'r.')
plot3(a,slave_cal_transf(1,:),slave_cal_transf(2,:),slave_cal_transf(3,:),'bo','markersize',5,'linewidth',1)
legend('Kinect Master (reference)','Kinect Slave (transformed)','location','southeast')
legend('boxon')

% plot coordinate system
xaxis=[0,0,0;0.2,0,0]';
yaxis=[0,0,0;0,0.2,0]';
zaxis=[0,0,0;0,0,0.2]';
plot3(a,xaxis(1,:),xaxis(2,:),xaxis(3,:),'linewidth',4,'color','r');
plot3(a,yaxis(1,:),yaxis(2,:),yaxis(3,:),'linewidth',4,'color','g');
plot3(a,zaxis(1,:),zaxis(2,:),zaxis(3,:),'linewidth',4,'color','b');
plot3(0,0,0,'ro')

% Set the camera:
view([0.9,0.1,0.1])
camup(a,[0,1,0])
%camroll(90)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Validation of the algorithm error using a predetermined transformation
%calibration_validation(replicated_jointsNum,replicated_jointsNames,replicated_ignoredJointsValues,KSlave)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now we apply the calculated transformation to the actual Kinect Slave data:
%file1 = 'X:\PhD\ACL injury puberty and footwear\Trials12Sept2014\Calibration test\K4WBodyData_20140912-10493307.trc';
%file2 = 'X:\PhD\ACL injury puberty and footwear\Trials12Sept2014\Calibration test\Karine vertical jump 1.trc';

% Preallocate the array for the transformed joints coordinates:
slave_data_transf = zeros(frames_slave,3*jointsNum);
for i=1:frames_slave
    slave_row = slave_data(i,:);
    slave_matrix = reshape(slave_row,[3,jointsNum]);
    % Calculate the coordinate of Kinect Slave in Kinect Master system:
    slave_matrix_transf = D * [slave_matrix; ones(1,jointsNum)];
    
    % Delete the last row (4th) of the matrix containing the transformed joints,
    % which is made of ones. The resulting matrix is [3 x jointsNum]:
    slave_matrix_transf(4,:) = [];
    slave_row_transf = reshape(slave_matrix_transf,[1,3*jointsNum]);
    slave_data_transf(i,:) = slave_row_transf;
end


slave_trc_transf_noHeader = [slave_trc.data(:,1:2),slave_data_transf(:,:)];

% Write the transformed .trc file
[path_slave,name_slave,ext_slave] = fileparts(file_slave);
file_slave_transf = [path_slave,filesep,name_slave,'_transformed',ext_slave];
file_slave_open_id = fopen(file_slave_transf,'w');
% Write the header copying the old one and editing only the path:
fprintf(file_slave_open_id,'%s\t%d\t%s\t%s\n','PathFileType',4,'(X/Y/Z)',file_slave_transf);
fprintf(file_slave_open_id,'%s\n',slave_trc.textdata{2,1});
fprintf(file_slave_open_id,'%s\n',slave_trc.textdata{3,1});
fprintf(file_slave_open_id,'%s\n',slave_trc.textdata{4,1});
fprintf(file_slave_open_id,'%s\n\n',slave_trc.textdata{5,1});
fclose(file_slave_open_id);

% Append the transformed coordinates below the header of the transformed .trc file:
dlmwrite(file_slave_transf,slave_trc_transf_noHeader,'-append','delimiter','\t');
