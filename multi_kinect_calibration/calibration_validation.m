% Evaluation of the calibration algorithm error using a predetermined
% transformation
% �2014 Alessandro Timmi
% jointsNum = number of joints in the skeleton
% jointsNames = cell array with joints names
% KSlave = 3xn array with joints coordinates in the slave coordinate system

function calibration_validation(replicated_jointsNum, replicated_jointsNames, replicated_ignoredJointsValues, KSlave)
fprintf('######################################################\n')
fprintf('Calibration validation\n')

% Invent a transformation matrix from KSlave to KMaster:
% D_exact=[1/sqrt(2), 1/sqrt(2),  0,  0.2;
%         0,          0,          1,  0.4;
%         1/sqrt(2),  -1/sqrt(2), 0,  0.4;
%         0,          0,          0,  1];
    
% Case 1: sensors close to each other and coplanar:    
d_cp = [0.1;0;0];
D_exact = [eye(3), d_cp;
            0, 0, 0, 1];    
    
% Obtain virtual KMaster points from KSlave using the exact matrix:
KSlave_transf_exact = D_exact * [KSlave; ones(1,replicated_jointsNum)];
% Delete the last row (4th) of the matrix
KSlave_transf_exact(4,:) = [];

% Obtain transform matrix from KSlave to KSlave_transf_exact using
% Gupta-Chutakanonta algorithm:
D_gupta = gupta_chuta_shiflett_laub_svd_std_Dmatrix(KSlave_transf_exact,KSlave);

% Transform KSlave cooords in KSlave_transf_exact system, but using
% Gupta's approx matrix:
KSlave_transf_gupta = D_gupta * [KSlave; ones(1,replicated_jointsNum)];

% Delete the last row (4th) of the matrix containing the transformed joints,
% which is made of ones. The resulting matrix is [3 x calib_markers]:
KSlave_transf_gupta(4,:) = [];

% Evaluate calibration results in terms of distances between corresponding
% joints from KSlave_transf_exact and KSlave_transf_gupta:
[rms_dist,max_dist,max_index,dist_array] =...
    calibration_error(KSlave_transf_exact,KSlave_transf_gupta);
% Print summary results:
fprintf('RMS = %0.4f m\tMax error = %0.4f m on joint %s\n',...
    rms_dist, max_dist, replicated_jointsNames{max_index});
% Print specific results for each joint:
for j=1:replicated_jointsNum
    fprintf('%s\t%0.4f', replicated_jointsNames{j}, dist_array(j))
    if any(replicated_ignoredJointsValues==j)
        fprintf(' (ignored for calibration)')
    end
    fprintf('\n')
end
fprintf('\n\n')






fprintf('\nEnd of calibration validation\n')
fprintf('######################################################\n')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot KSlave (original) and KSlave_transf_exact coordinates:
figure('Name','Calibration validation - Original data')
a=gca;
set(a,'DataAspectRatio',[1,1,1])
hold on
xlabel('X [m]');
ylabel('Y [m]');
zlabel('Z [m]');
title('Calibration validation')

% plot joints:
plot3(a,KSlave(1,:),KSlave(2,:),KSlave(3,:),'go','markersize',5,'linewidth',2)
plot3(a,KSlave_transf_exact(1,:),KSlave_transf_exact(2,:),KSlave_transf_exact(3,:),'r.')
legend('Kinect Slave original','Kinect Slave transf exact','location','southwest')
legend('boxon')

% plot coordinate system
xaxis = [0,0,0;0.2,0,0]';
yaxis = [0,0,0;0,0.2,0]';
zaxis = [0,0,0;0,0,0.2]';
plot3(a,xaxis(1,:),xaxis(2,:),xaxis(3,:),'linewidth',4,'color','r');
plot3(a,yaxis(1,:),yaxis(2,:),yaxis(3,:),'linewidth',4,'color','g');
plot3(a,zaxis(1,:),zaxis(2,:),zaxis(3,:),'linewidth',4,'color','b');
plot3(0,0,0,'ko')

% Set the camera:
%view([0.9,0.1,0.1])
camup(a,[1,0,0])
%camroll(90)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot KSlave_transf_exact and KSlave_transf_gupta coordinates:
figure('Name','Calibration validation - Transformed data')
a=gca;
set(a,'DataAspectRatio',[1,1,1])
hold on
xlabel('X [m]');
ylabel('Y [m]');
zlabel('Z [m]');
title('Calibration validation')

% plot joints:
plot3(a,KSlave_transf_exact(1,:),KSlave_transf_exact(2,:),KSlave_transf_exact(3,:),'r.')
plot3(a,KSlave_transf_gupta(1,:),KSlave_transf_gupta(2,:),KSlave_transf_gupta(3,:),'go','markersize',5,'linewidth',2)
legend('Kinect transf exact','Kinect transf Gupta','location','southwest')
legend('boxon')

% plot coordinate system
xaxis = [0,0,0;0.2,0,0]';
yaxis = [0,0,0;0,0.2,0]';
zaxis = [0,0,0;0,0,0.2]';
plot3(a,xaxis(1,:),xaxis(2,:),xaxis(3,:),'linewidth',4,'color','r');
plot3(a,yaxis(1,:),yaxis(2,:),yaxis(3,:),'linewidth',4,'color','g');
plot3(a,zaxis(1,:),zaxis(2,:),zaxis(3,:),'linewidth',4,'color','b');
plot3(0,0,0,'ko')

% Set the camera:
%view([0.9,0.1,0.1])
camup(a,[1,0,0])
%camroll(90)