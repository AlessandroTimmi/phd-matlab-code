% Distance between 2 sets of corresponding points.
% This function determines the distances between 2 sets of corresponding
% points and returns the RMS and max distance.
%
% Input:
%   P = a [3xn] array, whose columns contains the X,Y,Z coordinates of
%   markers in the reference coordinate system
%   p = a [3xn] array, whose columns contains the X,Y,Z coordinates of
%   markers transformed in the reference coordinate system 
%
% Output:
%   
%   rms_dist = the root mean square of the calculated distances.
%   max_dist = the maximum distance among those calculated;
%   max_index = the index of the pair of points being farthest apart;
%   dists = the array containing all the distances.

% �2014 Alessandro Timmi

function [rms_dist,max_dist,max_index,dist_array] = calibration_error(P,p)

if size(P)~=size(p)
    error('Error: the 2 set of points have different sizes.')
end

if size(P,1)~=3
    error('Error: the 2 sets of points don''t have 3 dimensions.')
end


pointsNum = size(P,2);

% Array of the difference vectors between the corresponding pairs of
% points. The difference vectors are written in columns (X;Y;Z) and
% represent the relative position of a point from set p with respect to
% its corresponding point from set P.
dist_matrix = p-P;

dist_array = zeros(1,pointsNum);
for j=1:pointsNum
    % Array containing a distance for each pair of points. The distance is
    % calculated as norm of the difference vector for each pair. 
    dist_array(j) = norm(dist_matrix(:,j));
end
% Max error:
[max_dist,max_index] = max(dist_array);
% Calculate the RMS error:
rms_dist = rms(dist_array);
