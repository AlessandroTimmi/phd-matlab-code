%GUPTA_CHUTA_SHIFLETT_LAUB_SVD_STD_DMATRIX    Calcola la matrice di
%   rotazione a partire dalle coordinate assolute e locali di un minimo di
%   3 marker individuanti il corpo rigido.
%
%   La procedura (ideata da Gupta e Chutakanonta) scarta gli inevitabili
%   errori dovuti alla non perfetta rigidit� del corpo stesso e ad
%   imprecisioni connesse al sistema di acquisizione
%   (vedi libro CDC pag. 37).
%
%   L'errore � distribuito uniformemente su tutta la matrice di rotazione,
%   applicando la procedura alternativa proposta dagli stessi Gupta e
%   Chutakanonta che permette di avere una matrice di errore simmetrica.
%
%   A differenza dell'algoritmo ideato da Gupta e Chutakanonta, il quale
%   utilizza la thin SVD ('econ' in Matlab), qui viene usata la SVD
%   standard. Il motivo � connesso al fatto che questo algoritmo nasce per
%   dati ridondanti, quindi per un numero di marker n>=4. Poich� in Virtual
%   Sensei molti corpi rigidi hanno 3 soli marker, la matrice V perderebbe
%   la sua ortogonalit� usando la thin SVD. L'aspetto negativo di questa
%   modifica � che la matrice S � rettangolare per n=3 e quindi nel
%   processo di inversione occorre creare S+ con le stesse dimensioni di S
%   (avr� quindi l'ultima colonna di zeri nel caso di 3 marker) per
%   permettere di moltiplicare le 3 matrici U, Sp, V' invertite.
%
%   L'eventualit� del det(A)=-1 � scongiurata applicando
%   l'accorgimento proposto da Shiflett e Laub (vedi libro CDC pag. 36)
%   alla matrice Ur ottenuta dalla decomposizione QR.
%
%   Input:
%   XYZ =   array le cui colonne costituiscono i vettori delle coordinate
%           assolute dei marker;
%   xyz =   array le cui colonne costituiscono i vettori delle coordinate
%           locali degli stessi marker, espresse nel riferimento del quale
%           si vuole calcolare la matrice di rotazione rispetto a quello
%           assoluto.
%   
%   Output:
%   D   =   matrice [4x4] di trasformazione dal sistema di riferimento xyz
%           al sistema di riferimento XYZ.
%           D =[R,d
%               0,1]
%           where R [3x3] is the rotation matrix with error symmetrically
%           on all the elements and d [3x1] is the translation vector.
%
%
% �2011 Alessandro Timmi, Virtual Sensei
% Last edit: Oct 2014

% TODO: to be updated using my latest findings


function D = gupta_chuta_shiflett_laub_svd_std_Dmatrix(XYZ,xyz)

% Valore di tolleranza: un numero n, tale che abs(n)<eps, viene
% approssimato a zero:
eps=1e-007;

% Numero di marker costituenti il corpo rigido in esame = n� colonne XYZ:
nMarker=size(XYZ,2);

% Se il corpo rigido � individuato da meno di 3 marker non � possibile
% determinarne l'orientamento:
if nMarker<3
    errordlg('Errore: corpo rigido costituito da meno di 3 marker',...
        'Rigid Body Error');
    return
end

% Build the two matrices [4 x nMarkers]. Their last row is made of ones:
P = [XYZ;
    ones(1,nMarker)];
p = [xyz;
    ones(1,nMarker)];

% Apply the SVD decomposition to the transpose of [p]:
[U,S,V] = svd(p');
% Le matrici ottenute dalla SVD standard sono:
% p'[3x4] = U(3x3) S(3x4) V'(4x4)
% p'[4x4] = U(4x4) S(4x4) V'(4x4)


% S � una matrice diagonale nx4, quindi quadrata nel caso di n=4 marker.
% tuttavia nel caso di n=3, l'ultima colonna � costituita da zeri, mentre
% nel caso di n=5 (non capita nel nostro scheletro) � l'ultima riga ad
% essera costituita da zeri.
% Pertanto, nel ciclo for necessario a costruire la pseudo-inversa Sp di S,
% viene calcolata la dimensione minima di S e questo valore viene usato
% come numero di iterazioni del ciclo for stesso. Le dimensioni della
% matrice Sp vengono invece assunte uguali a quelle della matrice S.
% IN QUESTO MODO QUESTA FUNZIONE � RESA COMPATIBILE CON QUALUNQUE NUMERO
% DI MARKER COSTITUENTI UN CORPO RIGIDO (modifica al metodo di Gupta e
% Chutakanonta effettuata da ALE)

% % La matrice S potrebbe essere singolare, pertanto, dovendo invertirla,
% % occorre scrivere una pseudo-inversa positiva (Sp) che abbia come
% % elementi della diagonale:
% % -     1/S(j,j)    se S(j,j) � diverso da zero
% % -     0           se S(j,j) � circa uguale a zero.
% dimS=size(S);
% Sp=zeros(dimS);
% for j=1:min(dimS)
    % if abs(S(j,j))<eps     % considera S(j,j)=0
        % Sp(j,j)=0;
    % else                    % S(j,j) diverso da zero
        % Sp(j,j)=1/S(j,j);
    % end
% end

% According to Gupta & Chutakanonta, we calculate the Sp matrix as it comes
% from the SVD decomposition, without substituting zero in place of very
% big numbers which may come from very small elements of S:
dimS = size(S);
Sp = zeros(dimS);
for j=1:min(dimS)
    Sp(j,j) = 1/S(j,j);
end

% Calculate the unknown D [4x4] matrix:
D = P*U*Sp*V';

% The minor M [3x3], made with the first 3 rows and 3 columns of D,
% represents a first stimate of the rotation matrix:
M = D(1:3,1:3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Checks:
% Extract the translation vector "d" [3x1] between the two coordinate
% systems. Remember that D [4x4] = [R d ; 0 1]
check_d = D(1:3,4)

% To have an indication of rounf-off errors, we need to check the 4th line
% of the displacement matrix:
check_last_row_D = D(4,:)

% To have an indication of the experimental errors in the point coordinate
% data, we need to check if the 3x3 principal minor of the displacement
% matrix D (which is a first estimate of the rotation matrix) is
% orthogonal, i.e. M'M=I.
% Errors in matrices are found using the L2 norm as:
% %err in M = norm(M'*M-I)/norm(I)*100
% Quantification of the orthogonality error before the correction:
orthogonality_err_before_QS = norm(M'*M-eye(3))/norm(eye(3))*100;
fprintf('Orthogonality error of rotation matrix before QS = %0.2f%%\n\n',orthogonality_err_before_QS)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% SVD/QS: secondo metodo per la distribuzione uniforme dell'errore sulla
% matrice di rotazione, proposto dagli stessi Gupta e Chutakanonta:
[Ur,Sr,Vr] = svd(M);

% Calcola la matrice di rotazione provvisoria:
A = Ur*Vr';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Error checks
% Symmetric error matrix:
Sym_err_matrix_QS = Vr*Sr*Vr'-eye(3)

% Quantification of the orthogonality error before the correction:
orthogonality_err_after_QS = norm(A'*A-eye(3))/norm(eye(3))*100;
fprintf('Orthogonality error of rotation matrix after QS = %0.2f%%\n\n',...
    orthogonality_err_after_QS)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Modifica sulla Ur proposta da Shiflett e Laub per ottenere il
% determinante della matrice di rotazione pari a 1:
detA=det(A);
% Se esso risulta diverso da 1 (con tolleranza eps), applica
% l'accorgimento proposto da Shiflett e Laub:
if detA<=1-eps || detA>=1+eps
    warning('Det(A) not 1')
    % Definisce Urp=[u1 u2 +u3]:
    Urp = Ur;
    % Definisce Urm=[u1 u2 -u3]:
    Urm = [Ur(:,1:2),-Ur(:,3)];
    % Calcola le due matrici di rotazione:
    Ap = Urp*Vr';
    Am = Urm*Vr';
    % Verifico se una delle nuove matrici ha det=1 (con tolleranza eps):
    % solo in questo caso una delle due diventa quella definitiva,
    % altrimenti viene mantenuta quella originale:
    detAp = det(Ap);
    detAm = det(Am);
    if detAp>=1-eps && detAp<=1+eps
        warning('Shiflett & Laub algorithm applied: [Ap] used')
        A = Ap;
    elseif detAm>=1-eps && detAm<=1+eps
        warning('Shiflett & Laub algorithm applied: [Am] used')
        A = Am;
    end
end
% mostra il determinante definitivo:
%disp(det(A));

% Restituisce la matrice D [4x4] di trasformazione:
D(1:3,1:3) = A;
