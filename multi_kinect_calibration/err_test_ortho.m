clear all
clc
close all

format

bad =   [0.8041  -0.0456   0.0457;
        -0.0456   1.0295  -0.0366;
        0.0457  -0.03664   0.9788]

bad2 =  [0.99999999999999  -0.0456   0.0457;
        -0.0456   1.0  -0.0366;
        0.0457  -0.03664   0.9788]

bad3 =  [0.8041  -0.0456   0.0457;
        -0.0456   1.0295  -0.0366;
        0.0457  -0.03664   0.9788]
  
norm(eye(3),inf)    
orthogonality_err1 = norm(bad'*bad-eye(3))/norm(eye(3))
orthogonality_err2 = norm(bad2'*bad2-eye(3))/norm(eye(3))
orthogonality_err3 = norm(bad3'*bad3-eye(3),inf)/norm(eye(3),inf)