function sTrim = TestTrimStruct(s)
% TODO this function was extracted from the synchronization function, in
% order to respect the "single responsibility principle".
% It needs work to make it functional.
%
% � October 2014 Alessandro Timmi

% trim_start = (optional parameter, default = 0) set the number of frames
%       to be removed from the beginning of both the synchronized signals.
%       This parameter is meant to be used in a second iteration of this
%       function, when in the first we notice some extra frames at the
%       beginning that we want to trim.
% trim_end = (optional parameter, default = 0) set the number of frames to
%       be removed from the end of both the synchronized signals. This
%       parameter is meant to be used in a second iteration of this
%       function, when in the first we notice some extra frames at the end
%       that we want to trim.


default.trim_start = 0;
default.trim_end = 0;


addParameter(p, 'trim_start', default.trim_start,...
    @(x) validateattributes(x, {'numeric'}, {'row', 'nonnegative', 'integer'}));
addParameter(p, 'trim_end', default.trim_end,...
    @(x) validateattributes(x, {'numeric'}, {'row', 'nonnegative', 'integer'}));

trim_start = p.Results.trim_start;
trim_end = p.Results.trim_end;



% Optional extra trim to remove unwanted (by the user) frames from start
% and end of the synced signals:
if trim_start || trim_end
    fprintf('Trimming extra frames after syncing...\n');
    
    s_short.markers = structfun(@(y) ( y(1 + trim_start : end - trim_end, :) ),...
        s_short.markers, 'UniformOutput', false);
    s_long.markers = structfun(@(y) ( y(1 + trim_start : end - trim_end, :) ),...
        s_long.markers, 'UniformOutput', false);
    
    s_short.time = s_short.time(1 + trim_start : end - trim_end);
    s_long.time = s_long.time(1 + trim_start : end - trim_end);
    
    fprintf('Trimmed %d frames at the beginning of the trials.\n', trim_start);
    fprintf('Trimmed %d frames at the end of the trials.\n', trim_end);
end


% TODO need to update the properties of the trc file after trimming.
