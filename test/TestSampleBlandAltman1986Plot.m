oldmethod = [1,7,56,89,3,44,59,33,34,22,87,98,54,2,9,99,24,56,67,87,91,45,12,17]';
oldmethod = [oldmethod;oldmethod*1.1+2;oldmethod*0.9-3];

newmethod = [1.1,7.5,58,87,5,42,57,31,33,23,89,95,58,1,10,97,25,58,65,85,93,47,13,18]';
newmethod = [newmethod;newmethod * 1.1+2.2;newmethod * 0.9-3.1];

bland_altman_1986(oldmethod, newmethod,'sample variable', 'distance', 'm')