% Testing the effect of synchronization error on agreement
% � February 2016 Alessandro Timmi
clc
clear
close all

fprintf('Effect of synchronization error on agreement\n')
fprintf('� February 2016 Alessandro Timmi\n\n')

% Data:
x1 = linspace(0, 4 * pi, 1000)';
y1 = sin(x1);

fprintf('Simulating a pure sync error:\n')
sync_error = [-.5, -1, -1.5, -2];

for i=1:4
    % Shifted data: the assumption here is that output data are the same
    % (y2 = y1) but there is a time delay between them (x2 ~= x1):
    y2 = sin(x1+sync_error(i));
    % TODO: this is wrong, we need to create a new y2, padded where is the
    % delta_x.
    
    %% Agreement
    dif = y2 - y1;
    avg = mean([y2, y1], 2);
    bias = mean(dif);
    sd = std(dif);
    lla = bias - 1.96 * sd;
    ula = bias + 1.96 * sd;
    
    fprintf('Sync error (phase shift): %0.2f rad\n', sync_error(i))
    fprintf('Bias (LLA, ULA): %0.2f (%0.2f, %0.2f) m\n\n', bias, lla, ula)
    
    
    %% Plot
    figure
    subplot(2,1,1, 'Ygrid', 'On')
    plot(y1, 'linewidth', 1.5)
    hold on
    plot(y2, 'linestyle', '--', 'linewidth', 1.5)
    legend('Original', sprintf('Sync err (phase shift): %0.2f rad', sync_error(i)))
    xlabel('x [rad]')
    ylabel('y [m]')
    
    
    subplot(2,1,2, 'Ygrid', 'On')
    plot(avg, dif, 'bo')
    refline(0, bias)
    refline(0, lla)
    refline(0, ula)
    
end

