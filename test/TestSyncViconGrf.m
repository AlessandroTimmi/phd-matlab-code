% Test synchronization of ground reaction forces stored in .mot files
%
% � September 2016 Alessandro Timmi
clc
clear
close all

drive = 'D';
subjectNumber = 69;
trialNumber = 2;


viconIkFilename = sprintf('%s:\\TEST_FOLDER\\ACL study - Kinect Vicon agreement - latest 14 subjects\\Kinect and Vicon OpenSim results\\Vicon\\Dev%d\\Dev%d SLS 0%d_ik.mot', drive, subjectNumber, subjectNumber, trialNumber);
viconIkSyncFilename = sprintf('%s:\\TEST_FOLDER\\ACL study - Kinect Vicon agreement - latest 14 subjects\\Kinect and Vicon OpenSim results\\Vicon\\Dev%d\\Dev%d SLS 0%d_ik_sync.mot', drive, subjectNumber, subjectNumber, trialNumber);

kinectIkSyncFilename = sprintf('%s:\\TEST_FOLDER\\ACL study - Kinect Vicon agreement - latest 14 subjects\\Kinect and Vicon OpenSim results\\Kinect\\Dev%d\\SLS_0%d_pre-processed_ik_sync.mot', drive, subjectNumber, trialNumber); 

viconGrfFilename = sprintf('%s:\\TEST_FOLDER\\ACL study - Kinect Vicon agreement - latest 14 subjects\\Kinect and Vicon OpenSim results\\Vicon\\Dev%d\\Dev%d SLS 0%d_grf.mot', drive, subjectNumber, subjectNumber, trialNumber);
viconGrfSyncFilename = sprintf('%s:\\TEST_FOLDER\\ACL study - Kinect Vicon agreement - latest 14 subjects\\Kinect and Vicon OpenSim results\\Vicon\\Dev%d\\Dev%d SLS 0%d_grf_sync.mot', drive, subjectNumber, subjectNumber, trialNumber);

viconIk = LoadMotFile('filename', viconIkFilename);
viconIkSync = LoadMotFile('filename', viconIkSyncFilename);

kinectIkSync = LoadMotFile('filename', kinectIkSyncFilename);

viconGrf = LoadMotFile('filename', viconGrfFilename);
viconGrfSync = LoadMotFile('filename', viconGrfSyncFilename);
fprintf('\n')

%% Get some info from the loaded trials:
% NOTE: for some unknown reason the OpenSim Inverse Kinematics seems to
% remove one frame from the results file (_ik.mot in our case), so that
% these results files turn out to be 1 frame shorter than the
% correspondent .trc files. This issues doesn't happen for all trials.

% Average GRF sampling frequency for current trial:
[~, ~, ~, grfRate] = check_sampling_freq(viconGrf.time);
fprintf('Vicon GRF sampling rate: %0.1f Hz\n\n', grfRate)

% Number of frames and durations:
viconIkLength = length(viconIk.time);
viconIkDuration = viconIk.time(end) - viconIk.time(1);

viconIkSyncLength = length(viconIkSync.time);
viconIkSyncDuration = viconIkSync.time(end) - viconIkSync.time(1);

kinectIkSyncLength = length(kinectIkSync.time);
kinectIkSyncDuration = kinectIkSync.time(end) - kinectIkSync.time(1);

viconGrfLength = length(viconGrf.time);
viconGrfDuration = viconGrf.time(end) - viconGrf.time(1);

viconGrfSyncLength = length(viconGrfSync.time);
viconGrfSyncDuration = viconGrfSync.time(end) - viconGrfSync.time(1);

fprintf('Vicon IK length and duration (@120 fps):\t\t\t%d frames,\t\t%0.4f s\n\n', viconIkLength, viconIkDuration)
fprintf('Vicon IK sync length and duration (@480 fps):\t\t%d frames,\t%0.4f s\n', viconIkSyncLength, viconIkSyncDuration)
fprintf('Kinect IK sync length and duration (@480 fps):\t\t%d frames,\t%0.4f s\n\n', kinectIkSyncLength, kinectIkSyncDuration)
fprintf('Vicon GRF length and duration (@%0.1f fps):\t\t%d frames,\t%0.4f s\n', grfRate, viconGrfLength, viconGrfDuration)
fprintf('Vicon GRF Sync length and duration (@%0.1f fps):\t%d frames,\t%0.4f s\n\n', grfRate, viconGrfSyncLength, viconGrfSyncDuration)


%% Find a reference point for each trial, in order to calculate the delay:
[minIk, idIk] = min(viconIk.coords.knee_angle_r);
[minIkSync, idIkSync] = min(viconIkSync.coords.knee_angle_r);
[minGrf, idGrf] = min(viconGrf.coords.N2_ground_force_vy);
[minGrfSync, idGrfSync] = min(viconGrfSync.coords.N2_ground_force_vy);

tMinIk = viconIk.time(idIk);
tMinIkSync = viconIkSync.time(idIkSync);
tMinGrf = viconGrf.time(idGrf);
tMinGrfSync = viconGrfSync.time(idGrfSync);

% Part of the difference between the following two time intervals might be
% caused by the relatively coarse framerate of the IK data compared to GRF
% this could have indeed affected the determination of the ideal min() of
% the signals.
deltaTimeIk = tMinIkSync - tMinIk;
deltaTimeGrf = tMinGrfSync - tMinGrf;
fprintf('Delta time IK:\t%0.4f s\n', deltaTimeIk)
fprintf('Delta time GRF:\t%0.4f s\n', deltaTimeGrf)


%% Plot signals
myTitle = 'Vicon joint angles and GRF sync test';
figure('Name', myTitle)

yyaxis left
hIk = plot(viconIk.time, viconIk.coords.knee_angle_r, '-.');
hold on
hIkSync = plot(viconIkSync.time, viconIkSync.coords.knee_angle_r, '-');
plot(tMinIk, minIk, 'mx')
plot(tMinIkSync, minIkSync,'gx')
ylabel('[�]')

yyaxis right
hGrf = plot(viconGrf.time, viconGrf.coords.N2_ground_force_vy, '-.');
hGrfSync = plot(viconGrfSync.time, viconGrfSync.coords.N2_ground_force_vy, '-');
plot(tMinGrf, minGrf,'mx')
plot(tMinGrfSync, minGrfSync,'gx')
ylabel('[N]')

legend([hIk, hIkSync, hGrf, hGrfSync],...
    'Knee angle', 'Knee angle sync', 'Fv2', 'Fv2 sync', 'location', 'best')
xlabel('Time [s]')
title(myTitle)



