% Test filtering Kinect marker coordinates from the ACL study (SLS task).
% � August 2016 Alessandro Timmi
clc
clear

% Input arguments:
subjectNumber = 90;
trialNumber = 01;
markerName = 'Metatarsus';
coordinate = 'Z';
coordinateNumber = coord2num(coordinate);

% Trials filenames:
kinectFilename = ['C:\TEST_FOLDER\ACL study - Kinect Vicon agreement - latest 14 subjects\Kinect\3 - pre-processed and OpenSim results\'...
    sprintf('DEV%d\\SLS_0%d_pre-processed.trc', subjectNumber, trialNumber)];



% Load trial:
s = LoadTrcFile('filename', kinectFilename);

markerNames = fieldnames(s.markers);


%% Fill gaps
sFilt = FillGapsTrc(s, 10);

% Filter Kinect trial:
filterType = 'butter';
sFilt = FilterTrc(sFilt, filterType,...
    'medianSpan', 5,...
    'sgolayOrder', 4,...
    'sgolaySpan', 19,...
    'butterOrder', 4,...
    'butterCutoff', 14);
% 
% filterType = 'sgolay';
% sFilt = FilterTrc(sFilt, filterType,...
%     'medianSpan', 5,...
%     'sgolayOrder', 4,...
%     'sgolaySpan', 19,...
%     'butterOrder', 4,...
%     'butterCutoff', 14);

% Write filtered Kinect trial:
% WriteTrcFile(sFilt);


%% Plot data:
figure('Name', 'ACL study - Kinect data - Filtering test')
hold on
plot(s.markers.(markerName)(:,coordinateNumber))
plot(sFilt.markers.(markerName)(:,coordinateNumber))
legend('Unfiltered', sprintf('%s', filterType))
xlabel('Frames [#]')
ylabel(sprintf('%s %s [m]', markerName, coordinate))
title(sprintf('Dev%d SLS0%d %s %s', subjectNumber, trialNumber, markerName, coordinate))



