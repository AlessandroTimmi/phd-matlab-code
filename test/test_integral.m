% Example of how to intergrate data.
% NOTE: with real data, the results of the integration might be 
% inaccurate, due to noise.
% See wikipedia to understand how cumtrapz() works:
% https://en.wikipedia.org/wiki/Trapezoidal_rule
%
% � November 2015 Alessandro Timmi

clc
clear
close all

% Time interval between data points:
dt = 1 / 2400;

% In real world this will be the array of timestamps:
t = 0 : dt : 2 * pi;
% t = -pi : dt : pi;
% t = pi/2 : dt : 5*pi/2;

% Generate data to be integrated. In real world, this could be an
% accelerometer signal:
a = cos(t);

% Numerical integral of the data: if the input is an acceleration, this
% would be the corresponding velocity:
v_num = cumtrapz(a) * dt;

% Numerical integral of the speed; this would be the displacement:
d_num = cumtrapz(v_num) * dt;


%% Alternative, discrete method (old shool):
% v_dis = zeros(1, length(a));
% v_dis(1) = a(1) * dt;
% for i=2:length(a)
%     v_dis(i) = a(i) * dt + v_dis(i-1);
% end
% 
% d_dis = zeros(1, length(a));
% d_dis(1) = v_dis(1) * dt;
% for i=2:length(a)
%     d_dis(i) = v_dis(i) * dt + d_dis(i-1);
% end


%% Analytical integrals. We can calculate them only because the input
% signal is a known function. We will plot them as reference: 
v_an = sin(t) - sin(t(1));
d_an = -cos(t) + cos(t(1)) - sin(t(1)) * (t - t(1));


%% Plot results:
figure
subplot(3,1,1)
plot(t, a, 'b', 'linewidth', 1.5)
ylabel('Acc. [m/s^2]')
set(gca, 'ygrid','on')
legend('cos(t)')
title('Example of double integration')

subplot(3,1,2)
plot(t, v_an, 'b', 'linewidth', 1)
hold on
plot(t, v_num, 'g--', 'linewidth', 1.5)
%plot(t, v_dis, 'r:', 'linewidth', 1)
ylabel('Vel. [m/s]')
set(gca, 'ygrid','on')
legend('Analytical',...
    'Numerical (cumtrapz)')

subplot(3,1,3)
plot(t, d_an, 'b', 'linewidth', 1)
hold on
plot(t, d_num, 'g--', 'linewidth', 1.5)
%plot(t, d_dis, 'r:', 'linewidth', 1)
ylabel('Displ. [m]')
set(gca, 'ygrid','on')
legend('Analytical',...
    'Numerical (cumtrapz)')
xlabel('Time [s]')