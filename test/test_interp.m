clear
clc
close all



[s, marker_names] = LoadTrcFile();

%% INTERPOLATION OF KINECT DATA TO MATCH VICON FRAMERATE
% New time array for Kinect, interpolated at Vicon framerate:
time_interp = s.time(1) : 1/120 : s.time(end);

fprintf('\nInterpolating Kinect joint angles at 120 fps...\n\n');

% Interpolate spline extrapolation
s_spline_extr = structfun(@(y)...
    ( interp1(s.time, y, time_interp, 'spline', 'extrap')' ),...
    s, 'UniformOutput', false);


% Interpolate cubic extrapolation
s_cubic_extr = structfun(@(y)...
    ( interp1(s.time, y, time_interp, 'pchip', 'extrap')' ),...
    s, 'UniformOutput', false);


% Interpolate cubic extrapolation
s_linear_extr = structfun(@(y)...
    ( interp1(s.time, y, time_interp, 'linear', 'extrap')' ),...
    s, 'UniformOutput', false);





figure
plot(s.time, s.X3, 'b')
hold on
plot(s_spline_extr.time, s_spline_extr.X3, 'r--')
plot(s_cubic_extr.time, s_cubic_extr.X3, 'g--')
plot(s_linear_extr.time, s_linear_extr.X3, 'm--')
legend('Raw', 'Spline extrap', 'cubic extrap', 'linear extrap')