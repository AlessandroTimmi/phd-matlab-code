clc
clear
close all

t = 0:1:100;
a = 0.1;

y = exp(-a * t);

t_adapt = 1/a;
y_adapt = exp(-a * t_adapt);


figure
plot(t,y)
hold on
plot(t_adapt, y_adapt, 'ro')
legend('Function', 'Adaptation')