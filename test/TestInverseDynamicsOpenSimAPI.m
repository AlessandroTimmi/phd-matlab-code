% Test OpenSim Inverse Dynamics API
% � September 2016 Alessandro Timmi

clc
clear

% TODO:
% - customize trial name into id.sto file;

%% Input:
subjectFolder = 'C:\TEST_FOLDER\ACL study - Kinect Vicon agreement - latest 14 subjects\TEST_INVERSE_DYNAMICS';
scaledModelFilename = fullfile(subjectFolder, 'Dev69_scaled_gait2392_simbody_Timmi.osim');
ikFilename = fullfile(subjectFolder, 'Dev69 SLS 01_ik.mot');
grfFilename = fullfile(subjectFolder, 'Dev69 SLS 01_grf.mot');


%% Output:
externalLoadsFilename = fullfile(subjectFolder, 'ExternalLoads.xml');
resultsFolder = fullfile(subjectFolder, 'InverseDynamicsResults');
[~, ikName, ~] = fileparts(ikFilename);
% The ID file name will be the same as IK, but with different suffix and extension:
idName = [ikName(1:end-2), 'id.sto'];


%% Import OpenSim modelling classes:
import org.opensim.modeling.*


%% Create External Loads object and print it to an .xml file:
fprintf('� Setting up External Loads and printing .xml file...\n')
SetExternalLoads(externalLoadsFilename, 2, 'calcn_r', grfFilename, ikFilename);

%% Get time range from current trial:
fprintf('� Getting start and end time from IK file...\n')
ikData = org.opensim.modeling.Storage(ikFilename);
initialTime = ikData.getFirstTime();
finalTime = ikData.getLastTime();

% Setup Inverse Dynamics tool for current trial.
fprintf('� Setting up ID tool...\n')
% The information required to setup ID is so little (and trial-specific) 
% that we don't need a generic ID setup .xml file for the constructor.
% We can simply pass the required information via API (below) and leave
% the parentheses empty:
idTool = org.opensim.modeling.InverseDynamicsTool();
idTool.setName('InverseDynamicsSetupAle')
idTool.setResultsDir(resultsFolder);
idTool.setModelFileName(scaledModelFilename);
% NOTE: in alternative to setModelFileName, we could pass a model object,
% but first we would need to initialize it using:
% scaledModel = org.opensim.modeling.Model(scaledModelFilename);
% scaledModel.initSystem() 
% idTool.setModel(scaledModel)

idTool.setStartTime(initialTime);
idTool.setEndTime(finalTime);
% Set parameter "forces_to_exclude":
excludedForces = ArrayStr();
excludedForces.append('Muscles');
idTool.setExcludedForces(excludedForces);
% Use the External Loads .xml file we have just created:
idTool.setExternalLoadsFileName(externalLoadsFilename);
idTool.setCoordinatesFileName(ikFilename);
idTool.setLowpassCutoffFrequency(-1);
% Important: this must be just the file name, without path:
idTool.setOutputGenForceFileName(idName);


% Run Inverse Kinematics tool on current trial:
fprintf('� Running ID tool...\n')
idTool.run();

fprintf('� Printing ID setup .xml file...\n')
idTool.print(fullfile(subjectFolder, 'customSetupID.xml'));