% Practicing with Student's t distribution.
% � February 2016 Alessandro Timmi

% Summary of the functions available in Matlab.
% NOTICE: probability p is intended for one-sided test.
% tpdf(x, nu): Student's t probability density function. Returns the value
%       of t, given x and the number of degrees of freedom nu.
% tcdf(x, nu): Student's t cumulative distribution function. Returns the
%       cumulative probability p, given x and the number of degrees of
%       freedom nu.
% tinv(p, nu): Student's t inverse cumulative distribution function.
%       Returns x, given the cumulative probability p and the number of
%       degrees of freedom nu.

clear
clc
close all

fprintf('Practicing with Student''s t distribution...\n')
fprintf('� February 2016 Alessandro Timmi\n\n')


x = (-3:0.01:3)';
% Range of samples. A sample of 1 doesn't make sense, so we skip it:
samples = (2:1000)';
dof = samples - 1;

% Calculate the t distribution for the first value of the DOF array:
t_dist_1 = tpdf(x, dof(1));
t_dist_10 = tpdf(x, 10);
% Calculate the t distribution for the last value of the DOF array:
t_dist_end = tpdf(x, dof(end));
norm_dist = normpdf(x);

% The t-value is used to determine the confidence intervals (CI). For
% example, in Bland-Altman analysis of agreement, we need to calculate the
% 95% CI.
% NOTICE: in their paper on The Lancet (1986), Bland and Altman
% correctly recommend to use the t-value to calculate the CI. However,
% Bland in his notes (https://www-users.york.ac.uk/~mb55/meas/sizemeth.htm)
% directly uses 1.96 to calculate the CI. I suppose the reason is that,
% for large sample sizes, the t distribution tends to the Normal
% distribution and consequently this t-value asymptotically tends to 1.96
% (see plot below).
alpha = 0.05;
% We need to convert the probability from two-sided to one-sided to use
% the inverse t-function:
P = 1 - alpha / 2;
% Calculate the t-value for all sample sizes:
t_value = tinv(P, samples - 1);

fprintf('alpha = %0.3f\n', alpha)
fprintf('P (two-sided test) = %0.3f\n', P)


%% Plots
figure('name', 'Student''s t distribution')

subplot(2,1,1)
plot(x, t_dist_1)
hold on
plot(x, t_dist_10)
plot(x, t_dist_end)
plot(x, norm_dist, 'linestyle', '--')
title({'Student''s t-distribution'; sprintf('DOF = %d : %d', dof(1), dof(end))})
xlabel('x')
ylabel('P(x)')
legend(sprintf('DOF = %d', dof(1)), sprintf('DOF = %d', 10),...
    sprintf('DOF = %d', dof(end)), 'Normal dist.')

subplot(2,1,2)
plot(samples, t_value)
h = refline(0,1.96);
set(h, 'color', 'r', 'linestyle', '--')
text(3, 1.7, '\uparrow 1.96')
xlabel('Samples (#)')
ylabel('t-value')
title({'t-value for different sample sizes'; sprintf('Cumulative probability (2-sided) = %0.3f', P)})




