% Sample code to deal with the case when a destination file already exist
% and we want the user to select what to do next.
% 
% � July 2016 Alessandro Timmi

% To see this code in action you need to create a file here with this name:
outputFilename = 'C:\Users\Alex\Desktop\test.mot';

% Check if the output file already exists:
if isequal(exist(outputFilename, 'file'), 2)
    fprintf('This output file already exists:\n\t%s\n', outputFilename)
    button = questdlg('Output file already exists. Overwrite?', 'Overwrite?',...
        'Yes', 'Rename', 'Exit', 'Rename');
    switch button
        case 'Yes'
            % do nothing, the file will be overwritten.
            fprintf('Overwriting file:\n\t%s.\n', outputFilename)
        case 'Rename'
            [outputName, outputPath] = uiputfile('*.mot', 'Select a new output filename (.mot)');
            if isempty(outputName)
                error('No output filename (.mot) was selected, exiting the analysis...\n')
            end
            outputFilename = fullfile(outputPath, outputName);
            fprintf('New output file selected:\n\t%s.\n', outputFilename)
        case {'Exit', ''}
            error('The user selected to exit the analysis...\n')
    end
end