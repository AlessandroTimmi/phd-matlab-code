% Angle between 2 vectors.
% Inspired by this Matlab forum thread:
%   http://au.mathworks.com/matlabcentral/newsreader/view_thread/151925
%
% � July 2016 Alessandro Timmi
clear
clc
close all


% Input 3D vectors:
a = [1; 0; 0];
b = [-1; 0.5; 0];

% acos() formula is defined in the interval [0, pi].
% WARNING: this formula only works between 0 and 180 degrees, due to the
% interval in which acos() is defined. Moreover, it is less precise than
% atan2 when the angle is close to 0 or pi.
theta_acos = rad2deg( acos(dot( a / norm(a), b / norm(b) ) ) );

% atan2() planar formula. atan2() is defined in the interval [-pi, pi].
% WARNING: this formula only works in 2D, when the plane is already
% defined. In this case the plane is XY, hence Z coords are ignored:
theta_atan2_2D = atan2d(b(2), b(1)) - atan2d(a(2), a(1));
% Same operation as above, but defined in the interval [-360,0] as in
% OpenSim knee angle:
theta_atan2_2D_360 = mod( atan2d(b(2), b(1)) - atan2d(a(2), a(1)), -360);

% atan2() 3D formula.
% WARNING: this formula always returns the smallest angle between the 2
% vectors, because it is based on the cross product, which in turn is based
% on the right hand rule. Depending on the application, this solution may
% or may not be what we want:
theta_atan2_3D = atan2d( norm(cross(a, b)), dot(a, b) );


% Print results:
fprintf('Results in degrees:\n')
fprintf('acos(): %0.1f\n', theta_acos);
fprintf('atan2() 2D [-180, 180]: %0.1f\n', theta_atan2_2D)
fprintf('atan2() 2D [0, -360]: %0.1f\n', theta_atan2_2D_360)
fprintf('atan2() 3D: %0.1f\n', theta_atan2_3D)


figure('Name', 'Input vectors')
plot3([0, a(1)], [0, a(2)], [0, a(3)], 'r')
hold on
plot3([0, b(1)], [0, b(2)], [0, b(3)], 'g')
xlabel('X')
ylabel('Y')
zlabel('Z')
title('Input vectors')
axis equal


% Test for shifting domain interval of atan2d():
x = -180 : 0.1 : 180;
y = mod(x, -360);
figure('Name', 'Shifting domain interval of atan2d()')
plot(x, y, '.')
title('Shifting domain interval of atan2d() from [-180�, 180�] to [-360�, 0�]')
xlabel('Input angle [�]')
ylabel('Output angle [�]')
