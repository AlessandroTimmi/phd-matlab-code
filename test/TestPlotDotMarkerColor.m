close all
clc

% NOTE: the color of the marker '.' (dot) is given via 'MarkerEdgeColor'
% and not via 'MarkerFaceColor'!!!!
% If you don't specify 'MarkerEdgeColor', Matlab assigns a default colour
% to the marker!

figure
plot(1:10)
hold on
plot(3,3, '.', 'MarkerSize', 28, 'MarkerEdgeColor', [1,0,0], 'MarkerFaceColor', [0,0,0]);


