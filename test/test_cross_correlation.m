clc
clear
close all

fprintf('Test cross-correlation\n')
fprintf('� September 2015 Alessandro Timmi\n')

% Data:
x=0 : 0.01 : 2.5 * pi;
y1 = sin(x);
x=0 : 0.01 : 2.5 * pi;
y2 = cos(x);

title_1 = 'Raw data';
figure('Name', title_1)
subplot(4,1,1)
plot(y1)
hold on
plot(y2)
title(title_1)


% Alignsignals() by Matlab:
[y3,y4,d] = alignsignals(y1,y2);
fprintf('Delay found by alignsignals(): %d.\n', d)
subplot(4,1,2)
plot(y3)
hold on
plot(y4)
title('Data synced by alignsignals()')

% Ale's method
[cc, lags] = xcorr(y1 - mean(y1), y2 - mean(y2));
[max_cc, id_max] = max(cc);
shift = lags(id_max);
fprintf('Shift found by xcorr(): %d.\n', shift)
[pks, id_pks] = findpeaks(cc);
minor_shift = lags(id_pks(1));
fprintf('Minor peak found by findpeaks: %d.\n', minor_shift)

% c2 = normxcorr2(y1,y2);
% [max_c2, id_c2] = max(c2);
% shift_c2 = lags(id_c2);
% fprintf('Shift found by normxcorr2(): %d.\n', shift_c2)

subplot(4,1,3)
plot(cc)
hold on
plot(id_max, max_cc, 'rx')
plot(id_pks, pks, 'gx')
title('Cross-correlation')


subplot(4,1,4)
plot(y1)
hold on
plot([zeros(1, shift), y2])
title('Data synced using xcorr()')

% figure
% plot(c2)


