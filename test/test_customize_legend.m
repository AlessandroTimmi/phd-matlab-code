% Customize plot legend in order to display only selected lines.
% � November 2015 Alessandro Timmi

close all
clc
clear

x1 = cos(0:0.01:2*pi);
x2 = x1 * 2;
x3 = x1 * 3;

xx = 1:length(x1);

figure
% Add a handle to each plot line:
h1 = plot(xx,x1, 'r');
hold on
h2 = plot(xx,x2 , 'g');
h3 = plot(xx,x3, 'b');
% Only display legend for selected handles:
legend([h1, h3], {'trial1', 'trial3'});

