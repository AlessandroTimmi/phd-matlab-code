% Comparison between population standard deviation, sample standard
% deviation and root mean square error

clear
close all
clc

x = 1:10;
y = 1.1 * x;

% Array of the differences between x and y:
dif = x - y;

bias = mean(dif);

% Size of the data:
n = length(dif);

% Using matlab functions:
% Population standard deviation (divisor = n)
sigma_matlab = std(dif, 1);
% Sample standard deviation (divisor = n-1)
s_matlab = std(dif);
% Root mean square error (RMS):
rms_matlab = rms(dif);


% Using equations:
% My population standard deviation:
sigma_my = sqrt( sum((dif - bias).^2) / n );
% My sample standard deviation:
s_my = sqrt( sum((dif - bias).^2) / (n - 1) );
% My RMS error:
rms_my = sqrt( sum(dif.^2) / n );


fprintf('Sigma matlab = %0.4f\n', sigma_matlab)
fprintf('SD matlab = %0.4f\n', s_matlab)
fprintf('RMS matlab = %0.4f\n\n', rms_matlab)
fprintf('Sigma my = %0.4f\n', sigma_my)
fprintf('SD my = %0.4f\n', s_my)
fprintf('RMS my = %0.4f\n', rms_my)


