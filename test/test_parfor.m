clear
close all
clc

cicles = 10^8;

% Serial:
tic

a=0;
for i=1:cicles
    a = a + 1;
end

toc

%% Parallel:
tic

a=0;
parfor i=1:cicles
    a = a + 1;
end

toc