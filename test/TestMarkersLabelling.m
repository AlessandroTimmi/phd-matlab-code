function TestMarkersLabelling()
% Labelling markers in dynamic trials, based on distances between rigidily
% connected markers measured in a labelled static trial.
%
% Input:
%   dynamicTrialFilename = filename of the dynamic trial to be labelled.
%   staticTrialFilename = filename of the static trial to be used as
%   reference for labelling.
%   segments = cell array where each row contains a pair of marker names,
%              representing 2 rigidly connecter markers. Markers are
%              considered rigidly connected when they are attached to the
%              same body segment.
%
% Output:
%  d =  struct containing the labelled dynamic trial.
%   
%
% � August 2016 Alessandro Timmi

% TODO:
% - if multiple segments have similar length, the program might return the
% wrong distance match between segments measured in the static and dynamic
% trials. We should probably implement another heuristic.
% - at the moment we clear the indices of rigid pairs matched using distances.
% however this approach is biased towards the first pairs: an error of
% detection at the beginning of the process will affect the detection of
% the following pairs. The alternative is not clearing the indices of
% matched pairs, but we might get duplicate matches for the same pair.
% - ASIS markers have low connectivity, so their detection can be randomly
% correct or swapped.


%% Tolerance on standard deviation of the distance between rigidly connected
% markers [m]. It is essential that this parameter is properly tuned. If
% too small, not enough rigid segments will be detected in the dynamic
% trial, leaving some rigid pairs out of the labelling process.
% If too large, non-rigid pairs of markers will be detected as rigid. While
% this might not seem a problem, it actually will allow non-rigid pairs
% to be associated to segments with similar mean distance, in turn
% affecting labelling. Reasonable values are 0.010 m for slow trials
% (e.g. squat) and 0.012 m for fast trials (e.g. landing).
sdThreshold = 0.012;

fprintf('Labelling markers based on static trial...')


%% Load coloured markers names:
colouredMarkers = markerset('CustomMarkers_single_leg_v1.6');
% Load Kinect markerless joints names:
markerlessJoints =  markerset('KinectMarkerlessSkeleton');

% Load static trial:
staticFilename = 'C:\TEST_FOLDER\ACL study - Kinect Vicon agreement - latest 14 subjects\Kinect\2 - pre-processed\DEV90\static_02_pre-processed.trc';
s = LoadTrcFile('filename', staticFilename,...
    'logMessage', sprintf('Loading static .trc trial...\n'));

% Load dynamic trial:
%dynamicFilename = 'C:\TEST_FOLDER\ACL study - Kinect Vicon agreement - latest 14 subjects\Kinect\3 - pre-processed and OpenSim results\DEV90\DVJDoubleLeg_04.trc';
dynamicFilename = 'C:\TEST_FOLDER\ACL study - Kinect Vicon agreement - latest 14 subjects\Kinect\3 - pre-processed and OpenSim results\DEV90\SLS_01.trc';
d = LoadTrcFile('filename', dynamicFilename,...
    'logMessage', sprintf('Loading dynamic .trc trial...\n'));
%d = load_trc_file;

% Make a copy of the dynamic trial, to allow checking labelling results at
% the end:
dOrig = d;


%% For testing purposes, we unlabel the dynamic trial.
% 1) remove markerless joints from markers:
fprintf('\nRemoving Kinect v2 markerless joints from dynamic trial, for testing purposes...\n\n')
d.markers = rmfield(d.markers, markerlessJoints);
fprintf('Renaming markers in dynamic trial using generic labels, for testing purposes:\n')
% 2) unlabel coloured markers, replacing names with generic ones (Marker#):
% To ensure algorithm works with randomly ordered markers (as in real world
% scenarios) we sort the unlabelled markers randomly:
randomMarkerNumbers = randperm(length(colouredMarkers));
for i=1:length(colouredMarkers)
    
    d.markers = RenameStructField(d.markers, colouredMarkers{i},...
        sprintf('Marker%d', randomMarkerNumbers(i)));
end


%% List pairs of markers constituting rigid segments. This information
% will be probably stored in the custom markerset file:
segments = {'ASISLeft', 'ASISRight';...
    'Thigh', 'KneeLat';...
    'Tibia', 'KneeLat';...
    'KneeLat', 'Malleolus';...
    'Tibia', 'Malleolus';...
    'Malleolus', 'Metatarsus'};

% Number of segments:
nSegments = size(segments, 1);

% Get mean and standard deviation of the segments lengths from the static
% trial:
meanDistancesStatic = zeros(1, nSegments);
sdDistancesStatic = zeros(1, nSegments);
for i=1:nSegments
    
    [~, avg, sd] = DistanceTwoDynamicPoints(s.markers.(segments{i,1}),...
        s.markers.(segments{i,2}));
    
    meanDistancesStatic(i) = avg;
    sdDistancesStatic(i) = sd;
end

clear avg sd


%% Plot static data:
figure('Name', 'Static and dynamic distances')
customLabelsX = cell(size(segments, 1), 1);
for i=1:size(segments, 1)
    customLabelsX{i} = strjoin(segments(i,:));
end

axImage = subplot(2,2,1);
plot(sdDistancesStatic, '.-', 'markersize', 10)
ylabel('SD of distance [m]')
title('SD of distances (static)')
% Need to set the correct number of ticks, otherwise Matlab will repeat
% the custom tick labels:
axImage.XTick = 1:length(sdDistancesStatic);
axImage.XTickLabel = customLabelsX;
axImage.XTickLabelRotation = 45;

axImage = subplot(2,2,2);
plot(meanDistancesStatic, '.-', 'markersize', 10)
ylabel('Mean distance [m]')
title('Mean distances (static)')
% Need to set the correct number of ticks, otherwise Matlab will repeat
% the custom tick labels:
axImage.XTick = 1:length(meanDistancesStatic);
axImage.XTickLabel = customLabelsX;
axImage.XTickLabelRotation = 45;


%% Get mean and standard deviation of the distance over time between ALL
% possible pairs of markers from the dynamic trial.
meanDistancesDynamic = NaN(length(colouredMarkers));
sdDistancesDynamic = NaN(length(colouredMarkers));
for r=1:length(colouredMarkers)
    for c = 1:length(colouredMarkers)
        if r == c || r > c
            % They are symmetric matrices, we don't need their lower
            % half, nor their diagonals (which refer to the distance of a
            % point from itself):
            continue
        end
        
        % Calculate mean and standard deviation of distance over time
        % between current pair of markers:
        [~, avg, sd] = DistanceTwoDynamicPoints(...
            d.markers.(sprintf('Marker%d', r)),...
            d.markers.(sprintf('Marker%d', c)) );
        
        % Fill the matrices:
        meanDistancesDynamic(r,c) = avg;
        sdDistancesDynamic(r,c) = sd;
        
    end
end
clear avg sd
fprintf('\n')


%% Find all rigidly connected pairs in the dynamic trial. Their standard
% deviation should be below the threshold. At this stage, there might be
% more solutions than the actual segments:
indicesRigidPairs = find(sdDistancesDynamic < sdThreshold);

% Convert linear indices to matrix subscripts, for plotting only:
[rowsRigidPairs, columnsRigidPairs] =...
    ind2sub(size(sdDistancesDynamic), indicesRigidPairs);

% Match segments from the static trial with rigid pairs from the dynamic trial.
% Since the segments are rigid, their length measured in the dynamic trial
% should be the same measured in the static trial (given a certain tolerance):
indicesMatchedPairs = zeros(1, nSegments);
marker1 = zeros(nSegments, 1);
marker2 = zeros(size(marker1));
fprintf('List of corresponding distances detected between static and dynamic trials:\n')
for i = 1 : nSegments
    
    % Calculate the absolute values of the differences between a static
    % distance and all dynamic distances of rigid pairs:
    differencesStaticDynamic = abs(meanDistancesStatic(i) -...
        meanDistancesDynamic(indicesRigidPairs));
    % Get the index of the minimum difference, corresponding to the dynamic
    % distance most similar to the static one:
    [~, indexMinDifference] = min(differencesStaticDynamic);
    % Get the corresponding index in the matrix of dynamic mean distances:
    indicesMatchedPairs(i) = indicesRigidPairs(indexMinDifference);
    % Convert this linear index to matrix subscripts, corresponding to 
    % markers numbers:
    [marker1(i), marker2(i)] = ind2sub(size(meanDistancesDynamic),...
        indicesMatchedPairs(i));
    % Clear index of this matched rigid pair, to avoid detecting it again.
    % [TODO: not ideal because biased towards the initial segments. An error
    % of detection will affect the detection of the following segments 
    % as well, because the previous index has been cleared]
    indicesRigidPairs(indexMinDifference) = [];
    
    fprintf('%s-%s (static):\t%0.3f m,\t(%d, %d) (dynamic): %0.3f m.\n',...
        segments{i,1}, segments{i,2}, meanDistancesStatic(i),...
        marker1(i), marker2(i), meanDistancesDynamic(indicesMatchedPairs(i)))
    
end

% Array of paired markers:
pairedMarkers = [marker1, marker2];
fprintf('\nPaired markers:\n')
fprintf('%d, %d\n', pairedMarkers');


%% Plot dynamic data:
axImage = subplot(2,2,3);
hImage = imagesc(sdDistancesDynamic);
set(hImage, 'AlphaData', ~isnan(sdDistancesDynamic))
hColorbar = colorbar('location', 'westoutside');
hColorbar.Label.String = 'SD of distances [m]';
hColorbar.Label.FontSize = 10;
xlabel('Marker [#]')
ylabel('Marker [#]')
title('SD of distances (dynamic)')
axis square
axImage.XAxisLocation = 'top';
axImage.YAxisLocation = 'right';
hold on
% Need to invert coordinates for plotting, because column = X and row = Y:
hRigid = plot(columnsRigidPairs, rowsRigidPairs, 'r.', 'markersize', 10);
hMatchedPairs = plot(marker2, marker1, 'go', 'linewidth', 1);
legend([hRigid, hMatchedPairs],...
    {sprintf('Rigid pairs (SD < %0.3f m)', sdThreshold),...
    'Pairs matched to segments'}, 'location', 'SouthWest')

axImage = subplot(2,2,4);
hImage = imagesc(meanDistancesDynamic);
set(hImage, 'AlphaData', ~isnan(meanDistancesDynamic))
hColorbar = colorbar('location', 'westoutside');
hColorbar.Label.String = 'Mean of distances [m]';
hColorbar.Label.FontSize = 10;
xlabel('Marker [#]')
ylabel('Marker [#]')
title('Mean distances (dynamic)')
axis square
axImage.XAxisLocation = 'top';
axImage.YAxisLocation = 'right';
hold on
% Need to invert coordinates for plotting, because column = X and row = Y:
hRigid = plot(columnsRigidPairs, rowsRigidPairs, 'r.', 'markersize', 10);
hMatchedPairs = plot(marker2, marker1, 'go', 'linewidth', 1);
legend([hRigid, hMatchedPairs],...
    {sprintf('Rigid pairs (SD < %0.3f m)', sdThreshold),...
    'Pairs matched to segments'}, 'location', 'SouthWest')


%% Identify markers in pairs using "connectivity", which is the number of
% connections departing from each marker, imagined as a node in a graph.
connectivity = zeros(1, length(colouredMarkers));

% Find number of occurrencies for each marker.
% Solution found here:
% http://au.mathworks.com/matlabcentral/answers/96504-how-can-i-count-the-occurrences-of-each-element-in-a-vector-in-matlab
for i = 1:length(colouredMarkers)
    connectivity(i) = sum([marker1; marker2] == i);
    
    % No marker can be disconnected from the others. If this is the case,
    % it means we didn't found all rigid segments in the dynamic trial.
    % The reason can be noise in the trial above the pre-set threshold on
    % the standard deviation.
    assert(connectivity(i)>0, ['Marker %d is not connected to any '...
        'other marker.\nThis can be caused by noisy marker coordinates.\n'...
        'Try filtering the dynamic trial or increasing the threshold\n'...
        'of the standard deviation of the distance (currently %0.3f m)'],...
        i, sdThreshold)
end

fprintf('\nConnectivity:\t')
fprintf('%d\t', connectivity)
fprintf('\n')
fprintf('Marker indices:\t')
fprintf('%d\t', 1:length(colouredMarkers))
fprintf('\n')

% Sort connectivity from largest to smallest and get corresponding markers
% indices:
[sortedConnectivity, markersConnectivityIndices] = sort(connectivity, 'descend');

fprintf('\nSorted connectivity:\t')
fprintf('%d\t', sortedConnectivity)
fprintf('\n')
fprintf('Markers indices:\t\t')
fprintf('%d\t', markersConnectivityIndices)
fprintf('\n\n')

% We cannot preallocate this cell array using cell(), because it will
% create a cell of empty matrices, which are not compatible with functions
% working on strings. Given the very small size of the final cell array, we
% don't really need to preallocate space for it.
labelledMarkers = {};

for i = 1:length(markersConnectivityIndices)
    % Find all pairs containing current marker: we use the [row, column]
    % syntax and get only the rows, which represent the pair numbers:
    [indicesPairsContainingMarker, ~] = find(pairedMarkers == markersConnectivityIndices(i));
    
    % Get corresponding marker names from segments:
    markersInSelectedPairs = segments(indicesPairsContainingMarker, :);
    
    % Remove already found markers from this list. We cannot use setdiff(),
    % because it also removes repetitions of other strings.
    unlabelledMarkersInSelectedPairs = markersInSelectedPairs(...
        ~ismember(markersInSelectedPairs, labelledMarkers));
        
    % Find the most frequently nominated (mode) marker in the remaining list:
    % Solution found here:
    % http://stackoverflow.com/questions/17450233/most-frequent-element-in-a-string-array-matlab
    [uniqueMarkers, ~, stringsMap] = unique(unlabelledMarkersInSelectedPairs);
    modeMarkerName = uniqueMarkers(mode(stringsMap));
    
    % Label current marker in the dynamic struct, using most common marker
    % name just found:
    d.markers = RenameStructField(d.markers,...
        sprintf('Marker%d', markersConnectivityIndices(i)), modeMarkerName{1});
    
    % Add marker name to the list of labelled markers:
    labelledMarkers(i) = modeMarkerName;  %#ok<AGROW>
end


%% Check results vs original (test) structure, comparing marker by marker:
fprintf('\n')
for i = 1:length(colouredMarkers)
    if ~isequal(d.markers.(colouredMarkers{i}), dOrig.markers.(colouredMarkers{i}))
        warning('Incorrect detection of marker %s.', colouredMarkers{i});
    end
end
fprintf('All markers detected successfully.\n\n')


end

