clear
clc
close all

% Input function: sin between 0 and 6*pi
x = 0:1:6*pi;
y = sin(x);


%% Test resampling to 100 samples:
% - Using resample():
yr = resample(y, 100, length(y));

% - Using interp1():
xi = linspace(0, x(end), 100);
yi = interp1(x, y, xi, 'linear');

% Plots
figure
subplot(2,1,1)
plot(y);
xlabel('Samples')
title('Original signal')

subplot(2,1,2)
hold on
plot(yr,' o:')
plot(yi, '-x')
xlabel('Samples')
title('Resampled signal')
legend('resample()', 'interp1()')


figure
plot(x,y)
hold on
plot(xi,yi, '--')
legend('Original','Interp1')