% test reg exp trials names
close all
clear
clc

% sample strings:
% C:\Users\atimmi\Desktop\Treadmill_22June2016\Kinect\TML14\gait_fast_01.trc
% C:\Users\atimmi\Desktop\Treadmill_22June2016\Vicon\TML14\gait_fast_01.c3d


%% Read all required filenames from the root folder containing all Kinect
% and Vicon data from a treadmill session for multiple subjects:
[~, ~,...
    kinect, vicon] =...
    root_folder_lister_agreement_study('TML',...
    'root_folder', 'C:\Users\atimmi\Desktop\Treadmill_22June2016');


for i=1:length(kinect)

    
    % This expression specifies that the string we are interested in:
    %   - begins with "TML"
    %   - followed by 2 numeric digits (\d\d)
    %   - followed by a slash (\\)
    %   - followed by 1 or more alpha-numeric characters (\w+)
    %   - ends with 2 numeric digits (\d\d)
    expression = 'TML\d\d\\\w+\d\d';

    vicon_trial_name = regexp(vicon{i}, expression, 'match');
    kinect_trial_name = regexp(kinect{i}, expression, 'match');
    
    assert(isequal(vicon_trial_name{1}, kinect_trial_name{1}),...
        'Strings mismatch:\nVicon:\t\t%s\nKinect:\t\t%s',...
        vicon_trial_name{1}, kinect_trial_name{1});
end