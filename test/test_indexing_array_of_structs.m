% Indexing array of structs
% � February 2016 Alessandro Timmi

a(1).b = 10;
a(2).b = 20;
a(3).b = 30;
a(4).b = 40;

% Simply indexing an array of struct using colon (:) returns separated
% answers:
a(:).b

% But putting square brackets around it will store data into an array:
[a(:).b]
