clc
clear
close all


PATCH_NAMES = {'FaceColor', 'FaceAlpha', 'EdgeColor'};
PATCH_VALUES = {[0,0,1], 0.2, 'none'};


x = 0:0.1:10;
y = sin(x);

low_ci = y - 0.2;
upp_ci = y + 0.2;


figure('Name', 'Test shaded area')
plot(x, y, 'r')
hold on

% Shaded area:
% NOTE: if upp_ci and low_ci are column vectors, remember to transpose them
% and be careful where you put the transpose symbol ('). It must be within
% flipr() and not outside it, otherwise you will get wrong results!
hp = fill([x, fliplr(x)], [upp_ci, fliplr(low_ci)], 'b');
set(hp, PATCH_NAMES, PATCH_VALUES)

% Rectangle
fill([x(1), x(end), x(end), x(1)], [0.4, 0.4, 0.2, 0.2], 'g', 'FaceAlpha', 0.2)

legend('sin(x)', 'sin \pm CI', 'Constant CI', 'location', 'best')
title('Test shaded area')
xlabel('x')
ylabel('y')