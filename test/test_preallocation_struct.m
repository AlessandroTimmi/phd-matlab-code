clc
clear

fprintf('Tests on preallocation and concatenation of arrays and structs.\n')
fprintf('� September 2015 Alessandro Timmi\n\n')
fprintf('Tip 1: run this script using the button "Run and Time",\nto enable Matlab Profiler.\n')
fprintf('Tip 2: feel free to comment out the slowest cases in the script,\nto highlight the differences among the more efficient ones.\n\n')

a = ones(100,1);
n = 100000;

fprintf('Length of sample array to be "grown" (copied) a: %d.\n', length(a));
fprintf('Number of "growing" iterations n: %d.\n', n);
expected_length = length(a) * n;
fprintf('Expected length of the resulting array: %g\n\n', expected_length)


%% 1) This is the GOLD STANDARD in terms of speed. However, this solution can
% be only used if data to be concatenated are exactly the same.
b1 = repmat(a, n, 1);


%% 2) Growing an array, without preallocation: NOT OPTIMAL and TOO SLOW!
% b2 = [];
% for i=1:n
%     b2 = [b2; a];
% end
% assert(isequal(b2, b1));


%% 3) Prealloating the array, but manually concatenating its parts:
b3 = zeros(expected_length, 1);
for i=1:n
    start = 1 + (i-1) * length(a);
    finish = i * length(a);
    b3(start:finish, 1) = a;
end
assert(isequal(b3, b1));


%% 4) Growing struct field without preallocation: NOT OPTIMAL and TOO SLOW!
% b4.appo = [];
% for i=1:n
%     b4.appo = [b4.appo; a];
% end
% assert(isequal(b4.appo, b1));


%% 5) Array of structures and manually concatenated array:
s5 = struct;             
len5 = 0;
% Read data:
for i=1:n
    s5(i).fld = a;
    len5 = len5 + length(a);
end
b5 = zeros(len5, 1);
% Write data:
for i=1:n
    start = 1 + (i-1) * length(a);
    finish = i * length(a);
    b5(start:finish, 1) = s5(i).fld ;
end
assert(isequal(b5, b1));


%% 6) Array of structures and vertcat(): BEST SOLUTION WHEN YOU NEED TO
% STORE VECTORS OF DIFFERENT LENGTHS.
% NOTICE: Matlab cannot index structs with more than one level
% using colon (:).
% Hence we need to reduce the two-level array of structs:
% Trial(1).markers.Marker_1
% Trial(1).markers.Marker_2
% Trial(2).markers.Marker_1
% Trial(2).markers.Marker_2
% ...
% Trial(N).markers.Marker_1
% Trial(N).markers.Marker_2
% to one-level before indexing it. In this way we obtain an
% array of structs with this structure (for both Vicon and
% Kinect):
% Trial(1).Marker_1 .Marker_2 ... .Marker_M;
% Trial(2).Marker_1 .Marker_2 ... .Marker_M;
% ...
% Trial(N).Marker_1 .Marker_2 ... .Marker_M;
% To do so we can use horzcat:
% vsm = horzcat(vicon_storage(:).markers);
% ksm = horzcat(kinect_storage(:).markers);
% then apply vertcat as shown in the example below.

s6 = struct;
len6 = 0;
% Read data:
for i=1:n
    s6(i).fld = a;
    len6 = len6 + length(a);
end
% Write data:
b6 = vertcat(s6(:).fld);
assert(isequal(b6, b1));


% Since all the assertions are passed, all arrays are equal:
fprintf('OK! All generated arrays are equal.\nDone.\n')



