function [locs_FS, locs_TO, pks_FS, pks_TO, mean_stride_duration,...
    mean_stance_percentage] = detect_gait_events_simple(s, varargin)
% Detect gait events during walking or running from kinematic data. This
% version of the algorithm only relies on the ankle marker X coordinate to
% detect foot strike (FS) and toe off (TO) events for both gait and
% running trials.
%
% This algorithm has been exclusively tested on treadmill trials with
% Kinect positioned on the side. The direction of progression is expected
% to be X and the motion is expected to occur in the XY plane.
%
% Only full strides are detected, which are delimited by 2 consecutive FS
% events. Any TO event before the first FS or after the last FS is ignored.
%
%   Input:
%       s = struct containing trial data.
%       min_peak_interval = (optional parameter, default = 0.15 s) min
%           time interval between consecutive peaks, to avoid false
%           detections.
%       min_peak_prominence_ANK = (optional parameter, default = 0.1 m)
%           min prominence that peaks need to have to be detected in the
%           ANK position along the direction of progression.
%       debug_mode = (optional parameter, default = false) if true shows
%           shows some extra info for debug purposes.
%
%   Output:
%       locs_FS = locations (in frames) of FS events.
%       locs_TO = locations (in frames) of TO events.
%       pks_FS = X coordinate of ankle marker corresponding to FS events.
%       pks_TO = X coordinate of ankle marker corresponding to TO events.
%       mean_stride_duration = mean of the duration of all the detected
%           strides, in seconds. A stride is the interval between two
%           consecutive FS events.
%       mean_stance_percentage = ratio between mean length (in frames) of
%           the stance phase over mean lenght of the whole stride (%). The
%           stance phase is the interval between FS and the following TO.
%
% � July 2016 Alessandro Timmi


% TODO:
% - double check SD threshold.


%% Default input values:
default.min_peak_interval = 0.15;
default.min_peak_prominence_ANK = 0.1;
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input argument:
addRequired(p, 's', @isstruct);

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'min_peak_interval', default.min_peak_interval,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative'}));
addParameter(p, 'min_peak_prominence_ANK', default.min_peak_prominence_ANK,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative'}));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, s, varargin{:});

% Copy input arguments into more memorable variables:
s = p.Results.s;
min_peak_interval = p.Results.min_peak_interval;
min_peak_prominence_ANK = p.Results.min_peak_prominence_ANK;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
fprintf('\nSimple gait events detection based on ankle marker X...\n')
if debug_mode
    fprintf('� July 2016 Alessandro Timmi.\n')
end


%% Check that X is the direction of progression in this trial.
% We assume that the direction of progression is the one having higher
% variability for the ankle marker:
sd_ANK = zeros(3,1);
for i=1:3
    sd_ANK(i) = std(s.markers.ANK(:,i));
    if debug_mode
        fprintf('[Debug] SD along %s axis: %0.3f m.\n', num2coord(i), sd_ANK(i));
    end
end

[~, progression_dir_num] = max(sd_ANK);
progression_dir_char = num2coord(progression_dir_num);
assert(strcmpi(progression_dir_char, 'X'),...
    'Unexpected direction of progression detected: %s instead of X',...
    progression_dir_char);
fprintf('Detected direction of progression: %s.\n', progression_dir_char);


%% Check that no major motion occurs along Z.
% Z should be the mediolateral axis, therefore the variability of the ankle
% marker coordinate along this axis should be less than along the other axes.
[~, mediolateral_dir_num] = min(sd_ANK);
mediolateral_dir_char = num2coord(mediolateral_dir_num);
assert(strcmpi(mediolateral_dir_char, 'Z'),...
    'Unexpected mediolateral direction detected: %s instead of Z',...
    mediolateral_dir_char);
fprintf('Detected mediolateral direction: %s.\n', mediolateral_dir_char);


% Convert min_peak_interval from s to frames:
[~, ~, ~, avg_freq_t] = check_sampling_freq(s.time);
min_peak_distance = min_peak_interval * avg_freq_t;


%% Detect gait events
% FS events are identified in correspondence of the POSITIVE PEAKS in
% the ankle marker position along the direction of progression:
[pks_FS, locs_FS] = findpeaks(s.markers.ANK(:,progression_dir_num),...
    'MinPeakProminence', min_peak_prominence_ANK,...
    'MinPeakDistance', min_peak_distance);

% TO events are identified in correspondence of the NEGATIVE PEAKS in
% the ankle marker position along the direction of progression:
[pks_TO, locs_TO] = findpeaks(-s.markers.ANK(:,progression_dir_num),...
    'MinPeakProminence', min_peak_prominence_ANK,...
    'MinPeakDistance', min_peak_distance);
% Revert to the initial sign of the ankle marker coordinate:
pks_TO = -pks_TO;


%% Strides can be considered complete only when they start and finish with
% an FS event. Therefore, we remove any TO event located before the first
% FS event and after the last FS event:
if locs_TO(1) < locs_FS(1)
    locs_TO(1) = [];
    pks_TO(1) = [];
end
if locs_TO(end) > locs_FS(end)
    locs_TO(end) = [];
    pks_TO(end) = [];
end


%% Verify that all peaks have been detected.
% If some events were not found, detected peaks will be irregularly spaced:
% therefore, the standard deviation of their locations will be higher than
% usual.
sd_diff_locs_FS = std(diff(locs_FS));
sd_diff_locs_TO = std(diff(locs_TO));

sd_diff_locs_FS_time = sd_diff_locs_FS / avg_freq_t;
sd_diff_locs_TO_time = sd_diff_locs_TO / avg_freq_t;

if debug_mode
    fprintf('[Debug] SD diff. locs FS = %0.3f s\n', sd_diff_locs_FS_time)
    fprintf('[Debug] SD diff. locs TO = %0.3f s\n', sd_diff_locs_TO_time)
end

% Check standard deviations against thresholds:
if sd_diff_locs_FS_time > min_peak_interval
    warning(...
        'FS events detection might have been unsuccessful (SD diff. locs > %0.2f s). See plots for more info.',...
        min_peak_interval)
end

if sd_diff_locs_TO_time > min_peak_interval
    warning(...
        'TO events detection might have been unsuccessful (SD diff. locs > %0.2f s). See plots for more info.',...
        min_peak_interval)
end


if debug_mode
    %% Plot detected gait events:
    figure('Name', 'Gait events detection')
    plot(s.time, s.markers.ANK(:,progression_dir_num), 'k')
    hold on
    plot(s.time(locs_FS), pks_FS, 'Marker', '>', 'linestyle', 'none', 'MarkerFaceColor', 'b');
    plot(s.time(locs_TO), pks_TO, 'Marker', '<', 'linestyle', 'none', 'MarkerFaceColor', 'r');
    xlabel('Time [s]')
    ylabel('ANK_X position [m]')
    title(sprintf('Gait events detection from peaks in ANK_%s position', progression_dir_char))
    legend('ANK_X position', 'FS events', 'TO events', 'location', 'northeastoutside')
end


%% Calculate mean stride properties (in frames)
% Stride: interval between 2 consecutive FS:
mean_stride_length = mean(diff(locs_FS));
mean_stride_duration = mean_stride_length / avg_freq_t;
% Stance: interval between FS and the following TO:
mean_stance_length = mean(locs_TO - locs_FS(1:end-1));
% Percent of the stance phase over the entire stride:
mean_stance_percentage = mean_stance_length / mean_stride_length * 100;

if debug_mode
    fprintf('[Debug] Mean stride length: %0.1f frames.\n', mean_stride_length);
    fprintf('[Debug] Mean stride duration: %0.2f s.\n', mean_stride_duration);
    fprintf('[Debug] Mean stance length: %0.1f frames.\n', mean_stance_length);
    fprintf('[Debug] Mean stance percentage: %0.1f%%.\n', mean_stance_percentage);
end

fprintf('\n')


end

