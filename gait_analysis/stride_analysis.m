function stride_analysis(vicon_storage, kinect_storage, varargin)
% Statistical analysis of multiple strides contained in multiple
% synchronized Vicon and Kinect trials. Gait events must be already
% detected from Vicon trials using the function detect_gait_events.m.
%
% Input:
%   vicon_storage = array of structs containing marker coordinates from
%           Vicon, already interpolated, synced and trimmed to match those
%           from Kinect. It must also contain gait events data for each trial.
%   kinect_storage = array of structs containing marker coordinates from
%           Kinect data, already interpolated, synced and trimmed to match
%           those from Vicon.
%   fields_to_analyse = (optional parameter, default = 'ANK') name of the
%           markers to be analysed.
%   debug_mode = (optional parameter, default = false) if true shows some
%           extra info for debug purposes.
%
% � February 2016 Alessandro Timmi

% TODO:
% - Adjust plots to take into different numbers of resampling points (e.g.
%   different from 100).
% - the check on the number of coordinates per field should probably be
%   moved to an external function.
% - Similarly for the check on the number of trials for Kinect and Vicon.


%% Default input values:
default.fields_to_analyse = {'ANK'};
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input argument:
addRequired(p, 'vicon_storage', @isstruct);
addRequired(p, 'kinect_storage', @isstruct);

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'fields_to_analyse', default.fields_to_analyse,...
    @(x) validateattributes(x, {'cell'}, {'nonempty'}));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, vicon_storage, kinect_storage, varargin{:});

% Copy input arguments into more memorable variables:
vicon_storage = p.Results.vicon_storage;
kinect_storage = p.Results.kinect_storage;
fields_to_analyse = p.Results.fields_to_analyse;
debug_mode = p.Results.debug_mode;

clear varargin default p


%% PLOT SETTINGS
% RGB colours:
ps.DARK_GRAY = [0.6, 0.6, 0.6];
ps.LIGHT_GRAY = [0.85, 0.85, 0.85];

ps.MY_LINE_WIDTH = 1.5;

ps.AXIS_NAMES = {'Ygrid'};
ps.AXIS_VALUES = {'on'};

ps.REFLINE_NAMES = {'Color', 'linestyle', 'linewidth'};
ps.REFLINE_ZERO = {'k', '--', 1};

ps.TITLE_NAMES = {'interpreter', 'fontweight', 'fontsize'};
ps.TITLE_VALUES = {'none', 'bold', 14};

% Figure handlers:
hfig1 = figure('Name', 'Mean of the differences (Kinect - Vicon)');
hfig2 = figure('Name', 'SD of the differences (Kinect - Vicon)');
hfig3 = figure('Name', 'Mean +/- SD of the differences (Kinect - Vicon)');
hfig4 = figure('Name', 'Velocity (Vicon data)');
hfig5 = figure('Name', 'abs(Velocity) (Vicon data)');
hfig6 = figure('Name', 'Speed (Vicon data)');


% Consistency in the number of coordinates per field has already been
% checked in the Bland-Altman pipeline, so here we simply determine it
% from the first trial:
n_cols = size(vicon_storage(1).markers.(fields_to_analyse{1}), 2);

% Since we want subplots to advance column-wise instead of row-wise (which
% is the default in MATLAB), we need to create custom subplot indices:
ps.sp_ids = columnwise_subplots_ids(numel(fields_to_analyse), n_cols);

% We sample each stride using 100 data points, because it makes easier to
% work in terms of stride percentage. For gait trials at 1.3 m/s we can
% assume a duration of 1.3 s/stride. Using 100 samples, we get a framerate
% of 77 fps, which is a fair sampling rate, given the slow speed of this
% task. With faster trials (e.g. running at 3 m/s), we can assume a
% duration of about 0.73 s/stride. Therefore, we get a framerate of
% 136 fps, which again is a fair sampling rate for this faster type of
% movement. DO NOT CHANGE THIS VALUE OTHERWISE THE PLOTS WILL BE AFFECTED.
n_resampling_points = 100;

% Check if we loaded the same number of trials for Vicon and Kinect:
assert(isequal(numel(vicon_storage), numel(kinect_storage)),...
    'Number of trials is different between Vicon (%d) and Kinect (%d)',...
    numel(vicon_storage), numel(kinect_storage));


%% GO THROUGH EACH VICON TRIAL AND COUNT THE STRIDES
% Preallocate array containing number of strides per trial:
strides_per_trial = zeros(numel(vicon_storage), 1);

for f=1:numel(vicon_storage)
    % Number of strides per trial: remember that each stride is delimited
    % by 2 FS events.
    strides_per_trial(f, 1) = length(vicon_storage(f).locs_FS) - 1;
end
% Total number of strides across all trials:
strides_num = sum(strides_per_trial);


% Go through each marker to be analysed:
for m = 1:numel(fields_to_analyse)
    
    % Preallocate array of velocity components from Vicon data: since the
    % velocities are obtained using finite differences, their length will
    % be 1 element shorter than the array of positions:
    mean_velocity = zeros(n_resampling_points - 1, 3);    
    
    for c = 1:n_cols
        % Preallocate (or overwrite in following loop iterations) arrays of strides data:       
        vicon_strides = zeros(n_resampling_points, strides_num);
        kinect_strides = zeros(size(vicon_strides));
        dif_strides = zeros(size(vicon_strides));
        
        % Initialize stride counter:
        sc = 0;
        for f=1:numel(vicon_storage)
            for i = 1:strides_per_trial(f, 1)
                sc = sc + 1;
                % Load current marker coordinate for current stride of current trial.
                % NOTICE: at the moment we are including FS in both
                % the previous and the next stride. Alternatively, we might exclude
                % the final FS in each stride, picking the frame just before it.
                % However, I think this is indifferent to the purposes of our analysis
                % and it would only make the code less readable.
                this_stride_vicon = vicon_storage(f).markers.(fields_to_analyse{m})...
                    (vicon_storage(f).locs_FS(i):vicon_storage(f).locs_FS(i+1), c);
                this_stride_kinect = kinect_storage(f).markers.(fields_to_analyse{m})...
                    (vicon_storage(f).locs_FS(i):vicon_storage(f).locs_FS(i+1), c);
                
                % Resample data of this stride using 100 datapoints and
                % store them in a 2D array with the following structure:
                % # Stride1 #   # Stride2 #   ...   # StrideN #
                %   [Sample1      Sample1     ...     Sample1;
                %    Sample2      Sample2     ...     Sample2;
                %       :             :        :         :   ;
                %    Sample100    Sample100   ...     Sample100]
                %   
                % NOTICE: we need to calculate the new vector of query points (xq)
                % for each stride, because each stride can originally have a
                % different lenght in terms of frames.
                xq = linspace(0, length(this_stride_vicon), n_resampling_points)';
                vicon_strides(:,sc) = interp1(this_stride_vicon, xq, 'spline');
                kinect_strides(:,sc) = interp1(this_stride_kinect, xq, 'spline');
                
                % Calculate the array of the differences (Kinect - Vicon) for the
                % current stride:
                dif_strides(:,sc) = kinect_strides(:,sc) - vicon_strides(:,sc);
            end
        end
        
        
        %% Calculate arrays of statistical parameters, datapoint by
        % datapoint (row by row):
        mean_dif_strides = mean(dif_strides, 2);
        sd_dif_strides = std(dif_strides, 0, 2);
        
        % To plot the standard deviation as a band around the mean, we need to
        % calculate the mean plus/minus the standard deviation:
        mean_plus_sd_dif = mean_dif_strides + sd_dif_strides;
        mean_minus_sd_dif = mean_dif_strides - sd_dif_strides;
        
        % Average duration of the stance phase expressed as percentage of the
        % duration of the entire stride, calculated across all trials:
        avg_stance_percentage = mean([vicon_storage(:).mean_stance_percentage]);
        
        % Input array for plotting. Leave it as row otherwise we have
        % problems with the fill() function:
        x_stride = linspace(0, n_resampling_points, n_resampling_points);
        
        
        %% Calculate the array of the velocity of the mean stride at each
        % instant, along the analysed coordinate from Vicon data:
        mean_vicon_strides = mean(vicon_strides, 2);
        avg_stride_duration = mean([vicon_storage(:).mean_stride_duration]);
        delta_t_resampled = avg_stride_duration / n_resampling_points;
        mean_velocity(:, c) = diff(mean_vicon_strides) / delta_t_resampled;
        
        % Input array for velocity:
        x_velocity = linspace(0, n_resampling_points, length(mean_velocity))';
        
        
        %% Plot 1: Mean of the differences
        figure(hfig1)
        ax1 = subplot(n_cols, numel(fields_to_analyse), ps.sp_ids(c, m), 'Ygrid', 'on');
        hold on
        % Set the exponent of the tick values for the Y axis:
        ax1.YAxis.Exponent = -3;
        plot(x_stride, mean_dif_strides, 'r', 'linewidth', ps.MY_LINE_WIDTH);
        h_zero = refline(0,0);
        set(h_zero, ps.REFLINE_NAMES, ps.REFLINE_ZERO)
        xlabel('Stride [%]')
        ylabel({'mean(Kinect2 - Vicon)', sprintf('%s [m]', num2coord(c))})
        % Add some text to the plot
        text_stride_plot(avg_stance_percentage)        
        if c==1
            title1 = title(fields_to_analyse{m});
            set(title1, ps.TITLE_NAMES, ps.TITLE_VALUES)
        end
        
        
        % Plot 2: SD of the differences
        figure(hfig2)        
        ax2 = subplot(n_cols, numel(fields_to_analyse), ps.sp_ids(c, m), 'Ygrid', 'on');
        hold on
        % Set the exponent of the tick values for the Y axis:
        ax2.YAxis.Exponent = -3;
        plot(x_stride, sd_dif_strides, 'b', 'linewidth', ps.MY_LINE_WIDTH);
        xlabel('Stride [%]')
        ylabel({'SD(Kinect2 - Vicon)', sprintf('%s [m]', num2coord(c))})
        % Add some text to the plot:
        text_stride_plot(avg_stance_percentage)        
        if c==1
            title2 = title(fields_to_analyse{m});
            set(title2, ps.TITLE_NAMES, ps.TITLE_VALUES)
        end      
        
        
        % Plot 3: Mean +/- SD of the differences
        figure(hfig3)        
        ax3 = subplot(n_cols, numel(fields_to_analyse), ps.sp_ids(c, m), 'Ygrid', 'on');
        hold on
        % Set the exponent of the tick values for the Y axis:
        ax3.YAxis.Exponent = -3;
        h_dif_sd = fill([x_stride, fliplr(x_stride)],...
            [mean_plus_sd_dif', fliplr(mean_minus_sd_dif')],...
            'b', 'FaceAlpha', 0.2, 'EdgeColor', 'none');
        h_dif_mean = plot(x_stride, mean_dif_strides, 'r', 'linewidth', ps.MY_LINE_WIDTH);
        legend([h_dif_mean, h_dif_sd], {'mean(Kinect2 - Vicon)', 'mean \pm SD (Kinect2 - Vicon)'},...
            'location', 'NorthEast')
        xlabel('Stride [%]')
        ylabel({'Kinect2 - Vicon', sprintf('%s [m]', num2coord(c))})
        % Add some text to the plot:
        text_stride_plot(avg_stance_percentage)        
        if c==1
            title3 = title(fields_to_analyse{m});
            set(title3, ps.TITLE_NAMES, ps.TITLE_VALUES)
        end
        
        
        % Plot 4: velocity from Vicon data
        figure(hfig4)
        subplot(n_cols, numel(fields_to_analyse), ps.sp_ids(c, m), 'Ygrid', 'on');
        hold on
        plot(x_velocity, mean_velocity(:,c), 'linewidth', ps.MY_LINE_WIDTH);
        h_zero = refline(0,0);
        set(h_zero, ps.REFLINE_NAMES, ps.REFLINE_ZERO)
        xlabel('Stride [%]')
        ylabel(sprintf('Velocity_%s [m/s]', num2coord(c)))
        % Add some text to the plot
        text_stride_plot(avg_stance_percentage)        
        if c==1
            title4 = title(fields_to_analyse{m});
            set(title4, ps.TITLE_NAMES, ps.TITLE_VALUES)
        end
        
        
        % Plot 5: absolute value of velocity from Vicon data, to be used
        % as a reference for SD which is always positive.
        figure(hfig5)
        subplot(n_cols, numel(fields_to_analyse), ps.sp_ids(c, m), 'Ygrid', 'on');
        hold on
        plot(x_velocity, abs(mean_velocity(:,c)), 'linewidth', ps.MY_LINE_WIDTH, 'color', [0, 0.7, 0.3]);
        xlabel('Stride [%]')
        ylabel(sprintf('abs(velocity_%s) [m/s]', num2coord(c)))
        % Add some text to the plot:
        text_stride_plot(avg_stance_percentage)        
        if c==1
            title5 = title(fields_to_analyse{m});
            set(title5, ps.TITLE_NAMES, ps.TITLE_VALUES)
        end
        
    end
    
    
    %% Calculate marker speed from Vicon data:
    marker_speed = zeros(length(mean_velocity), 1);
    for j=1:length(mean_velocity)
        marker_speed(j,1) = norm([...
                                mean_velocity(j,1),...
                                mean_velocity(j,2),...
                                mean_velocity(j,3)...
                                ]);
    end    

    % Plot 6: speed from Vicon data
    figure(hfig6)
    subplot(numel(fields_to_analyse), 1, m, 'Ygrid', 'on');
    hold on
    plot(x_velocity, marker_speed, 'linewidth', ps.MY_LINE_WIDTH, 'color', 'k');
    xlabel('Stride [%]')
    ylabel('Speed [m/s]')
    % Add some text to the plot:
    text_stride_plot(avg_stance_percentage)
    title6 = title(fields_to_analyse{m});
    set(title6, ps.TITLE_NAMES, ps.TITLE_VALUES)
    
end

end

function text_stride_plot(avg_stance_percent)
% Since sometimes adding a vertical line might change the y limits of the
% plot, we store them before and then re-apply them after:
current_ylim = get(gca, 'ylim');
line([avg_stance_percent avg_stance_percent], current_ylim,...
    'linestyle', '-.', 'linewidth', 1, 'color', 'k');
ylim(current_ylim)

% Add some text to the plot:
text(2, sum(get(gca, 'ylim')) / 2, '\leftarrow FS', 'HorizontalAlignment', 'center')
text(avg_stance_percent + 2, sum(get(gca, 'ylim')) / 2, '\leftarrow TO', 'HorizontalAlignment', 'center')
text(98, sum(get(gca, 'ylim')) / 2, 'FS \rightarrow', 'HorizontalAlignment', 'center')

text(0.5 * avg_stance_percent, sum(get(gca, 'ylim'))/2, 'Stance phase',...
    'fontweight', 'bold', 'HorizontalAlignment', 'center')
text(avg_stance_percent + 0.5 * (100 - avg_stance_percent), sum(get(gca, 'ylim')) / 2, 'Swing phase',...
    'fontweight', 'bold', 'HorizontalAlignment', 'center')

end