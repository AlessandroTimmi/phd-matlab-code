function theta = KneeFlexionAngle2dTreadmillStudy(s)
% Calculate the knee flexion angle from data collected for the treadmill
% study. NOTE: this algorithm only works under the following assumptions:
% i) Kinect is positioned on the side of the treadmill. 
% ii) The gait direction is X for the entire trial.
%
% Input:
%   s = struct containing data from a .trc file recorded during a treadmill
%       trial.
% 
% Output:
%   theta = knee flexion angle according to OpenSim convention. The angle
%         is 0 when the leg is extended and negative when the leg is bent.
%
% � July 2016 Alessandro Timmi



%% Define the following points in the XY plane of Kinect coordinate system:
H = s.markers.HIP(:, 1:2);
K = s.markers.KNE(:, 1:2);
A = s.markers.ANK(:, 1:2);

% Calculate the knee flexion angle using atan2(). This is better than
% using acos(), because atan2() returns the correct sign for the angle.
% The function atan2d() directly returns angles in degrees, which is more
% suitable for our purposes.
% Given:
%   alpha = orientation of the femur in Kinect XY plane;
%   beta = orientation of the tibia in Kinect XY plane;
% We can write the knee flexion angle (theta) as:
%   theta = beta - alpha
alpha = atan2d(K(:,2) - H(:,2), K(:,1) - H(:,1));

% The function atan2d() is defined within [-180, 180]. Sometimes the actual
% knee angle (beta) can be <-180 (e.g. -182), but atan2d() returns the
% corresponding positive value (e.g. +178).
% The same issue happens using atan2(), which returns the angle in radians.
% The negative and the corresponding positive angles are geometrically
% equivalent in this case, because the reference plane is pre-determined (XY).
% However the positive angle causes a discontinuity (false peak)
% in our knee angle signal, which prevents the correct detection of actual
% peaks. For this reason, we need to shift the range of the function atan2()
% from [-180,180] to [-360, 0] using the mod() function:
beta = mod( atan2d(A(:,2) - K(:,2), A(:,1) - K(:,1)), -360);

% Knee flexion angle in degrees:
theta = beta - alpha;


end