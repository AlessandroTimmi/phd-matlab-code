function phi = KneeAdductionAngle2dTreadmillStudy(s)
% Calculate the knee adduction angle from data collected for the treadmill
% study. NOTE: this algorithm only works under the following assumptions:
% i) Kinect is positioned on the side of the treadmill. 
% ii) The gait direction is X for the entire trial.
%
% Input:
%   s = struct containing data from a .trc file recorded during a treadmill
%       trial.
% 
% Output:
%   phi = knee adduction angle according to the sign convention used in the 
%         OpenSim model by Xu et al 2014, i.e. Adduction is positive and
%         abduction is negative.
%
% � February 2017 Alessandro Timmi


%% Define the following points in the YZ plane of Kinect coordinate system:
yH = s.markers.HIP(:, 2);
yK = s.markers.KNE(:, 2);
yA = s.markers.ANK(:, 2);

zH = s.markers.HIP(:, 3);
zK = s.markers.KNE(:, 3);
zA = s.markers.ANK(:, 3);

% Calculate the knee adduction angle using atan2(). This is better than
% using acos(), because atan2() returns the correct sign for the angle.
% The function atan2d() directly returns angles in degrees, which is more
% suitable for our purposes.
% Given:
%   alpha = orientation of the femur in Kinect YZ plane;
%   beta = orientation of the tibia in Kinect YZ plane;
% We can write the knee adduction angle (phi) as:
%   phi = alpha - beta
alpha = atan2d(yK - yH, zK - zH);

beta = atan2d(yA - yK, zA - zK);

% Knee adduction angle in degrees:
phi = alpha - beta;


end