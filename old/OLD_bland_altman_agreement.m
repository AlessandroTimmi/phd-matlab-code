function OLD_bland_altman_agreement(varargin)
% Read and sync IK results from OpenSim, in order to perform a
% validation of Kinect vs Vicon in terms of joint angles agreement and
% repeatability (Bland-Altman analysis).
%
% Input:
%   vicon_filenames = (optional parameter) one or more filenames containing
%                   measurements from the gold standard system (.mot or
%                   .trc). They must be of the same type.
%   kinect_filenames = (optional parameter) one or more filenames containing
%                   measurements from the new system (.mot or .trc). They
%                   must be of the same type of vicon_filenames.
%
%   fields_to_analyse = (optional parameter, default = {'ankle_angle_l'})
%                   cell array with names of the fields to be analysed.
%                   For .mot files, they must be joint angles (e.g.
%                   'knee_angle_r'). For .trc files, they must be marker
%                   names (e.g. 'RASI'); in this case, all 3 coordinates
%                   are analysed. It is assumed that fields to be
%                   analysed have same names in both Vicon and Kinect data.
%                   If they are sorted differently in the two datasets, it
%                   doesn't matter. For each field, all columns are
%                   analysed.
%
%   fields_to_sync = (optional parameter, default = fields_to_analyse) cell
%                   array with the name(s) of the field(s) to be used for
%                   syncronization. For .mot files, they must be joint
%                   angles (e.g. {'knee_angle_r'}). For .trc files, they
%                   must be marker names (e.g. {'RASI'}). If multiple
%                   markers are defined, synchronization will be based on
%                   all of them.%
%   coords_to_sync = (optional parameter, default = {'Z'}) cell array with
%                   coordinates to be used for synchronization. For .mot
%                   files, this parameter is not used, because each joint
%                   angle is a single coordinate. If multiple coordinates
%                   are defined, synchronization will be based on all of
%                   them.
%   maxlag_s        = (optional parameter, default = 2) maximum time lag
%                   (in s) between Kinect and Vicon data. Decimal numbers
%                   are allowed. This parameter is used to constrain the
%                   synchronization problem. Since the lag between Vicon
%                   and Kinect trials is usually not larger than 2 s, this
%                   is a reasonable default value.
%
%   analysis_type = (optional parameter, default = 'standard') A string
%                   indicating which kind of Bland-Altman analysis will be
%                   performed: 'standard', 'log-transformed' or
%                   'regression-transformed'.
%   save_figures =  (optional parameter, default = false) if true, it will
%                   save figures with results of the analysis.
%   export_synced_data = (optional parameter, default = false) if true, it
%                   will store synced data in the same format as the
%                   input files (.trc or .mot).
%   analyse_stride = (optional parameter, default = false) if true,
%                   performes the stride analysis on gait or running
%                   trials.
%   debug_mode =    (optional parameter, default = false). If true, displays
%                   some extra info for debug purposes.
%
%
% � October 2014 Alessandro Timmi

% TODO:
% - Write the log on a file in the results folder.
% - Add support for coordinates with different names to be synced and
% analysed.
% - Check the log-transformed analysis.
% - Check the "export synced data" part, for .mot files.
% - Clean the code.
% - this function currently doesn't support .mot files anymore. Make it
%   work with this format too.
% - check default values to match those in the description above.
% - Add (optional) line of equality in the BA plots (horizontal line at y=0).
% refine plot of differences over time, which is shown when a single pair
%  of trials is loaded.
% - If analyse_stride is false, the stride detection should not be performed
%  at all.



%% Default input values:
default.vicon_filenames = {};
default.kinect_filenames = {};

default.fields_to_analyse = {'HIP', 'KNE', 'ANK'};
default.fields_to_sync = default.fields_to_analyse;
default.coords_to_sync = {'X', 'Y', 'Z'};
default.maxlag_s = 2;

% default.fields_to_analyse = {'T12_L1_LB', 'T4_T5_LB', 'T3_T4_LB'};
% default.fields_to_sync = default.fields_to_analyse;
% default.coords_to_sync = {'X'};
% default.maxlag_s = 2;

% default.fields_to_analyse = {'hip_flexion_r', 'knee_angle_r', 'ankle_angle_r'};
% default.fields_to_sync = default.fields_to_analyse;
% default.coords_to_sync = {'X'};
% default.maxlag_s = 2;

default.analysis_type = 'standard';
expected_analysis_types = {'standard', 'regression_transformed', 'log_transformed'};

default.save_figures = false;
default.export_synced_data = false;
default.analyse_stride = false;
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'vicon_filenames', default.vicon_filenames,...
    @(x) validateattributes(x,{'char','cell'}, {'nonempty'}));
addParameter(p, 'kinect_filenames', default.kinect_filenames,...
    @(x) validateattributes(x,{'char','cell'}, {'nonempty'}));

addParameter(p, 'fields_to_analyse', default.fields_to_analyse,...
    @(x) validateattributes(x, {'cell'}, {'vector', 'nonempty'}));
addParameter(p, 'fields_to_sync', default.fields_to_sync,...
    @(x) validateattributes(x, {'cell'}, {'vector', 'nonempty'}));
addParameter(p, 'coords_to_sync', default.coords_to_sync,...
    @(x) validateattributes(x, {'cell'}, {'vector', 'nonempty'}));
addParameter(p, 'maxlag_s', default.maxlag_s,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative'}));

addParameter(p, 'analysis_type', default.analysis_type,...
    @(x) any(validatestring(x, expected_analysis_types)));
addParameter(p, 'save_figures', default.save_figures, @islogical);
addParameter(p, 'export_synced_data', default.export_synced_data, @islogical);
addParameter(p, 'analyse_stride', default.analyse_stride, @islogical);
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
vicon_filenames = p.Results.vicon_filenames;
kinect_filenames = p.Results.kinect_filenames;

fields_to_analyse = p.Results.fields_to_analyse;
fields_to_sync = p.Results.fields_to_sync;
coords_to_sync = p.Results.coords_to_sync;
maxlag_s = p.Results.maxlag_s;

analysis_type = p.Results.analysis_type;
save_figures = p.Results.save_figures;
export_synced_data = p.Results.export_synced_data;
analyse_stride = p.Results.analyse_stride;
debug_mode = p.Results.debug_mode;

clear varargin default p


%% LOAD DATA
fprintf('BLAND-ALTMAN ANALYSIS OF AGREEMENT\n');
fprintf('� October 2014 Alessandro Timmi.\n\n')

% If not provided as input argument, select files to be loaded:
if isempty(vicon_filenames)
    [vicon_fname, vicon_path] = uigetfile({...
        '*.trc;*.mot', 'OpenSim files';...
        '*.trc', 'Trace files';...
        '*.mot', 'Motion files'},...
        'Select trial(s) for Vicon (*.mot or *.trc)',...
        'MultiSelect', 'on');
    vicon_filenames = fullfile(vicon_path, vicon_fname);
end

if isempty(kinect_filenames)
    [kinect_fname, kinect_path] = uigetfile({...
        '*.trc;*.mot', 'OpenSim files';...
        '*.trc', 'Trace files';...
        '*.mot', 'Motion files'},...
        'Select matching trial(s) for Kinect (*.mot or *.trc)', 'MultiSelect', 'on');
    kinect_filenames = fullfile(kinect_path, kinect_fname);
end

% If a single file is selected, the output of the dialog is a string. We
% want a cell array in any case (single or multiple selection):
if ~iscell(vicon_filenames)
    vicon_filenames = {vicon_filenames};
end
if ~iscell(kinect_filenames)
    kinect_filenames = {kinect_filenames};
end

% Folder where we will store the results:
if ~exist('kinect_path', 'var')
    kinect_path = [fileparts(kinect_filenames{1}), filesep];
end
results_folder = [kinect_path 'Bland_Altman_results'];



%% Sort Vicon and Kinect trials in alphabetical order. Better now than
% later, to avoid any mess with files order.
fprintf('Sorting filenames...\n')
vicon_filenames = sort(vicon_filenames);
kinect_filenames = sort(kinect_filenames);
fprintf('Vicon:\n')
fprintf('\t%s.\n', vicon_filenames{:});
fprintf('Kinect:\n')
fprintf('\t%s.\n', kinect_filenames{:});


%% Check if the same number of trials has been selected for both Vicon and
% Kinect:
if numel(vicon_filenames)~=numel(kinect_filenames)
    error('Different number of trials selected for Vicon and Kinect.');
else
    fprintf('Number of trials selected for each system: %d.\n', numel(vicon_filenames));
end

% Extract trial names and extenstions from files:
vicon_name = cell(size(vicon_filenames));
vicon_ext = cell(size(vicon_filenames));
kinect_name = cell(size(kinect_filenames));
kinect_ext = cell(size(kinect_filenames));
for i=1:numel(vicon_filenames)
    [~, vicon_name{i}, vicon_ext{i}] = fileparts(vicon_filenames{i});
    [~, kinect_name{i}, kinect_ext{i}] = fileparts(kinect_filenames{i});
end
clear i

% Determine Vicon and Kinect file(s) type:
is_vicon_mot = all(strcmpi('.mot', vicon_ext));
is_kinect_mot = all(strcmpi('.mot', kinect_ext));
is_vicon_trc = all(strcmpi('.trc', vicon_ext));
is_kinect_trc = all(strcmpi('.trc', kinect_ext));

if is_vicon_mot
    vicon_filetype = '.mot';
elseif is_vicon_trc
    vicon_filetype = '.trc';
else
    error('Selected Vicon trial(s) are not of the same type.')
end
fprintf('Vicon filetype: "%s".\n', vicon_filetype);

if is_kinect_mot
    kinect_filetype = '.mot';
elseif is_kinect_trc
    kinect_filetype = '.trc';
else
    error('Selected Kinect trial(s) are not of the same type.')
end
fprintf('Kinect filetype: "%s".\n', kinect_filetype);

% Compare Vicon and Kinect file types:
if ~strcmpi(vicon_filetype, kinect_filetype)
    % If they are not the same, raise an error:
    error('Different file types loaded for gold standard and new systems.')
else
    % File type will affect several aspects of the code below:
    filetype = vicon_filetype;
    fprintf('File types match.\n');
end


%% PLOT SETTINGS
% RGB colours:
ps.DARK_GRAY = [0.6, 0.6, 0.6];
ps.LIGHT_GRAY = [0.85, 0.85, 0.85];
% ps.BLUE_UNIMELB = [0 41/255 82/255];

ps.TITLE_NAMES = {'interpreter', 'fontweight', 'fontsize'};
ps.TITLE_VALUES = {'none', 'bold', 14};

ps.AXIS_NAMES = {'Ygrid'};
ps.AXIS_VALUES = {'on'};

ps.LABEL_NAMES = {'interpreter', 'fontweight', 'fontsize'};
ps.LABEL_VALUES = {'none', 'normal', 12};

ps.REFLINE_NAMES = {'Color', 'linestyle', 'linewidth'};
ps.REFLINE_VALUES_DASH = {'k', '--', 1.5};
ps.REFLINE_VALUES_SOLID = {'k', '-', 1.5};

ps.PATCH_NAMES = {'FaceAlpha', 'EdgeColor'};
ps.PATCH_VALUES = {0.5, 'none'};

% Margin to be left around data when manually computing axes limits [0-1]:
ps.MARGIN = 0.08;

switch filetype
    case '.trc'
        ps.UNIT_SYMBOL = '[m]';
        ps.UNIT_LABEL = 'm';
    case '.mot'
        ps.UNIT_SYMBOL = '[�]';
        ps.UNIT_LABEL = 'degrees';
end

% The number of columns for each field depends on the file type:
n_cols = number_of_columns_per_field(filetype);

% Since we want subplots to advance column-wise instead of row-wise (which
% is the default in MATLAB), we need to create custom subplot indices:
ps.sp_ids = columnwise_subplots_ids(numel(fields_to_analyse), n_cols);


%% PRE-PROCESS EACH PAIR OF TRIALS:
for f=1:numel(vicon_filenames)
    fprintf('\nPair of trials number %d of %d.\n', f, numel(vicon_filenames))
    
    %% Load files to be compared:
    switch filetype
        case '.trc'
            vicon_raw = LoadTrcFile(...
                'filename', vicon_filenames{f},...
                'logMessage', sprintf('Loading Vicon .trc file...\n'),...
                'dialogTitle', sprintf('Select Vicon .trc file\n'));
            kinect_raw = LoadTrcFile(...
                'filename', kinect_filenames{f},...
                'logMessage', sprintf('Loading Kinect .trc file...\n'),...
                'dialogTitle', sprintf('Select Kinect .trc file\n'));
            
        case '.mot'
            vicon_raw = LoadMotFile('filename', vicon_filenames{f});
            kinect_raw = LoadMotFile('filename', kinect_filenames{f});
            
            % Rename "coords" field to "markers" to match the format used
            % for .trc files:
            vicon_raw.markers = vicon_raw.coords;
            kinect_raw.markers = kinect_raw.coords;
            vicon_raw = rmfield(vicon_raw, 'coords');
            kinect_raw = rmfield(kinect_raw, 'coords');
            
            [~, ~, ~, vicon_raw.DataRate] = check_sampling_freq(vicon_raw.time);
            [~, ~, ~, kinect_raw.DataRate] = check_sampling_freq(kinect_raw.time);
    end    
    
    
    %% Check field names and number of coordinates for current trials:
    if f == 1
        % Read field names from current trials, sorting them in
        % alphabetical order:
        vicon_sorted_fieldnames = sort(fieldnames(vicon_raw.markers));
        kinect_sorted_fieldnames = sort(fieldnames(kinect_raw.markers));
        
        % Check that Vicon and Kinect trials have same field names. The
        % order doesn't matter:
        if isequal(vicon_sorted_fieldnames, kinect_sorted_fieldnames)
            sorted_fieldnames = vicon_sorted_fieldnames;
        else
            error('Vicon and Kinect marker names don''t match for this trial.')
        end
    else
        % Compare field names of current trials with those from previous
        % trials, to ensure consistency:
        assert(isequal(sort(fieldnames(vicon_raw.markers)), sorted_fieldnames),...
            'Field names from Vicon trial number %d don''t match those from previous trial.', f);        
        assert(isequal(sort(fieldnames(kinect_raw.markers)), sorted_fieldnames),...
            'Field names from Kinect trial number %d don''t match those from previous trial.', f);
    end

    % Number of coordinates per field. Markers have 3 coords: X, Y and Z.
    % Joint angles have 1 coordinate per field. Here we assume that
    % all fields in a trial have the same number of coordinates, so we
    % check only the first field for each trial vs the expected number of
    % columns:
    n_cols_vicon = size(vicon_raw.markers.(vicon_sorted_fieldnames{1}), 2);
    n_cols_kinect = size(kinect_raw.markers.(kinect_sorted_fieldnames{1}), 2);    
    assert(isequal(n_cols, n_cols_vicon),...
        'Wrong number of coordinates for Vicon fields: expected %d, found %d.', n_cols, n_cols_vicon);
    assert(isequal(n_cols, n_cols_kinect),...
        'Wrong number of coordinates for Kinect fields: expected %d, found %d.', n_cols, n_cols_kinect);
    
    
    %% RESET TIME ARRAYS
    % Kinect time array usually starts from zero, while Vicon time array from
    % 0.083 (which is 1/120 s). Moreover, sometimes trials might have been trimmed
    % into OpenSim, leaving a time array which starts from different time
    % values. In order to correctly synchronize two different trials, we need
    % to ensure that their starting timestamps are the same:
    fprintf('\nResetting timestamps of Vicon data...\n')
    vicon_raw.time = reset_time_array(vicon_raw.time, 'debug_mode', debug_mode);
    fprintf('Resetting timestamps of Kinect data...\n')
    kinect_raw.time = reset_time_array(kinect_raw.time, 'debug_mode', debug_mode);
    
    
    %% INTERPOLATION OF VICON DATA
    % A higher framerate allows a finer synchronization between Vicon and
    % Kinect data. For this reason and after some testing, I decided to
    % increase Vicon framerate.
    
    % Copy Vicon raw struct into the interpolated one, skipping
    % time and marker coordinates:
    vicon_interp = rmfield(vicon_raw, {'time', 'markers'});
    
    % Manually set the new data rate to the desired value:
    vicon_interp.DataRate = 480;
    
    % New time array for Vicon, interpolated from 120 to the new requested rate:
    vicon_interp.time = (vicon_raw.time(1) : 1/vicon_interp.DataRate : vicon_raw.time(end))';
    
    fprintf('\nInterpolating Vicon data at %g fps...\n\n', vicon_interp.DataRate);
    
    % Interpolate each field of Vicon's struct using splines. This
    % is not the case (because we specified the same time range of the raw
    % data) but values outside the range might be extrapolated.
    vicon_interp.markers = structfun(@(y)...
        ( interp1(vicon_raw.time, y, vicon_interp.time, 'spline', 'extrap') ),...
        vicon_raw.markers, 'UniformOutput', false);
    
    switch filetype
        case '.trc'
            % Update info in Vicon interpolated struct, calculating them
            % from data:
            vicon_interp = UpdateTrcStructInfo(vicon_interp, 'debug_mode', debug_mode);
        case '.mot'
            % do nothing.
    end
    
    
    %% INTERPOLATION OF KINECT DATA TO MATCH VICON FRAMERATE
    % Copy the Kinect raw struct into the interpolated one, skipping
    % time and marker coordinates:
    kinect_interp = rmfield(kinect_raw, {'time', 'markers'});
    
    % New time array for Kinect, interpolated at Vicon framerate:
    kinect_interp.time = (kinect_raw.time(1) : 1/vicon_interp.DataRate : kinect_raw.time(end))';
    
    fprintf('\nInterpolating Kinect data at %g fps...\n\n', vicon_interp.DataRate);
    
    % Interpolate each field of Kinect's struct at Vicon's framerate,
    % using splines. This is not the case (because we specified the same time
    % range of the raw data) but values outside the range might be extrapolated.
    kinect_interp.markers = structfun(@(y)...
        ( interp1(kinect_raw.time, y, kinect_interp.time, 'spline', 'extrap') ),...
        kinect_raw.markers, 'UniformOutput', false);
    
    switch filetype
        case '.trc'
            % Update info in Kinect interpolated struct, calculating them
            % from data:
            kinect_interp = UpdateTrcStructInfo(kinect_interp, 'debug_mode', debug_mode);
        case '.mot'
            % do nothing.
    end
    
    
    %% SYNC THE TRIALS
    % Convert max lag from s to frames. We round because frames must always
    % be integers.
    maxlag = round(maxlag_s * vicon_interp.DataRate);
    fprintf('Max lag was set to %0.1f s (%d frames).\n', maxlag_s, maxlag);
    
    [vicon_sync, kinect_sync, shift] =...
        SyncStructs(vicon_interp, kinect_interp,...
        fields_to_sync,...
        'coords_to_sync', coords_to_sync,...
        'maxlag', maxlag,...
        'debug_mode', debug_mode);
    
    fprintf('Kinect signals have been shifted by %g frames to sync with Vicon signals.\n', shift)
    
    
    %% GAIT EVENT DETECTION
    % Since data have been interpolated at the same framerate and synced
    % we can detect gait events using Vicon data only, which are
    % supposed to be more accurate than those from Kinect.
    % Detected events will be stored as new fields into the vicon_sync
    % struct:
    % TODO FIX SD THRESHOLD in events detection
%     [vicon_sync.locs_FS, vicon_sync.locs_TO, ~, ~,...
%         vicon_sync.mean_stride_duration, vicon_sync.mean_stance_percentage] =...
%         detect_gait_events(vicon_sync, 'debug_mode', debug_mode);    
    
    
    %% STORE SYNCED DATA IN AN ARRAY OF STRUCTS,
    % to be used later in case of multiple pair of trials being analysed.
    % No need to preallocate the structs in this case, as it would just
    % create an error when copying during the first iteration, without any
    % improvement in terms of performance.
    vicon_storage(f) = vicon_sync;
    kinect_storage(f) = kinect_sync;    
      
    
    %% EXPORT SYNCED DATA INTO ASCII FILES (.TRC OR .MOT)
    if export_synced_data
        fprintf('Exporting synced data into %s files...\n', filetype);
        
        if ~exist(results_folder, 'dir')
            mkdir(results_folder);
        end
        
        vicon_sync_filename = fullfile(results_folder,...
            [vicon_name{f}, '_sync', filetype]);
        kinect_sync_filename = fullfile(results_folder,...
            [kinect_name{f}, '_sync', filetype]);
        
        switch filetype
            case '.trc'
                WriteTrcFile(vicon_sync,...
                    'destinationFilename', vicon_sync_filename);
                WriteTrcFile(kinect_sync,...
                    'destinationFilename', kinect_sync_filename);
                
            case '.mot'
                vicon_sync_2 = vicon_sync;
                kinect_sync_2 = kinect_sync;
                
                % Rename "markers" field to "coords" to match the format
                % used for .mot files:
                vicon_sync_2.coords = vicon_sync_2.markers;
                kinect_sync_2.coords = kinect_sync_2.markers;
                vicon_sync_2 = rmfield(vicon_sync_2, 'markers');
                kinect_sync_2 = rmfield(kinect_sync_2, 'markers');
                
                WriteMotFile(vicon_sync_2,...
                    'destinationFilename', vicon_sync_filename);
                WriteMotFile(kinect_sync_2,...
                    'destinationFilename', kinect_sync_filename);
        end
        
        fprintf('\tVicon: %s\n', vicon_sync_filename);
        fprintf('\tKinect: %s\n', kinect_sync_filename);
    end
    
end
clear f


%% DO INDIVIDUAL PLOTS ONLY IF WE ARE ANALYSING A SINGLE PAIR OF TRIALS
% to avoid having too many plots.
if numel(vicon_filenames) == 1
    
    %% PLOT RAW AND SYNCED DATA
    if debug_mode
        fprintf('[Debug] See debug plot of raw and synced data.\n')
        figure('Name', '[Debug] Raw and synced data');
        for i=1:numel(fields_to_analyse)
            for j=1:n_cols
                hax_raw = subplot(n_cols, numel(fields_to_analyse), ps.sp_ids(j, i));
                plot(vicon_raw.time, vicon_raw.markers.(fields_to_analyse{i})(:, j),...
                    'linestyle', ':', 'Color', ps.LIGHT_GRAY, 'linewidth', 2)
                hold on
                plot(kinect_raw.time, kinect_raw.markers.(fields_to_analyse{i})(:, j),...
                    'linestyle', '-.', 'Color', ps.LIGHT_GRAY, 'linewidth', 2)
                plot(vicon_sync.time, vicon_sync.markers.(fields_to_analyse{i})(:, j),...
                    'linestyle', '-', 'Color', 'k', 'linewidth', 2)
                plot(kinect_sync.time, kinect_sync.markers.(fields_to_analyse{i})(:, j),...
                    'linestyle', '--', 'Color', ps.DARK_GRAY, 'linewidth', 2)
                set(hax_raw, ps.AXIS_NAMES, ps.AXIS_VALUES)
                legend('Vicon', 'Kinect2', 'Vicon sync', 'Kinect2 sync')
                
                if j==1
                    title_raw = title(fields_to_analyse{i});
                    set(title_raw, ps.TITLE_NAMES, ps.TITLE_VALUES);
                end
                
                switch filetype
                    case '.trc'
                        ylabel([num2coord(j), ' ', ps.UNIT_SYMBOL],...
                            ps.LABEL_NAMES, ps.LABEL_VALUES)
                    case '.mot'
                        ylabel([fields_to_analyse{i}, ' ', ps.UNIT_SYMBOL],...
                            ps.LABEL_NAMES, ps.LABEL_VALUES)
                end
                
                xlabel('Time [s]', ps.LABEL_NAMES, ps.LABEL_VALUES)
            end
        end
    end
    clear i j
    
    
    %% PLOT SYNCED DATA ONLY
    hfig_data_sync = figure('Name','Synced data');
    for i=1:numel(fields_to_analyse)
        for j=1:n_cols
            hax_sync = subplot(n_cols, numel(fields_to_analyse), ps.sp_ids(j, i));
            plot(vicon_sync.time, vicon_sync.markers.(fields_to_analyse{i})(:, j),...
                'linestyle', '-', 'Color', 'k', 'linewidth', 2)
            hold on
            plot(kinect_sync.time, kinect_sync.markers.(fields_to_analyse{i})(:, j),...
                'linestyle', '--', 'Color', ps.DARK_GRAY, 'linewidth', 2)
            set(hax_sync, ps.AXIS_NAMES, ps.AXIS_VALUES)
            legend('Vicon sync', 'Kinect2 sync')
            
            if j==1
                title_sync = title(fields_to_analyse{i});
                set(title_sync, ps.TITLE_NAMES, ps.TITLE_VALUES);
            end
            
            switch filetype
                case '.trc'
                    ylabel([num2coord(j), ' ', ps.UNIT_SYMBOL],...
                        ps.LABEL_NAMES, ps.LABEL_VALUES)
                case '.mot'
                    ylabel([fields_to_analyse{i}, ' ', ps.UNIT_SYMBOL],...
                        ps.LABEL_NAMES, ps.LABEL_VALUES)
            end
            
            xlabel('Time [s]', ps.LABEL_NAMES, ps.LABEL_VALUES)
        end
    end
    clear i j
    
    %% PLOT DIFFERENCES OVER TIME
    % TODO REFINE THIS plot
    hfig_data_sync = figure('Name','DIFFERENCES OVER TIME');
    for i=1:numel(fields_to_analyse)
        for j=1:n_cols
            hax_sync = subplot(n_cols, numel(fields_to_analyse), ps.sp_ids(j, i));
            plot(vicon_sync.time,...
                kinect_sync.markers.(fields_to_analyse{i})(:, j)-vicon_sync.markers.(fields_to_analyse{i})(:, j),...
                'linestyle', '-', 'Color', 'k', 'linewidth', 2)
            
            %plot(kinect_sync.time, kinect_sync.markers.(fields_to_analyse{i})(:, j),...
              %  'linestyle', '--', 'Color', ps.DARK_GRAY, 'linewidth', 2)
            set(hax_sync, ps.AXIS_NAMES, ps.AXIS_VALUES)
            legend('Kinect  - Vicon sync')
            
            if j==1
                title_sync = title(fields_to_analyse{i});
                set(title_sync, ps.TITLE_NAMES, ps.TITLE_VALUES);
            end
            
            switch filetype
                case '.trc'
                    ylabel([num2coord(j), ' ', ps.UNIT_SYMBOL],...
                        ps.LABEL_NAMES, ps.LABEL_VALUES)
                case '.mot'
                    ylabel([fields_to_analyse{i}, ' ', ps.UNIT_SYMBOL],...
                        ps.LABEL_NAMES, ps.LABEL_VALUES)
            end
            
            xlabel('Time [s]', ps.LABEL_NAMES, ps.LABEL_VALUES)
        end
    end
    clear i j
    
    
    %% SAVE FIGURE OF SYNCED SIGNALS FOR CURRENT DATA PAIR
    if save_figures
        if ~exist(results_folder, 'dir')
            mkdir(results_folder);
        end
        
        fprintf('Saving figures in:\n\t%s.\n', results_folder);
        % The file name will contain all the filenames of the trials from
        % Kinect data:
        savefig(hfig_data_sync, fullfile(results_folder, ['Sync_', kinect_name{f}, '.fig']))
    end
    
end


% %% TODO: REMOVE AND USE THE STORAGE ARRAYS OF STRUCT instead of concatenating
% if numel(vicon_filenames) > 1
%     %% Concatenate stored data, to be used for Bland-Altman analysis.
%     % For ease of coding, we will overwrite the previously used arrays
%     % "_sync":
%     clear vicon_sync kinect_sync
%     
%     % Matlab cannot index structs with more than one level using colon (:).
%     % Hence we need to reduce the two-level structs to one-level before
%     % indexing. In this way we obtain an array of structs like this:
%     % Trial_1: Marker_1, Marker_2,... Marker_M;
%     % Trial_2: Marker_1, Marker_2,... Marker_M;
%     % ...
%     % Trial_N: Marker_1, Marker_2,... Marker_M;
%     vsm = horzcat(vicon_storage(:).markers);
%     ksm = horzcat(kinect_storage(:).markers);        
%     
%     % Create a new struct containing concatenated signals, as if it was a
%     % very long trial:
%     for n=1:numel(sorted_fieldnames)
%         vicon_sync.markers.(sorted_fieldnames{n}) =...
%             vertcat(vsm(:).(sorted_fieldnames{n}));
%         
%         kinect_sync.markers.(sorted_fieldnames{n}) =...
%             vertcat(ksm(:).(sorted_fieldnames{n}));
%     end
%     
%     % Build a time array for the concatenated struct, concatenating the
%     % time arrays of all the trials:
%     for f=1:numel(vicon_filenames)        
%         if f==1
%             vicon_sync.time = vicon_storage(f).time;
%             kinect_sync.time = kinect_storage(f).time;
%         else
%             vicon_sync.time = vertcat(vicon_sync.time,...
%                 vicon_storage(f).time + vicon_sync.time(end) + 1 / vicon_interp.DataRate);
%             kinect_sync.time = vertcat(kinect_sync.time,...
%                 kinect_storage(f).time + kinect_sync.time(end) + 1 / vicon_interp.DataRate);
%         end        
%     end
%  
%     switch filetype
%         case '.trc'
%             % Create empty info variables:
%             vicon_sync.DataRate = [];
%             vicon_sync.NumFrames = [];
%             vicon_sync.NumMarkers = [];
%             kinect_sync.DataRate = [];
%             kinect_sync.NumFrames = [];
%             kinect_sync.NumMarkers = [];
%             % Update info in concatenated structs, calculating them from data:
%             vicon_sync = UpdateTrcStructInfo(vicon_sync, 'debug_mode', debug_mode);
%             kinect_sync = UpdateTrcStructInfo(kinect_sync, 'debug_mode', debug_mode);
%         case '.mot'
%             % do nothing.
%     end
%     
%     figure('Name', 'Test time concatenated')
%     plot(vicon_sync.time)
%     hold on
%     plot(kinect_sync.time, 'linestyle', '--')
%     title('Test concatenated time arrays')
%     
%     
%     %% Plot CONCATENATED synced data
%     hfig_sync_contenated = figure('Name', 'Concatenated synced data');
%     for i=1:numel(fields_to_analyse)
%         for j=1:n_cols
%             hax_sync = subplot(n_cols, numel(fields_to_analyse), ps.sp_ids(j, i));
%             plot(vicon_sync.markers.(fields_to_analyse{i})(:, j),...
%                 'linestyle', '-', 'Color', 'k', 'linewidth', 2)
%             hold on
%             plot(kinect_sync.markers.(fields_to_analyse{i})(:, j),...
%                 'linestyle', '--', 'Color', ps.DARK_GRAY, 'linewidth', 2)
%             set(hax_sync, ps.AXIS_NAMES, ps.AXIS_VALUES)
%             legend('Vicon sync', 'Kinect2 sync')
%             
%             if j==1
%                 title_conc = title(fields_to_analyse{i});
%                 set(title_conc, ps.TITLE_NAMES, ps.TITLE_VALUES);
%             end
%             
%             switch filetype
%                 case '.trc'
%                     ylabel([num2coord(j), ' ', ps.UNIT_SYMBOL],...
%                         ps.LABEL_NAMES, ps.LABEL_VALUES)
%                 case '.mot'
%                     ylabel([fields_to_analyse{i}, ' ', ps.UNIT_SYMBOL],...
%                         ps.LABEL_NAMES, ps.LABEL_VALUES)
%             end
%             
%             xlabel('Time [frames]', ps.LABEL_NAMES, ps.LABEL_VALUES)
%         end
%     end
%     clear i j
%     
%     
%     %% SAVE FIGURE OF SYNCED SIGNALS FOR CONCATENATED DATA:
%     if save_figures
%         if ~exist(results_folder, 'dir')
%             mkdir(results_folder);
%         end
%         
%         fprintf('Saving figures of synced signals for concatenated data in:\n\t%s.\n', results_folder);
%         savefig(hfig_sync_contenated, fullfile(results_folder, ['Sync_', strjoin(kinect_name, '_'), '.fig']))
%     end
%         
% end


% TODO: this pipeline should actually start from here, the rest is
% pre-processing.
%% BLAND-ALTMAN ANALYSIS
fprintf('\nAnalysis of agreement\n');
fprintf('Analysis type: %s.\n', analysis_type);

% The sample size is the number of Kinect frames before interpolation.
% However, since during sync we might have trimmed the trials, we need to
% back calculate the number of frames using the synced signals and the
% original framerate, in order to get the number of frames we have actually
% used for the analysis.
sample_size = 0;
for f=1:numel(vicon_filenames)
    sample_size = sample_size +...
        round(length(kinect_storage(f).markers.(fields_to_analyse{1})) *...
        kinect_raw.OrigDataRate / vicon_interp.DataRate);
end
fprintf('Sample size (number of Kinect frames at original framerate, used for BA): n = %d frames.\n',...
    sample_size)

% t-value for a two-sided test with n-1 degrees of freedom (n = sample
% size) and level of significance alpha = 0.05. In their paper Bland and
% Altman imply the use of a two-sided test, so we need to convert the
% probability from one-sided (95%) to two-sided.
% For larger sample sizes, this t-value tends to 1.96, because Student's
% t distribution tends to the Normal distribution.
alpha = 0.05;
prob = 1 - alpha / 2;
dof = sample_size - 1;
t_value = tinv(prob, dof);
fprintf('T-value (alpha = %0.3f, 2-sided, dof = %d): %4.4f.\n\n',...
    alpha, dof, t_value);

% Generate figures handles outside the for-loop, to avoid having multiple
% figures:
hfig_hist = figure('Name', 'Histograms of the differences');
hfig_agreement = figure('Name', 'Bland-Altman plots');

% Matlab cannot index structs with more than one level
% using colon (:).
% Hence we need to reduce the two-level array of structs:
% Trial(1).markers.Marker_1
% Trial(1).markers.Marker_2
% Trial(2).markers.Marker_1
% Trial(2).markers.Marker_2
% ...
% Trial(N).markers.Marker_1
% Trial(N).markers.Marker_2
% to one-level before indexing it. In this way we obtain an
% array of structs with this structure (for both Vicon and
% Kinect):
% Trial(1).Marker_1 .Marker_2 ... .Marker_M;
% Trial(2).Marker_1 .Marker_2 ... .Marker_M;
% ...
% Trial(N).Marker_1 .Marker_2 ... .Marker_M;
vsm = horzcat(vicon_storage(:).markers);
ksm = horzcat(kinect_storage(:).markers);

% Now build a new struct containing all concatenated signals from trials,
% as if it was a single very long trial:
for n=1:numel(sorted_fieldnames)
    vicon_long.markers.(sorted_fieldnames{n}) =...
        vertcat(vsm(:).(sorted_fieldnames{n}));
    
    kinect_long.markers.(sorted_fieldnames{n}) =...
        vertcat(ksm(:).(sorted_fieldnames{n}));
end


for i=1:numel(fields_to_analyse)
    for j=1:n_cols
        
        switch analysis_type
            case 'standard'                                     
                % On the y-axes we plot the differences between the 2 devices (NEW - OLD):
                dif = kinect_long.markers.(fields_to_analyse{i})(:, j) - ...
                    vicon_long.markers.(fields_to_analyse{i})(:, j); 
               
                % The mean of the differences is also referred to as the
                % "bias":
                mean_dif = mean(dif);
                sd = std(dif);
                
                % % We might test for the presence of a bias (mean value of the difference
                % % differs significantly from 0) using a one-sample t-test:
                % [h,p] = ttest(dif_1)
                % % If there is a consistent bias, we can adjust for it by subtracting the mean
                % % of the differences from the new method (or, as in this case, from the
                % % difference: new_method - old_method).
                % adj_dif.(joints_analysed{i}) = dif.(joints_analysed{i}) - mean_dif.(joints_analysed{i});
                %
                % mean_adj_dif.(joints_analysed{i}) = mean(adj_dif.(joints_analysed{i}));
                
                % The 95% limits of agreement are defined as (mean � 1.96 * sd). It is
                % where the 95% of differences are expected to lie (if the assumption
                % of Normal distribution for the differences is true).
                loa = mean_dif + 1.96 * [-sd, sd];
                fprintf('%s %s\n', fields_to_analyse{i}, num2coord(j))
                
                % On the x-axes we plot the mean between the two methods,
                % because it is the best estimate of the true value:
                avg = mean(...
                    [kinect_long.markers.(fields_to_analyse{i})(:, j),...
                    vicon_long.markers.(fields_to_analyse{i})(:, j)],...
                    2);
               
                %% CONFIDENCE INTERVALS (CI) FOR THE LIMITS OF AGREEMENT
                % CI tell how precise our estimates are in representing the
                % values of the whole population, provided the differences
                % follow a distribution which is approximatively Normal.
                fprintf('Estimate (95%% CI):\n')
                
                % Standard error of the mean difference:
                se_dif = sqrt(sd^2 / sample_size);
                
                % Standard error of the limits of agreement.
                % NOTICE: there is a (not straightforward) derivation to
                % obtain the standard error below (and in particular
                % the "3" in it). See derivation provided by Prof Ian Gordon.
                se_loa = sqrt(3 * sd^2 / sample_size);
                
                % 95% CI for the mean difference (or bias):
                ci_mean_dif = mean_dif + [-t_value, t_value] * se_dif;
                fprintf('Bias = %4.4f (%4.4f, %4.4f) %s.\n',...
                    mean_dif, ci_mean_dif', ps.UNIT_LABEL)
                
                % 95% CI for the limits of agreement:
                ci_loa_low = min(loa) + [-t_value, t_value] * se_loa;
                ci_loa_upp = max(loa) + [-t_value, t_value] * se_loa;
                fprintf('Lower LOA = %4.4f (%4.4f, %4.4f) %s.\n',...
                    min(loa), ci_loa_low', ps.UNIT_LABEL)
                fprintf('Upper LOA = %4.4f (%4.4f, %4.4f) %s.\n',...
                    max(loa), ci_loa_upp', ps.UNIT_LABEL)
                
                % The width of the limits of agreement (upper limit - lower limit)
                % is not mentioned in Bland-Altman papers, but it conveniently
                % conveys the level of disagreement using a single number.
                % Moreover, it is mentioned in Myles and Cui (2007).
                width_loa = loa(2) - loa(1);
                fprintf('Width of the limits of agreement: %.4f %s.\n\n',...
                    width_loa, ps.UNIT_LABEL)
                
                
            case 'log_transformed'
                % TODO: fix and complete with 95% CI.
                % If the variability of the differences increases as the magnitude of
                % measurements increases, we could ignore this relationship between
                % difference and magnitude, but the limits of agreement will be wider
                % apart for small magnitudes and narrow for large magnitudes. We can
                % try to remove this relationship log-transforming our data before the
                % analysis, and then back-transform.
                dif.(fields_to_analyse{i}) =...
                    log(kinect_sync.markers.(fields_to_analyse{i})) -...
                    log(vicon_sync.markers.(fields_to_analyse{i}));
                
                mean_dif.(fields_to_analyse{i}) =...
                    mean(dif.(fields_to_analyse{i}));
                
                sd.(fields_to_analyse{i}) =...
                    std(dif.(fields_to_analyse{i}));
                
                loa.(fields_to_analyse{i}) = mean_dif.(fields_to_analyse{i}) + 1.96 *...
                    [-sd.(fields_to_analyse{i}), sd.(fields_to_analyse{i})];
                
                avg.(fields_to_analyse{i}) = mean([log(kinect_sync.markers.(fields_to_analyse{i})),...
                    log(vicon_sync.markers.(fields_to_analyse{i}))], 2);
                
                
            case 'regression_transformed'
                % TODO: fix and complete with 95% CI.
                dif.(fields_to_analyse{i}) =...
                    kinect_sync.markers.(fields_to_analyse{i}) - vicon_sync.markers.(fields_to_analyse{i});
                
                avg.(fields_to_analyse{i}) = mean([kinect_sync.markers.(fields_to_analyse{i}),...
                    vicon_sync.markers.(fields_to_analyse{i})],2);
                
                % According to (Bland-Altman, 1999), we try to remove the
                % relationship between differences and average using a linear
                % regression of our data:
                [p.(fields_to_analyse{i})] = polyfit(avg.(fields_to_analyse{i}),...
                    dif.(fields_to_analyse{i}), 1);
                
                % (D^) evaluate the fitting of the differences vs the average:
                fit_dif.(fields_to_analyse{i}) =...
                    polyval(p.(fields_to_analyse{i}),...
                    avg.(fields_to_analyse{i}));
                
                % Calculate the residuals of this fitting:
                resid_dif.(fields_to_analyse{i}) =...
                    dif.(fields_to_analyse{i}) -...
                    fit_dif.(fields_to_analyse{i});
                
                % Regress the absolute values of the residuals:
                [p_resid.(fields_to_analyse{i})] =...
                    polyfit(avg.(fields_to_analyse{i}),...
                    abs(resid_dif.(fields_to_analyse{i})), 1);
                
                % (R^) evaluate the fitting of the residuals:
                fit_resid.(fields_to_analyse{i}) =...
                    polyval(p_resid.(fields_to_analyse{i}),...
                    avg.(fields_to_analyse{i}));
                
                % The 95% limits of agreement are obtained as:
                % loa = D^ +/- 1.96 * sqrt(pi/2) * R^.
                % TODO: Why did I put the minus in front of R instead of 1.96?
                fit_low_loa.(fields_to_analyse{i}) =...
                    fit_dif.(fields_to_analyse{i}) + 1.96 * sqrt(pi/2) *...
                    -fit_resid.(fields_to_analyse{i});
                
                fit_upp_loa.(fields_to_analyse{i}) =...
                    fit_dif.(fields_to_analyse{i}) + 1.96 * sqrt(pi/2) *...
                    fit_resid.(fields_to_analyse{i});
                
                fprintf('TODO: print regression equations with values.\n');
        end
        
        
        %% Histogram of the differences, to visually check if they are
        % Normally distributed:
        figure(hfig_hist);
        hax_hist = subplot(n_cols, numel(fields_to_analyse),  ps.sp_ids(j, i));
        hist(hax_hist, dif);
        set(hax_hist, ps.AXIS_NAMES, ps.AXIS_VALUES)
        
        if j==1
            title_hist = title(fields_to_analyse{i});
            set(title_hist, ps.TITLE_NAMES, ps.TITLE_VALUES)
        end
        
        switch filetype
            case '.trc'
                xlabel({'Differences';...
                    sprintf('%s %s', num2coord(j), ps.UNIT_SYMBOL)},...
                    ps.LABEL_NAMES, ps.LABEL_VALUES)
            case '.mot'
                xlabel({'Differences';...
                    sprintf('%s %s', fields_to_analyse{i}, ps.UNIT_SYMBOL)},...
                    ps.LABEL_NAMES, ps.LABEL_VALUES)
        end
        ylabel('Occurrencies', ps.LABEL_NAMES, ps.LABEL_VALUES)
        
        
        %% Bland-Altman plots:
        figure(hfig_agreement);
        hax_ba = subplot(n_cols, numel(fields_to_analyse), ps.sp_ids(j, i));
        plot(avg, dif, 'Marker', '.', 'Color', ps.LIGHT_GRAY,...
            'MarkerSize', 4, 'linestyle', 'none')
        hold on
        % Set axis limits manually, because MATLAB sometimes leaves some
        % useless empty space around data, depending on the format of the
        % computer screen and on the current size of the figure.
        delta_xlim = abs((max(avg) - min(avg)));
        delta_ylim = abs((max(dif) - min(dif)));
        hax_ba.XLim = [min(avg) - ps.MARGIN * delta_xlim, max(avg) + ps.MARGIN * delta_xlim];
        hax_ba.YLim = [min(dif) - ps.MARGIN * delta_ylim, max(dif) + ps.MARGIN * delta_ylim];
        
        % Some plot settings depend on the analysis type:
        switch analysis_type
            case 'standard'    
                %% Plot 95% Confidence intervals (CI) as shaded areas:
                patch_ci_mean = fill([min(xlim), max(xlim), max(xlim), min(xlim)],...
                    [max(ci_mean_dif), max(ci_mean_dif), min(ci_mean_dif), min(ci_mean_dif)],...
                    ps.DARK_GRAY);                
                patch_ci_low = fill([min(xlim), max(xlim), max(xlim), min(xlim)],...
                    [max(ci_loa_low), max(ci_loa_low), min(ci_loa_low), min(ci_loa_low)],...
                    ps.DARK_GRAY);
                patch_ci_upp = fill([min(xlim), max(xlim), max(xlim), min(xlim)],...
                    [max(ci_loa_upp), max(ci_loa_upp), min(ci_loa_upp), min(ci_loa_upp)],...
                    ps.DARK_GRAY);
                
                set(patch_ci_mean, ps.PATCH_NAMES, ps.PATCH_VALUES)
                set(patch_ci_low, ps.PATCH_NAMES, ps.PATCH_VALUES)
                set(patch_ci_upp, ps.PATCH_NAMES, ps.PATCH_VALUES)
                                
                % Limits of agreement:
                ref_mean = refline(0, mean_dif);
                ref_loa_low = refline(0, loa(1));
                ref_loa_high = refline(0, loa(2));
                
                set(ref_mean, ps.REFLINE_NAMES, ps.REFLINE_VALUES_DASH)
                set(ref_loa_low, ps.REFLINE_NAMES, ps.REFLINE_VALUES_SOLID)
                set(ref_loa_high, ps.REFLINE_NAMES, ps.REFLINE_VALUES_SOLID)
                
                % Set tick values for the Y axis:
                hax_ba.YTick = [min(loa), mean_dif, max(loa)];
                                
                % Axes labels:
                switch filetype
                    case '.trc'
                        my_ylabel = ylabel({'Kinect2 - Vicon';...
                            sprintf('%s %s', num2coord(j), ps.UNIT_SYMBOL)});
                        my_xlabel = xlabel({'Average(Kinect2, Vicon)';...
                            sprintf('%s %s', num2coord(j), ps.UNIT_SYMBOL)});
                        
                        % Set the exponent of the tick values for the Y axis:
                        hax_ba.YAxis.Exponent = -3;
                        % Set the format of the tick values for the Y axis
                        hax_ba.YAxis.TickLabelFormat = '%,.0f';
                        
                    case '.mot'
                        my_ylabel = ylabel({'Kinect2 - Vicon';...
                            sprintf('%s %s', fields_to_analyse{i}, ps.UNIT_SYMBOL)});
                        my_xlabel = xlabel({'Average(Kinect2, Vicon)';...
                            sprintf('%s %s', fields_to_analyse{i}, ps.UNIT_SYMBOL)});
                end
                set(my_ylabel, ps.LABEL_NAMES, ps.LABEL_VALUES);
                set(my_xlabel, ps.LABEL_NAMES, ps.LABEL_VALUES);
                
                
            case 'log_transformed'
                % TODO: fix and complete with 95% CI.
                % Limits of agreement:
                ref_mean.(fields_to_analyse{i}) =...
                    refline(0, mean_dif.(fields_to_analyse{i}));
                ref_loa_low.(fields_to_analyse{i}) =...
                    refline(0, loa.(fields_to_analyse{i})(1));
                ref_loa_high.(fields_to_analyse{i}) =...
                    refline(0, loa.(fields_to_analyse{i})(2));
                
                set(ref_mean.(fields_to_analyse{i}),...
                    ps.REFLINE_NAMES, ps.REFLINE_VALUES_DASH)
                set(ref_loa_low.(fields_to_analyse{i}),...
                    ps.REFLINE_NAMES, ps.REFLINE_VALUES_SOLID)
                set(ref_loa_high.(fields_to_analyse{i}),...
                    ps.REFLINE_NAMES, ps.REFLINE_VALUES_SOLID)
                
                % Axes labels:
                switch filetype
                    case '.trc'
                        my_ylabel = ylabel({'log(Kinect2) - log(Vicon)';...
                            sprintf('%s', num2coord(j))});
                        my_xlabel = xlabel({'Average(log(Kinect2), log(Vicon))';...
                            sprintf('%s', num2coord(j))});
                    case '.mot'
                        my_ylabel = ylabel({'log(Kinect2) - log(Vicon)';...
                            sprintf('%s', fields_to_analyse{i})});
                        my_xlabel = xlabel({'Average(log(Kinect2), log(Vicon))';...
                            sprintf('%s', fields_to_analyse{i})});
                end
                set(my_ylabel, ps.LABEL_NAMES, ps.LABEL_VALUES);
                set(my_xlabel, ps.LABEL_NAMES, ps.LABEL_VALUES);
                
                
            case 'regression_transformed'
                % TODO: fix and complete with 95% CI.
                % Plot the linear regression:
                h_regr = plot(avg.(fields_to_analyse{i}),...
                    fit_dif.(fields_to_analyse{i}));
                set(h_regr, ps.REFLINE_NAMES, ps.REFLINE_VALUES_DASH)
                % Plot the limits of agreement calculated from the fit:
                h_low_loa = plot(avg.(fields_to_analyse{i}),...
                    fit_low_loa.(fields_to_analyse{i}));
                set(h_low_loa, ps.REFLINE_NAMES, ps.REFLINE_VALUES_SOLID);
                h_upp_loa = plot(avg.(fields_to_analyse{i}),...
                    fit_upp_loa.(fields_to_analyse{i}));
                set(h_upp_loa, ps.REFLINE_NAMES, ps.REFLINE_VALUES_SOLID);
                
                % Axes labels:
                switch filetype
                    case '.trc'
                        my_ylabel = ylabel({'Kinect2 - Vicon';...
                            sprintf('%s %s', num2coord(j), ps.UNIT_SYMBOL)});
                        my_xlabel = xlabel({'Average(Kinect2, Vicon)';...
                            sprintf('%s %s', num2coord(j), ps.UNIT_SYMBOL)});
                    case '.mot'
                        my_ylabel = ylabel({'Kinect2 - Vicon';...
                            sprintf('%s %s', fields_to_analyse{i}, ps.UNIT_SYMBOL)});
                        my_xlabel = xlabel({'Average(Kinect2, Vicon)';...
                            sprintf('%s %s', fields_to_analyse{i}, ps.UNIT_SYMBOL)});
                end
                set(my_ylabel, ps.LABEL_NAMES, ps.LABEL_VALUES);
                set(my_xlabel, ps.LABEL_NAMES, ps.LABEL_VALUES);
        end
        
        
        set(hax_ba, ps.AXIS_NAMES, ps.AXIS_VALUES)
        if j==1
            title_ba = title(fields_to_analyse{i});
            set(title_ba, ps.TITLE_NAMES, ps.TITLE_VALUES);
        end
        
        
        
        %% TODO: REPEATABILITY
        
        
        
        
        %% TODO: CONFIDENCE INTERVALS FOR THE REPEATABILITY
        
    end
end
clear i


%% SAVE FIGURES OF BLAND-ALTMAN ANALYSIS
if save_figures
    if ~exist(results_folder, 'dir')
        mkdir(results_folder);
    end
    
    fprintf('Saving figures in:\n\t%s.\n', results_folder);
    % The file name will contain all the filenames of the trials from
    % Kinect data:
    savefig(hfig_hist, fullfile(results_folder, ['Hist_', strjoin(kinect_name, '_'), '.fig']))
    savefig(hfig_agreement, fullfile(results_folder, ['B-A_', strjoin(kinect_name, '_'), '.fig']))
end


%% STRIDE ANALYSIS
% Using synced data and gait events found above, perform stride analysis on
% paired trials:
if analyse_stride
    stride_analysis(vicon_storage, kinect_storage,...
        'fields_to_analyse', fields_to_analyse, 'debug_mode', debug_mode);
end
    