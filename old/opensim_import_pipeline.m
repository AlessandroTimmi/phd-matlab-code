function [c3d_filenames, extracted_trc_filenames, extracted_mot_filenames] =...
    opensim_import_pipeline(varargin)
% Pre-process .c3d files and extract the content in .trc and .mot files,
% which are compatible with OpenSim.
%
% Input:
%   c3d_filenames = (optional parameter, default = '') string or cell array
%                  of strings containing full filenames of the .c3d file(s)
%                  to be converted.
%   dialog_title = (optional parameter, default = 'Select .c3d file(s) to
%                 convert') title of the dialog window used to select the
%                 c3d file(s).
%   this_markerset = (optional parameter, default = see below) string
%                 containing the name of the markerset to be used for
%                 extracting data from the .c3d file(s). If the string is
%                 empty, an input dialog will open and ask for the
%                 markerset name.
%   filter_data   = (optional parameter, default = false) true if you want
%                 to filter data.
%   butter_order  = (optional parameter, default = 0) order of Butterworth
%                 filter. If 0, opens a dialog to set it.
%   butter_cutoff = (optional parameter, default = 0) cutoff frequency of
%                 lowpass Butterworth filter in Hz. If 0, opens a dialog
%                 to set it.
%   trim_trial    = (optional parameter, default = true) true if you want
%                 to trim the trial, based on vertical ground reaction
%                 forces. Useuful to cut the trial in correspondence of foot
%                 initial/final contact.
%   Fv_min        = (optional parameter, default = 20 N) threshold on
%                 vertical force component (Fv [N]). Used in 3 instances:
%                 1) If the max Fv from a force platform is below or equal
%                 to this value, that force platform is completely
%                 ignored for trimming.
%                 2) Threshold used to detect contact events, defined as
%                 the first and last frames in which Fv > Fv_min. Force
%                 plates at CHESM lab sometimes have a random noise on the
%                 vertical axis ranging within +/- 10 N. For this reason,
%                 we set our default minimum threshold to 20 N, to ensure
%                 that random noise cannot generate false events.
%                 3) Used in btk_loadc3d() to set all points of wrench
%                 application computed with Fv <= Fv_min to the centre(?)
%                 of the force plate. See btkGroundReactionWrenches.m for
%                 further info.
%   rotate_vicon_to_opensim = (optional parameter, default = true) if
%                 true, applies a rotation to imported data, in order to
%                 transform coordinates from Vicon to OpenSim reference
%                 frame.
%   newRootFolder = (optional parameter, default = '') new root folder in
%                 which trials will be stored. If the string is not empty,
%                 it will replace the one in the current trial filename.
%   dest_fname = (optional parameter, default = '') name of the
%                 extracted trial. It must not contain any extension. Only
%                 usable when a single trial is selected.
%   debug_mode    = (optional parameter, default = false) if true, shows
%                 some extra info for debug purposes.
%
% Output
%
%   c3d_filenames = see above.
%   extracted_trc_filenames = full filenames of the extracted .trc files.
%   extracted_mot_filenames = full filenames of the extracted .mot files.
%
% Inspired by Matlab_OpenSim_Toolbox_v2, by Glen Lichtwark et al.
% � October 2014 Alessandro Timmi.

%% TODO:
% - add a check on missing markers, even if missing only in a part of the
%   trial.
% - there might be problems if a newRootFolder is specified, but the original
%   folder does not contain 'Dev' in the path.
% - add a list dialog for the markerset choice, when this_markerset is empty.
% - add debug plots and debug fprintf() where required.
% - move the variables for plotting (marker coord, GRF coord) to input
%   parser.
% - Add possibility to use different filters.
% - it looks like set_contact_events() doesn't work when the force plates
% where not actively used during the trial. The easy solution is to ensure
% that the "trim_trial" parameter is false, but it would be nice to make
% the function more robust in these cases.
% - Add a log reporting trials in which markers where missing.

%% Default input values:
default.c3d_filenames = '';
default.dialog_title = 'Select .C3D file(s) to convert';
default.this_markerset = 'Schache_CHESM_v092F';
default.filter_data = false;
default.butter_order = 0;
default.butter_cutoff = 0;
default.trim_trial = true;
default.Fv_min = 20;
default.rotate_vicon_to_opensim = true;
default.newRootFolder = '';
default.dest_fname = '';
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'c3d_filenames', default.c3d_filenames,...
    @(x) validateattributes(x, {'char', 'cell'}, {'nonempty'}));
addParameter(p, 'dialog_title', default.dialog_title,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'this_markerset', default.this_markerset,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'filter_data', default.filter_data, @islogical);
addParameter(p, 'butter_cutoff', default.butter_cutoff,...
    @(x) validateattributes(x, {'numeric'}, {'nonnegative'}));
addParameter(p, 'butter_order', default.butter_order, ...
    @(x) validateattributes(x, {'numeric'}, {'nonnegative'}));
addParameter(p, 'trim_trial', default.trim_trial, @islogical);
addParameter(p, 'Fv_min', default.Fv_min, @isnumeric);
addParameter(p, 'rotate_vicon_to_opensim',...
    default.rotate_vicon_to_opensim, @islogical);
addParameter(p, 'newRootFolder', default.newRootFolder,...
    @(x) validateattributes(x, {'char'}, {}));
addParameter(p, 'dest_fname', default.dest_fname,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
c3d_filenames = p.Results.c3d_filenames;
dialog_title = p.Results.dialog_title;
this_markerset = p.Results.this_markerset;
filter_data = p.Results.filter_data;
butter_cutoff = p.Results.butter_cutoff;
butter_order = p.Results.butter_order;
trim_trial = p.Results.trim_trial;
Fv_min = p.Results.Fv_min;
rotate_vicon_to_opensim = p.Results.rotate_vicon_to_opensim;
newRootFolder = p.Results.newRootFolder;
dest_fname = p.Results.dest_fname;
debug_mode = p.Results.debug_mode;


%%
if isempty(c3d_filenames)    
    assert(isempty(newRootFolder),...
        'Cannot change root folder when manually selecting .c3d files. Set parameter newRootfolder = ''''.')    
    
    % Manually select .c3d files in a folder:
    [fname, pname] = uigetfile('*.c3d', dialog_title,...
        'MultiSelect', 'on');
    
    % Cannot use ~fname as condition here, because it would fail when fname is a cell.
    if isequal(fname, 0)
        fprintf('No .c3d files selected.\n')
        return
    end
    
    c3d_filenames = fullfile(pname, fname);
        
    % Matlab diary will be stored in the same folder of the .c3d trials:
    diary(fullfile(pname, 'c3d_export.log'));
    
    clear pname fname    
else
    if newRootFolder
        % Check for its existence:
        if ~isequal(exist(newRootFolder, 'dir'), 7)
            % Create the folder if required:
            mkdir(newRootFolder);
        end
        % Matlab diary will be stored in the new root folder:
         diary(fullfile(newRootFolder, 'c3d_export.log'));
    end
    % otherwise don't store the diary at all.
end


%% Preallocate cell arrays for extracted filenames:
extracted_trc_filenames = cell(size(c3d_filenames));
% Since not all .c3d files have force plates data, some cells of
% extracted_mot_filenames might be left empty at the end:
extracted_mot_filenames = cell(size(c3d_filenames));


%% At the moment the destination filename can be passed as input argument
% only if a single trial is loaded:
if ~isempty(dest_fname)
    assert(isequal(length(c3d_filenames), 1),...
        'The destination filename can be specified only if a single trial is selected.')
end


%%
fprintf('OpenSim import pipeline\n')
if debug_mode
    fprintf('Inspired by Matlab_OpenSim_Toolbox_v2, by Glen Lichtwark et al.\n')
    fprintf('� October 2014 Alessandro Timmi.\n\n')
end

if isempty(this_markerset)
    answer = inputdlg('Name of the markerset to be used for extracting data from the .c3d file(s)',...
        'Markerset name?');
    this_markerset = answer{1};
end

%% Loop on the trial names:
for f=1:numel(c3d_filenames)
    
    % Get path and file name of current trial:
    [destFolder, fname, ~] = fileparts(c3d_filenames{f});
    
    % Change root folder if requested:
    if ~isempty(newRootFolder)
        destFolder = ChangeRootFolderACLStudy(destFolder, newRootFolder);
        
        % Check for its existence:
        if ~isequal(exist(destFolder, 'dir'), 7)
            % Create the folder if required:
            mkdir(destFolder);
        end
    end
    
    fprintf('Loaded file %d of %d:\n', f, numel(c3d_filenames))
    fprintf('\t%s\n', c3d_filenames{f})
    
    % Load the C3D file using BTK, but first suppress the warning from BTK
    % library related to the centre of the force plates:
    warning('off', 'btk:GetGroundReactionWrenches')
    % Fv_min here is used to set all points of wrench application computed
    % with a Fv <= Fv_min to the centre(?) of the force plate:
    data = btk_loadc3d(c3d_filenames{f}, Fv_min);
    
    % Marker names present in the loaded file:
    found_markers = fields(data.marker_data.Markers);
    
    % Load the proper markerset for this trial:
    [dynamic_markers_list, static_markers_list, calc_markers_list] =...
        markerset(this_markerset);
    
    % Assemble a cell array of the full markerset:
    full_markers_list = [dynamic_markers_list;...
        static_markers_list;...
        calc_markers_list];
    
    % Sort the C3D file so we know what is Marker data and what is calculated
    % data -- THIS STEP ISN'T REALLY REQUIRED FOR THIS DATASET WHERE ALL
    % THE MARKERS ARE REQUIRED
    data.marker_data = btk_sortc3d(data.marker_data,...
        full_markers_list, calc_markers_list);    
    
    if filter_data
        % Filter marker data:
        fprintf('Filtering marker data...\n')
        data.marker_data.Markers = filter_struct(data.marker_data.Markers,...
            'time', data.marker_data.Time,...
            'filter_type', 'butter',...
            'butter_order', butter_order,...
            'butter_cutoff', butter_cutoff,...
            'debug_mode', debug_mode);
    end    
    
    % If the loaded trial is NOT a static (a.k.a. calibration) one:
    if isempty(strfind(lower(fname), 'static')) &&...
            isempty(strfind(lower(fname), 'cal'))
        
        fprintf('This is a dynamic trial.\n')
        
        % Check for missing or extra markers using the dynamic markerset:
        CheckMissingExtraMarkers(dynamic_markers_list, found_markers);
        
        if filter_data
            % Filter force plates data, using the same filter applied to markers.
            % Loops on all the force platforms (i):
            for i = 1:length(data.fp_data.GRF_data)
                % Each force platform struct contains 3 fields:
                % 1) P (point of application);
                % 2) F (ground reaction force);
                % 3) M (ground reaction moment).
                fprintf('Filtering force plates data...\n')
                data.fp_data.GRF_data(i) = filter_struct(data.fp_data.GRF_data(i),...
                    'time', data.fp_data.Time,...
                    'filter_type', 'butter',...
                    'butter_order', butter_order,...
                    'butter_cutoff', butter_cutoff,...
                    'debug_mode', debug_mode);
            end
        end
        
        % Set the start and end frame based on vertical ground reaction
        % forces.
        if trim_trial
            data = set_contact_events(data, Fv_min, 'debug_mode', debug_mode);
        end        
        
        % Now do conversions to .trc and .mot files. This function also
        % transforms markers coordinates and GRF from Vicon to OpenSim
        % coordinate system:
        data = btk_c3d2trc('data', data,...
            'rotate_vicon_to_opensim', rotate_vicon_to_opensim,...
            'dest_folder', destFolder,...
            'dest_fname', dest_fname);        
        
    else
        fprintf('This is a static (a.k.a. calibration) trial.\n')
        
        % Check for missing or extra markers using the static markerset:
        static_markerset = [dynamic_markers_list;...
            static_markers_list];
        CheckMissingExtraMarkers(static_markerset, found_markers);
        
        % Convert the static data into a .trc file for scaling. This
        % function also transforms markers coordinates from Vicon
        % to OpenSim coordinate system:
        data = btk_c3d2trc('data', data,...
            'rotate_vicon_to_opensim', rotate_vicon_to_opensim,...
            'dest_folder', destFolder,...
            'dest_fname', dest_fname);
        
    end
    
    % Store full filenames of the extracted trials:
    extracted_trc_filenames{f} = data.TRC_Filename;
    if isfield(data,'GRF_Filename')
        extracted_mot_filenames{f} = data.GRF_Filename;
    end
    
end

fprintf('End of the pipeline.\n\n')
diary off

end
