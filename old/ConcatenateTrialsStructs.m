function [viconLong, kinectLong, viconArrayOfStructs, kinectArrayOfStructs] =...
    ConcatenateTrialsStructs(viconFilenames, kinectFilenames, subjectIdString)
% Concatenate Vicon and Kinect trials, to generate a single struct and/or
% an array of struct for each device. Pairs of Vicon and Kinect trials must
% be already synced and have same length (in terms of number of frames).
%
% Input:
%   viconFilenames = row cell array of Vicon .trc filenames.
%   kinectFilenames = row cell array of Kinect .trc filenames.
%   subjectIdString = string identifying subjects, e.g. 'TML'. Used to 
%                   detect the subject's number from the trial filename. 
%
% Output:
%   viconLong = struct containing all concatenated signals from Vicon
%               trials, as if it was a single long trial.
%   kinectLong = struct containing all concatenated signals from Kinect
%               trials, as if it was a single long trial.
%   viconArrayOfStructs = array of structs containing all Vicon trials,
%               not concatenated in a single struct yet.
%   kinectArrayOfStructs = array of structs containing all Kinect trials,
%               not concatenated in a single struct yet.
%
% � October 2014 Alessandro Timmi

% TODO:
% - this function currently doesn't support .mot files anymore. Make
% another function to concatenate .mot files and clean this function from
% all the code related to .mot files.
% - make an external function to check the number of columns for each
%   field of the struct (currently we are checking only the first field).


%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'viconFilenames',...
    @(x) validateattributes(x, {'cell'}, {'row', 'nonempty'}));
addRequired(p, 'kinectFilenames',...
    @(x) validateattributes(x, {'cell'}, {'row', 'nonempty'}));
addRequired(p, 'subjectIdString',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}))

% Parse input arguments:
parse(p, viconFilenames, kinectFilenames, subjectIdString);

% Copy input arguments into more memorable variables:
viconFilenames = p.Results.viconFilenames;
kinectFilenames = p.Results.kinectFilenames;
subjectIdString = p.Results.subjectIdString;

clear p


%% Sort Vicon and Kinect trials in alphabetical order. Better now than
% later, to avoid any mess with files order.
fprintf('Concatenating trials...\n');
fprintf('Sorting filenames in alphabetical order...\n')
viconFilenames = sort(viconFilenames);
kinectFilenames = sort(kinectFilenames);
fprintf('Vicon:\n')
fprintf('\t%s.\n', viconFilenames{:});
fprintf('Kinect:\n')
fprintf('\t%s.\n', kinectFilenames{:});


%% Check if the same trials have been selected for both Vicon and Kinect
% Compare number of trials:
if numel(viconFilenames)~=numel(kinectFilenames)
    error('Different number of trials selected for Vicon and Kinect.');
else
    nTrials = numel(viconFilenames);
    fprintf('Number of trials selected for each system: %d.\n', nTrials);
end

% Extract trial names and extensions from files:
viconName = cell(size(viconFilenames));
viconExt = cell(size(viconFilenames));
kinectName = cell(size(kinectFilenames));
kinectExt = cell(size(kinectFilenames));
for i=1:nTrials
    [~, viconName{i}, viconExt{i}] = fileparts(viconFilenames{i});
    [~, kinectName{i}, kinectExt{i}] = fileparts(kinectFilenames{i});
    
    % Check that Vicon and Kinect trial names in current pair match:
    if ~isequal(viconName{i}, kinectName{i})
        warning('Vicon and Kinect trial names don''t match.\n%s\n%s',...
            viconName{i}, kinectName{i});
        
        % If they are different, ask the user what to do:
        promptString = input('Continue anyway? (y/n) [y]: ', 's');
        if isempty(promptString)
            promptString = 'y';
        end
        
        switch promptString
            case {'Y', 'y'}
                fprintf('Not a problem, move on.\n')
            case {'N', 'n'}
                fprintf('User decided to stop the execution.\n')
                return
            otherwise
                fprintf('Wrong choice, stopping the execution.\n')
                return
        end
    end
    
end
clear i

fprintf('Trial names for Vicon and Kinect match.\n')

% Determine Vicon and Kinect file(s) type:
isViconMot = all(strcmpi('.mot', viconExt));
isKinectMot = all(strcmpi('.mot', kinectExt));
isViconTrc = all(strcmpi('.trc', viconExt));
isKinectTrc = all(strcmpi('.trc', kinectExt));

if isViconMot
    viconFileType = '.mot';
elseif isViconTrc
    viconFileType = '.trc';
else
    error('Selected Vicon trial(s) are not of the same type.')
end
fprintf('Vicon filetype: "%s".\n', viconFileType);

if isKinectMot
    kinectFileType = '.mot';
elseif isKinectTrc
    kinectFileType = '.trc';
else
    error('Selected Kinect trial(s) are not of the same type.')
end
fprintf('Kinect filetype: "%s".\n', kinectFileType);

% Compare Vicon and Kinect file types:
if ~strcmpi(viconFileType, kinectFileType)
    % If they are not the same, raise an error:
    error('Different file types loaded for Vicon (%s) and Kinect (%s).',...
        viconFileType, kinectFileType)
else
    % File type will affect several aspects of the code below:
    filetype = viconFileType;
    fprintf('File types match.\n');
end

% The number of columns for each field depends on the file type:
nColumns = number_of_columns_per_field(filetype);


%% Load each pair of trials:
for f=1:nTrials
    fprintf('\nPair of trials number %d of %d.\n', f, nTrials)
    
    %% Load files to be compared:
    switch filetype
        case '.trc'
            viconStruct = LoadTrcFile(...
                'filename', viconFilenames{f},...
                'logMessage', sprintf('Loading Vicon .trc file...\n'),...
                'dialogTitle', sprintf('Select Vicon .trc file\n'));
            kinectStruct = LoadTrcFile(...
                'filename', kinectFilenames{f},...
                'logMessage', sprintf('Loading Kinect .trc file...\n'),...
                'dialogTitle', sprintf('Select Kinect .trc file\n'));
            
        case '.mot'
            viconStruct = LoadMotFile('filename', viconFilenames{f});
            kinectStruct = LoadMotFile('filename', kinectFilenames{f});
            
            % Rename "coords" field to "markers" to match the format used
            % for .trc files:
            % TODO: I now have a function for renaming struct fields.
            viconStruct.markers = viconStruct.coords;
            kinectStruct.markers = kinectStruct.coords;
            viconStruct = rmfield(viconStruct, 'coords');
            kinectStruct = rmfield(kinectStruct, 'coords');
            
            [~, ~, ~, viconStruct.DataRate] = check_sampling_freq(viconStruct.time);
            [~, ~, ~, kinectStruct.DataRate] = check_sampling_freq(kinectStruct.time);
    end
    
    
    %% Check field names and number of coordinates for current trials:
    if f == 1
        % Read field names from current trials, sorting them in
        % alphabetical order:
        viconSortedFieldNames = sort(fieldnames(viconStruct.markers));
        kinectSortedFieldNames = sort(fieldnames(kinectStruct.markers));
        
        % Check that Vicon and Kinect trials have same field names. The
        % order doesn't matter:
        if isequal(viconSortedFieldNames, kinectSortedFieldNames)
            sortedFieldnames = viconSortedFieldNames;
        else
            error('Vicon and Kinect marker names don''t match for this trial.')
        end
    else
        % Compare field names of current trials with those from previous
        % trials, to ensure consistency:
        assert(isequal(sort(fieldnames(viconStruct.markers)), sortedFieldnames),...
            'Field names from Vicon trial number %d don''t match those from previous trials.', f);
        assert(isequal(sort(fieldnames(kinectStruct.markers)), sortedFieldnames),...
            'Field names from Kinect trial number %d don''t match those from previous trials.', f);
    end
    
    % Number of coordinates per field. Markers have 3 coords: X, Y and Z.
    % Joint angles have 1 coordinate per field. Here we assume that
    % all fields in a trial have the same number of coordinates, so we
    % check only the first field for each trial against the expected number
    % of columns:
    nColumnsVicon = size(viconStruct.markers.(viconSortedFieldNames{1}), 2);
    nColumnsKinect = size(kinectStruct.markers.(kinectSortedFieldNames{1}), 2);
    assert(isequal(nColumns, nColumnsVicon),...
        'Wrong number of coordinates for Vicon fields: expected %d, found %d.', nColumns, nColumnsVicon);
    assert(isequal(nColumns, nColumnsKinect),...
        'Wrong number of coordinates for Kinect fields: expected %d, found %d.', nColumns, nColumnsKinect);
    
    
    % Get subject's ID number from filenames and add it to the structs:
    subjectRegularExpression = [subjectIdString '(\d+)'];
    viconSubjectString = regexp(viconFilenames{f},...
        subjectRegularExpression, 'tokens', 'once');
    kinectSubjectString = regexp(kinectFilenames{f},...
        subjectRegularExpression, 'tokens', 'once');
    viconStruct.subjectNumber = str2double(viconSubjectString{1});
    kinectStruct.subjectNumber = str2double(kinectSubjectString{1});
    
    % Check that the subject's ID number is the same for current Vicon and Kinect trials:
    assert(isequal(viconStruct.subjectNumber, kinectStruct.subjectNumber),...
        'Different subjects'' ID numbers for Vicon and Kinect trials:\n%s\n%s',...
        viconFilenames{f}, kinectFilenames{f});
   
    
    %% Store current structs in an array of structs, to be concatenated later.
    % No need to preallocate the structs in this case, as it would just
    % create an error when copying during the first iteration, without any
    % improvement in terms of performance.
    viconArrayOfStructs(f) = viconStruct; %#ok<AGROW>
    kinectArrayOfStructs(f) = kinectStruct; %#ok<AGROW>
        
    
end
clear f


%% Concatenate array of markers structs in a single struct:
% Matlab cannot index structs with more than one level
% using colon (:).
% Hence we need to reduce the two-level array of markers structs:
%   Trial(1).markers.Marker_1
%   Trial(1).markers.Marker_2
%   Trial(2).markers.Marker_1
%   Trial(2).markers.Marker_2
%   ...
%   Trial(N).markers.Marker_1
%   Trial(N).markers.Marker_2
%
% to one-level before indexing it. In this way we obtain an array of
% structs containing markers coordinates with this structure (for both
% Vicon and Kinect):
%   Trial(1).Marker_1 .Marker_2 ... .Marker_M;
%   Trial(2).Marker_1 .Marker_2 ... .Marker_M;
%   ...
%   Trial(N).Marker_1 .Marker_2 ... .Marker_M;
viconMarkersOneLevel = horzcat(viconArrayOfStructs(:).markers);
kinectMarkersOneLevel = horzcat(kinectArrayOfStructs(:).markers);

% Build a new struct containing all concatenated signals from trials,
% as if it was a single long trial:
for n=1:numel(sortedFieldnames)
    viconLong.markers.(sortedFieldnames{n}) =...
        vertcat(viconMarkersOneLevel(:).(sortedFieldnames{n}));
    
    kinectLong.markers.(sortedFieldnames{n}) =...
        vertcat(kinectMarkersOneLevel(:).(sortedFieldnames{n}));
end


%% Check lengths of concatenated trials:
for n=1:numel(sortedFieldnames)
    lengthVicon = length(viconLong.markers.(sortedFieldnames{n}));
    lengthKinect = length(kinectLong.markers.(sortedFieldnames{n}));
    
    assert(isequal(lengthVicon, lengthKinect),...
        'Field %d has different length for Vicon (%d) and Kinect (%d) concatenated trials.',...
        n, lengthVicon, lengthKinect);
end

fprintf('\nConcatenated Vicon and Kinect trials have same length.\n\n')


