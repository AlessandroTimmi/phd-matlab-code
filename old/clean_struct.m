function s = clean_struct(s, varargin)
% Clean marker coordinates contained into a struct, performing the
% following operations:
% 1) fill the gaps if present in the input struct;
% 2) filter data using the specified or default technique;
% 3) return resulting data into a new struct.
%
% Input:
%   s            = struct containing trial data.
%   max_gap_size = (optional, default = []) Max size of gaps (in frames)
%                   which will be filled. Gaps larger than this will be
%                   filled with "Nan" and a warning message will be
%                   printed on screen.
%   filter_type  = (optional, default = '') one of the following strings:
%                   'none', 'butter', 'sgolay', 'median', 'median_sgolay'.
%   butter_cutoff = (optional, default = 0) Cutoff frequency for
%                   Butterworth filter.
%   butter_order  = (optional, default = 0) Order of Butterworth filter.
%   sgolay_order  = (optional, default = 0) Order of Savitzky-Golay filter.
%   sgolay_span   = (optional, default = 0) Span of Savitzky-Golay filter.
%   median_span   = (optional, default = 0) span of median filter.
%                   for other filter options see file filter_struct.m.
%   debug_mode    = (optional parameter, default = false). If true, shows
%                   extra info for debug purposes.
%
% Output:
%   s_clean       = struct containing cleaned trial data.
%
% � November 2014 Alessandro Timmi


%% TODO:
% - currently all fields of the struct are filled: however, we should avoid
%   doing this for "frames" and "time" fields. We can use the function
%   "IsFiltrable" to detect these fields.
% - TRY TO PASS FILTER SETTINGS ALL TOGETHER INTO A STRUCT
% - Add intermediate plots at each step of this function.
% - Add support for .mot files and change the name of the function
%   accordingly.
% - Print filter settings in the log file.


%% Default input values:
default.max_gap_size = [];
default.filter_type = '';
expected_filters = {'none', 'butter', 'sgolay', 'median', 'median_sgolay'};
default.butter_cutoff = 0;  % cutoff freq. for lowpass Butter. filter (Hz)
default.butter_order = 0;   % Butterworth filter order
default.sgolay_order = 0;   % Savitzky-Golay filter order
default.sgolay_span = 0;    % (samples)
default.median_span = 0;    % (samples)
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 's', @isstruct);

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'max_gap_size', default.max_gap_size,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'filter_type', default.filter_type,...
    @(x) any(validatestring(x, expected_filters)));
addParameter(p, 'butter_cutoff', default.butter_cutoff,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'butter_order', default.butter_order,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'sgolay_order', default.sgolay_order,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'sgolay_span', default.sgolay_span,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'median_span', default.median_span,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, s, varargin{:});

% Copy input arguments into more memorable variables:
s = p.Results.s;
max_gap_size = p.Results.max_gap_size;
filter.type = p.Results.filter_type;
filter.butter.cutoff = p.Results.butter_cutoff;
filter.butter.order = p.Results.butter_order;
filter.sgolay.order = p.Results.sgolay_order;
filter.sgolay.span = p.Results.sgolay_span;
filter.median.span = p.Results.median_span;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
fprintf('Cleaning data...\n');
if debug_mode
    fprintf('� November 2014 Alessandro Timmi.\n');
end

if isempty(max_gap_size)
    % Set max gap size:
    answer = {};
    while isempty(answer)
        prompts = ['Gaps larger than this will be filled with "Nan"',...
            'and a warning message will be printed on screen:'];
        dlg_title = 'Set max gap size';
        num_lines = 1;
        default_ans = {'10'};
        options.WindowStyle = 'normal';
        options.Resize = 'on';
        answer = inputdlg(prompts, dlg_title, num_lines, default_ans, options);
    end
    max_gap_size = str2double(answer{1});
end

% We don't want to extrapolate because, due to big gaps in coloured
% markers data, we would obtain very bad results. Since the spline
% interpolation requires a form of extrapolation, we use Nan as default
% value to fill the gaps at the extremities of the arrays.
%extrapval = nan;
% However, now that we improved our colour marker tracking algorithm and
% we have smaller gaps, it is better to extrapolate. Otherwise, filtering
% will increase these gaps at the extremities, making the problem much
% worse. Hence, we leave the default behaviour of the spline interpolation,
% which is to extrapolate.


%% Choose the filter type:
if isempty(filter.type)
    ok = false;
    while ~ok
        [selection, ok] = listdlg('ListString', expected_filters,...
            'SelectionMode', 'single',...
            'InitialValue', 5,...
            'Name', 'Filter selection',...
            'PromptString', 'Select filter type:');
    end
    filter.type = expected_filters{selection};
end


%% Clean data
% Fill the gaps if present, using spline interpolation. Gaps larger
% than max_gap_size are filled with Nan by default. OpenSim
% seems to accept Nan without any problem, hence I prefer Nan to empty
% positions, because at least it is easier to determine the length of a
% vector containing Nan compared to an empty vector.
fprintf('Filling gaps in the coordinate arrays...\n');
fprintf('Max size of gaps to be filled: %d frames.\n', max_gap_size)

marker_names = fieldnames(s.markers);

for m = 1 : length(marker_names)
    for c=1:3
        s.markers.(marker_names{m})(:,c) =...
            interp1gap(s.markers.(marker_names{m})(:,c),...
            'spline', max_gap_size);
    end
end

% If requested, filter filled data:
if strcmpi(filter.type, 'none')
    fprintf('Filtering not requested by the user.\n')
else
    s.markers = filter_struct(s.markers,...
        'time', s.time,...
        'filter_type', filter.type,...
        'butter_cutoff', filter.butter.cutoff,...
        'butter_order', filter.butter.order,...
        'sgolay_order', filter.sgolay.order,...
        'sgolay_span', filter.sgolay.span,...
        'median_span', filter.median.span,...
        'debug_mode', debug_mode);
end



end