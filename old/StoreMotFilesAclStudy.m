function StoreMotFilesAclStudy(kinectIkResultsFolder, viconIkResultsFolder,...
    outputRootFolder)
% Copy inverse kinematics results and GRF data (.mot files) from Kinect and
% Vicon folders to a new folder, preserving the original folders structure.
%
% Input:
%   kinectIkResultsFolder = root folder containing all Kinect files,
%                      including Inverse Kinematics (IK) results.
%   viconIkResultsFolder = root folder containing all Vicon files,
%                      including Inverse Kinematics (IK) results.
%   outputRootFolder = destination folder in which Kinect and Vicon .mot
%                      files will be copied.
%
% � August 2016 Alessandro Timmi


%% Vicon and Kinect output subfolders:
kinectOutputFolder = fullfile(outputRootFolder, 'Kinect');
viconOutputFolder = fullfile(outputRootFolder, 'Vicon');


% Subjects prefix (case insensitive):
subjectsPrefix = 'Dev';

% Trials identifier string. We want to include IK results and GRF files:
trialsIdentifierString = 'SLS*.mot';

fprintf('Storing Kinect and Vicon IK results files in the following folder:\n')
fprintf('\t%s\n\n', outputRootFolder);


%% Load Kinect IK results:
fprintf('Loading Kinect IK results from:\n\t%s\n', kinectIkResultsFolder)
kinectMot = list_trials_in_all_subjects(kinectIkResultsFolder, subjectsPrefix,...
    {''}, '', trialsIdentifierString);
fprintf('Found %d files:\n', length(kinectMot))
fprintf('\t%s\n', kinectMot{:})

CopyTrialsToFolder(kinectMot, subjectsPrefix, kinectOutputFolder);


%% Load Vicon IK results and GRF data:
fprintf('Loading Vicon IK results and GRF data from:\n\t%s\n', viconIkResultsFolder)
viconMot = list_trials_in_all_subjects(viconIkResultsFolder, subjectsPrefix,...
    {'Barefoot'}, '', trialsIdentifierString);
fprintf('Found %d files:\n', length(viconMot))
fprintf('\t%s\n', viconMot{:})

CopyTrialsToFolder(viconMot, subjectsPrefix, viconOutputFolder);
fprintf('\n')

end



function CopyTrialsToFolder(trialsFilenames, subjectsPrefix,...
    outputFolder)
% Copy trials from original to destination folder, preserving the initial
% structure of subjects' folders, but removing condition subfolders (e.g.
% 'Barefoot').
%
% Input:
%   trialsFilenames = cell array containing trials filenames.
%   subjects_prefix = case insensitive string containing the prefix of the
%           subjects' folders names. Generally a 3 characters string,
%           e.g. 'Dev'.
%   outputRootFolder = folder in which all Vicon and Kinect trials will be
%           copied, maintaining the original structure of subjects'
%           subfolders.
%
% � August 2016 Alessandro Timmi


for i=1:length(trialsFilenames)
    expression = [subjectsPrefix, '(\d+)'];
    
    % Get current subject number, using regular expressions and tokens:
    subjectNumber = regexpi(trialsFilenames{i}, expression, 'tokens', 'once');
    
    if isempty(subjectNumber)
        error('Subject number not found in trial:\n\t%s', trialsFilenames{i})
    end
    
    outputSubjectFolder = fullfile(outputFolder, [subjectsPrefix, subjectNumber{1}]);
    % If the folder doesn't exist, we need to create it, otherwise copyfile()
    % will raise an error because it cannot find the path specified:
    if ~isequal(exist(outputSubjectFolder, 'dir'), 7)
        mkdir(outputSubjectFolder);
    end
    
    % Copy current trial to the subject's output folder:
    copyfile(trialsFilenames{i}, outputSubjectFolder);
end

fprintf('Copied %d files to:\n\t%s\n', length(trialsFilenames), outputFolder)
end