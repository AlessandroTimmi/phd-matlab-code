function s_filt = filter_struct(s, varargin)
% This function filters a struct containing coordinates, using one among
% several possible filtering techniques.
% NOTE: all filtering functions used below operate along the first
% nonsingleton dimension of the input matrix.
%
% Input:
% s             = a struct whose first level fields contain marker
%                 coordinates or joint angles.
% time          = (optional, default = []) column vector of timestamps, to
%                  be passed as input argument when it is not already part
%                  of the input struct.
% filter_type   = (optional, default = []) one of the following strings:
%                 'butter', 'sgolay', 'median', 'median_sgolay'.
% butter_cutoff = (optional, default = 0) Cutoff frequency for Butterworth
%                 filter. If 0, opens a dialog to set it.
% butter_order  = (optional, default = 0) order of Butterworth filter.
%                 If 0, opens a dialog to set it.
% sgolay_order  = (optional, default = 0) order of Savitzky-Golay filter.
%                 If 0, opens a dialog to set it.
% sgolay_span   = (optional, default = 0) span of Savitzky-Golay filter.
%                 If 0, opens a dialog to set it.
% median_span   = (optional, default = 0) span of median filter.
%                 If 0, opens a dialog to set it.
% debug_mode    = (optional, default = false) plots results filtered with
%                 different techniques.
%
% Output:
% s_filt        = the filtered struct.
%
% � November 2014 Alessandro Timmi

% TODO:
% TRY TO PASS FILTER SETTINGS ALL TOGETHER INTO A STRUCT
% - add filter settings as output arguments for this function.
% - When there are gaps in data, filter only the good data instead of
%   returning the vector as it is (see function IsFiltrable() below)


%% Default input values:
default.time = [];
default.filter_type = '';
expected_filters = {'butter', 'sgolay', 'median', 'median_sgolay'};
default.butter_order = 0; % Butterworth filter order
default.butter_cutoff = 0; % cutoff frequency for lowpass butter filter (Hz)
default.sgolay_order = 0; % Savitzky-Golay filter order
default.sgolay_span = 0; % (samples)
default.median_span = 0; % (samples)
default.debug_mode = false;


% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 's', @isstruct);

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'time', default.time, @(x) validateattributes(x,...
    {'numeric'}, {'column', 'increasing'}));
addParameter(p, 'filter_type', default.filter_type,...
    @(x) any(validatestring(x, expected_filters)));
addParameter(p, 'butter_cutoff', default.butter_cutoff,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'butter_order', default.butter_order,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'sgolay_order', default.sgolay_order,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'sgolay_span', default.sgolay_span,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'median_span', default.median_span,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, s, varargin{:});

% Copy input arguments into more memorable variables:
s = p.Results.s;
time = p.Results.time;
filter.type = p.Results.filter_type;
filter.butter.cutoff = p.Results.butter_cutoff;
filter.butter.order = p.Results.butter_order;
filter.sgolay.order = p.Results.sgolay_order;
filter.sgolay.span = p.Results.sgolay_span;
filter.median.span = p.Results.median_span;
debug_mode = p.Results.debug_mode;


%%
fprintf('Filtering data...\n')


%% Check data sampling frequency:
if isfield(s, 'time')
    % "time" array is a field of the input struct:
    [~, ~, ~, sampling_freq] = check_sampling_freq(s.time, 'debug_mode', debug_mode);
    
elseif iscolumn(time)
    % "time" is not a field and was passed as separate column vector:
    [~, ~, ~, sampling_freq] = check_sampling_freq(time, 'debug_mode', debug_mode);
    
else
    error('"time" array missing or not a column vector.');
end


%% Get the names of the fields in the input struct:
field_names = fieldnames(s);


%% Choose the filter type:
if isempty(filter.type)
    ok = false;
    while ~ok
        [selection, ok] = listdlg('ListString', expected_filters,...
            'SelectionMode', 'single',...
            'InitialValue', 4,...
            'Name', 'Filter selection',...
            'PromptString', 'Select filter type:');
    end
    filter.type = expected_filters{selection};
end


%% Filter coordinates using the selected method:
fprintf('Selected filter: ')
switch filter.type
    
    case 'butter'
        fprintf('Butterworth.\n')
        
        % Butterworth filter settings:
        if ~filter.butter.order || ~filter.butter.cutoff
            answer = {};
            while isempty(answer)
                prompts = {'Enter filter order', 'Enter cutoff frequency'};
                dlg_title = 'Setup Butterworth filter:';
                num_lines = 1;
                default_ans = {'4', '20'};
                options.WindowStyle = 'normal';
                options.Resize = 'on';
                answer = inputdlg(prompts, dlg_title, num_lines, default_ans, options);
            end
            filter.butter.order = str2double(answer{1});
            filter.butter.cutoff = str2double(answer{2});
        end
        % As suggested by Matlab Help for butter filter, the normalized
        % cutoff frequency used by the filter will be calculated as:
        % Wn = cutoff_frequency / Nyquist_frequency,
        % where Nyquist_frequency is half the sampling rate.
        fprintf('Filter design:\n');
        fprintf('Cutoff frequency = %.1f Hz;\n', filter.butter.cutoff);
        fprintf('Order = %d;\n', filter.butter.order);
        
        nyquist_freq = sampling_freq / 2;
        fprintf('Nyquist frequency (1/2 sampling frequency of current trial) = %.1f Hz.\n', nyquist_freq)
        
        % TODO: look at zp2sos() function and try to implement the filter using the
        % z,p,k method instead of a,b for improved numerical stability:
        [b, a] = butter(filter.butter.order, filter.butter.cutoff / nyquist_freq, 'low');
        
        % Apply Butterworth filter to each field of the input struct:
        for i=1:numel(field_names)
            
            % Store current unfiltered field into a temporary array,
            % just because it has a shorter name:
            y = s.(field_names{i});
            
            if ~IsFiltrable(field_names{i}, y, 'debug_mode', debug_mode)
                s_filt.(field_names{i}) = y;
                continue
            end
            
            % Zero phase shift filtering:
            % TODO: filtfilt() is a zero phase shifting filtering techniques, but
            % it doubles the order of the filter. Maybe we should halve the order of
            % the filter at design time (see: Winter 2009).
            s_filt.(field_names{i}) = filtfilt(b, a, y);
        end
        
        
    case 'median'
        fprintf('Median.\n')
        
        if ~filter.median.span
            % Median filter settings:
            answer = {};
            while isempty(answer)
                prompts = 'Enter filter span';
                dlg_title = 'Setup Median filter:';
                num_lines = 1;
                default_ans = {'5'};
                options.WindowStyle = 'normal';
                options.Resize = 'on';
                answer = inputdlg(prompts, dlg_title, num_lines, default_ans, options);
            end
            filter.median.span = str2double(answer{1});
        end
        fprintf('Filter design:\n');
        fprintf('Median span = %d.\n', filter.median.span);
        
        
        % Apply median filter to data:
        for i=1:numel(field_names)
            
            % Store current unfiltered field into a temporary array,
            % just because it has a shorter name:
            y = s.(field_names{i});
            
            if ~IsFiltrable(field_names{i}, y, 'debug_mode', debug_mode)
                s_filt.(field_names{i}) = y;
                continue
            end
            
            % Even if Matlab documentation is not clear on this aspect, the median
            % filter order represents the number of samples the median is
            % calculated on, so it is actually a span (or window), as in Python
            % Scipy implementation:
            s_filt.(field_names{i}) = medfilt1(y, filter.median.span);
            
        end
        
    case 'sgolay'
        fprintf('Sgolay.\n')
        
        if ~filter.sgolay.order || ~filter.sgolay.span
            % Savitzky-Golay filter settings:
            answer = {};
            while isempty(answer)
                prompts = {'Enter filter order', 'Enter filter span'};
                dlg_title = 'Setup Sgolay filter:';
                num_lines = 1;
                default_ans = {'4', '19'};
                options.WindowStyle = 'normal';
                options.Resize = 'on';
                answer = inputdlg(prompts, dlg_title, num_lines, default_ans, options);
            end
            filter.sgolay.order = str2double(answer{1});
            filter.sgolay.span = str2double(answer{2});
        end
        
        fprintf('Filter design:\n');
        fprintf('Sgolay order = %d;\n', filter.sgolay.order);
        fprintf('Sgolay span = %d.\n', filter.sgolay.span);
        
        % Apply filter to data:
        for i=1:numel(field_names)
            
            % Store current unfiltered field into a temporary array,
            % just because it has a shorter name:
            y = s.(field_names{i});
            
            if ~IsFiltrable(field_names{i}, y, 'debug_mode', debug_mode)
                s_filt.(field_names{i}) = y;
                continue
            end
            
            s_filt.(field_names{i}) = sgolayfilt(y, filter.sgolay.order,...
                filter.sgolay.span);
        end
        
    case 'median_sgolay'
        fprintf('Median + Sgolay.\n')
        
        if ~filter.median.span
            % Median filter settings:
            answer = {};
            while isempty(answer)
                prompts = 'Enter filter span';
                dlg_title = 'Setup Median filter:';
                num_lines = 1;
                default_ans = {'5'};
                options.WindowStyle = 'normal';
                options.Resize = 'on';
                answer = inputdlg(prompts, dlg_title, num_lines, default_ans, options);
            end
            filter.median.span = str2double(answer{1});
        end
        
        fprintf('Filter design:\n');
        fprintf('Median span = %d.\n', filter.median.span);
        
        if ~filter.sgolay.order || ~filter.sgolay.span
            % Savitzky-Golay filter settings:
            answer = {};
            while isempty(answer)
                prompts = {'Enter filter order', 'Enter filter span'};
                dlg_title = 'Setup Sgolay filter:';
                num_lines = 1;
                default_ans = {'4', '19'};
                options.WindowStyle = 'normal';
                options.Resize = 'on';
                answer = inputdlg(prompts, dlg_title, num_lines, default_ans, options);
            end
            filter.sgolay.order = str2double(answer{1});
            filter.sgolay.span = str2double(answer{2});
        end
        
        fprintf('Sgolay order = %d, span = %d.\n', filter.sgolay.order,...
            filter.sgolay.span);
        
        % Apply filter to data:
        for i=1:numel(field_names)
            
            % Store current unfiltered field into a temporary array,
            % just because it has a shorter name:
            y = s.(field_names{i});
            
            if ~IsFiltrable(field_names{i}, y, 'debug_mode', debug_mode)
                s_filt.(field_names{i}) = y;
                continue
            end
            
            % Apply median filter:
            y_median = medfilt1(y, filter.median.span);
            % Apply Savitzky-Golay filter:
            s_filt.(field_names{i}) = sgolayfilt(y_median,...
                filter.sgolay.order, filter.sgolay.span);
        end
end


end
