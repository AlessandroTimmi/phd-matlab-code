function [bool, varargout] = is_unit_vectors_matrix(M)
% Check if column vectors of M are unit vectors, i.e. if they have been
% normalized.
% 
% Input:
%   M = the matrix whose column vectors must be checked.
%
% Output:
%   bool = true if all columns are unit vectors, false otherwise.
%   colnorm = [optional] norms of column vectors of M.
%
% � July 2015 Alessandro Timmi

% TODO
% ) Find a better name for this function, such as is_unit_vectors_matrix().

nargoutchk(0,2)

%% Compute the norms of the column vectors of the input matrix:
colnorm = sqrt(sum(M.^2, 1));

if all(abs(colnorm-1)<=eps(1))
    % Solution of the inequality above is: 1 - eps <= colnorm <= 1 + eps.
    % If all the norms are within the tolerance given by the roundoff
    % error around 1, then all columns of the input matrix can be
    % considered unit vectors:
    bool = true;
else
    % Otherwise some columns are not unit vectors:
    bool = false;
end

if nargout==2
    % Return also colnorm:
    varargout{1} = colnorm;
end

end


