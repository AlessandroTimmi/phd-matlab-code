function [s_transf, orientation_trc, rig_vicon_trc, rig_kinect_trc] =...
    transform_vicon_coords_to_kinect_with_refl_markers(s, varargin)
% This function applies a coordinate transformation from Vicon to Kinect
% space, using data collected with reflective markers.
%
% Input:
%            s = struct containing 3D coordinates of points to be
%                transformed from Vicon to Kinect space.
%   orientation_trc = (optional parameter, default = '') .trc file 
%                containing coordinates of reflective markers attached on
%                Kinect surface, used for estimating Kinect orientation in
%                Vicon space. If the string is empty, a dialog will open 
%                to select a .trc file.
%	rig_vicon_trc = (optional parameter, default = '') .trc file
%                containing coordinates of a 3D printed rig in Vicon space.
%                This point will be used to determine the translation
%                vector from Vicon to Kinect origin. If the string is
%                empty, a dialog will open to select a .trc file.
%	rig_kinect_trc = (optional parameter, default = '') .trc file
%                containing coordinates of a 3D printed rig in Kinect
%                space. This point will be used to determine the
%                translation vector from Vicon to Kinect origin. If the 
%                string is empty, a dialog will open to select a .trc file.
%   debug_mode = (optional parameter, default = false) if true, shows some
%                extra info for debug purposes.
%
% Output:
%     s_transf = struct containing coordinates transformed from Vicon
%                to Kinect space.
%   orientation_trc = filename of the selected .trc file used to
%                determine the orientation of Kinect frame in Vicon space.
%	rig_vicon_trc = filename of the selected .trc file containing rig 
%                position in Vicon space.
%	rig_kinect_trc = filename of the selected .trc file containing rig 
%                position in Kinect space.
%
% � October 2015 Alessandro Timmi

%% TODO:
% - See if we can avoid converting from struct to cell and just copy each
% field of the struct into a matrix. Also, check the time performance of
% the two methods.
% - Plot results.
% This function is almost identical to transform_vicon_coordinates_to_kinect().
% See if there is a way to merge them.



%% Default input values:
default.orientation_trc = '';
default.rig_vicon_trc = '';
default.rig_kinect_trc = '';
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input parameters:
addRequired(p, 's', @isstruct);

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'orientation_trc', default.orientation_trc, @ischar);
addParameter(p, 'rig_vicon_trc', default.rig_vicon_trc, @ischar);
addParameter(p, 'rig_kinect_trc', @ischar);
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, s, varargin{:});

% Copy input arguments into more memorable variables:
s = p.Results.s;
orientation_trc = p.Results.orientation_trc;
rig_vicon_trc = p.Results.rig_vicon_trc;
rig_kinect_trc = p.Results.rig_kinect_trc;
debug_mode = p.Results.debug_mode;

clear varargin default p

fprintf('Applying coordinate transformation from Vicon to Kinect space,\n')
fprintf('based on reflective markers data and 3D-printed rig data...\n')
if debug_mode
    fprintf('[Debug] � October 2015 Alessandro Timmi.\n')
end



%% Determine transformation:
% Rk = rotation matrix of Kinect in Vicon space.
% d_vk = distance from Vicon to Kinect origin, expressed in Vicon space.
[Rk, d_vk, orientation_trc, rig_vicon_trc, rig_kinect_trc] = pose_estimation_kinect_in_vicon(...
    'orientation_trc', orientation_trc,...
    'rig_vicon_trc', rig_vicon_trc,...
    'rig_kinect_trc', rig_kinect_trc,...
    'debug_mode', debug_mode);


%% Apply coordinate transformation from Vicon to Kinect space:
% Convert markers from struct to cell array:
a = struct2cell(s.markers);
% Convert from cell array to matrix:
rv = cell2mat(a');

% Preallocate the array of transformed coordinates:
rk = zeros(size(rv));

% For each frame, apply the coordinate transformation to each 3D point:
for f=1:length(s.time)
    % Reshape the current frame row as a 3 x n_points matrix:
    Pv = reshape(rv(f,:), 3, []);
    % Apply transformation to each 3D point according to this equation:
    % {rk} = [Rk]' * {rv - d_vk}
    Pk = Rk' * bsxfun(@minus, Pv, d_vk);
    % Reshape back to row format:
    rk(f,:) = reshape(Pk, 1, []);
end

% Create the output struct as copy of the input one, except for marker
% coordinates:
s_transf = rmfield(s, 'markers');

% Read marker names from input struct:
marker_names = fieldnames(s.markers);

for j=1:s.NumMarkers
    % Copy each markers from the transformed coordinates matrix into the
    % destination struct:
    s_transf.markers.(marker_names{j}) = rk(:, (j-1)*3+(1:3));
end

end


