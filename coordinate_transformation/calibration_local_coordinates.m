function local_coords = calibration_local_coordinates(...
    calibration_object, varargin)
% Returns the coordinates of calibration markers expressed in the local
% reference frame of calibration object.
%
% Input:
%   calibration_object = string containig the name of the object used for
%                calibration.
%   debug_mode    = (optional parameter, default = false). If true, shows
%                   extra info for debug purposes.
%
% Output:
%   local_coords = coordinates of calibration markers expressed in the
%                reference frame of the calibration object.
%
% � October 2015 Alessandro Timmi


%% Default input values:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'calibration_object', @(x) validateattributes(x, {'char'},...
    {'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, calibration_object, varargin{:});

% Copy input arguments into more memorable variables:
calibration_object = p.Results.calibration_object;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
fprintf('Getting local coordinated of calibration markers...\n')
if debug_mode
    fprintf('� October 2015 Alessandro Timmi.\n');
end

switch calibration_object
    case {'L-frame_theoretical', 'l-frame_theoretical'}
        % Coordinates of the markers in the L-frame reference system:
        local_coords.Ov = [0.020; 0.020; 0.032];
        local_coords.X1 = [0.350; 0.020; 0.032];
        local_coords.X2 = [0.450; 0.020; 0.032];
        local_coords.Y1 = [0.020; 0.400; 0.032];
        
    case {'L-frame_adjusted', 'l-frame_adjusted'}
        % Taken measurements again on 19 October 2015, taking into account
        % the holes in the carpet, which increase the Z coordinate by 5 mm:
        local_coords.Ov = [0.020; 0.020; 0.037];
        local_coords.X1 = [0.350; 0.020; 0.037];
        local_coords.X2 = [0.4495; 0.020; 0.037];
        local_coords.Y1 = [0.020; 0.399; 0.037];
                
    case {'A3_sheet', 'a3_sheet'}
        % Printed coloured circles on A3 calibration sheet:
        % NOTE: these coordinates need to be double checked using an
        % accurate ruler on the A3 sheet, instead of the plastic one.
        local_coords.Ov = [0.020; 0.020; 0];
        local_coords.X1 = [0.300; 0.020; 0];
        local_coords.X2 = [0.400; 0.020; 0];
        local_coords.Y1 = [0.020; 0.280; 0];
                
    otherwise
        error('Calibration object "%s" doesn''t exist', calibration_object);
end