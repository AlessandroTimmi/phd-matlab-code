function err = orthonormality_error(varargin)
% Calculate the orthonormality error of a matrix using Frobenius norm, as
% illustrated in the help of the MATLAB orth() function:
%   err = ||I-M'M||_F
% 
% Input:
%   M = the matrix whose orthonormality error must be calculated.
%   r = [optional] rank to be used to build the identity matrix.
%       Sometimes we don't want to use the rank of the input matrix, so we
%       can pass it as input argument to this function.
%
% Output:
%   err = the orthonormality error of M.
%
% � July 2015 Alessandro Timmi

%% Check input:
narginchk(1,2);

switch nargin
    case 1
        M = varargin{1};
        r = rank(M);
        
    case 2
        M = varargin{1};
        r = varargin{2};
end

%% Calculate orthonormality error using Frobenius norm:
err = norm(eye(r) - M' * M, 'fro');
