function sRotated = ViconToOpenSimRotation(s)
% Rotate 3D coordinates from Vicon to OpenSim coordinate system (Y-axis
% upwards).
%
% Input:
%        s = struct containing coordinates of points to be rotated.
%
% Output:
%        sRotated = struct containing the rotated coordinates.
%
% � September 2015 Alessandro Timmi


%% TODO:
% - this function is almost identical to transform_kinect_coordinates_to_vicon().
%   It might be worthwhile to replace this function with that.

%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 's', @isstruct);

% Parse input arguments:
parse(p, s);

% Copy input arguments into more memorable variables:
s = p.Results.s;


%%
fprintf('Rotating coordinates from lab to OpenSim reference system...\n')

%% Rotate selected file(s):

% Rotate marker data contained into the struct:
Rx90negative = rotx(-90);
Ry90negative = roty(-90);

% Convert markers from struct to cell array:
markersCell = struct2cell(s.markers);

% Convert from cell array to matrix:
markersVicon = cell2mat(markersCell');

% Preallocate the array of transformed coordinates:
markersOpenSim = zeros(size(markersVicon));

% For each frame, apply the coordinate transformation to each 3D point:
for f=1:length(s.time)
    % Reshape the current frame row as a 3 x nPoints matrix:
    frameVicon = reshape(markersVicon(f,:), 3, []);
    
    % Apply transformation to each 3D point according to this equation:
    % {frameOpenSim} = [Ry(-90)] [Rx(-90)] {frameVicon}
    frameOpenSim = Ry90negative * Rx90negative * frameVicon;
    % Reshape back to row format:
    markersOpenSim(f,:) = reshape(frameOpenSim, 1, []);
end

% Create the output struct as copy of the input one, except for marker
% coordinates:
sRotated = rmfield(s, 'markers');

% Read marker names from input struct:
markerNames = fieldnames(s.markers);

for i=1:s.NumMarkers
    % Copy each marker from the transformed coordinates matrix into the
    % output struct:
    sRotated.markers.(markerNames{i}) = markersOpenSim(:, (i-1)*3+(1:3));
end


end
