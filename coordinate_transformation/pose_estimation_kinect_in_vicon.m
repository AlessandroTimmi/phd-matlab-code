function [R_k, d_vk, orientation_trc, rig_vicon_trc, rig_kinect_trc] =...
    pose_estimation_kinect_in_vicon(varargin)
% Pose estimation of Kinect coordinate system using reflective
% marker data from Vicon and 3D-printed rig data from Kinect.
%
% Input:
%   orientation_trc = (optional parameter, default = '') .trc file 
%                containing coordinates of reflective markers attached on
%                Kinect surface, used for estimating Kinect orientation in
%                Vicon space. If the string is empty, a dialog will open 
%                to select a .trc file.
%	rig_vicon_trc = (optional parameter, default = '') .trc file
%                containing coordinates of a 3D printed rig in Vicon space.
%                This point will be used to determine the translation
%                vector from Vicon to Kinect origin. If the string is
%                empty, a dialog will open to select a .trc file.
%	rig_kinect_trc = (optional parameter, default = '') .trc file
%                containing coordinates of a 3D printed rig in Kinect
%                space. This point will be used to determine the
%                translation vector from Vicon to Kinect origin. If the 
%                string is empty, a dialog will open to select a .trc file.
%     debug_mode = (optional parameter, default = false) if true,
%                shows some extra info and plot Kinect and Vicon
%                coordinate systems in 3D.
%
% Output:
%            R_k = a 3x3 rotation matrix, representing the orientation
%`               of Kinect coordinate system in Vicon space.
%           d_vk = the translation vector from Vicon to Kinect origin,
%                expressed in Vicon coordinate system.
%   orientation_trc = filename of the selected .trc file used to
%                determine the orientation of Kinect frame in Vicon space.
%	rig_vicon_trc =  filename of the selected .trc file containing rig 
%                position in Vicon space.
%	rig_kinect_trc = filename of the selected .trc file containing rig 
%                position in Kinect space.
%
% � October 2015 Alessandro Timmi


%% TODO:
% - use marker names for reading rigs coordinates, instead of static
%   coordinate numbering.
% - Write a function to check if a matrix is a rotation matrix and use it
%   in sym_orthogonalization() too.
% - Represent Kinect as a surface in 3D in plot.
% - Clean up this function.

%% Default input values:
default.orientation_trc = '';
default.rig_vicon_trc = '';
default.rig_kinect_trc = '';
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'orientation_trc', default.orientation_trc, @ischar);
addParameter(p, 'rig_vicon_trc', default.rig_vicon_trc, @ischar);
addParameter(p, 'rig_kinect_trc', default.rig_kinect_trc, @ischar);
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
orientation_trc = p.Results.orientation_trc;
rig_vicon_trc = p.Results.rig_vicon_trc;
rig_kinect_trc = p.Results.rig_kinect_trc;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
fprintf('Pose estimation of Kinect frame in Vicon space...\n');
if debug_mode
    fprintf('[Debug] � October 2015 Alessandro Timmi.\n');
end

%% Load a .trc file containing Kinect orientation data. If the filename is
% empty, a dialog will open to select a .trc file:
[trc_data, orientation_trc] = LoadTrcFile(...
    'filename', orientation_trc,...
    'logMessage', sprintf('Loading file to be used for orientation estimation:\n'),...
    'dialogTitle', 'Select a .trc file containing Kinect orientation data');


% Find indices of orientation markers within the .trc file:
orientation_marker_names = markerset('Agreement_orientation_reflective');

fprintf('Names and indices (from .trc file) of markers to be used for orientation estimation:\n');
for i=1:length(orientation_marker_names)
    fprintf('\t%s;\n', orientation_marker_names{i});
end

%% Copy the orientation markers data from a struct into arrays. Then
% average them over time, because they are supposed to be static along the
% calibration trial. We obtain a 3x1 vector for each marker:
for i=1:length(orientation_marker_names)
    orient_markers.(orientation_marker_names{i}) = mean(...
        trc_data.markers.(orientation_marker_names{i}), 'omitnan')';
end


% Determine the 3x3 rotation matrix of the orientation of Kinect frame in
% Vicon space.
% The x-axis is determined as the line of best fit for the n aligned points.
% We need an nx3 matrix P as input, having the 3D coordinates of the n
% points as rows:
P = [orient_markers.X1, orient_markers.X2, orient_markers.X3]';
[v0, v] = FitLine3D(P, 'debugMode', debug_mode);
% Determine 2 points of the fitted line, to allow plotting it in 3D:
fit_line_point_1 = v0 + v * -1;
fit_line_point_2 = v0 + v * 1;
% We divide by the norm, because we are interested in unit vectors.
kinect_axes.X = (fit_line_point_2 - fit_line_point_1) / ...
    norm(fit_line_point_2 - fit_line_point_1);

% Vicon's method, using perpendicularity and the singleton point:
[p_perpendicular, v_perpendicular] = perpendicular_line_one_point(fit_line_point_1,...
    fit_line_point_2, orient_markers.Z1, 'debug_mode', debug_mode);
kinect_axes.Z = v_perpendicular / norm(v_perpendicular);

% Calculate the Y axis of Kinect coordinate system using the cross product.
% We normalize again to ensure there is no rounding error:
kinect_axes.Y = cross(kinect_axes.Z, kinect_axes.X) /...
    norm(cross(kinect_axes.Z, kinect_axes.X));

% Build the 3x3 rotation matrix representing the orientation of Kinect axes
% in Vicon space. It should be already orthogonal, but we will check it
% later.
R_k = [kinect_axes.X, kinect_axes.Y, kinect_axes.Z];


%% Check detected rotation matrix:
fprintf('Checking the properties of Kinect orientation matrix:\n');
fprintf('TODO: write a function with all these checks.\n')
% Check matrix rank:
rank_R_k = rank(R_k);
if rank_R_k ~= 3
    error('Rank(R_k) = %g, while it should be 3.', rank_R_k);
else
    fprintf('\tRank(R_k) = %g, OK.\n', rank_R_k);
end

% Check matrix determinant:
det_R_k = det(R_k);
fprintf('\tDet(R_k) = %g (it should be 1).\n', det_R_k);

% Check if column vectors are unit vectors:
if is_unit_vectors_matrix(R_k);
    fprintf('\tDetected Kinect axes are unit vectors, OK.\n')
else
    error('Not all detected Kinect axes are unit vectors');
end

% Check orthonormality error of this matrix (see "help orth" for this formula):
orthonorm_err_R_k = orthonormality_error(R_k);
fprintf('\tOrthonormality error of R_k = %g.\n', orthonorm_err_R_k);

% The orthonormality error should be in the order of magnitude of eps (machine precision):
if orthonorm_err_R_k>10*eps
    warning('The orthonormality error is not on the order of eps (%g).', eps)
else
    fprintf('\tOK, the orthonormality error is on the order of eps (%g).\n', eps)
end



%% Load a .trc file containing coordinates of rig in Vicon space. If the
% filename is empty, a dialog will open to select a .trc file:
[rig_vicon, rig_vicon_trc] = LoadTrcFile(...
    'filename', rig_vicon_trc,...
    'logMessage', sprintf('Loading .trc file of Vicon rig:\n'),...
    'dialogTitle', 'Select a .trc file containing Vicon rig data');

rig_vicon_marker_names = fieldnames(rig_vicon.markers);

r_v = mean(rig_vicon.markers.(rig_vicon_marker_names{1}), 'omitnan')';

[rig_kinect, rig_kinect_trc] = LoadTrcFile(...
    'filename', rig_kinect_trc,...
    'logMessage', sprintf('Loading .trc file of Kinect rig:\n'),...
    'dialogTitle', 'Select a .trc file containing Kinect rig data');

rig_kinect_marker_names = fieldnames(rig_kinect.markers);

r_k = mean(rig_kinect.markers.(rig_kinect_marker_names{1}), 'omitnan')';


% Determine the translation vector from Vicon to Kinect origin:
d_vk = r_v - R_k * r_k;



%% 3D plots
if debug_mode
    fprintf('[Debug] See debug plots for pose estimation.\n')
    figure('Name', '[Debug] Pose estimation of Vicon coord system');
%     % Represent Kinect as a black square:
%     plot3(0,0,0, 'ks', 'MarkerSize', 15, 'MarkerFaceColor', 'k');
%     hold on
    
    % Vicon coordinate system:
    plot3([0;0.5],  [0;0],      [0;0], 'r', 'linewidth', 3);
    hold on
    plot3([0;0],    [0;0.5],    [0;0], 'g', 'linewidth', 3);
    plot3([0;0],    [0;0],      [0;0.5], 'b', 'linewidth', 3);
    text(0.02, 0.02, 0.02, 'O_v');
    
    
    % Reflective markers attached on Kinect, expressed in Vicon space.
    % Create a unit sphere (sphere with radius 1 m) with 20-by-20 faces:
    [sph_1m.x, sph_1m.y, sph_1m.z] = sphere;
    % Scale the sphere to get markers with radius 9 mm, as the reflective
    % markers attached on Kinect:
    sph.x = 0.0045 * sph_1m.x;
    sph.y = 0.0045 * sph_1m.y;
    sph.z = 0.0045 * sph_1m.z;
    % Draw the 4 markers, scaling and translating the sphere above:
    gray = [0.4 0.4 0.4];
    surf(sph.x + orient_markers.X1(1), sph.y + orient_markers.X1(2),...
        sph.z + orient_markers.X1(3), 'FaceColor', gray, 'linestyle', 'none')
    surf(sph.x + orient_markers.X2(1), sph.y + orient_markers.X2(2),...
        sph.z + orient_markers.X2(3), 'FaceColor', gray, 'linestyle', 'none')
    surf(sph.x + orient_markers.X3(1), sph.y + orient_markers.X3(2),...
        sph.z + orient_markers.X3(3), 'FaceColor', gray, 'linestyle', 'none')
    surf(sph.x + orient_markers.Z1(1), sph.y + orient_markers.Z1(2),...
        sph.z + orient_markers.Z1(3), 'FaceColor', gray, 'linestyle', 'none')
    
    % Line of best fit for the 3 aligned points:
    plot3(  [fit_line_point_1(1), fit_line_point_2(1)],...
            [fit_line_point_1(2), fit_line_point_2(2)],...
            [fit_line_point_1(3), fit_line_point_2(3)],...
        'Color', 'k', 'linewidth', 1.5, 'linestyle', ':')
    
    % Shortest vector perpendicular to the X axis and passing through the
    % singleton marker:
    plot3(  [p_perpendicular(1), p_perpendicular(1) + kinect_axes.Z(1)],...
            [p_perpendicular(2), p_perpendicular(2) + kinect_axes.Z(2)],...
            [p_perpendicular(3), p_perpendicular(3) + kinect_axes.Z(3)],...
        'Color', [0, 0.3, 0.7], 'linewidth', 1.5, 'linestyle', ':');
    
    % Point in which the perpendicular line crosses the line of best fit:
    plot3(p_perpendicular(1), p_perpendicular(2), p_perpendicular(3),...
        'Color', 'k', 'Marker', 'x', 'MarkerSize', 11)
    
    % Kinect coordinate system (calculated) in Vicon space:
    plot3(  [d_vk(1), d_vk(1) + kinect_axes.X(1)],...
            [d_vk(2), d_vk(2) + kinect_axes.X(2)],...
            [d_vk(3), d_vk(3) + kinect_axes.X(3)],...
        'r', 'linewidth', 1.5);
    plot3(  [d_vk(1), d_vk(1) + kinect_axes.Y(1)],...
            [d_vk(2), d_vk(2) + kinect_axes.Y(2)],...
            [d_vk(3), d_vk(3) + kinect_axes.Y(3)],....
        'g', 'linewidth', 1.5);
    plot3(  [d_vk(1), d_vk(1) + kinect_axes.Z(1)],...
            [d_vk(2), d_vk(2) + kinect_axes.Z(2)],...
            [d_vk(3), d_vk(3) + kinect_axes.Z(3)],...
        'b', 'linewidth', 1.5);
    text(d_vk(1) + 0.02, d_vk(2) + 0.02, d_vk(3) + 0.02, 'O_k');
    
    
    % Translation vector from Vicon to Kinect origin, expressed in Vicon
    % coordinate system:
    plot3([0, d_vk(1)], [0, d_vk(2)], [0, d_vk(3)], 'k:');
    text(d_vk(1)/2, d_vk(2)/2, d_vk(3)/2, 'd_{vk}');
    
    xlabel('X [m]')
    ylabel('Y [m]');
    zlabel('Z [m]');
    title('Kinect coordinate system in Vicon space', 'fontweight', 'bold');
    view([-180,-60])
    axis equal
end


