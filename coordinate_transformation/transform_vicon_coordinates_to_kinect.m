function [s_transf, calibration_object, trcReferenceFrame] =...
    transform_vicon_coordinates_to_kinect(s, varargin)
% This function applies a coordinate transformation from Vicon to Kinect
% space.
%
% Input:
%            s = struct containing 3D coordinates of points to be
%                transformed from Vicon to Kinect space.
%   calibration_object = (optional parameter, default = '') string
%                containig the name of the object used for calibration. If
%                empty, a list dialog appears to allow choosing one.
%   trcReferenceFrame = (optional parameter, default = '') .trc file
%                containing coordinates of Vicon reference frame, recorded
%                by Kinect. If the string is empty, a dialog will open to
%                select a .trc file.
%   debug_mode = (optional parameter, default = false) if true, shows some
%                extra info for debug purposes.
%
% Output:
%     s_transf = struct containing coordinates transformed from Vicon
%                to Kinect space.
%   calibration_object = string containig the name of the selected object
%                used for calibration.
%   trcReferenceFrame = filename of the selected .trc file used as
%                reference for coordinate transformation.
%
% � October 2015 Alessandro Timmi


%% TODO:
% - See if we can avoid converting from struct to cell and just copy each
% field of the struct into a matrix. Also, check the time performance of
% the two methods.
% - Plot results.
% This function is almost identical to transform_kinect_coordinates_to_vicon().
% See if there is a way to merge them.



%% Default input values:
default.calibration_object = '';
default.trcReferenceFrame = '';
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input parameters:
addRequired(p, 's', @isstruct);

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'calibration_object', default.calibration_object,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'trcReferenceFrame', default.trcReferenceFrame,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, s, varargin{:});

% Copy input arguments into more memorable variables:
s = p.Results.s;
calibration_object = p.Results.calibration_object;
trcReferenceFrame = p.Results.trcReferenceFrame;
debug_mode = p.Results.debug_mode;

clear varargin default p

fprintf('Applying coordinate transformation from Vicon to Kinect space...\n')
if debug_mode
    fprintf('[Debug] � October 2015 Alessandro Timmi.\n')
end


%% Select calibration object:
if isempty(calibration_object)
    ok = 0;
    while ok==0
        cal_objects_cell = {'L-frame_adjusted', 'Ergocal', 'A3_sheet'};
        
        [Selection, ok] = listdlg('ListString', cal_objects_cell,...
            'SelectionMode', 'Single',...
            'Name', 'Select calibration object',...
            'PromptString', 'Select calibration object');
        calibration_object = cal_objects_cell{Selection};
    end
end


%% Pose estimation of Vicon frame in Kinect space:
% Rv = rotation matrix of Vicon in Kinect space;
% d_kv = translation vector from Kinect to Vicon origin, expressed in
% Kinect space.
[Rv, d_kv, trcReferenceFrame] = pose_estimation_vicon_in_kinect(...
    calibration_object, 'trcReferenceFrame', trcReferenceFrame, 'debug_mode', debug_mode);


%% Apply coordinate transformation from Vicon to Kinect space:
% Convert markers from struct to cell array:
a = struct2cell(s.markers);
% Convert from cell array to matrix:
rv = cell2mat(a');

% Preallocate the array of transformed coordinates:
rk = zeros(size(rv));

% For each frame, apply the coordinate transformation to each 3D point:
for f=1:length(s.time)
    % Reshape the current frame row as a 3 x n_points matrix:
    Pv = reshape(rv(f,:), 3, []);
    % Apply transformation to each 3D point according to this equation:
    % {rk} = [Rv] * {rv} + {d_kv}
    Pk = bsxfun(@plus, Rv * Pv, d_kv);
    % Reshape back to row format:
    rk(f,:) = reshape(Pk, 1, []);
end

% Create the output struct as copy of the input one, except for marker
% coordinates:
s_transf = remfield(s, 'markers');

% Read marker names from input struct:
marker_names = fieldnames(s.markers);

for j=1:s.NumMarkers
    % Copy each markers from the transformed coordinates matrix into the
    % destination struct:
    s_transf.markers.(marker_names{j}) = rk(:, (j-1)*3+(1:3));
end

end


