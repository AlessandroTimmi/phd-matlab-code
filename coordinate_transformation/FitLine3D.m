function [v0, v] = FitLine3D(P, varargin)
% Find the line which best fits some 3D points. This is the implementation
% of the algorithm proposed here:
% http://stackoverflow.com/questions/2352256/fit-a-3d-line-to-3d-point-data-in-java
%
% Input:
%            P = nx3 matrix containing the 3D coordinates of the n points as rows:
%                P = [x1, y1, z1;
%                     x2, y2, z2;
%                         ...
%                     xn, yn, zn];
%   debugMode = (optional parameter, default = false) if true, calculates
%                and plots fit error for each input point.
%            
% Output:
%           v0 = 3x1 vector of the centroid of the n points. The fitted
%                line passes through it. 
%            v = 3x1 vector of the direction of the fitted line.
%
% � July 2015 Alessandro Timmi


%% Default input values:
default.debugMode = true;

% Setup input parser:
ip = inputParser;
ip.CaseSensitive = false;

% Add required input arguments: check if P has 3 columns:
addRequired(ip, 'P', @(x) isequal(size(x, 2), 3));

% Add optional input arguments in the form of name-value pairs:
addParameter(ip, 'debugMode', default.debugMode, @islogical);
        
% Parse input arguments:
parse(ip, P, varargin{:});

% Copy input arguments into more memorable variables:
P = ip.Results.P;
debugMode = ip.Results.debugMode;

clear varargin default ip


%%
fprintf('Calculating 3D line of best fit...\n')

% Number of points used to determine the line of best fit:
nPoints = size(P,1);

%% Centroid of the point cloud P (mean of column vectors of P).
%  v0 = [xbar;
%        ybar;
%        zbar]
v0 = mean(P, 1)';

% Transpose v0 and subtract it to each row vector of P:
% A = [ x1 - xbar, y1 - ybar, z1 - zbar;
%       x2 - xbar, y2 - ybar, z2 - zbar;
%         ...        ...        ...
%       xn - xbar, yn - ybar, zn - zbar]
A = bsxfun(@minus, P, v0');

% Determine the singular value decomposition: we need only the right-
% singular vectors (matrix V).
[~,~,V] = svd(A);

% Take the right singular vector (column of V) that corresponds to the
% largest singular value (first element of diag(S)) of A. We know it is the
% first one, because the singular values in MATLAB SVD implementation are
%  sorted in decreasing order along the diagonal of S:
v = V(:,1);

if debugMode
    fprintf('[Debug] Calculating error for each point:\n')
    % Calculate errors for data points as orthogonal distance to the
    % fitting line:
    errors = zeros(nPoints,1);
    for i=1:nPoints
        % Coordinate vector of the current point:
        p = P(i,:)';
        errors(i,1) = norm(cross(v0 - p, v)) / norm(v); 
        fprintf('\tError for point #%d: %0.4f m.\n', i, errors(i,1))
    end
    meanErrors = mean(errors);
    stdErrors = std(errors);
    fprintf('\tMean(std) of errors: %0.4f(%0.4f) m.\n', meanErrors, stdErrors); 
    
    fprintf('[Debug] See debug plots for 3D line of best fit.\n')
    % Plot errors:
    titleError = '[Debug] Point errors for line of best fit';
    figure('Name', titleError)
    plot(errors(:,1), 'o',...
        'markeredgecolor', 'b',...
        'markerfacecolor', 'r',...
        'markersize', 8)
    ylabel('Error [m]')
    xlabel('Point [#]')
    xlim([0, nPoints + 1])
    set(gca, 'xtick', 1:nPoints)
    title(titleError, 'fontweight', 'bold')
end


