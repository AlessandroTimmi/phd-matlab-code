function R = sym_orthogonalization(M)
%% Determine the symmetric orthogonalization of the transformation matrix, 
% which is the least distorted (in a least square sense) corresponding
% orthonormal basis.
% Cfr.: A. Naidu, "Singular Value Decomposition and the Centrality of
% Lowdin Orthogonalizations", AJCAM, 2013.
% 
% Input:
%   M = a 3x3 matrix containing 3 normalized and independent vectors as
%   columns, whose orthogonality is affected by measurement error.
%
% Output:
%   R = the symmetric orthogonalization matrix of M.
%
% NOTE: the SVD decomposition doesn't care of the handedness of the
% matrices U and V. Therefore, we might get an R matrix which is still
% orthogonal, but has determinant = -1 (reflection matrix), instead of +1
% (rotation matrix).
% A solution to this issue (here implemented) is proposed by:
% Shiflett and Laub, "The analysis of rigid body motion from measured
% data", JDSMAA, 1995.
% NOTE: instead of checking the sign of the cross-product of the
% 2 elements of the matrix as suggested by Shiflett and Laub, I directly
% check the determinant of the matrix as suggested by Cheli & Pennestŕ,
% "Cinematica e Dinamica dei Sistemi Multibody". This approach is slightly
% less efficient, but more intuitive and easy to remember.
%
% © July 2015 Alessandro Timmi


%% TODO:
% - Write a function to check if a matrix is a rotation matrix and use it
%   in pose_estimation... functions too.

fprintf('\tSymmetric orthogonalization R of the input matrix M...\n');


%% Checks on input matrix:
% Rank of the input matrix, to be used later for verification of results:
rank_M = rank(M);
if rank_M ~= 3
    error('Rank(M) = %g, while it should be 3.', rank_M);
else
    fprintf('\t\tRank(M) = %g, OK.\n', rank_M);
end

% Check if column vectors of the input matrix are unit vectors:
if is_unit_vectors_matrix(M);
    fprintf('\t\tColumns of M are unit vectors, OK.\n')
else
    error('Not all column vectors of M are unit vectors');
end

%% Symmetric orthogonalization R of M:
[U,~,V] = svd(M);
R = U * V';


%% Check orthogonalized matrix:
rank_R = rank(R);
if rank_R ~= 3
    error('Rank(R) = %g, while it should be 3.', rank_R);
else
    fprintf('\t\tRank(R) = %g, OK.\n', rank_R);
end


%% Check if R is a proper rotation matrix and apply Shiflett and Laub's
% correction if required:
det_R = det(R);
fprintf('\t\tDet(R) = %g.\n', det_R);

if abs(det_R-1)<=eps(1)
    % Solution of the inequality above is: 1 - eps <= det(R) <= 1 + eps.
    fprintf('\t\tR is a proper ROTATION matrix, OK.\n');
    % Check the orthonormality error of this matrix
    orthonorm_err_R = orthonormality_error(R, rank_M);
    % We want the orthonormality error after orthogonalization to be in the
    % same order of magnitude of eps:
    if orthonorm_err_R > 10*eps
        error('Orthonormality error of R is not in the same order of eps: %g', orthonorm_err_R)
    else
        fprintf('\t\tOrthonormality error of R = %g. It has the same order of magnitude of eps, OK.\n', orthonorm_err_R);
    end
      
elseif abs(det_R+1)<=eps(1)   
    % Solution of the inequality above is: -1 - eps <= det(R) <= -1 + eps.
    % Determinant = -1 (reflection matrix). We need to modify the U as
    % to obtain det = 1 (rotation matrix), as proposed by Shiflett and
    % Laub.
    fprintf('\t\tR is an improper rotation matrix (REFLECTION matrix).\n');
    fprintf('\t\tDetermining the nearest proper orthogonal matrix using Shiflett and Laub''s method:\n')
    % Define Um = [u1 u2 -u3]:
    Um = [U(:,1:2), -U(:,3)];
    % Determine the neares proper rotation matrix:
    R = Um * V';
    % Check the determinant after the correction:
    det_R = det(R);
    if abs(det_R-1)<=eps(1)
        fprintf('\t\tR is a proper ROTATION matrix now, OK.\n');
        fprintf('\t\tDet(R) = %g (after Shiflett and Laub''s correction).\n', det_R);
    else
        error('Even after Shiflett and Laub''s correction, det(R) is not 1.');
    end
    % Check the orthonormality error of this matrix:
    orthonorm_err_R = orthonormality_error(R, rank_M);
    % We want the orthonormality error after orthogonalization to be in the
    % same order of magnitude of eps:
    if orthonorm_err_R > 10*eps
        error('Orthonormality error of R is not in the same order of eps: %g', orthonorm_err_R)
    else
        fprintf('\t\tOrthonormality error of R = %g. It has the same order of magnitude of eps, OK.\n', orthonorm_err_R);
    end

else
    fprintf('\t\tabs(det(R)-1)=%0.18f\n', abs(det(R)-1));
    fprintf('\t\tabs(det(R)+1)=%0.18f\n', abs(det(R)+1));
    error('Det(R) is not +/- 1. R is NOT ORTHOGONAL.');    
end


%% Error of fit between orthogonalized and original matrices:
fit_err = matrix_fit_error(M, R);
fprintf('\t\tFit error between R and M: %g.\n', fit_err);