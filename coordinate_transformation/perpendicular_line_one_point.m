function [p, v] = perpendicular_line_one_point(p1, p2, p0, varargin)
% Determine the shortest vector perpendicular to a line and passing through
% a point in 3D.
% Algorithm found here:
% http://answers.unity3d.com/questions/535822/how-to-find-the-vector-that-passes-through-a-point.html
% Note: until 21 August 2015 there is an error in the deduction:
% V = P - B
% should be:
% V = B - P
% and the following lines must be adjusted consequently. I submitted a
% reply to fix this error.
%
% Input:
%           p1 = (3x1 vector) first point of the line.
%           p2 = (3x1 vector) second point of the line.
%           p0 = (3x1 vector) point through which the unknown vector must
%                pass.
%   debug_mode = (optional parameter, default = false). If true, shows
%                   extra info.
%
% Output
%            p = (3x1 vector) closest point to p0 on the line.
%            v = (3x1 vector) shortest vector perpendicular to the line
%                and passing through p0.
%
% � August 2015 Alessandro Timmi


%% Default input values:
default.debug_mode = false;

% Setup input parser:
ip = inputParser;
ip.CaseSensitive = false;

% Add required input arguments:
addRequired(ip, 'p1', @(x) validateattributes(x, {'numeric'},...
    {'size', [3, 1]}));
addRequired(ip, 'p2', @(x) validateattributes(x, {'numeric'},...
    {'size', [3, 1]}));
addRequired(ip, 'p0', @(x) validateattributes(x, {'numeric'},...
    {'size', [3, 1]}));

% Add optional input arguments in the form of name-value pairs:
addParameter(ip, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(ip, p1, p2, p0, varargin{:});

% Copy input arguments into more memorable variables:
p1 = ip.Results.p1;
p2 = ip.Results.p2;
p0 = ip.Results.p0;
debug_mode = ip.Results.debug_mode;

clear varargin default ip


%%
fprintf('Calculating the shortest vector perpendicular to a line and passing through a point...\n')
if debug_mode
    fprintf('[Debug] � August 2015 Alessandro Timmi.\n');
end

% Direction of the line:
d = p2 - p1;
% Just another variable:
o = p0 - p1;

% Determine p using the equation of the line p = p1 + gamma(p2 - p1):
gamma = dot(o,d) / dot(d,d);
p = p1 + gamma * d;
if debug_mode
    fprintf('[Debug] Closest point on the line: [%g; %g; %g]\n', p)
end

% Determine v:
v = p0 - p;
if debug_mode
    fprintf('[Debug] Direction vector v: [%g; %g; %g]\n', v)
    fprintf('[Debug] Norm(v): %g\n', norm(v))
end

