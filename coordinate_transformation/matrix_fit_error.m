function fit_err = matrix_fit_error(M, M_fit)
% Check the fit error between two matrices. Useful for checking the
% closeness of fit between an orthogonalized matrix and the original one.
% 
% Input:
%   M = the original matrix.
%   M_fit = the fitted matrix.
%
% Output:
%   fit_err = the fitting error, calculated using Frobenius norm:
%               fit_err = ||M_fit - M||_F
%
% � July 2015 Alessandro Timmi

% From Wikipedia on SVD - Nearest orthogonal matrix:
fit_err = norm(M_fit - M, 'fro');
