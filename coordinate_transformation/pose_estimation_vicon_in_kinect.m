function [R_v, d_kv, trcReferenceFrame] = pose_estimation_vicon_in_kinect(...
    calibration_object, varargin)
% Pose estimation of Vicon coordinate system using Kinect v2 data.
%
% Input:
%   calibration_object = string containig the name of the object used for
%                calibration.
%   trcReferenceFrame = (optional parameter, default = '') filename of a
%                      .trc file containing static markers used for pose
%                      estimation. If the string is empty, a dialog will
%                      open to select a .trc file.
%   cal_marker_names = [optional parameter, default = (see below)] cell
%                      array with name of markers to be used for pose
%                      estimation. The part of the marker name after the
%                      underscore is ignored.
%         debug_mode = (optional parameter, default = false) if true,
%                      shows some extra info and plot Kinect and Vicon
%                      coordinate systems in 3D.
%
% Output:
%                R_v = a 3x3 rotation matrix, representing the orientation
%                      of Vicon coordinate system in Kinect space.
%                d_kv = the translation vector from Kinect to Vicon origin,
%                      expressed in Kinect coordinate system.
%   trcReferenceFrame = filename of the .trc file used for pose estimation.
%                      If the input "trcReferenceFrame" was empty, this
%                      one will be obtained from the dialog window.
%
% � June 2015 Alessandro Timmi


%% TODO:
% - Write a function to check if a matrix is a rotation matrix and use it
%   in sym_orthogonalization() too.
% - Compare Gupta vs Vicon method (based on fit and perpendicular).
% - Represent Kinect as a surface in 3D in plot.
% - Clean up this function.

%% Default input values:
default.trcReferenceFrame = '';
default.cal_marker_names = markerset('Ov_X1_X2_Y1');
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required parameters:
addRequired(p, 'calibration_object',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'trcReferenceFrame', default.trcReferenceFrame, @ischar);
addParameter(p, 'cal_marker_names', default.cal_marker_names, @iscell);
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, calibration_object, varargin{:});

% Copy input arguments into more memorable variables:
calibration_object = p.Results.calibration_object;
trcReferenceFrame = p.Results.trcReferenceFrame;
cal_marker_names = p.Results.cal_marker_names;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
fprintf('Pose estimation of Vicon frame in Kinect space...\n');
if debug_mode
    fprintf('[Debug] � June 2015 Alessandro Timmi.\n');
end

% Load a .trc file containing calibration data. If the filename is empty, a
% dialog will open to select a .trc file:
[trc_data, trcReferenceFrame] = LoadTrcFile(...
    'filename', trcReferenceFrame,...
    'logMessage', sprintf('Loading file to be used for pose estimation:\n'),...
    'dialogTitle', 'Select a calibration .trc file');



%% Copy the calibration markers data from a struct into arrays. Then average
% them over time, because they are supposed to be static along the
% calibration trial. We obtain a 3x1 vector for each marker:
for i=1:length(cal_marker_names)
    cal_markers.kinect.(cal_marker_names{i}) = mean(...
        trc_data.markers.(cal_marker_names{i}), 'omitnan')';
end


%% DETERMINE the transformation parameters (3x3 rotation matrix and translation vector).
% Orientation of Vicon coordinate system vectors in Kinect space.
% The x-axis is determined as the line of best fit for n 3D points.
% We need an nx3 matrix P as input, having the 3D coordinates of the n
% points as rows:
P = [cal_markers.kinect.Ov, cal_markers.kinect.X1, cal_markers.kinect.X2]';
[v0, v] = FitLine3D(P, 'debugMode', debug_mode);
% Determine 2 points of the fitted line, to allow plotting it in 3D:
fit_line_point_1 = v0 + v * -1;
fit_line_point_2 = v0 + v * 1;
% We divide by the norm, because we are interested in unit vectors.
vicon_axes.X = (fit_line_point_2 - fit_line_point_1) / ...
    norm(fit_line_point_2 - fit_line_point_1);

% Vicon's method, using perpendicularity and the singleton point:
[p_perpendicular, v_perpendicular] = perpendicular_line_one_point(fit_line_point_1,...
    fit_line_point_2, cal_markers.kinect.Y1, 'debug_mode', debug_mode);
vicon_axes.Y = v_perpendicular / norm(v_perpendicular);

% Calculate the Z axis of Vicon coordinate system using the right hand
% rule. We normalize again to ensure there is no rounding error:
vicon_axes.Z = cross(vicon_axes.X, vicon_axes.Y) /...
    norm(cross(vicon_axes.X, vicon_axes.Y));

% Build the 3x3 rotation matrix representing the orientation of Vicon axes
% in Kinect space. It should be already orthogonal, but we will check it
% later.
R_v = [vicon_axes.X, vicon_axes.Y, vicon_axes.Z];


%% Check detected Vicon axes matrix:
fprintf('Checking the properties of Vicon orientation matrix:\n');
fprintf('TODO: write a function with all these checks.\n')
% Check matrix rank:
rank_R_v = rank(R_v);
if rank_R_v ~= 3
    error('Rank(R_v) = %g, while it should be 3.', rank_R_v);
else
    fprintf('\tRank(R_v) = %g, OK.\n', rank_R_v);
end

% Check matrix determinant:
det_R_v = det(R_v);
fprintf('\tDet(R_v) = %g (it should be 1).\n', det_R_v);

% Check if column vectors are unit vectors:
if is_unit_vectors_matrix(R_v);
    fprintf('\tDetected Vicon axes are unit vectors, OK.\n')
else
    error('Not all detected Vicon axes are unit vectors');
end

% Check orthonormality error of this matrix (see "help orth" for this formula):
orthonorm_err_R_v = orthonormality_error(R_v);
fprintf('\tOrthonormality error of R_v = %g.\n', orthonorm_err_R_v);

% The orthonormality error should be in the order of magnitude of eps (machine precision):
if orthonorm_err_R_v>10*eps
    warning('The orthonormality error is not on the order of eps (%g).', eps)
else
    fprintf('\tOK, the orthonormality error is on the order of eps (%g).\n', eps)
end


%% To determine the translation vector from Kinect to Vicon origin
% (expressed in Kinect space) we use the centroid of the markers,
% expressed in both coordinate system.

% To determine the centroid in Vicon space, we use the coordinates of
% calibration markers expressed in the local reference frame of
% calibration object.
cal_markers.vicon = calibration_local_coordinates(calibration_object);

% Assemble these points into a matrix:
% Av = [  cal_markers.vicon.Ov, cal_markers.vicon.X1,...
%         cal_markers.vicon.X2, cal_markers.vicon.Y1];
% Apparently estimation of dv is more accurate using just the 3 markers
% which are closer to Kinect: this is probably due to the inaccuracy of
% Kinect itself, which increases with distance:
Av = [  cal_markers.vicon.Ov, cal_markers.vicon.X1,...
    cal_markers.vicon.X2];

% Calculate the coordinates of the centroid as average of the coordinate
% of the markers:
Cv = mean(Av,2);

% Repeat the same procedure using coordinates in Kinect space:
% Ak = [  cal_markers.kinect.Ov, cal_markers.kinect.X1,...
%         cal_markers.kinect.X2, cal_markers.kinect.Y1];
Ak = [  cal_markers.kinect.Ov, cal_markers.kinect.X1,...
    cal_markers.kinect.X2];

Ck = mean(Ak,2);

% The translation vector is given by the following relation:
d_kv = Ck - R_v * Cv;


%% 3D plots
if debug_mode
    fprintf('[Debug] See debug plots for pose estimation.\n')
    figure('Name', '[Debug] Pose estimation of Vicon coord system');
    % Represent Kinect as a black square:
    plot3(0,0,0, 'ks', 'MarkerSize', 15, 'MarkerFaceColor', 'k');
    hold on
    
    % Kinect coordinate system:
    plot3([0;0.5],  [0;0],      [0;0], 'r', 'linewidth', 3);
    plot3([0;0],    [0;0.5],    [0;0], 'g', 'linewidth', 3);
    plot3([0;0],    [0;0],      [0;0.5], 'b', 'linewidth', 3);
    
    % Coloured markers on the L-frame, in Kinect space.
    % Create a unit sphere (sphere with radius 1 m) with 20-by-20 faces:
    [sph_1m.x, sph_1m.y, sph_1m.z] = sphere;
    % Scale the sphere to get markers with radius 1 cm, as those attached
    % on the L-frame:
    sph.x = 0.01 * sph_1m.x;
    sph.y = 0.01 * sph_1m.y;
    sph.z = 0.01 * sph_1m.z;
    % Draw 4 markers scaling and translating the sphere above:
    surf(sph.x + cal_markers.kinect.Ov(1), sph.y + cal_markers.kinect.Ov(2),...
        sph.z + cal_markers.kinect.Ov(3), 'FaceColor', 'green', 'linestyle', 'none')
    surf(sph.x + cal_markers.kinect.X1(1), sph.y + cal_markers.kinect.X1(2),...
        sph.z + cal_markers.kinect.X1(3), 'FaceColor', 'blue', 'linestyle', 'none')
    surf(sph.x + cal_markers.kinect.X2(1), sph.y + cal_markers.kinect.X2(2),...
        sph.z + cal_markers.kinect.X2(3), 'FaceColor', 'red', 'linestyle', 'none')
    surf(sph.x + cal_markers.kinect.Y1(1), sph.y + cal_markers.kinect.Y1(2),...
        sph.z + cal_markers.kinect.Y1(3), 'FaceColor', 'yellow', 'linestyle', 'none')
    
    % Line of best fit for the 3 aligned points of the L-frame:
    plot3(  [fit_line_point_1(1), fit_line_point_2(1)],...
        [fit_line_point_1(2), fit_line_point_2(2)],...
        [fit_line_point_1(3), fit_line_point_2(3)],...
        'Color', 'k', 'linewidth', 1.5, 'linestyle', ':')
    
    % Shortest vector perpendicular to the X axis and passing through the
    % singleton marker:
    plot3(  [p_perpendicular(1), p_perpendicular(1) + vicon_axes.Y(1)],...
        [p_perpendicular(2), p_perpendicular(2) + vicon_axes.Y(2)],...
        [p_perpendicular(3), p_perpendicular(3) + vicon_axes.Y(3)],...
        'Color', [0,0.3,0.7], 'linewidth', 1.5, 'linestyle', ':');
    
    % Point in which the perpendicular line crosses the line of best fit:
    plot3(p_perpendicular(1), p_perpendicular(2), p_perpendicular(3),...
        'Color', 'k', 'Marker', 'x', 'MarkerSize', 11)
    
    % Vicon coordinate system (calculated) in Kinect space:
    plot3(  [d_kv(1), d_kv(1) + vicon_axes.X(1)],...
        [d_kv(2), d_kv(2) + vicon_axes.X(2)],...
        [d_kv(3), d_kv(3) + vicon_axes.X(3)],...
        'r', 'linewidth', 1.5);
    plot3(  [d_kv(1), d_kv(1) + vicon_axes.Y(1)],...
        [d_kv(2), d_kv(2) + vicon_axes.Y(2)],...
        [d_kv(3), d_kv(3) + vicon_axes.Y(3)],....
        'g', 'linewidth', 1.5);
    plot3(  [d_kv(1), d_kv(1) + vicon_axes.Z(1)],...
        [d_kv(2), d_kv(2) + vicon_axes.Z(2)],...
        [d_kv(3), d_kv(3) + vicon_axes.Z(3)],...
        'b', 'linewidth', 1.5);
    
    % Translation vector from Kinect origin to Vicon origin, expressed in
    % Kinect coordinate system:
    plot3([0, d_kv(1)], [0, d_kv(2)], [0, d_kv(3)], 'k:');
    
    xlabel('X [m]')
    ylabel('Y [m]');
    zlabel('Z [m]');
    title('Vicon coordinate system in Kinect space', 'fontweight', 'bold');
    view([-180,-60])
    axis equal
end


