function [kinectArrayOfStructs, viconArrayOfStructs] =...
    GetKneeAdductionAngle(kinectArrayOfStructs, viconArrayOfStructs)
% Calculate knee adduction angle (phi) from data collected during the
% Treadmill Study. Input arrays of structs are returned with results
% added as new fields.
%
% Input:
%   kinectArrayOfStructs = array of structs containing .trc data from the
%           Treadmill study, collected using Kinect.
%   viconArrayOfStructs = array of structs containing .trc data from the
%           Treadmill study (collected using Vicon) AND GAIT EVENTS.
% Output:
%   kinectArrayOfStructs = same as input, but with added fields:
%            phi, phiFS, phiTO.
%   viconArrayOfStructs = same as input, but with added fields:
%            locFS, locsTO, phi, phiFS, phiTO.
%
% � February 2017 Alessandro Timmi


for f = 1:length(viconArrayOfStructs)
    
    % Calculate knee adduction angle (phi) for Vicon and Kinect:
    viconArrayOfStructs(f).phi =...
        KneeAdductionAngle2dTreadmillStudy(viconArrayOfStructs(f));
    kinectArrayOfStructs(f).phi =...
        KneeAdductionAngle2dTreadmillStudy(kinectArrayOfStructs(f));
    
 
    % Find knee adduction angle for both Vicon and Kinect structs in
    % correspondence of gait events. Since data have been interpolated
    % at the same framerate, synced and trimmed, we can detect gait events
    % USING JUST VICON DATA, which are supposed to be more accurate than
    % those from Kinect.
    viconArrayOfStructs(f).phiFS =...
        viconArrayOfStructs(f).phi(viconArrayOfStructs(f).locsFS);
    viconArrayOfStructs(f).phiTO =...
        viconArrayOfStructs(f).phi(viconArrayOfStructs(f).locsTO);
    
    kinectArrayOfStructs(f).phiFS =...
        kinectArrayOfStructs(f).phi(viconArrayOfStructs(f).locsFS);
    kinectArrayOfStructs(f).phiTO =...
        kinectArrayOfStructs(f).phi(viconArrayOfStructs(f).locsTO);
    
end

end