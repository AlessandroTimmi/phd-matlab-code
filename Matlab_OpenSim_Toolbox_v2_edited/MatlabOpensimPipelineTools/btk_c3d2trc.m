function data = btk_c3d2trc(varargin)
% Function to convert data from a C3D file into the TRC and _grf.MOT file
% formats for OpenSim
%
% Input:
%       filename = the .C3D full filename that you wish to load (leave blank
%              to choose from a dialog box)
%       data = (optional) structure containing fields from previously loaded
%              .C3D file using btk_loadc3d.m.
%       anim = (optional parameter, default = false) show trial animation.
%       rotate_vicon_to_opensim = (optional parameter, default = false) if
%               true, rotate trial coordinates in order to match OpenSim
%               coordinate system.
%       dest_folder = (optional parameter, default = '') folder in which
%               .trc and .mot files will be saved. If empty, pre-processed
%               data will be saved in the same folder as the .c3d file.
%       dest_fname = (optional parameter, default = '') filename of the
%                 extracted trial. It must not contain any extension. 
%
% Output:
%       data = struct containing .c3d data. At the end of this function,
%              the following fields are added to the struct:
%               TRC_Filename = full filename of the exported .TRC file.
%               GRF_Filename = full filename of the exported .MOT file
%               (only present if the .c3d file contains force plates data).
%               
%
% Written by Glen Lichtwark (University of Queensland)
% Updated September 2012
% Edited by Alessandro Timmi (2015) (look for "Ale" in the comments below).


%% TODO Ale:
% - add a parameter in input parser for the GRF value used as input for
% btk_loadc3d. At the moment it is set to 10.
% - current coordinate rotation for opensim is correct, but not elegant:
%   use the function based on rotation matrices.


%% Input parser added by Ale.
% Default input values:
default.filename = '';
default.data = struct;
default.anim = false;
default.rotate_vicon_to_opensim = false;
default.dest_folder = '';
default.dest_fname = '';


% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'filename', default.filename,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'data', default.data,...
    @(x) validateattributes(x, {'struct'}, {}));
addParameter(p, 'anim', default.anim, @islogical);
addParameter(p, 'rotate_vicon_to_opensim',...
    default.rotate_vicon_to_opensim, @islogical);
addParameter(p, 'dest_folder', default.dest_folder, @ischar);
addParameter(p, 'dest_fname', default.dest_fname, @ischar);

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
filename = p.Results.filename;
data = p.Results.data;
anim = p.Results.anim;
rotate_vicon_to_opensim = p.Results.rotate_vicon_to_opensim;
dest_folder = p.Results.dest_folder;
dest_fname = p.Results.dest_fname;

clear varargin default p


%% Ale: Let's look at what we have as input arguments:
if isempty(fieldnames(data))
    % If the data struct is empty, look at the "filename" parameter:
    if isempty(filename)
        % select a .c3d file:
        [fname, pname] = uigetfile('*.c3d', 'Select a .c3d file');
        % and load it:
        data = btk_loadc3d([pname, fname], 10);
    else
        % directly load .c3d file:
        data = btk_loadc3d(filename, 10);
        % Split path and name of .c3d file:
        [pname, name, ext] = fileparts(filename);
        pname = [pname filesep];
        fname = [name, ext];
    end
else
    [pname, name, ext] = fileparts(data.marker_data.Filename);
    pname = [pname filesep];
    fname = [name, ext];
end

if ~isfield(data, 'marker_data')
    error(['Please ensure that the following field is included '...
        'in structure - marker_data. Please use btk_loadc3d '...
        'for correct outputs']);
end


%% OLD, to be removed after proper testing of input parser by Ale
% %% load data
% if nargin > 0
%     if ~isstruct(varargin{1})
%         % load C3d file
%         file = varargin{1};
%         if isempty(fileparts(file))
%             pname = cd;
%             if ispc
%                 pname = [pname '\'];
%             else pname = [pname '/'];
%             end
%             fname = file;
%         else [pname, name, ext] = fileparts(file);
%             fname = [name ext];
%         end
%         % load the c3dfile
%         [data.marker_data, data.analog_data, data.fp_data, data.sub_info] =...
%             btk_loadc3d([pname, fname], 10);
%
%     else
%         data = varargin{1};
%         if ~isfield(data,'marker_data')
%             error(['Please ensure that the following field is included '...
%                 'in structure - marker_data. Please use btk_loadc3d '...
%                 'for correct outputs']);
%         end
%         if isfield(data,'marker_data')
%             [pname, name, ext] = fileparts(data.marker_data.Filename);
%             if ispc
%                 pname = [pname '\'];
%             else pname = [pname '/'];
%             end
%             fname = [name ext];
%         else fname = data.marker_data.Filename;
%         end
%
%     end
%     if length(varargin) < 2
%         anim = 'on';
%     else anim = varargin{2};
%     end
% else [fname, pname] = uigetfile('*.c3d', 'Select C3D file');
%     % load the c3dfile
%     [data.marker_data, data.analog_data, data.fp_data, data.sub_info] =...
%         btk_loadc3d([pname, fname], 10);
%     anim = 'on';
% end

%%
% if the mass, height and name aren't present then presribe - it is
% preferrable to have these defined in the data structure before running
% this function - btk_loadc3d should try and do this for vicon data
if ~isfield(data,'Mass')
    data.Mass = 75;
end

if ~isfield(data,'Height')
    data.Height = 1750;
end

if ~isfield(data,'Name')
    data.Name = 'NoName';
end

%% define the start and end frame for analysis as first and last frame unless
% this has already been done to change the analysed frames
if ~isfield(data,'Start_Frame')
    data.Start_Frame = 1;
    data.End_Frame = data.marker_data.Info.NumFrames;
end

%%
% define some parameters
nrows = data.End_Frame-data.Start_Frame+1;
nmarkers = length(fieldnames(data.marker_data.Markers));

% Ale: why not starting from t=0?
data.time = (1/data.marker_data.Info.frequency:...
    1/data.marker_data.Info.frequency:...
    (data.End_Frame-data.Start_Frame+1)/data.marker_data.Info.frequency)';

nframe = 1:nrows;

% If requested, animate the trial:
if anim % Ale
    data.marker_data.First_Frame = data.Start_Frame;
    data.marker_data.Last_Frame = data.End_Frame;
    if isfield(data,'fp_data')
        btk_animate_markers(data.marker_data, data.fp_data, 5)
    else btk_animate_markers(data.marker_data)
    end
end


%% ALE: Check unit for marker coordinates:
point_units = data.marker_data.Info.units.ALLMARKERS;
if strcmpi(point_units, 'mm')
    mm2m = 1000;
    fprintf('Converting marker coordinates from mm to m...\n')
    % Convert each field of the struct containing marker coordinates:
    data.marker_data.Markers = structfun(@(x) x / mm2m,...
        data.marker_data.Markers, 'UniformOutput', false);
    % Update the unit in the marker metadata:
    data.marker_data.Info.units.ALLMARKERS = 'm';
    
elseif strcmpi(point_units, 'm')
    fprintf('Marker coordinates are in m, no need to convert.\n')
    
else
    error('Unknown unit for marker coordinates. Check C3D "point" metadata.')
end

% We need to reorder the lab coordinate system to match that of the OpenSim
% system --> SKIP THIS STEP IF LAB COORDINATE SYSTEM IS SAME AS MODEL
% SYSTEM
markers = fieldnames(data.marker_data.Markers); % get markers names
% Ale: coordinate rotation for OpenSim is now optional and selected via an
% input parameter:

if rotate_vicon_to_opensim
    % If true, applies a rotation to .c3d marker data in order to match
    % OpenSim coordinate system. It goes through each marker field and
    % re-orders from X Y Z to Y Z X:
    fprintf('Rotating marker coordinates to match OpenSim reference system...\n')
    % Go through each marker field and re-order from X Y Z to Y Z X:
    % TODO: this method is correct but not elegant, we should use rotation
    % matrices.
    for i = 1:nmarkers
        data.marker_data.Markers.(markers{i}) =  [...
            data.marker_data.Markers.(markers{i})(:,2),...
            data.marker_data.Markers.(markers{i})(:,3),...
            data.marker_data.Markers.(markers{i})(:,1)];
    end
else
    fprintf('Coordinate rotation of markers for OpenSim disabled.\n')
end


%%
% now we need to make the headers for the column headings for the TRC file
% which are made up of the marker names and the XYZ for each marker

% first initialise the header with a column for the Frame # and the Time
% also initialise the format for the columns of data to be written to file
dataheader1 = 'Frame#\tTime\t';
dataheader2 = '\t\t';
format_text = '%i\t%2.4f\t';
% initialise the matrix that contains the data as a frame number and time row
data_out = [nframe; data.time'];

% Now loop through each maker name and make marker name with 3 tabs for the
% first line and the X Y Z columns with the marker number on the second
% line all separated by tab delimeters.
% Each of the data columns (3 per marker) will be in floating format with a
% tab delimiter - also add to the data matrix.
for i = 1:nmarkers
    dataheader1 = [dataheader1 markers{i} '\t\t\t'];
    dataheader2 = [dataheader2 'X' num2str(i) '\t' 'Y' num2str(i) '\t'...
        'Z' num2str(i) '\t'];
    format_text = [format_text '%f\t%f\t%f\t'];
    % add 3 rows of data for the X Y Z coordinates of the current marker
    % first check for NaN's and fill with a linear interpolant - warn the
    % user of the gaps
    clear m
    m = find(isnan(data.marker_data.Markers.(markers{i})((data.Start_Frame:data.End_Frame),1))>0);
    if ~isempty(m);
        clear t d
        % Ale:
        %disp(['Warning -' markers{i} ' data missing in parts. Frames ' num2str(m(1)) '-'  num2str(m(end))])
        warning('data for marker "%s" missing in frames %d-%d. Filling with linear interpolant...',...
            markers{i}, m(1), m(end));
        t = time;
        t(m) = [];
        d = data.marker_data.Markers.(markers{i})((data.Start_Frame:data.End_Frame),:);
        d(m,:) = [];
        data.marker_data.Markers.(markers{i})((data.Start_Frame:data.End_Frame),:) = interp1(t,d,time,'linear','extrap');
    end
    data_out = [data_out; data.marker_data.Markers.(markers{i})((data.Start_Frame:data.End_Frame),:)'];
end
dataheader1 = [dataheader1 '\n'];
dataheader2 = [dataheader2 '\n'];
format_text = [format_text '\n'];

fprintf('Writing .trc file...\n')

% Ale: Output marker data to an OpenSim TRC file:
if isempty(dest_fname)
    newfilename = strrep(fname,'c3d','trc');
else
    newfilename = [dest_fname, '.trc'];
end

% Ale: select the destination folder:
if isempty(dest_folder)
    % If destination folder was not specified, pre-processed data will be
    % saved in the same folder as the .c3d file:
   dest_folder = pname;
end
data.TRC_Filename = fullfile(dest_folder, newfilename);


%% Ale
% Check if the destination .trc file already exists:
if exist(data.TRC_Filename, 'file')
    fprintf('File %s already exists...\n', data.TRC_Filename)
    choice = questdlg(sprintf('File %s already exists: overwrite?', data.TRC_Filename),...
        'Overwrite file?', 'Yes', 'No', 'No');
    switch choice
        case 'Yes'
            fprintf('Overwriting existing .trc file...\n')
        case {'No', ''}
            [newfilename, dest_folder] = uiputfile('*.trc', 'Select new .trc filename');
            data.TRC_Filename = fullfile(dest_folder, newfilename);
            fprintf('Selected a new .trc filename:\n')
            fprintf('\t%s\n', data.TRC_Filename);
    end
end

% open the file
fid_1 = fopen(data.TRC_Filename, 'w');

% first write the header data
fprintf(fid_1,'PathFileType\t4\t(X/Y/Z)\t %s\n', newfilename);
fprintf(fid_1,'DataRate\tCameraRate\tNumFrames\tNumMarkers\tUnits\tOrigDataRate\tOrigDataStartFrame\tOrigNumFrames\n');
fprintf(fid_1,'%d\t%d\t%d\t%d\t%s\t%d\t%d\t%d\n', data.marker_data.Info.frequency, data.marker_data.Info.frequency, nrows, nmarkers, data.marker_data.Info.units.ALLMARKERS, data.marker_data.Info.frequency,data.Start_Frame,data.End_Frame);
fprintf(fid_1, dataheader1);
fprintf(fid_1, dataheader2);

% Ale: add an empty line between the header and the data, for consistency
% with OpenSim sample .trc data.
fprintf(fid_1, '\n');

% then write the output marker data
fprintf(fid_1, format_text, data_out);

% close the file
fclose(fid_1);
fprintf('TRC data stored in:\n')
fprintf('\t%s.\n', data.TRC_Filename)


%% Write motion file (MOT) containing GRFs
if isfield(data, 'fp_data')
    % Assume that all force plates are collected at the same frequency!!!
    F = data.fp_data.Info(1).frequency/data.marker_data.Info.frequency;
    
    fp_time = 1 / data.marker_data.Info.frequency :...
        1 / data.fp_data.Info(1).frequency :...
        (F * (data.End_Frame - data.Start_Frame + 1)) / data.fp_data.Info(1).frequency;
    
    % initialise force data matrix with the time array and column header
    force_data_out = fp_time';
    force_header = 'time\t';
    force_format = '%20.6f\t';
    
    
    % Loop through each force plate:
    for i = 1:length(data.fp_data.FP_data)
        % The unit used for markers affects force plates as well: point of
        % application P and moments M, but obviously not forces.
        if strcmpi(point_units, 'mm')
            fprintf('Converting point of application of GRF %d from mm to m...\n', i)
            data.fp_data.GRF_data(i).P = data.fp_data.GRF_data(i).P / mm2m;
            fprintf('Converting moment of GRF %d from N mm to N m...\n', i)
            data.fp_data.GRF_data(i).M = data.fp_data.GRF_data(i).M / mm2m;
            
        elseif strcmpi(point_units, 'm')
            fprintf('Force plate data are in m, no need to convert.\n')
            
        else
            error('Unknown unit for marker coordinates. Check C3D "point" metadata.')
        end
        
        if rotate_vicon_to_opensim
            % If true, applies a rotation to c3d force plates data in order to
            % match OpenSim coordinate system. It goes through each force plate
            % field and re-orders from X Y Z to Y Z X:
            fprintf('Rotating force plates %d data to match OpenSim reference system...\n', i)
            
            data.fp_data.GRF_data(i).P = [...
                data.fp_data.GRF_data(i).P(:,2),...
                data.fp_data.GRF_data(i).P(:,3),...
                data.fp_data.GRF_data(i).P(:,1)];
            
            data.fp_data.GRF_data(i).F = [...
                data.fp_data.GRF_data(i).F(:,2),...
                data.fp_data.GRF_data(i).F(:,3),...
                data.fp_data.GRF_data(i).F(:,1)];
            
            data.fp_data.GRF_data(i).M = [...
                data.fp_data.GRF_data(i).M(:,2),...
                data.fp_data.GRF_data(i).M(:,3),...
                data.fp_data.GRF_data(i).M(:,1)];
        else
            fprintf('Coordinate transformation of force plates data for OpenSim disabled.\n')
            
        end
        
        % do some cleaning of the COP before and after contact
        b = find(abs(diff(data.fp_data.GRF_data(i).P(:,3)))>0);
        if ~isempty(b)
            for j = 1:3
                data.fp_data.GRF_data(i).P(1:b(1),j) = data.fp_data.GRF_data(i).P(b(1)+1,j);
                data.fp_data.GRF_data(i).P(b(end):end,j) = data.fp_data.GRF_data(i).P(b(end)-1,j);
            end
        end
        
        % define the period which we are analysing
        K = (F * data.Start_Frame) : 1 : (F * data.End_Frame);
        
        % add the force, COP and moment data for current plate to the force matrix
        force_data_out = [force_data_out data.fp_data.GRF_data(i).F(K,:) data.fp_data.GRF_data(i).P(K,:) data.fp_data.GRF_data(i).M(K,:)];
        % define the header and formats
        force_header = [force_header num2str(i) '_ground_force_vx\t' num2str(i) '_ground_force_vy\t' num2str(i) '_ground_force_vz\t'...
            num2str(i) '_ground_force_px\t' num2str(i) '_ground_force_py\t' num2str(i) '_ground_force_pz\t' ...
            num2str(i) '_ground_torque_x\t' num2str(i) '_ground_torque_y\t' num2str(i) '_ground_torque_z\t'];
        force_format = [force_format '%20.6f\t%20.6f\t%20.6f\t%20.6f\t%20.6f\t%20.6f\t%20.6f\t%20.6f\t%20.6f\t'];
        
    end
    
    force_header = [force_header(1:end-2) '\n'];
    force_format = [force_format(1:end-2) '\n'];
    
    % assign a value of zero to any NaNs
    force_data_out(logical(isnan(force_data_out))) = 0;
        
    fprintf('Writing GRF .mot file...\n');
    newfilename = [newfilename(1:end-4) '_grf.mot'];
    % Ale: use destination folder:
    data.GRF_Filename = fullfile(dest_folder, newfilename);
    
    %% Ale
    % Check if the destination .mot file already exists:
    if exist(data.GRF_Filename, 'file')
        fprintf('File %s already exists...\n', data.GRF_Filename)
        choice = questdlg(sprintf('File %s already exists: overwrite?', data.GRF_Filename),...
            'Overwrite file?', 'Yes', 'No', 'No');
        switch choice
            case 'Yes'
                fprintf('Overwriting existing .mot file...\n')
            case {'No', ''}
                [newfilename, dest_folder] = uiputfile('*.mot', 'Select new .mot filename');
                data.GRF_Filename = fullfile(dest_folder, newfilename);
                fprintf('Selected a new .mot filename:\n')
                fprintf('\t%s\n', data.GRF_Filename);
        end
    end

    fid_2 = fopen(data.GRF_Filename, 'w');
    
    % Ale: updated the .mot file header according to the new format,
    % compatible with both OpenSim and SIMM. For more info see:
    % http://simtk-confluence.stanford.edu:8080/display/OpenSim/Motion+(.mot)+Files
    % the sample file "subject01_walk1_grf.mot" provided with OpenSim and
    % any other IK results .mot file produced by OpenSim.
    % NOTE: the comment lines "Units are S.I. units..." which are added by
    % OpenSim in its IK results files are not reported here, because they are
    % not essential.
    % Write the header information:
    fprintf(fid_2, '%s\n', newfilename);
    fprintf(fid_2, 'version=1\n');
    fprintf(fid_2, 'nRows=%d\n', length(fp_time)); % number of datarows
    fprintf(fid_2, 'nColumns=%d\n', size(force_data_out, 2));  % total # of datacolumns
    fprintf(fid_2, 'inDegrees=yes\n');
    fprintf(fid_2, 'endheader\n');
    fprintf(fid_2, force_header);
    
    % Write the data:
    fprintf(fid_2, force_format, force_data_out');
    fclose(fid_2);
    fprintf('GRF data stored in:\n')
    fprintf('\t%s.\n\n', data.GRF_Filename)
    
else
    fprintf('No force plate information available.\n\n')
end
