function CompareLengthConcatenatedMotStructs(kinectConcatenated,...
    viconConcatenated, coordinatesNames)
% Compare length of arrays containing coordinates beteween two concatenated
% structs for the ACL Study.
%
% Input:
%   kinectConcatenated = concatenated struct containing .trc data collected
%               using Kinect.
%   viconConcatenated = concatenated struct containing .trc data collected
%               using Vicon.
%   coordinatesNames = cell array containing names of the coordinates whose
%               arrays lenghts must be compared.
%
% � September 2016 Alessandro Timmi



%% Check lengths of concatenated trials:
for c = 1:numel(coordinatesNames)
    lengthVicon = length(viconConcatenated.coords.(coordinatesNames{c}));
    lengthKinect = length(kinectConcatenated.coords.(coordinatesNames{c}));
    
    assert(isequal(lengthVicon, lengthKinect),...
        'Coordinate "%s" has different length for Vicon (%d) and Kinect (%d) concatenated trials.',...
        coordinatesNames{c}, lengthVicon, lengthKinect);
end

fprintf('\nConcatenated Vicon and Kinect coordinates have same length.\n\n')
