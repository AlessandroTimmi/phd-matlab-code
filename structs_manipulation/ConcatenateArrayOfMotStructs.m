function s = ConcatenateArrayOfMotStructs(arrayOfStructs)
% Concatenate an array of structs containing .mot data into a single struct.
% 
% Input:
%   arrayOfStructs = array of structs containing trials data.
%
% Output:
%   s = struct containing all data from the array of struct, vertically
%       concatenated.
%
% � October 2016 Alessandro Timmi


%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'arrayOfStructs', @isstruct);

% Parse input arguments:
parse(p, arrayOfStructs);

% Copy input arguments into more memorable variables:
arrayOfStructs = p.Results.arrayOfStructs;

clear p


%% Coordinates names were already checked for consistency across all structs
% when the array of structs was created, so we can just read the first one:
coordinatesNames = fieldnames(arrayOfStructs(1).coords);

%% Concatenate array of structs in a single struct:
% Matlab cannot index structs with more than one level
% using colon (:).
% Hence we need to reduce the two-level array of structs:
%   Trial(1).coords.Coordinate_1
%   Trial(1).coords.Coordinate_2
%   Trial(2).coords.Coordinate_1
%   Trial(2).coords.Coordinate_2
%   ...
%   Trial(N).coords.Coordinate_1
%   Trial(N).coords.Coordinate_2
%
% to one-level before indexing it. In this way we obtain an array of
% structs containing coordinates with this structure:
%   Trial(1).Coordinate_1 .Coordinate_2 ... .Coordinate_M;
%   Trial(2).Coordinate_1 .Coordinate_2 ... .Coordinate_M;
%   ...
%   Trial(N).Coordinate_1 .Coordinate_2 ... .Coordinate_M;
coordinatesOneLevel = horzcat(arrayOfStructs(:).coords);


% Build a new struct containing all concatenated signals from trials,
% as if it was a single long trial:
for c = 1:numel(coordinatesNames)
    s.coords.(coordinatesNames{c}) =...
        vertcat(coordinatesOneLevel(:).(coordinatesNames{c}));
end


end
