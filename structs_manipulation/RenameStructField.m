function myStruct = RenameStructField(myStruct, oldFieldName, newFieldName)
% Rename a field of a struct.
%
% Input:
%   myStruct = struct containing the field to be renamed.
%   oldFieldName = current name of the field to be renamed.
%   newFieldName = new name of the field to be renamed.
%
% Output:
%   myStruct = struct with the field renamed.
%
% � August 2016 Alessandro Timmi


% Check that new and old fields have different names:
assert(~strcmp(oldFieldName, newFieldName),...
    'Field "%s" already exists, cannot overwrite it.', oldFieldName)

fprintf('Renaming field from "%s" to "%s"...\n', oldFieldName, newFieldName)

% Copy the old field into the new one:
myStruct.(newFieldName) = myStruct.(oldFieldName);
% Remove the old field from the struct:
myStruct = rmfield(myStruct, oldFieldName);

