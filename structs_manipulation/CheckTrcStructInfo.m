function CheckTrcStructInfo(s, varargin)
% Check if the info contained in the .trc header are consistent with the
% data and display warnings if they are not.
%
% Input:
%   s =  a struct containing data from a .trc file.
%   debug_mode  = (optional parameter, default = false) if true, shows some
%               extra info for debug purposes.
%
% � November 2015 Alessandro Timmi


%% Default input values:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 's', @isstruct);

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, s, varargin{:});

% Copy input arguments into more memorable variables:
s = p.Results.s;
debug_mode = p.Results.debug_mode;


%%
if debug_mode
    fprintf('� November 2015 Alessandro Timmi.\n');
end

% Calculate the sampling frequency from the timestamps array:
[~, ~, ~, avg_freq] = check_sampling_freq(s.time, 'debug_mode', debug_mode);

% Get marker names:
marker_names = fieldnames(s.markers);


if ~isequal(s.DataRate, avg_freq)
    warning('"DataRate" in the header of the .trc file (%0.1f) is different from detected data sampling frequency (%0.1f)',...
        s.DataRate, avg_freq);
end

if ~isequal(s.NumFrames, length(s.time))
    warning('"NumFrames" in the header of the .trc file (%d) is different from detected frames number (%d)',...
        s.NumFrames, length(s.time));
end

if ~isequal(s.NumMarkers, length(marker_names))
    warning('"NumMarkers" in the header of the .trc file (%d) is different from detected markers number (%d)',...
        s.NumMarkers, length(marker_names));
end
