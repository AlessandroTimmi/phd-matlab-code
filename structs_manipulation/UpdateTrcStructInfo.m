function s = UpdateTrcStructInfo(s, varargin)
% Update the info in the .trc struct, calculating them from marker data.
%
% Input:
%   s = struct containing .trc data, with info to be updated.
%
% Output:
%   s = struct containing .trc data, with updated info.
%
% � November 2015 Alessandro Timmi


%% Default input values:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 's', @isstruct);

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, s, varargin{:});

% Copy input arguments into more memorable variables:
s = p.Results.s;
debug_mode = p.Results.debug_mode;


%%
if debug_mode
    fprintf('� November 2015 Alessandro Timmi.\n');
end

% Calculate the sampling frequency from the timestamps array:
[~, ~, ~, avg_freq] = check_sampling_freq(s.time, 'debug_mode', debug_mode);

% Get marker names:
marker_names = fieldnames(s.markers);

if ~isequal(s.DataRate, avg_freq)
    s.DataRate = avg_freq;
    fprintf('"DataRate" has been updated.\n');
end

if ~isequal(s.NumFrames, length(s.time))
    s.NumFrames = length(s.time);
    fprintf('"NumFrames" has been updated.\n');
end

if ~isequal(s.NumMarkers, length(marker_names))
    s.NumMarkers = length(marker_names);
    fprintf('"NumMarkers" has been updated.\n');
end


end

