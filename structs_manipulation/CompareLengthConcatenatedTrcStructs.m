function CompareLengthConcatenatedTrcStructs(kinectConcatenated, viconConcatenated)
% Compare length of arrays containing marker coordinates beteween two
% concatenated structs for the Treadmill Study.
%
% Input:
%   kinectConcatenated = concatenated struct containing .trc data collected
%               using Kinect.
%   viconConcatenated = concatenated struct containing .trc data collected
%               using Vicon.
%
% � September 2016 Alessandro Timmi


sortedFieldnames = sort(fieldnames(kinectConcatenated.markers));

%% Check lengths of concatenated trials:
for n = 1:numel(sortedFieldnames)
    lengthVicon = length(viconConcatenated.markers.(sortedFieldnames{n}));
    lengthKinect = length(kinectConcatenated.markers.(sortedFieldnames{n}));
    
    assert(isequal(lengthVicon, lengthKinect),...
        'Field %d has different length for Vicon (%d) and Kinect (%d) concatenated trials.',...
        n, lengthVicon, lengthKinect);
end

fprintf('\nConcatenated Vicon and Kinect trials have same length.\n\n')
