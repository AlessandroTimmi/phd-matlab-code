function s = ConcatenateArrayOfTrcStructs(arrayOfStructs)
% Concatenate an array of structs containing .trc data into a single struct.
% 
% Input:
%   arrayOfStructs = array of structs containing trials data.
%
% Output:
%   s = struct containing all data from the array of struct, vertically
%       concatenated.
%
% � September 2016 Alessandro Timmi


%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'arrayOfStructs', @isstruct);

% Parse input arguments:
parse(p, arrayOfStructs);

% Copy input arguments into more memorable variables:
arrayOfStructs = p.Results.arrayOfStructs;

clear p


%% Marker names were already checked for consistency across all structs
% when the array of structs was created, so we can just read the first one:
markerNames = fieldnames(arrayOfStructs(1).markers);

%% Concatenate array of markers structs in a single struct:
% Matlab cannot index structs with more than one level
% using colon (:).
% Hence we need to reduce the two-level array of markers structs:
%   Trial(1).markers.Marker_1
%   Trial(1).markers.Marker_2
%   Trial(2).markers.Marker_1
%   Trial(2).markers.Marker_2
%   ...
%   Trial(N).markers.Marker_1
%   Trial(N).markers.Marker_2
%
% to one-level before indexing it. In this way we obtain an array of
% structs containing markers coordinates with this structure:
%   Trial(1).Marker_1 .Marker_2 ... .Marker_M;
%   Trial(2).Marker_1 .Marker_2 ... .Marker_M;
%   ...
%   Trial(N).Marker_1 .Marker_2 ... .Marker_M;
markersOneLevel = horzcat(arrayOfStructs(:).markers);


% Build a new struct containing all concatenated signals from trials,
% as if it was a single long trial:
for m = 1:numel(markerNames)
    s.markers.(markerNames{m}) =...
        vertcat(markersOneLevel(:).(markerNames{m}));
end


end
