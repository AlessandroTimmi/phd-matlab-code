function clean_mot_filenames = clean_mot_files(varargin)
% Clean marker coordinates contained into .mot files, performing the
% following operations:
% 1) load the .mot file(s);
% 2) fill the gaps if present;
% 3) filter data using the specified or default technique;
% 4) write the resulting data into new .mot file(s).
%
% Input:
%   mot_filenames = (optional parameter) cell array of strings containing each one
%                   the path and filename of each .mot file to be cleaned.
%                   If not provided, the program opens a dialog box for
%                   retrieving files.
%   max_gap_size = (optional, default = []) Max size of gaps (in frames)
%                   which will be filled. Gaps larger than this will be
%                   filled with "Nan" and a warning message will be
%                   printed on screen.
%   filter_type  = [optional, default = ''] one of the following strings:
%                   'none', 'butter', 'sgolay', 'median', 'median_sgolay'.
%   butter_cutoff = (optional, default = 0) Cutoff frequency for
%                   Butterworth filter.
%   butter_order  = (optional, default = 0) Order of Butterworth filter.
%   sgolay_order  = (optional, default = 0) Order of Savitzky-Golay filter.
%   sgolay_span   = (optional, default = 0) Span of Savitzky-Golay filter.
%   median_span   = (optional, default = 0) span of median filter.
%                   for other filter options see file filter_struct.m.
%   write_mot     = (optional parameter, default = true).
%   debug_mode    = (optional parameter, default = false).
%
% Output:
%   clean_mot_filenames = [optional] the cleaned file(s) are stored in
%                         the subfolder "Clean" of the source folder
%                         and the word "_clean" is appended to their
%                         names.
%
% � November 2014 Alessandro Timmi


%% TODO:
% TRY TO PASS FILTER SETTINGS ALL TOGETHER INTO A STRUCT
% - Add intermediate plots at each step of this function.
% - Add support for .mot files and change the name of the function
%   accordingly.
% - Print filter settings in the log file.
% THIS FUNCTION HAS BEEN WRITTEN IN A HURRY FOR MR GINO COATES, as a copy
% of clean_trc_files. Double check it when you have time. We can probably
% merge it with clean_trc_files().



%% Check input/output arguments:
nargoutchk(0, 1);

% Default input values:
default.mot_filenames = {};
default.max_gap_size = [];
default.filter_type = '';
expected_filters = {'none', 'butter', 'sgolay', 'median', 'median_sgolay'};
default.butter_cutoff = 0; % cutoff frequency for lowpass butter filter (Hz)
default.butter_order = 0; % Butterworth filter order
default.sgolay_order = 0; % Savitzky-Golay filter order
default.sgolay_span = 0; % (samples)
default.median_span = 0; % (samples)
default.write_mot = true;
default.debug_mode = false;


% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'mot_filenames', default.mot_filenames,...
    @(x) validateattributes(x, {'char','cell'}, {'nonempty'}));
addParameter(p, 'max_gap_size', default.max_gap_size,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'filter_type', default.filter_type,...
    @(x) any(validatestring(x, expected_filters)));
addParameter(p, 'butter_cutoff', default.butter_cutoff,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'butter_order', default.butter_order,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'sgolay_order', default.sgolay_order,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'sgolay_span', default.sgolay_span,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'median_span', default.median_span,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'write_mot', default.write_mot, @islogical);
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
mot.filenames = p.Results.mot_filenames;
max_gap_size = p.Results.max_gap_size;
filter.type = p.Results.filter_type;
filter.butter.cutoff = p.Results.butter_cutoff;
filter.butter.order = p.Results.butter_order;
filter.sgolay.order = p.Results.sgolay_order;
filter.sgolay.span = p.Results.sgolay_span;
filter.median.span = p.Results.median_span;
write_mot = p.Results.write_mot;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
fprintf('Cleaning .mot files...\n');
fprintf('� November 2014 Alessandro Timmi.\n')
fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n')
fprintf('WRITTEN IN A HURRY FOR MR GINO COATES, ESQUIRE.\n')
fprintf('DISCLAIMER: I''M NOT RESPONSIBLE IF YOUR PC AND THE PLANET EXPLODE.\n')
fprintf('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n')


%% If no input .mot file, show a dialog window to select
% one or more .mot files:
if isempty(mot.filenames)
    [filename, pathname] = uigetfile('*.mot', 'Select .mot file(s) to clean',...
        'MultiSelect', 'on');
    mot.filenames = fullfile(pathname, filename);
    clear filename pathname
end

% If a single file is selected, the output of the dialog is a string. We
% want a cell array in any case (single or multiple selection):
if ~iscell(mot.filenames)
    mot.filenames = {mot.filenames};
end


if isempty(max_gap_size)
    % Set max gap size:
    answer = {};
    while isempty(answer)
        prompts = ['Gaps larger than this will be filled with "Nan"',...
            'and a warning message will be printed on screen:'];
        dlg_title = 'Set max gap size';
        num_lines = 1;
        default_ans = {'10'};
        options.WindowStyle = 'normal';
        options.Resize = 'on';
        answer = inputdlg(prompts, dlg_title, num_lines, default_ans, options);
    end
    max_gap_size = str2double(answer{1});
end

% We don't want to extrapolate because, due to big gaps in coloured
% markers data, we would obtain very bad results. Since the spline
% interpolation requires a form of extrapolation, we use Nan as default
% value to fill the gaps at the extremities of the arrays.
%extrapval = nan;
% However, now that we improved coloured marker tracking and we have
% smaller gaps, it is better to extrapolate. Otherwise, filtering with
% increase these gaps at the extremities, making the problem much worse.
% Hence, we leave the default behaviour of the spline interpolation, which
% is to extrapolate.


%% Choose the filter type:
if isempty(filter.type)
    ok = false;
    while ~ok
        [selection, ok] = listdlg('ListString', expected_filters,...
            'SelectionMode', 'single',...
            'InitialValue', 5,...
            'Name', 'Filter selection',...
            'PromptString', 'Select filter type:');
    end
    filter.type = expected_filters{selection};
end


%% Preallocate a cell array (output) for the filenames of the cleaned files:
mot.clean_filenames = cell(numel(mot.filenames), 1);


%% Clean selected file(s):
for i=1:numel(mot.filenames)
    % Load the .mot file to be filtered and store its content into a
    % struct:
    fprintf('\nLoading file:\n\t%s.\n', mot.filenames{i});
    s = load_sto_file(mot.filenames{i});
    
%     % Fill the gaps if present, using spline interpolation. Gaps larger
%     % than max_gap_size are filled with Nan by default. OpenSim
%     % seems to accept Nan without any problem, hence I prefer Nan to empty
%     % positions, because at least it is easier to determine the length of a
%     % vector containing Nan compared to an empty vector.
%     fprintf('Filling gaps in the coordinate arrays...\n');
%     fprintf('Max size of gaps to be filled: %d frames.\n', max_gap_size)
%     s_nogaps = structfun(@(y)...
%         ( interp1gap(y, 'spline', max_gap_size)' ),...
%         s, 'UniformOutput', false);
%     
    % If requested, filter filled data:
    if strcmpi(filter.type, 'none')
        fprintf('Filtering not requested by the user.\n')
        %s_filt = s_nogaps;
        s_filt = s;
    else
        fprintf('Filtering data...\n')
        %s_filt = filter_struct(s_nogaps,...
        s_filt = filter_struct(s,...
            'filter_type', filter.type,...
            'butter_cutoff', filter.butter.cutoff,...
            'butter_order', filter.butter.order,...
            'sgolay_order', filter.sgolay.order,...
            'sgolay_span', filter.sgolay.span,...
            'median_span', filter.median.span,...
            'debug_mode', debug_mode);
    end
    
    
    if write_mot
        % Generate the destination file name(s):
        [pathstr, name, ext] = fileparts(mot.filenames{i});
        dest_folder = [pathstr filesep 'Clean'];
        if ~exist(dest_folder, 'dir')
            mkdir(dest_folder);
        end
        mot.clean_filenames{i} = fullfile(dest_folder, [name,'_clean', ext] );
        
        % Write the resulting .mot into an ASCII file:
        coord2mot(s_filt, mot.clean_filenames{i}); 
        
        fprintf('Cleaned data stored in:\n\t%s.\n\n', mot.clean_filenames{i});
        
        % Report cleaning settings into a log file:
        info_file_id = fopen(fullfile(dest_folder, 'Cleaning.log'), 'w');
        fprintf(info_file_id, 'Cleaning log:\n');
        fprintf(info_file_id, 'Filled gaps with size up to: %d frames.\n', max_gap_size);
        fprintf(info_file_id, 'Filter used: %s.\n', filter.type);
        fprintf(info_file_id, 'Filter settings: %s.\n', 'TODO');
        fclose(info_file_id);
    end
end

clean_mot_filenames = mot.clean_filenames;
end