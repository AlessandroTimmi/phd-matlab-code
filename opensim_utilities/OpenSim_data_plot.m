% Plot results from OpenSim analyses.
% � Oct 2014 Alessandro Timmi

%% TODO
% - replace importdata() with load_sto_file() (or LoadMotFile?) for JRL and do required
%   adjustments.
% - add selector for leg (R/L).
% - add selector for force plate (1,2,3).
% - convert from script to function.
% - add input parser and uigetfile()/uigetfolder() for input output of data. 
% - add possibilty to plot IK results;
% - add plot of reserve torque from SO vs joint torque from ID.
%         figure
%         plot(id.time, id.knee_angle_r_moment,'b')
%         hold on
%         plot(so_x1.time, so_x1.knee_angle_r_reserve,'r')
%         plot(so_x3.time,so_x3.knee_angle_r_reserve,'g')
%         title('Knee reserve torques for different max isometric forces (MIF)', 'fontweight','bold')
%         ylabel('Force [Nm]')
%         xlabel('Time [s]')
%         legend('Knee moment', 'Knee reserve (1 x MIF)', 'Knee reserve (3 x MIF)')

clear
close all
clc

%% 
fprintf('Plot OpenSim analyses results\n\n')

%% Strings
project_path = 'X:\PhD\ACL injury puberty and footwear\Trials\Vicon\Dev Biomech 2015\Dev02'; 
% Trial types, from which the folders names comes from:
trial_conditions = {'Barefoot'};
subject = 'DEV02';
trial_labels = {' SLS 01'};
side = {'right'};
output_folder = [project_path filesep 'Results_plots' filesep];

% Switches:
IK_plot = false;
GRF_plot = false;
ID_plot = true;
SO_plot = true;
JRL_plot = false;
export_plots = true;

% Preallocate figs and titles arrays:
figs = [];
titles = {};

if IK_plot
    % TODO
    IK_suffix = '_ik';
    IK_ext = '.mot';
    
    title_IK = sprintf('IK %s %s ', side, coord);
    fig0 = figure('Name', title_IK);
    
    % Append fig handles and titles:
    figs = [figs, fig0];
    titles = [titles; title_IK];
end

if GRF_plot
    % Ground reaction forces strings:
    GRF_suffix = '_grf';
    GRF_ext = '.mot';
    
    title_GRF = 'GRF (Vertical - Right foot)';
    fig1 = figure('Name', title_GRF);
    
    % Append fig handles and titles:
    figs = [figs, fig1];
    titles = [titles; title_GRF];
end

if ID_plot
    % Inverse dynamics strings
    ID_suffix = '_id';
    ID_ext = '.sto';

    title_hip_flex_m = sprintf('%s %s - Hip flex moment (Right)', subject, trial_labels{1});
    title_hip_add_m = sprintf('%s %s - Hip add moment (Right)', subject, trial_labels{1});
    title_hip_rot_m = sprintf('%s %s - Hip rot moment (Right)', subject, trial_labels{1});
    title_knee_m = sprintf('%s %s - Knee moment (Right)', subject, trial_labels{1});
    title_ankle_m = sprintf('%s %s - Ankle moment (Right)', subject, trial_labels{1});

    fig2 = figure('Name', title_hip_flex_m);
    fig3 = figure('Name', title_hip_add_m);
    fig4 = figure('Name', title_hip_rot_m);
    fig5 = figure('Name', title_knee_m);
    fig6 = figure('Name', title_ankle_m);
    
    % Append fig handles and titles:
    figs = [figs, fig2, fig3, fig4, fig5, fig6];
    titles = [titles; title_hip_flex_m; title_hip_add_m;...
        title_hip_rot_m; title_knee_m; title_ankle_m];
end

if SO_plot
    % Static optimization strings:
    SO_suffix = '_StrongX3_StaticOptimization_force';
    SO_ext = '.sto';
        
    title_ham = sprintf('%s %s - Hamstring (Right)', subject, trial_labels{1});
    title_quad = sprintf('%s %s - Quadriceps (Right)', subject, trial_labels{1});
    title_gastroc = sprintf('%s %s - Gastrocnemius (Right)', subject, trial_labels{1});

    fig7 = figure('Name', title_ham);
    fig8 = figure('Name', title_quad);
    fig9 = figure('Name', title_gastroc);
    
    % Append fig handles and titles:
    figs = [figs, fig7, fig8, fig9];
    titles = [titles; title_ham; title_quad; title_gastroc];
end

if JRL_plot
    % Joint reaction loads strings:
    JRL_suffix = '_JointReactionLoads';
    JRL_ext = '.sto';

    title_knee_fx = 'Knee (R) on tibia in tibia fx';
    title_knee_fy = 'Knee (R) on tibia in tibia fy';
    title_knee_fz = 'Knee (R) on tibia in tibia fz';

    fig10 = figure('Name', title_knee_fx);
    fig11 = figure('Name', title_knee_fy);
    fig12 = figure('Name', title_knee_fz);
    
    % Append fig handles and titles:
    figs = [figs, fig10, fig11, fig12];
    titles = [titles, title_knee_fx, title_knee_fy, title_knee_fz];
end
     
        
gray = [0.7, 0.7, 0.7];
colors = {'k', 'k', gray, gray};
linestyles = {'-', '--', '-', '--'};
my_linewidth = 2;
x_label_str = 'Time [s]';


%% Plot the results for all the trials types
for i=1:length(trial_conditions)
    if GRF_plot
        fprintf('Plotting GRF data...\n')
        %% Ground reaction forces:
        GRF_file = [project_path filesep trial_conditions{i} filesep subject...
            trial_labels{i} GRF_suffix GRF_ext];
        GRF = load_sto_file(GRF_file);

        
        figure(fig1)
        % 2nd force plate, vertical component according to OpenSim
        % coordinate system (Y):
        plot(GRF.time, GRF.N2_ground_force_vy, 'linestyle', linestyles{i},...
            'Color', colors{i}, 'linewidth', my_linewidth)
        % refresh the legend with the trials plotted until now:
        legend(trial_conditions{1:i})
        hold on
        if i==1
            title(title_GRF, 'fontweight', 'bold')
            xlabel(x_label_str)
            ylabel('GRF_{vert} [N]')
        end
    end
   
    if ID_plot
        fprintf('Plotting Inverse Dynamics (ID) results...\n')
        %% Joint moments from inverse dynamics (ID):
        ID_file = [project_path filesep trial_conditions{i} filesep subject...
            trial_labels{i} ID_suffix ID_ext];
        ID = load_sto_file(ID_file);

        figure(fig2)
        plot(ID.time, ID.hip_flexion_r_moment, 'linestyle', linestyles{i},...
            'Color', colors{i}, 'linewidth', my_linewidth)
        legend(trial_conditions{1:i})
        hold on
        if i==1
            title(title_hip_flex_m, 'fontweight', 'bold')
            xlabel(x_label_str)
            ylabel('Moment [Nm]')
        end

        figure(fig3)
        plot(ID.time, ID.hip_adduction_r_moment, 'linestyle', linestyles{i},...
            'Color', colors{i}, 'linewidth', my_linewidth)
        legend(trial_conditions{1:i})
        hold on
        if i==1
            title(title_hip_add_m, 'fontweight', 'bold')
            xlabel(x_label_str)
            ylabel('Moment [Nm]')
        end

        figure(fig4)
        plot(ID.time, ID.hip_rotation_r_moment, 'linestyle', linestyles{i},...
            'Color', colors{i}, 'linewidth', my_linewidth)
        legend(trial_conditions{1:i})
        hold on
        if i==1
            title(title_hip_rot_m, 'fontweight', 'bold')
            xlabel(x_label_str)
            ylabel('Moment [Nm]')
        end

        figure(fig5)
        plot(ID.time, ID.knee_angle_r_moment, 'linestyle', linestyles{i},...
            'Color', colors{i}, 'linewidth', my_linewidth)
        legend(trial_conditions{1:i})
        hold on
        if i==1
            title(title_knee_m, 'fontweight', 'bold')
            xlabel(x_label_str)
            ylabel('Moment [Nm]')
        end

        figure(fig6)
        plot(ID.time, ID.ankle_angle_r_moment, 'linestyle', linestyles{i},...
            'Color', colors{i}, 'linewidth', my_linewidth)
        legend(trial_conditions{1:i})
        hold on
        if i==1
            title(title_ankle_m, 'fontweight', 'bold')
            xlabel(x_label_str)
            ylabel('Moment [Nm]')
        end
    end
    
    
    if SO_plot
        fprintf('Plotting Static Optimization (SO) results...\n')
        %% Muscle force from Static Optimization:
        SO_forces_file = [project_path filesep trial_conditions{i} filesep subject...
            trial_labels{i} SO_suffix SO_ext];
        SO = load_sto_file(SO_forces_file);


        % Sum the muscles constituting the right ham:
        % - semimembranous;
        % - semitendinous;
        % - biceps femoris long head;
        % - biceps femoris short head.        
        ham_r = sum([SO.semimem_r, SO.semiten_r, SO.bifemlh_r, SO.bifemsh_r], 2);

        figure(fig7)
        plot(SO.time, ham_r, 'linestyle', linestyles{i},...
            'Color', colors{i}, 'linewidth', my_linewidth)
        legend(trial_conditions{1:i})
        hold on
        if i==1
            title(title_ham, 'fontweight', 'bold')
            xlabel(x_label_str)
            ylabel('Muscle force [N]')
        end

        % Sum the muscles constituting the quadriceps:
        % - rectus femoris;
        % - vastus medialis;
        % - vastus intermedius;
        % - vastus lateralis.
        quad_r = sum([SO.rect_fem_r, SO.vas_med_r, SO.vas_int_r, SO.vas_lat_r], 2);

        figure(fig8)
        plot(SO.time, quad_r, 'linestyle', linestyles{i},...
            'Color', colors{i}, 'linewidth', my_linewidth)
        legend(trial_conditions{1:i})
        hold on
        if i==1
            title(title_quad, 'fontweight', 'bold')
            xlabel(x_label_str)
            ylabel('Muscle force [N]')
        end
        
        % Sum the muscles constituting the gastrocnemius:
        % - medial gastrocnemius;
        % - lateral gastrocnemius.
        gastroc_r = sum([SO.med_gas_r, SO.lat_gas_r,], 2);
        figure(fig9)
        plot(SO.time, gastroc_r, 'linestyle', linestyles{i},...
            'Color', colors{i}, 'linewidth', my_linewidth)
        legend(trial_conditions{1:i})
        hold on
        if i==1
            title(title_gastroc, 'fontweight', 'bold')
            xlabel(x_label_str)
            ylabel('Muscle force [N]')
        end
        
    end
    
    %% Joint reaction loads:
    if JRL_plot
        fprintf('Plotting Joint Reaction Loads (JRL) results...\n')
        JRL_file = [project_path filesep trial_conditions{i} filesep subject...
            trial_labels{i} JRL_suffix JRL_ext];
        JRL = importdata(JRL_file,'\t',12);

        time_JRL = JRL.data(:,1);

        figure(fig10)
        plot(time_JRL, JRL.data(:,2), 'linestyle', linestyles{i},...
            'Color', colors{i}, 'linewidth', my_linewidth)
        legend(trial_conditions{1:i})
        hold on
        if i==1
            title(title_knee_fx, 'fontweight', 'bold')
            xlabel(x_label_str)
            ylabel('Reaction [N]')
        end

        figure(fig11)
        plot(time_JRL, JRL.data(:,3), 'linestyle', linestyles{i},...
            'Color', colors{i}, 'linewidth', my_linewidth)
        legend(trial_conditions{1:i})
        hold on
        if i==1
            title(title_knee_fy, 'fontweight', 'bold')
            xlabel(x_label_str)
            ylabel('Reaction [N]')
        end

        figure(fig12)
        plot(time_JRL, JRL.data(:,4), 'linestyle', linestyles{i},...
            'Color', colors{i}, 'linewidth', my_linewidth)
        legend(trial_conditions{1:i})
        hold on
        if i==1
            title(title_knee_fz, 'fontweight', 'bold')
            xlabel(x_label_str)
            ylabel('Reaction [N]')
        end
    end
    fprintf('Plots done.\n')
end

if export_plots
    if ~exist(output_folder, 'dir')
        mkdir(output_folder);
    end
    fprintf('Saving figures in:\n')
    fprintf('\t%s.\n', output_folder)
    for i=1:length(figs)
        fig_filename = [output_folder, subject, trial_labels{1}, ' ', titles{i}, '.fig'];
        if exist(fig_filename, 'file')==2
            fprintf('The file "%s" already exists: ', fig_filename)
            choice = questdlg('The destination file already exists: do you want to overwrite it?', 'default', 'Yes');
            % TODO: Improve this "overwrite" choice management:
            switch choice
                case 'Yes'
                    fprintf('overwriting...\n')
                    % do nothing, just go ahead.
                case 'No'
                    fprintf('skipping...\n')
                    continue
                case 'Cancel'
                    fprintf('skipping...\n')
                    continue
            end
        end
        savefig(figs(i), fig_filename)
    end
end


