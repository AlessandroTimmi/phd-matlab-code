function scaledModelFilename = FindScaledOpenSimModel(subjectPath, subjectPrefix)
% Find the full filename of the scaled OpenSim (.osim) model, given the
% subject's folder and prefix. It is assumed that the folder contains only
% a single scaled model for the subject.
%
% Input:
%   subjectPath = path to the subject's folder containing the scaled .osim
%       model to be selected.
%   subjectPrefix = case insensitive prefix indentifying the subject,
%       e.g. 'Dev'.   
%
% Output:
%   scaledModelFilename = filename of the scaled .osim model.
%
% � October 2016 Alessandro Timmi


%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'subjectPath',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'subjectPrefix',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Parse input arguments:
parse(p, subjectPath, subjectPrefix);

% Copy input arguments into more memorable variables:
subjectPath = p.Results.subjectPath;
subjectPrefix = p.Results.subjectPrefix;

clear p


%% Get subject name from path:
subjectName = regexpi(subjectPath, [subjectPrefix, '\d+'], 'match', 'once');

% Get filename of the scaled model for current subject:
scaledModelName = ls(fullfile(subjectPath, [subjectName, '_scaled*.osim']));

% Check that there is only a single scaled model in this subject's folder:
if size(scaledModelName, 1) > 1
    error('More than one scaled model found for subject %s.', subjectName)
elseif size(scaledModelName, 1) < 1
    error('No scaled model found for subject %s.', subjectName)
end

scaledModelFilename = fullfile(subjectPath, scaledModelName);


end