function SynthesizeAnimation()
% This function animates an OpenSim model based on user-defined coordinate
% values. Coordinates signals are generated interpolating splines
% across the input values. It is possible to keep the skeleton still for a
% few seconds before and after the animation, changing the lengths of the
% paddings.
%
% � July 2015 Alessandro Timmi

% TODO:
% - variable number of input and output arguments.
% - add a GUI with checkboxes to select the joints to be animated and text
% fields for the ranges of motion.


clear
clc
close all


%% Input data
% We need the OpenSim model to read all the coordinates defined in it:
osimFilename = ['D:\OneDrive - The University of Melbourne\PhD\OpenSim\'...
    'Xu et al. 2014 - OpenSim model with knee ligaments\ModelwithKneeLigaments.osim'];

trialName = 'KneeFlexion';
destinationFilename = fullfile('D:\OneDrive - The University of Melbourne\PhD\ACL injury puberty and footwear\Videos',...
    [trialName, '.mot']);

% Coordinates to be animated:
coordinatesNames = {'knee_extend_r'};

% Input values for coordinates to be animated (one row for each coordinate
% to be animated):
coordinatesValues = [-45, -75, -15, -45];

% If some coordinates must be constant and different from zero during the
% entire animation, set their names and values here. Otherwise leave both
% variables empty:
defaultPoseCoords = {};
defaultPoseValues = [];

% Desired framerate for the animation:
fps = 30;   

% Duration of the motion (s):
motionDuration = 5;

% We might want some "paddings" at the extremities of the animation, in
% which the model is still.
% Initial padding (s):
paddingStart = 0;

% Final padding (s):
paddingEnd = 0;


%% Calculate some parameters
% Total duration of the animation (s), including paddings:
totalDuration = paddingStart + motionDuration + paddingEnd;

% Motion duration in frames:
motionFrames = motionDuration * fps;

% Total number of frames in the animation:
totalFrames = totalDuration * fps;

% Start frame of motion (one frame after the end of the initial padding):
motionStartFrame = paddingStart * fps + 1;

% End frame of motion:
motionEndFrame = (paddingStart + motionDuration) * fps;


%% Read coordinate names from model (.osim) file:
fprintf('OpenSim animation synthesizer\n\n');
fprintf('Selected OpenSim model (.osim file):\n');
fprintf('\t%s\n\n', osimFilename);

% The built-in Matlab xmlread() function cannot read the OpenSim .xml file.
% As suggested by Prasanna Sritharan (see function "writeOsimFoM_KAE.m"), a
% workaround is to go through the .osim file line by line, looking for
% regular expressions.

% Open the .osim file in read mode:
osimFile = fopen(osimFilename, 'r');

% Create an empty cell array to store the coordinate names:
osimCoordinatesNames = {};

% Advance the .osim file line by line until its end:
while ~feof(osimFile)
    
    % Parse current line (removing newline character):
    thisLine = fgetl(osimFile);
    
    % Check for the regular expression and extract the coordinate name if present:
    thisCoord = regexp(thisLine, '<Coordinate name="(\w*)">', 'tokens', 'once');
    
    if ~isempty(thisCoord)
        % If the expression is matched, store the coordinate name:
        osimCoordinatesNames = [osimCoordinatesNames, thisCoord]; %#ok<AGROW>
    end
end

fprintf('Joint coordinate names found in the .osim file:\n')
fprintf('\t%s\n', osimCoordinatesNames{:});


%% Time array for the destination .mot file:
s.time = linspace(0, totalDuration, totalFrames)';
 
% Preallocate all coordinates arrays:
for c = 1:length(osimCoordinatesNames)
    s.coords.(osimCoordinatesNames{c}) = zeros(totalFrames, 1);
end


%% Set the default pose, if requested by the user:
if ~isempty(defaultPoseCoords)
    for c = 1:length(defaultPoseCoords)
        s.coords.(defaultPoseCoords{c})(:) = defaultPoseValues(c);
    end
end


%% Generate coordinates values and insert them into the coordinate arrays,
% between the paddings (if present):
for c = 1:length(coordinatesNames)
    
    % Check that current coordinate is available in the .osim model:
    assert(any(strcmp(coordinatesNames{c}, osimCoordinatesNames)),...
        'Coordinate "%s" not found in .osim model.', coordinatesNames{c})
      
    % Generate signal for current coordinate:
    signal = GenerateSplineFromJointCoordinates(...
        coordinatesValues(c,:), motionDuration, motionFrames, coordinatesNames{c});
    
    % Add signal to the coordinates struct:
    s.coords.(coordinatesNames{c})(motionStartFrame : motionEndFrame) = signal;
        
    % In the final padding, the skeleton has to maintain the final pose:
    s.coords.(coordinatesNames{c})(motionEndFrame + 1 : totalFrames) = signal(end);
end


%% Write data into a .mot file:
WriteMotFile(s, 'destinationFilename', destinationFilename);

end



function yy = GenerateSplineFromJointCoordinates(y, duration, durationFrames, coordinateName)
% Generate a spline, given input coordinate values and duration of the
% motion. Input values are assumed to be uniformly spaced in time.
%
% Input:
%   y = coordinate values.
%   duration = duration of the motion in s.
%   durationFrames = duration of the motion in frames.
%   coordinateName = name of the coordinate to be interpolated.
%
% Output:
%   yy = spline representing the coordinate signal, based on input data.
%
% � October 2016 Alessandro Timmi


% Original time array, assuming that input coordinate values are equally spaced:
stepsNumber = length(y) - 1;
stepDuration = duration / stepsNumber;
t = (0 : stepDuration : duration)';

% Interpolated time array:
tt = linspace(0, duration, durationFrames);


% Plot of the coordinate signal:
yy = spline(t, y, tt);
figure('Name', coordinateName)
plot(t, y, 'ro')
hold on
plot(tt, yy, 'b.')
hAxes = gca;
hAxes.XLim = custom_axis_margins(tt, 0.1);
hAxes.YLim = custom_axis_margins(yy, 0.1);
xlabel('Time (s)')
ylabel(sprintf('%s (�)', coordinateName), 'interpreter', 'none')
title(coordinateName, 'interpreter', 'none')
legend('Input values', 'Spline interpolation',...
    'location', 'best')

end



