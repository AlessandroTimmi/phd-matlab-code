function [meanRmsError, meanMaxError] = ReadOpenSimIkLog(varargin)
% Read the log messages from OpenSim v3.2 IK tool and calculate
% the average of the RMS error and of the maximum marker error of an
% Inverse Kinematics execution.
% To be read, the log must be copied from OpenSim GUI and pasted in an
% ASCII file.
% The file reader stops when it finds a line which is formatted differently
% from what expected or when it reaches the end of the text file.
%
% Input:
%   filename = filename of a text file containing the Inverse Kinematics
%              log.
%
% Output: 
%   meanRmsError = mean of the RMS marker error over the trial duration.
%   meanMaxError = mean of the max marker error over the trial duration.
%              Note that the max marker error might be referred to a
%              different marker for each frame of the trial.
%
% � September 2014 Alessandro Timmi

%% Default input values:
default.filename = '';
default.debugMode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addOptional(p, 'filename', default.filename,...
            @(x)validateattributes(x,{'char','cell'}, {'nonempty'})); 
addParameter(p, 'debugMode', default.debugMode, @islogical);
        
% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
filename = p.Results.filename;
debugMode = p.Results.debugMode;


%% Load data
fprintf('Reading OpenSim Inverse Kinematics log...\n\n');

% If not provided as input argument, select file(s) to be loaded:
if isempty(filename)
    [fname, path] = uigetfile({'*.log'; '*.txt'},...
        'Select the log file(s) (*.log or *.txt)', 'MultiSelect', 'on');
    filename = fullfile(path, fname);
end
if isequal(fname,0)
    fprintf('No log file selected.\n')
    return
end

if ~iscell(filename)
    filename = {filename};
end

for i=1:numel(filename)
    fprintf('Loaded log file(s):\n\t%s\n', filename{i})
    
    fileID = fopen(filename{i});
    % We need to read 5 different kind of values, so we preallocate the array:
    sizeA = [5, Inf];
    % Scan the file line by line:
    A = fscanf(fileID,...
        'Frame %d (t=%f):	total squared error = %f, marker error: RMS=%f, max=%f %*s\n',...
        sizeA);
    fclose(fileID);

    % Transpose the resulting array to match the orientation of the data
    % in the input file:
    A = A';

    % Copy read data into specific arrays:
    frame = A(:,1);
    t = A(:,2);
    tse = A(:,3);
    rmsError = A(:,4);
    maxError = A(:,5);

    % Calculate the average of the interesting errors:
    meanRmsError = mean(rmsError);
    meanMaxError = mean(maxError);
    
    fprintf('Mean RMS error = %g m\nMean MAX error = %g m\n\n',...
        meanRmsError, meanMaxError);
    
    % Recommended IK error limits according to OpenSim documentation:
    % http://simtk-confluence.stanford.edu:8080/display/OpenSim/Simulation+with+OpenSim+-+Best+Practices
    recommendedRmsError = 0.02; % m
    recommendedMaxError = 0.04; % m
    
    % Plot results over time
    [~, fname] = fileparts(filename{i});
    title_1 = sprintf('%s - IK error over time', fname);
    figure('Name', title_1)
    plot(t, rmsError, 'b', 'linewidth', 1.5)
    hold on
    plot(t, maxError, 'k', 'linewidth', 1.5)
    ylim([0, 0.1])
    xlabel('Time [s]')
    ylabel('Error [m]')
    title(title_1, 'fontweight', 'bold', 'interpreter', 'none')
    h1 = refline(0, recommendedRmsError);
    h2 = refline(0, recommendedMaxError);
    set(h1, 'linestyle', '--', 'color', 'b')
    set(h2, 'linestyle', '--', 'color', 'k')
    legend('RMS error', 'MAX error', 'Recommended RMS', 'Recommended MAX',...
        'location', 'best')
    
end