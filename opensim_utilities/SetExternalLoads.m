function externalLoads = SetExternalLoads(externalLoadsFilename,...
    forcePlateNumber, appliedToBody, grfFilename, ikFilename)
% Create OpenSim External Loads object (used for Inverse Dynamics) and
% export its content to an .xml file.
%
% Input:
%   externalLoadsFilename = destination filename of the External Loads .xml.
%   forcePlateNumber = number of the force plate from which the external
%       force is measured.
%   appliedToBody = name of the body segment to which the external force is
%       applied, e.g. 'calcn_r'.
%   grfFilename = name of the input file containing ground reaction forces.
%   ikFilename = name of the input file containing Inverse Kinematics results.
%
% Output
%   externalLoads = External Loads object, to be used by Inverse Dynamics.
%
% � October 2016 Alessandro Timmi


import org.opensim.modeling.*

% Get External Loads name:
[~, externalLoadsName, ~] = fileparts(externalLoadsFilename);

% Create an External Loads object:
externalLoads = ExternalLoads();
externalLoads.setName(externalLoadsName);

% Create an External Force object and set its properties:
externalForce = ExternalForce();
externalForce.setName(sprintf('%s_GRF', appliedToBody));
externalForce.setAppliedToBodyName(appliedToBody)
externalForce.setForceExpressedInBodyName('ground')
externalForce.setPointExpressedInBodyName('ground')
externalForce.setForceIdentifier(sprintf('%d_ground_force_v', forcePlateNumber))
externalForce.setPointIdentifier(sprintf('%d_ground_force_p', forcePlateNumber))
externalForce.setTorqueIdentifier(sprintf('%d_ground_torque_', forcePlateNumber))

% Add the External Force to the External Loads:
externalLoads.insert(0, externalForce);
% Not sure why but using set instead of insert makes Matlab crash.
% I'm not even sure about the difference between "insert()" and "set()".
%externalLoads.set(0, externalForce);

% Set the input GRF filename:
externalLoads.setDataFileName(grfFilename);

% Set the input IK filename:
externalLoads.setExternalLoadsModelKinematicsFileName(ikFilename);

% Set the optional low pass cut off frequency for filtering: -1 means no
% filtering. Accoring to the (poor) documentation it seems to filter only
% the kinematics of the point of application of the force.
externalLoads.setLowpassCutoffFrequencyForLoadKinematics(-1);

% Print the External Loads .xml file
externalLoads.print(externalLoadsFilename);