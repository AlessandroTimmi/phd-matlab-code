function data = set_contact_events(data, Fv_min, varargin)
% Set start and end frame for a C3D file, based on the vertical ground
% reaction force (Fv) value. Useful to cut trials in correspondence of
% initial and final foot contact events.
% Force plates with negligible Fv are automatically skipped by
% this function, as they were not loaded during the trial.
%
% Input
%   data       = struct containing marker and force plate data from
%                a C3D file.
%   Fv_min     = threshold on vertical force component (Fv [N]). If the
%                max Fv from a force platform is below or equal to this 
%                value, that force platform is completely ignored for
%                trimming.
%                Moreover, this is the threshold used to detect contact
%                events, defined as the first and last frames in which
%                Fv > Fv_min.
%                NOTE: frame numbers of contact events are converted from
%                force plates to Vicon framerate and rounded: for this
%                reason the Fv in output data at first and last contact
%                might slighlty differ from Fv_min.
%   v_coord    = (optional parameter, default = 3) position of the
%                vertical coordinate in the force plate F array.
%                Possible values: 1, 2 or 3.
%   debug_mode = (optional parameter, default = true]. If true,
%                displays some extra info for debug purposes.
%
% Output
%   data       = the input struct, updated with start and finish
%                events based on vertical ground reaction forces.
%
% Adapted from Matlab_OpenSim_Toolbox_v2 (Glen Lichtwark et al.).
% � September 2015 Alessandro Timmi

%% Default input values:
default.v_coord = 3;
default.debug_mode = true;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'data', @isstruct)
addRequired(p, 'Fv_min',...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'v_coord', default.v_coord,...
    @(x) any(x == [1, 2, 3]));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, data, Fv_min, varargin{:});

% Copy input arguments into more memorable variables:
data = p.Results.data;
Fv_min = p.Results.Fv_min;
v_coord = p.Results.v_coord;
debug_mode = p.Results.debug_mode;


%% Find when feet are on each forceplate and define the trial
% events as the first foot contact and the final foot contact
fprintf('Trimming trial based on vertical ground reaction force...\n')
if debug_mode
    fprintf('[Debug] Min threshold for vertical force: %.2f.\n',...
        Fv_min)
    fprintf('[Debug] Vertical coordinate: %d.\n', v_coord)
end

% Array that will contain the frame numbers corresponding to initial and
% final contact. We need it to be initially empty, to allow comparison
% with min/max frame later:
E = [];

% Loop through all the force platforms (i):
for i = 1:length(data.fp_data.GRF_data)
    % Read the Fv array of the current force platform (i):
    Fvi_array = data.fp_data.GRF_data(i).F(:,v_coord);
    % Read the max Fv value of the current force platform (i):
    max_Fvi = max(Fvi_array);
    
    % Some force platforms may be not used at all. We want to discard
    % them, so that our events generation is not affected by their
    % signals (which contain only noise).
    if max_Fvi <= Fv_min
        % Skip to the next iteration of the for-loop:
        continue
    end
        
    % Find indices where Fv > Fv_min for this force plate:
    ids = find(Fvi_array > Fv_min);
    if ~isempty(ids)
        % Force plates and markers have different sampling frequencies:
        markers_fps = data.marker_data.Info.frequency;
        fpi_fps = data.fp_data.Info(i).frequency;
        % We need to convert the two indices (corresponding to first and
        % last contact) from force plate to marker sampling frequency.
        % Rounding is fundamental because indices must be integer:
        extreme_ids_fpi(1) = round(ids(1) * markers_fps / fpi_fps);
        extreme_ids_fpi(2) = round(ids(end) * markers_fps / fpi_fps);
        
        % However, rounding close to zero could return a zero index, which
        % is not allowed in Matlab. In this case we need to set the index
        % of the first contact to 1:
        if extreme_ids_fpi(1) == 0
            extreme_ids_fpi(1) = 1;
        end
        % Similarly, for the final contact, we cannot have a frame
        % index > number of frames of the trial. In this case we set the
        % index of the final contact equal to the number of frames:
        if extreme_ids_fpi(2) > data.marker_data.Info.NumFrames
            extreme_ids_fpi(2) = data.marker_data.Info.NumFrames;
        end      
        
        % Update the array of events, comparing contact events found
        % previously with those from current force plate (i).
        % Note we use min(E) and max(E) because E is initially empty and
        % indexing it (e.g. E(1)) would cause an error:
        E(1) = min( [min(E), extreme_ids_fpi(1)] );
        E(2) = max( [max(E), extreme_ids_fpi(2)] );
    end
end


% Define start and end frame obtained above, to be used for writing
% .trc and .mot files.
% If these parameters are not defined during the trimming procedure
% above, the actual extraction function (btk_c3d2trc) will
% automatically set them to the extreme frame numbers of the trial:
data.Start_Frame = E(1);
data.End_Frame = E(2);