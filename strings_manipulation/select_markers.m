function [s2, selected_marker_names] = select_markers(s, varargin)
% Returns a struct containing only coordinates of selected markers.
% Coordinate fields in the returned struct are numbered starting from 1
% and preserving the original order of markers. Time array is preserved
% (if originally available).
%
% Input
%                s = struct containing marker coordinates.
%   my_prompt_string = (optional parameter, default = 'Select marker(s)')
%                  prompt string of the list dialog.
%   my_dlg_name =  (optional parameter, default = 'Select marker(s)')
%                  title of the list dialog.
%   selected_marker_names = (optional, default = {}) sometimes we already
%                  know what markers we need. Instead of selecting them
%                  via a list dialog, we can pass their names as input
%                  argument, (char if single marker, cell array of strings
%                  if multiple).
%     debug_mode = (optional parameter, default = false). If true, shows
%                  extra info for debug purposes.
%
% Output:
%               s2 = struct containing coordinates of selected markers.
%  selected_marker_names = cell array with names of selected markers,
%               whose coordinates have been copied in the returned struct.
%
% � October 2015 Alessandro Timmi

%% Default input values:
default.my_prompt_string = 'Select marker(s)';
default.my_dlg_name = 'Select marker(s)';
default.selected_marker_names = {};
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 's', @isstruct);

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'my_prompt_string', default.my_prompt_string,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'my_dlg_name', default.my_dlg_name,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'selected_marker_names', default.selected_marker_names,...
    @(x) validateattributes(x, {'char', 'cell'}, {'nonempty'}));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);


% Parse input arguments:
parse(p, s, varargin{:});

% Copy input arguments into more memorable variables:
s = p.Results.s;
my_prompt_string = p.Results.my_prompt_string;
my_dlg_name = p.Results.my_dlg_name;
selected_marker_names = p.Results.selected_marker_names;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
fprintf('Selecting markers from struct...\n')
if debug_mode
    fprintf('� October 2015 Alessandro Timmi.\n')
end

marker_names = fieldnames(s.markers);

if isempty(selected_marker_names)
    ok = 0;
    while(ok==0)
        [selection, ok] = listdlg('ListString', marker_names,...
            'SelectionMode', 'multiple',...
            'ListSize', [300, 300],...
            'InitialValue', 1,...
            'Name', my_dlg_name,...
            'PromptString', my_prompt_string);
    end
    % Names of selected markers:
    selected_marker_names = marker_names(selection);
end

fprintf('Selected markers:\n');
fprintf('\t%s\n', selected_marker_names{:});


% Create the output struct as copy of the input one, except for marker
% coordinates:
s2 = rmfield(s, 'markers');
% Update the number of markers:
s2.NumMarkers = length(selected_marker_names);

% Copy only the selected markers into the destination struct:
for i=1:s2.NumMarkers
    s2.markers.(selected_marker_names{i}) =...
        s.markers.(selected_marker_names{i});
end

fprintf('Markers not selected have been discarded from the struct.\n')

end




