function [paired_markers, pair_names] = find_marker_pairs(marker_names,...
    varargin)
% Pair markers by name. The last character of marker names is ignored,
% then matching strings are paired. This function is case insensitive.
%
% Input:
%   marker_names = cell array containg marker names.
%   debug_mode   = (optional parameter, default = false) if true, displays
%                 some extra info for debug purposes.
%
% Output:
%   paired_markers = array containing paired marker names. Each row of the
%                  cell array contains the names of 2 corresponding
%                  markers.
%   pair_names   = cell array with names of marker pairs, obtained removing
%                  the last character from marker names.
%
% � October 2015 Alessandro Timmi.


%% Default input parameters:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input argument:
addRequired(p, 'marker_names', @(x) validateattributes(x, {'cell'},...
    {'nonempty'}));

% Add optional parameters:
addParameter(p, 'debug_mode', default.debug_mode, @islogical)

% Parse input arguments:
parse(p, marker_names, varargin{:});

% Copy input arguments into more memorable variables:
marker_names = p.Results.marker_names;
debug_mode = p.Results.debug_mode;

%%
fprintf('Finding indices of paired markers, removing the last character...\n')
if debug_mode
    fprintf('[Debug] � October 2015 Alessandro Timmi.\n');
    fprintf('[Debug] Marker names:\n');
    fprintf('\t%s\n', marker_names{:});
    fprintf('\n');
end


% Total number of markers:
n_markers = length(marker_names);
% The max number of pairs will be half the number of markers, rounded
% towards zero:
n_pairs = floor(n_markers / 2);
% Preallocate a cell array to store the names of paired markers:
paired_markers = cell(n_pairs, 2);
% Preallocate a cell array to store the names of the pairs:
pair_names = cell(n_pairs, 1);
% This counter will be updated every time a new pair is found:
pair_count = 0;

% Cell array of marker names with last char removed:
marker_names_no_last = cellfun(@(x) x(1:end-1), marker_names,...
    'UniformOutput', false);


fprintf('Marker pairs (and corresponding marker names):\n');
for i=1:n_markers
    % If marker corresponding to current index has not been paired yet...
    if ~strcmpi(marker_names{i}, paired_markers)
        
        % ... compare current marker name with all marker names, ignoring
        % the last character and returning the matching marker names:
        matching_marker_names = ...
            marker_names(...
                strcmpi(marker_names_no_last{i}, marker_names_no_last) );
        
        if length(matching_marker_names) == 2
            % Increment the pair counter:
            pair_count = pair_count + 1;
            % Store paired marker names:
            paired_markers(pair_count, :) = matching_marker_names;
            % Store marker pair name:
            pair_names{pair_count} = marker_names_no_last{i};
            
            fprintf('\t%s (%s, %s);\n',...
                pair_names{pair_count},...
                paired_markers{pair_count, 1},...
                paired_markers{pair_count, 2});
        else
            if debug_mode
                fprintf('[Debug] Marker "%s" has 0 or more than 1 correspondent markers.\n',...
                    marker_names{i});
            end            
        end
        
    end    
end

% Remove empty cells:
paired_markers(pair_count + 1 : n_pairs, :) = [];
pair_names(pair_count + 1 : n_pairs) = [];

end