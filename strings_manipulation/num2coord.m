function coord_char = num2coord(coord_num, varargin)
% Converts coordinate numbers (1, 2 or 3) to the corresponding chars ('X',
% 'Y' or 'Z'). This function supports both scalars and cell arrays.
% Input:
%   coord_num = one of the following scalar: 1, 2, 3. Cell arrays of
%               scalars are supported too, but must be row or column
%               vectors.
%
% Output:
%   coord_char = if coord_num is a scalar, returns the corresponding char.
%                If coord_num is a cell array, returns a cell array of the
%                same sizes with the corresponding chars.
%
% � November 2015 Alessandro Timmi

%% Default input values:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'coord_num',...
    @(x) validateattributes(x, {'numeric', 'cell'}, {'vector', 'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, coord_num, varargin{:});

% Copy input arguments into more memorable variables:
coord_num = p.Results.coord_num;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
if debug_mode
    fprintf('� November 2015 Alessandro Timmi.\n')
end

% If the input is a scalar, convert it to a cell array and enable a flag:
conversion_flag = false;
if ~iscell(coord_num)
    coord_num = {coord_num};
    conversion_flag = true;
end

% Preallocate a cell array for output, with the same sizes of the input:
coord_char = cell(size(coord_num));

for i=1:length(coord_num)
    switch coord_num{i}
        case 1
            coord_char{i} = 'X';
        case 2
            coord_char{i} = 'Y';
        case 3
            coord_char{i} = 'Z';
        otherwise
            error('Wrong coordinate number: %0.1f.', coord_num{i});
    end
end

% If coord_num was previously converted to a cell array, revert the output
% cell to a char:
if conversion_flag
    coord_char = coord_char{1};
end


end