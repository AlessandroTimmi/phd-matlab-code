function [missingMarkers, extraMarkers] =...
    CheckMissingExtraMarkers(expectedMarkers, trialMarkers)
% This function compares two cell arrays containg two markers lists (not
% necessarily of the same length). It returns the missing and extra
% markers (if present) found in the second cell array compared to the first.
%
% Input:
%   expectedMarkers = a cell array containing the markers we expect to
%                     find.
%   trialMarkers    = a cell array containing the markers found in the
%                     trial.
%
% Output:
%   missingMarkers = markers expected but not found in the trial markerset.
%   extraMarkers = extra markers not expected, but found in the trial markerset.
%
% � November 2014 Alessandro Timmi


%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'expectedMarkers',...
    @(x) validateattributes(x, {'cell'}, {'nonempty'}));
addRequired(p, 'trialMarkers',...
    @(x) validateattributes(x, {'cell'}, {'nonempty'}));

% Parse input arguments:
parse(p, expectedMarkers, trialMarkers);

% Copy input arguments into more memorable variables:
expectedMarkers = p.Results.expectedMarkers;
trialMarkers = p.Results.trialMarkers;


%% Evaluate the two cell arrays
% If the two cell arrays are different, find the indices of markers not
% present in both our marker list and in the loaded trial:
[~, ia,ib] = setxor(expectedMarkers, trialMarkers);

% Missing markers in the loaded trial:
missingMarkers = expectedMarkers(ia);
% Extra markers not expected, but found in the loaded trial:
extraMarkers = trialMarkers(ib);


%% Print information on screen:
if ~isempty(missingMarkers) 
    warning('%d markers not found:\n', length(missingMarkers))
    fprintf('%s\n', missingMarkers{:})
    fprintf('Press any key to continue...\n')
    fprintf('\n')
end
if ~isempty(extraMarkers)
    fprintf('%d extra markers found:\n', length(extraMarkers))
    fprintf('%s\n', extraMarkers{:})
    fprintf('\n')
end
