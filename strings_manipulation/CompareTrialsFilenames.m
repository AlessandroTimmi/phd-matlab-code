function CompareTrialsFilenames(filename1, filename2, regularExpression)
% Compare names of current pair of Vicon and Kinect trials using
% case insensitive regular expressions, to see if subject number,
% trial type and trial number match:
% 
% Input:
%   filename1 = full filename of the first trial.
%   filename2 = full filename of the second trial.
%   regularExpression = case insensitive regular expression used to extract
%               tokens to be compared. It must contain the following
%               named tokens: subjectNumber, trialType, trialNumber.
%                
% � October 2016 Alessandro Timmi

%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'filename1',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'filename2',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'regularExpression',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Parse input arguments:
parse(p, filename1, filename2, regularExpression);

% Copy input arguments into more memorable variables:
filename1 = p.Results.filename1;
filename2 = p.Results.filename2;
regularExpression = p.Results.regularExpression;

clear p


%% Extract tokens from filenames using case insensitive regular expressions:
tokens1 = regexpi(filename1, regularExpression, 'names');
tokens2 = regexpi(filename2, regularExpression, 'names');

% Compare tokens:
assert(isequal(tokens1.subjectNumber, tokens2.subjectNumber) &&...
    isequal(tokens1.trialType, tokens2.trialType) &&...
    isequal(tokens1.trialNumber, tokens2.trialNumber) ,...
    'Trials filenames mismatch:\n\t%s\n\t%s', filename1, filename2);

end