function coord_num = coord2num(coord_char, varargin)
% Converts coordinate chars (X, Y or Z) to the corresponding numbers (1,
% 2 or 3). This function supports both chars and cell arrays.
% Input:
%   coord_char = one of the following chars: X, Y, Z. Cell arrays of chars
%                are supported too, but must be row or column vectors.
%
% Output:
%   coords_nums = if coord_char is a char, returns the corresponding
%                 scalar. If coord_char is a cell array, returns a cell
%                 array of the same sizes with the corresponding scalars.
%
% � November 2015 Alessandro Timmi

%% Default input values:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'coord_char',...
    @(x) validateattributes(x, {'char', 'cell'}, {'vector', 'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, coord_char, varargin{:});

% Copy input arguments into more memorable variables:
coord_char = p.Results.coord_char;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
if debug_mode
    fprintf('� November 2015 Alessandro Timmi.\n')
end

% If the input is a char, convert it to a cell array and enable a flag:
conversion_flag = false;
if ~iscell(coord_char)
    coord_char = {coord_char};
    conversion_flag = true;
end

% Preallocate a cell array for output, with the same sizes of the input:
coord_num = cell(size(coord_char));

for i=1:length(coord_char)    
    switch coord_char{i}
        case 'X'
            coord_num{i} = 1;
        case 'Y'
            coord_num{i} = 2;
        case 'Z'
            coord_num{i} = 3;
        otherwise
            error('Wrong coordinate character: %s.', coord_char{i});
    end    
end

% If coord_char was previously converted to a cell array, revert the output
% cell to a scalar:
if conversion_flag
    coord_num = coord_num{1};
end


end
