function [marker_ids, all_markers_clean] = find_marker_indices(...
    my_markers, all_markers, varargin)
% Returns the indices of requested markers contained in a cell array.
% Output indices "marker_ids" are sorted accordingly to the order of the
% input variable "my_markers".
%
% The part of the marker name after the underscore is ignored. Usually we
% define the marker names using their landmark position, followed by
% underscore and the colour of the corresponding marker, e.g.
% "MalleolusRight_Red". The colour is added just for debug purposes and
% must be disregarded in normal use. Hence, we remove the part of the
% string after the underscore and output the cleaned marker names in
% the original order in the variable "all_markers_clean".
%
% Input:
%   my_markers  = the names of the markers of interest. If it is a single
%                 marker, it can be a char. For multiple markers, it must
%                 be a cell array of strings. This is not case sensitive.
%   all_markers = a cell array containing all markers names.
%   debug_mode  = (optional parameter, default = false) if true, displays
%                 some extra info for debug purposes.
%
% Output:
%   marker_ids  = an array with the indices of the selected markers.
%                 Indices are sorted accordingly to the order of
%                 "my_markers".
%   all_markers_clean = cell array with names of all markers after removing
%                 the parts after the underscore. Since we are interested
%                 only in the anatomical landmark names, this is useful to
%                 remove the colour names from the marker names.
%
% � July 2015 Alessandro Timmi


%% Default input parameters:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input argument:
addRequired(p, 'my_markers', @(x) validateattributes(x,{'char','cell'},...
    {'nonempty'}));
addRequired(p, 'all_markers', @(x) validateattributes(x,{'cell'},...
    {'nonempty'}));

% Add optional parameters:
addParameter(p, 'debug_mode', default.debug_mode, @islogical)

% Parse input arguments:
parse(p, my_markers, all_markers, varargin{:});

% Copy input arguments into more memorable variables:
my_markers = p.Results.my_markers;
all_markers = p.Results.all_markers;
debug_mode = p.Results.debug_mode;


%%
fprintf('Finding marker indices...\n')
if debug_mode
    fprintf('[Debug] � July 2015 Alessandro Timmi.\n');
end

if ~iscell(my_markers)
    my_markers = {my_markers};
end

%% Preallocate an output array to store the indices of the requested markers:
marker_ids = zeros(1, length(my_markers));

% Remove the part of the marker names after the underscore:
[all_markers_clean, all_removed_cell] = strtok(all_markers, '_');

if debug_mode
    % Print the removed strings:
    removed_cell = all_removed_cell(~cellfun(@isempty, all_removed_cell));
    if ~isempty(removed_cell)
        fprintf('[Debug] Removed strings (these should be colours and not anatomical landmark names):\n')
        fprintf('\t%s\n', removed_cell{:});
    end
end

% Find indices of requested markers within the cell array of all markers:
for i=1:length(my_markers)
    % Find the indices of all markers having the currently requested name:
    ids = find(strcmpi(all_markers_clean, my_markers{i}));
    
    % Check if there are multiple markers with same name or if an expected
    % marker name is missing:
    if length(ids)>1
        warning('%d markers found with name "%s". Taking only the first instance...',...
            length(ids), my_markers{i})
        fprintf('Duplicate markers ids:\n')
        fprintf('\t%d\n', ids);
    elseif isempty(ids)
        error('Marker with name "%s" not found.', my_markers{i})
    end
    
    % Store found marker into the output array:
    marker_ids(i) = ids(1);
    
end

