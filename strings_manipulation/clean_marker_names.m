function [marker_names, all_removed_cell] =...
    clean_marker_names(marker_names_orig, varargin)
% Clean marker names, removing the underscore and the characters after it.
% If there are two underscore, the name is truncated before the first one.
% This function works with both row and column cell arrays.
%
% Input:
%   marker_names_orig = cell array of strings with marker names before
%                       cleaning.
%   debug_mode  = (optional parameter, default = false) if true, shows some
%               extra info for debug purposes.
%
% Output:
%   marker_names = cell array of strings with marker names after removing
%                  the underscore and every character after it.
%   all_removed_cell = cell array of strings with the same size of
%                      the input cell array, containing the removed
%                      strings.
%
% � November 2015 Alessandro Timmi


%% Default input values:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'marker_names_orig',...
    @(x) validateattributes(x, {'cell'}, {'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, marker_names_orig, varargin{:});

% Copy input arguments into more memorable variables:
marker_names_orig = p.Results.marker_names_orig;
debug_mode = p.Results.debug_mode;

clear varargin default p


%% Remove the underscore and everything after it from each marker name:
[marker_names, all_removed_cell] = strtok(marker_names_orig, '_');


if debug_mode
    % Print the removed strings:
    removed_cell = all_removed_cell(~cellfun(@isempty, all_removed_cell));
    
    if ~isempty(removed_cell)
        fprintf('[Debug] Strings removed from marker names (these should be colour names):\n')
        fprintf('\t%s\n', removed_cell{:});
    end
end