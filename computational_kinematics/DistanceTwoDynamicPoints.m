function [distance, avgDistance, sdDistance] = DistanceTwoDynamicPoints(p1, p2)
% Euclidean distance over time between two moving points.
% Input:
%   p1 = array of position vectors of point 1.
%   p2 = array of position vectors of point 2.
%
% Output:
%   distance = array of Euclidean distances between points 1 and 2 over
%              time.
%   avgDistance = average of the distance over time.
%   sdDistance = standard deviation of the distance over time.
% 
% � August 2016 Alessandro Timmi


%% Array of the components of the vector connecting the two points over
% time:
p12 = p2 - p1;

% Array of Euclidean distances between points 1 and 2 over time:
distance = zeros(length(p12), 1);
for i=1:length(p12)
    distance(i) = norm(p12(i, :));
end

% Average and standard deviation of the distance over time between the two
% points:
avgDistance = mean(distance, 'omitnan');
sdDistance = std(distance, 'omitnan');
end