% This script was written to help understanding how to set a tolerance
% threshold to be applied on cosine values, in order to detect a specific
% pose in a subject whose motion is recorded via motion capture.
% In particular we are interested in finding if the tolerance is doubled
% when detecting angles close to zero (parallel vectors), to pi
% (antiparallel vectors) and to pi/2 (perpendicular vectors).
%
% �2014 Alessandro Timmi

clear all
close all
clc

% Set the tolerance threshold: it is expressed in terms of cosine:
t0 = 0.9
t90 = 0.1


% Display the angle corresponding to this threshold:
t0_angle = rad2deg(acos(t0))
t90_angle = rad2deg(acos(t90))

% Let's take an angle between our vectors, in order to do our test:
%fi =

% % Plot the cosine function
% x = 0:0.01:pi;
% figure
% subplot(2,1,1)
% plot(rad2deg(x),cos(x)), grid on
% hold on
% plot(t0_angle,t0,'bo')
% plot(t90_angle,t90,'ro')
% xlabel('\phi [deg]')
% ylabel('cos(\phi)')
% set(gca,'XTick',[0,45,90,135,180])
% title('The cosine function','fontweight','bold')

% Plot the acos function
cos_x = -1:0.01:1;
%subplot(2,1,2)
figure
plot(cos_x,rad2deg(acos(cos_x))), grid on
hold on
plot(t0,t0_angle,'bo')
plot(t90,t90_angle,'ro')
xlabel('cos(\phi)')
ylabel('\phi [deg]')
%set(gca,'XTick',[-3*pi,-2*pi,-pi,0,pi,2*pi,3*pi])
set(gca,'YTick',[0,45,90,135,180])
title('The cos^{-1} function','fontweight','bold')
