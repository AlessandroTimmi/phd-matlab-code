%% Simple vector rotation by means of quaternion



clc
clear all
close all

% Initial vector:
v=[1,1,0];

% Rotation to be applied in axis-angle notation:
fi=deg2rad(90);
u=[0,1,0];

% Definition of the quaternion according to the requested orientation:
q0=cos(fi/2);
q1=u(1)*sin(fi/2);
q2=u(2)*sin(fi/2);
q3=u(3)*sin(fi/2);

q=[q0,q1,q2,q3];

% Rotation of the vector using MatLab Aerospace Toolbox "quatrotate"
% command: IT SEEMS TO PRODUCE WRONG RESULTS, like if it works in a left
% handed system. Check this forum:
% http://www.mathworks.cn/matlabcentral/newsreader/view_thread/329249
v_rot_aerospace_toolbox=quatrotate(q,v)

% Rotation of the vector using basic quaternion operations:
p = [0,v];
% p_rot=qpq*
qp=quatmultiply(q,p);
p_rot=quatmultiply(qp,quatconj(q));
% Extract the rotated vector from the quaternion:
v_rot_quat_multiply=p_rot(2:end)

% Test inverting the order of operations:
pq_conj=quatmultiply(p,quatconj(q));
p_rot2=quatmultiply(q,pq_conj);

v_rot_quat_multiply_inv=p_rot2(2:end)