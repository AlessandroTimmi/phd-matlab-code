% Kutzbach formula to calculate the number of degrees of freedom (DOFs)
% in a mechanism.
%
% � February 2017 Alessandro Timmi

clc
clear

% Number of bodies including the fixed frame:
% fixed frame, pelvis, femur, tibia, foot:
N = 5;

% Number of kinematic pairs.
% Revolute joint: femur-tibia and tibia-foot:
nRevolute = 2;
% Ball joint: (pelvis - femur)
nBall = 1;

% Total number of kinematic pairs:
j = nRevolute + nBall;


%% Number of DOFs for each kinematic pair:
revoluteDofs = 1;
ballDofs = 3;

% Kutzbach formula of DOFs:
dofs = 6 * (N - 1 - j) + nRevolute * revoluteDofs + nBall * ballDofs;

% Print results:
fprintf('Number of bodies: N = %d.\n', N)
fprintf('Number of revolute joints: %d.\n', nRevolute)
fprintf('Number of ball joints: %d.\n', nBall)
fprintf('Number of kinematic pairs: j = %d.\n\n', j)
fprintf('Kutzbach DOFs: N = %d.\n', dofs)

