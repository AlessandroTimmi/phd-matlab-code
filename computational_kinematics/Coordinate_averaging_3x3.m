% Marker coordinates using spatial average
%
% Coloured marker tracking using Kinect is based on detecting a point from
% the 2D RGB stream (generally the centre of the contour representing the
% marker) and mapping it in the 3D stream, to obtain its spatial
% coordinates.
% 
% While we could just rely on a simple 1 to 1 pixel mapping between the
% two streams, the noise affecting the depth map could degrade the accuracy
% of the 3D marker coordinates. To reduce the effect of this noise, we
% want to average the coordinates of the 3D pixel of interest across a
% certain number of neighbour pixels.
%
% The code below is a proof of concept to see how this algorithm works
% with different kind and levels of noise in the original pixels
% coordinates and using different averaging techniques.
%
% � May 2015 Alessandro Timmi
clc
clear 
close all


% DATA
% Generic coordinate matrix (could be x, y or z) containg the centre of
% the marker (central element) and 8 pixels around it:
m = ones(3);
% Signal to noise ratio for white Gaussian noise to be added:
snr = 35;
legend_base = {'Data', 'Mean (ideal)', 'Mean (all)', 'Mean (filt)'};
% Colors
green_dark = [0 0.5 0.2];
green_fluo = [.49 1 .63];
grey_light = [0.8, 0.8, 0.8];
white = [1, 1, 1];



fprintf('## MARKER COORDINATE USING SPATIAL AVERAGE ##\n');
fprintf('\n1) IDEAL sample coordinate matrix (m):\n');
fprintf('%0.3f, %0.3f, %0.3f\n', m');
fprintf('\n');
fprintf('Marker coordinate as CENTRE of the matrix:\t %0.3f m\n', m(2,2));

% Mean of ideal data:
mean_ideal = mean2(m);
fprintf('Marker coordinate as MEAN of ideal data:\t %0.3f m\n', mean_ideal);

fprintf('\n##############################################################\n')
% Add white Gaussian noise to the original coordinate matrix:
m_white = awgn(m, snr);
fprintf('2) Coordinate matrix with WHITE GAUSSIAN NOISE (snr = %d) added (m):\n', snr);
fprintf('%0.3f, %0.3f, %0.3f\n', m_white');
fprintf('\n');
fprintf('Marker coordinate as CENTRE of the matrix:\t %0.3f m\n', m_white(2,2));

% Reshape data from matrix to row vector (no harm if it's already a vector):
m_white_v = reshape(m_white, 1, []);

% Mean before removing outliers:
mean_before = mean(m_white_v);
fprintf('Marker coordinate as MEAN before removing outliers:\t %0.3f m\n', mean_before);

% Outliers removal using modified Thompson tau:
[mean_thompson, outlier_indices, outlier_values] = remove_outliers_thompson(m_white_v);

figure('Name', 'White Gaussian noise')
plot(m_white_v, 'ko',...
    'markersize', 8,...
    'LineWidth', 2,...
    'markeredgecolor', 'k',...
    'markerfacecolor', green_fluo)
hold on
set(gca, 'XTick', 1:length(m_white_v))
h_ideal = refline(0, mean_ideal);
set(h_ideal, 'color', grey_light, 'linestyle', '-.', 'linewidth', 1.5);
h_before = refline(0, mean_before);
set(h_before, 'color', 'm', 'linestyle', '--', 'linewidth', 1.5);
h_thompson = refline(0, mean_thompson);
set(h_thompson, 'color', green_dark, 'linestyle', ':', 'linewidth', 1.5);
if ~isempty(outlier_values)
    plot(outlier_indices, outlier_values, 'ko',...
        'markersize', 8,...
        'LineWidth', 2,...
        'markeredgecolor', 'k',...
        'markerfacecolor', 'm')
    legend([legend_base, 'Outliers'],...
        'location','NorthEastOutside')
else
    legend(legend_base,...
        'location', 'NorthEastOutside')
end
title('Coordinates with white Gaussian noise added',...
    'fontweight', 'bold');
xlabel('Elements of the 3x3 coordinate matrix (px)');
ylabel('Generic coordinate [m]')


fprintf('\n##############################################################\n')
% Now add also a spike to the data:
m_spike = m_white;
m_spike(2,2) = 1.040;
fprintf('3) Coordinate matrix with WHITE GAUSSIAN NOISE and a SPIKE (m):\n');
fprintf('%0.3f, %0.3f, %0.3f\n', m_spike');
fprintf('\n');
fprintf('Marker coordinate as CENTRE of the matrix:\t %0.3f m\n', m_spike(2,2));

% Reshape data from matrix to row vector (no harm if it's already a vector):
m_spike_v = reshape(m_spike, 1, []);

% Mean before removing outliers:
mean_before = mean(m_spike_v);
fprintf('Marker coordinate as MEAN before removing outliers:\t %0.3f m\n', mean_before);

% Outliers removal using modified Thompson tau:
[mean_thompson, outlier_indices, outlier_values] = remove_outliers_thompson(m_spike_v);

figure('Name', 'White Gaussian and spike noise')
plot(m_spike_v, 'ko',...
    'markersize', 8,...
    'LineWidth', 2,...
    'markeredgecolor', 'k',...
    'markerfacecolor', green_fluo)
hold on
set(gca, 'XTick', 1:length(m_spike_v))
h_ideal = refline(0, mean_ideal);
set(h_ideal, 'color', grey_light, 'linestyle', '-.', 'linewidth', 1.5);
h_before = refline(0, mean_before);
set(h_before, 'color', 'm', 'linestyle', '--', 'linewidth', 1.5);
h_thompson = refline(0, mean_thompson);
set(h_thompson, 'color', green_dark, 'linestyle', ':', 'linewidth', 1.5);
if ~isempty(outlier_values)
    plot(outlier_indices, outlier_values, 'ko',...
        'markersize', 8,...
        'LineWidth', 2,...
        'markeredgecolor', 'k',...
        'markerfacecolor', 'm')
    legend([legend_base, 'Outliers'],...
        'location','NorthEastOutside')
else
    legend(legend_base,...
        'location', 'NorthEastOutside')
end
title('Coordinates with white Gaussian and spike noise added', 'fontweight', 'bold');
xlabel('Elements of the 3x3 coordinate matrix (px)');
ylabel('Generic coordinate [m]');