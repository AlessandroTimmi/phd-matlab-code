function VelocityTrc()
% Velocity of markers contained in a .trc file.
%
% � Novemeber 2015 Alessandro Timmi

% TODO:
% - Add plot of velocities along the 3 axes.


clear
close all
clc

trialFilename = 'C:\TEST_FOLDER\5 - Ready for statistical analysis (Bland-Altman 1986 analysis included)\Kinect\TML01\gait_fast_01.trc';

%%
fprintf('Pipeline to compute the velocity of markers from a .trc file\n')

% Load file:
if isempty(trialFilename)
    [fname, path] = uigetfile('*.trc', 'Select a .trc file', 'MultiSelect', 'off');
    trialFilename = fullfile(path, fname);
end
s = LoadTrcFile('filename', trialFilename);

% Fill gaps in marker coordinates:
maxGapSize = 10;
s = FillGapsTrc(s, maxGapSize);


%% Calculate VELOCITY:
markerNames = fieldnames(s.markers);
%markerNames = {'KneeLat', 'Metatarsus', 'Malleolus'};
%markerNames = {'RLMAL', 'LLMAL', 'RTOE2', 'LTOE2'};
for m = 1:length(markerNames)
    for c = 1:3
        vel.markers.(markerNames{m})(:,c) =...
            gradient(s.markers.(markerNames{m})(:,c), s.time);
    end
end

% Add the time array to the velocity struct, copying it from the original
% struct:
vel.time = s.time;


%% Calculate SPEED as norm of the velocity vector:
for m = 1:length(markerNames)
    for j = 1:s.NumFrames
        speed.markers.(markerNames{m})(j) = norm([...
            vel.markers.(markerNames{m})(j,1),...
            vel.markers.(markerNames{m})(j,2),...
            vel.markers.(markerNames{m})(j,3)]);
    end
end

% Add the time array to the speed struct:
speed.time = s.time;


%% Plot speed
figure('Name', 'Markers speed')
for m = 1:length(markerNames)
    subplot(length(markerNames), 1, m, 'Ygrid', 'on')
    
    plot(speed.time, speed.markers.(markerNames{m}))
    title(sprintf('%s', markerNames{m}))
    xlabel('Time [s]')
    ylabel('Speed [m/s]')
    
end

end

