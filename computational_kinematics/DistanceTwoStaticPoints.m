function distance = DistanceTwoStaticPoints(p1, p2)
% Euclidean distance between two points.
% Input:
%   p1 = position vector of point 1.
%   p2 = position vector of point 2.
%
% Output:
%   distance = Euclidean distance between points 1 and 2.
% 
% � August 2016 Alessandro Timmi


%% Vector connecting the centre of point 1 to that of point 2:
p12 = p2 - p1;

% Distance between the 2 points:
distance = norm(p12);


