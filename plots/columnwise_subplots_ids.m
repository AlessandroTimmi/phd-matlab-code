function sp_ids = columnwise_subplots_ids(n_fields_to_analyse, n_cols)
% Since we want subplots to advance column-wise instead of row-wise (which
% is the default in MATLAB), we need to create custom subplot indices.
%
%   Input
%       n_fields_to_analyse = number of fields to be analysed.
%       n_cols = number of columns per field.
% 
%   Output
%       sp_ids = array with indices of subplot, advancing column-wise.
%
% � March 2016 Alessandro Timmi


sp_ids = reshape(1 : n_cols * n_fields_to_analyse,...
    n_fields_to_analyse, [])';

end