function limits = custom_axis_margins(data, margin)
% Returns custom margins for data plot, expressed as a fraction of the
% range of the input data. This function is also useful when default
% margins calculated by Matlab are affected by some datapoints we are not
% interested in viewing.
% 
% Input:
%   data = array of data to be plotted.
%   margin = space to be left above the max value and below the min value,
%           espressed as fraction (between 0 and 1) of the data range.
%
% Output:
%   limits = plot limits [min, max], calculated using the specified margin.
%
% � June 2016 Alessandro Timmi

range = abs(max(data) - min(data));

limits = [min(data) - margin * range, max(data) + margin * range];