% Pipeline to perform a Bland-Altman analysis of agreement of the Thoracic
% Kyphosis Angle (TKA) from Kinect and Flexicurve data.
%
% � July 2016 Alessandro Timmi.

clc
clear
close all

%% Kinect TK angles:
tka_kinect = [34.08, 35.44, 31.27,...
    52.96, 52.17, 53.24,...
    47.51, 44.42, 45.17]';


%% Load Flexicurve data from .csv file:
flexicurve_data = load_flexicurve_points('Flexicurve.csv');

% Number of measurements from Flexicurve data:
measurements = size(flexicurve_data, 3);

% Preallocate output array of Flexicurve angles:
tka_flexicurve = zeros(measurements, 1);

fprintf('TKA:\n')
for i=1:measurements
    % Copy coordinates of current Flexicurve measurement in a more
    % memorable variables:
    coordinates = flexicurve_data(:,:,i);
    
    % Calculate TK angle from Flexicuve points:
    tka_flexicurve(i,1) = tk_angle_flexicurve(coordinates);
    
    %% Print results:
    fprintf('%d) Flexicurve = %0.1f�, Kinect = %0.1f�.\n',...
        i, tka_flexicurve(i,1), tka_kinect(i,1));
    
end

%% Bland-Altman analysis of agreement:
[bias_storage, lloa_storage, uloa_storage] =...
    bland_altman_1986(...
    tka_flexicurve,...
    tka_kinect,...
    'TKA',...
    'TKA',...
    '�');


