function tk_angle = tk_angle_flexicurve(coordinates)
% Calculate angle between normals at T1 and T12 using coordinates digitized
% from a Flexicurve.
%
% Input:
%   coordinates = column array of 10 points (X,Y) digitized from a Flexicurve.
%
% Output:
%   tk_angle = angle between the tangents at T1 and T12.
%
% � July 2016 Alessandro Timmi


%% Load coordinates of current spine measurement in more memorable variables:
x = coordinates(:, 1);
y = coordinates(:, 2);

% Interpolate x array of the spine curve using 100 points:
xx = linspace(min(x), max(x))';
% Include input x points and sort the resulting array:
xx = sort([xx; x]);
% Remove duplicate entries, if any:
xx = unique(xx);
% Calculate the corresponding y values using spline interpolation:
yy = spline(x, y, xx);


%% Calculate the derivative of the spline, which is the tangent (or slope):
m = gradient(yy, xx);

% X at T1 (P2) and T12 (P9):
x_T1 = x(2);
x_T12 = x(9);

% Y at T1 (P2) and T12 (P9):
y_T1 = y(2);
y_T12 = y(9);

% Find indices of T1 and T12 in the spline:
id_T1 = find(xx == x_T1);
id_T12 = find(xx == x_T12);

% Slope of the tangent at T1 and T12:
m_T1 = m(id_T1);
m_T12 = m(id_T12);

% Angles of the tangent at T1 and T12:
alpha = atand(m_T1);
beta = atand(m_T12);

% TK angle:
tk_angle = alpha - beta;


%% Plot:
title_1 = 'TKA from Flexicurve data';
figure('Name', title_1)

% Plot spline:
h_spline = plot(xx,yy, '.', 'MarkerEdgeColor', [0.6, 0.6, 0.6]);
hold on

% Plot tangents in T1 and T12:
q_T1 = y_T1 - m_T1 * x_T1;
q_T12 = y_T12 - m_T12 * x_T12;
h_tan1 = refline(m_T1, q_T1);
h_tan2 = refline(m_T12, q_T12);
set(h_tan1, 'Color', 'm')
set(h_tan2, 'Color', 'g')

% Plot input data points:
h_data = scatter(x, y, 'o', 'MarkerEdgeColor', 'b', 'MarkerFaceColor', 'b');

% Highlight T1 and T12 points:
scatter([x_T1, x_T12], [y_T1, y_T12], 'ro', 'MarkerFaceColor', 'r');
text(x_T1 + 5, y_T1 - 5, 'T1')
text(x_T12 + 5, y_T12 + 5, 'T12')

% Set plot properties:
xlim([min(xx), max(xx)])
axis equal
xlabel('X [mm]')
ylabel('Y [mm]')
title(title_1)
legend([h_data, h_spline, h_tan1, h_tan2], 'Datapoints', 'Spline', 'Tangent in T1', 'Tangent in T12')


end