function data = load_flexicurve_points(csv)
% Load 10 Flexicurve datapoints manually digitized and stored in a .csv
% file with the following structure:
%
%   Subject# P1x, P1y, P2x, P2y,... P10x, P10y.
%
% The first line is a header and the first column contains subjects' names.
% P1 represents the C7 vertebra. P2 represent T1 and P9 represents T12. The
% other points are obtained via ??????????.
% The origin O(0,0) of the coordinate system is located in P1.
% The X axis is directed as the supero-inferior axis.
% The Y axis is directed as the antero-posterior axis.
% All coordinates are in mm.
%
% Input:
%   csv = filename of a .csv file containing flexicurve data.
%
% Output:
%   data = flexicurve data points for all subjects, structured as follows:
%       data(:,:,1) = [ P1x, P1y;
%                       P2x, P2y;
%                       :   :;
%                       P10x, P10y]
%
%       data(:,:,m) = [ P1x, P1y;
%                       P2x, P2y;
%                       :   :;
%                       P10x, P10y]
% where m is the number of measurements (rows) stored in the .csv file.



%% Load content of the Flexicurve .csv file:
csv_content = dlmread(csv, ',', 1, 1);

% Number of measurements contained in the .csv file:
measurements = size(csv_content, 1);

% Preallocate output array:
data = zeros(10, 2, measurements);

for r = 1:measurements
    % X coordinates for current measurement:
    data(:,1,r) = csv_content(r, 1:2:end);
    % Y coordinates for current measurement:
    data(:,2,r) = csv_content(r, 2:2:end);
end