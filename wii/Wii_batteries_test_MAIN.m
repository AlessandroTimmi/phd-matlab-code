% Wii Balance Board battery level test.
% In order to assess the effect of different battery levels on Wii Board
% output raw signals, we performed some tests using different sets of
% batteries, both alkaline and NIMH rechargeable.
% Before each test, we measured the total voltage of the 4 batteries in
% series using a voltmeter.
% The test consisted in reading the raw values of the load cells when the
% Wii Board was unloaded and put upside-down on an even surface.
%
% � February 2015 Alessandro Timmi

clc
clear all
close all

% Wii Balance Board serial code:
wii_serial = 'a45c2785cbac';

labels = {'TL','TR','BL','BR'};

% Data format: [Battery series voltage, TL, TR, BL, BR];
alk_4full =         [6.4, 2884, 5216, 2692, 3510];
alk_3full_1dead =   [5.9, 2884, 5212, 2688, 3507];
rec_4full =         [5.5, 2884, 5212, 2688, 3508];
alk_2full_2dead =   [5.3, 2884, 5212, 2688, 3507];
rec_good =          [5.2, 2884, 5216, 2687, 3506];
alk_1full_3dead =   [4.8, 2884, 5212, 2688, 3507];
alk_4dead =         [4.4, 0, 0, 0, 0];


% Plot:
figure('Name', 'Wii Board batteries test')

scatter(1:4,        alk_4full(2:5), 210)
hold on
scatter(1:4,        alk_3full_1dead(2:5), 180)
scatter(1:4,        rec_4full(2:5), 150)
scatter(1:4,        alk_2full_2dead(2:5), 120)
scatter(1:4,        rec_good(2:5), 90)
scatter(1:4,        alk_1full_3dead(2:5), 60)
scatter(1:4,        alk_4dead(2:5), 210, 'marker', 'x')

title(['Wii Balance Board (', wii_serial,') batteries status test'], 'fontweight', 'bold')
xlabel('Load cell position')
ylabel('Load cell output')

legend( [num2str(alk_4full(1)),' V'],...
        [num2str(alk_3full_1dead(1)),' V'],...
        [num2str(rec_4full(1)),' V'],...
        [num2str(alk_2full_2dead(1)),' V'],...
        [num2str(rec_good(1)),' V'],...
        [num2str(alk_1full_3dead(1)),' V'],...
        [num2str(alk_4dead(1)),' V'],...
        'location', 'NorthWest')
xlim([-1,5])
ylim([-1000,8000])
    
set(gca, 'XTick', 1:4, 'XTickLabel', labels);
