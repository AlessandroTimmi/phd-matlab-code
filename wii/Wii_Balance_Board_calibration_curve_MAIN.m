% This program reads and synchronizes signals exported from Wii Balance
% Board and Instron loading machine, in order to obtain a calibration curve
% for each sensor of the Wii Balance Board.
%
% It is not possible to sync the two signals using cross-correlation,
% bacause the two quantities are dimensionally different. Therefore I
% synced the signals using the point of peak load. Indeed,
% the Instron machine was set to stop the test at a load of 37.5 Kg (368
% N), which is 1/4 of the maximum load allowed on the board (150 kg).
% 
% The Wii Balance Board has a sampling rate of ~100 Hz. We collected data
% using Wii�s native sampling rate (without polling at a regular frequency).
% A timestamp measured from the beginning of the acquisition is attached
% to each sample. The timestamp from Wii Balance Board is in the format:
%
% d:hh:mm:ss:fffffff
%
% where:
% d = days;
% hh = hours;
% mm = mins;
% ss = seconds;
% fffffff = fractions of a second.
%
% � January 2015 Alessandro Timmi

clc
clear
close all

%%%%%%%%%%%%%%%%%% USER'S INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Flag to subtract the preload from the Instron data:
instron.subtract_preload = true;

% Under this threshold, we consider the load to be caused only by the
% preload of the load cell: 
instron.preload_thresh_percent = 0.01;

gravity_acc = 9.81;

% Tare weight: weight of the top part of the Wii board (N). This weight is
% supported by the 4 load cells. It has been measured using the Wii board
% and our custom calibration curves:
% We read this value from our Wii program (after the calibration curves
% have been added).
% TODO remove next comment:
%Then divided it by 4 and subtracted it from the forces
% provided by the calibration curves of the sensors. Finally plot the 3
% calibration curves:
% - factory;
% - custom;
% - custom with top plate weight removed.
wii.tare_N = 29;

% Frame size for the moving average filter to be used with Wii data:
wii.filt.frame_size = 10;

% Order of the polynomial used for the calibration curve:
wii.custom_cal.poly_ord = 1;

% RGB colors:
dark_grey = [0.65,0.65,0.65];
light_grey = [0.8,0.8,0.8];


fprintf('##############################################################\n')
fprintf('CALIBRATION CURVE of a Wii Balance Board load sensor\n\n')

% Set the current path and store the old one:
oldPath = cd('X:\PhD\Wii Balance Board\Wii vs Instron - 16 Feb 2015');


% Choose the files to be used:
[instron.fname, instron.path] = uigetfile('*.csv', 'Select the .csv file for Instron machine');
instron.fullname = fullfile(instron.path, instron.fname);

[wii.fname, wii.path] = uigetfile('*.csv', 'Select the .csv file for Wii Balance Board');
wii.fullname = fullfile(wii.path, wii.fname);


%%%%%% INSTRON DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read data collected by Instron machine:
instron_csv = importdata(instron.fullname);
% Time from Instron is expressed in s:
instron.raw.time = instron_csv.data(:,1);
% Instron position in mm:
%instron.raw.position = instron_data.data(:,8);
% Load from Instron in N (originally it is stored in kN). Compression is
% negative, so we change sign to make it positive:
instron.raw.load = -1000 * instron_csv.data(:,9);
% Instron has a regular acquisition rate, so we can use two adjacent
% samples to determine it:
instron.rate = 1 / (instron.raw.time(2) - instron.raw.time(1));
fprintf('Instron acquisition rate:\n\t%.4f samples/s.\n\n',instron.rate);


%%%%%%%%%%%%%%% PRELOAD OF INSTRON DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if instron.subtract_preload == true
    % The point of first contact is detected as the first point in which
    % the load > the initial one plus a percentual threshold:
    instron.raw.first_contact_id = find(instron.raw.load > instron.raw.load(1)...
        + instron.preload_thresh_percent * instron.raw.load(1), 1, 'first');
    
    % The preload is calculated as average of the signal from start until
    % the point of first contact:
    instron.preload = mean(instron.raw.load(1:instron.raw.first_contact_id));
    % Subtract the preload from the Instron data:
    instron.zeroed.load = instron.raw.load - instron.preload;
    
    % Just copy the time array from the raw one:
    instron.zeroed.time = instron.raw.time;
    instron.preload_string = sprintf('%.2f N preload subtracted', instron.preload);

else
    instron.preload_string = sprintf('no preload subtracted'); 

end

fprintf(['Instron preload:\n\t', instron.preload_string,'.\n\n']);


%%%%%%%%% WII DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read csv file produced by Wii Balance Board:
header_lines = 9;
wii_csv = importdata(wii.fullname,',', header_lines);

% Read info from header:
temp_cell = strsplit(wii_csv.textdata{1}, ',');
wii.serial = temp_cell{2};
temp_cell = strsplit(wii_csv.textdata{2}, ',');
wii.sensor = temp_cell{2};
temp_cell = strsplit(wii_csv.textdata{8}, ',');
wii.trial_date = temp_cell{2};
clear temp_cell

fprintf('Wii Balance Board data:\n');
fprintf('\tSerial number: %s;\n', wii.serial);
fprintf('\tSensor position: %s;\n', wii.sensor);
fprintf('\tTrial date: %s.\n\n', wii.trial_date);

% Read Wii raw signal:
wii.raw.signal = wii_csv.data(:,1);

% Extract time from collected data. We are only interested in seconds and
% their fractions:
wii.raw.time = zeros(length(wii.raw.signal), 1);
for i=1:length(wii.raw.signal)
    % Read each timestamp, starting from seconds onwards (skip d, h, m):
    wii.raw.time(i) = str2double(wii_csv.textdata{i+header_lines, 1}(9:end));
end

% Wii has an irregular acquisition rate, therefore we can determine it
% from the average of the differences between adjacent timestamps:
wii.raw.rate = 1 / (mean(diff(wii.raw.time)));
fprintf('Wii average acquisition rate: %4.4f samples/s\n\n', wii.raw.rate)

% Read factory calibration curve for the load cell:
wii.factory_cal.kg = [0; 17; 34];
wii.factory_cal.N = wii.factory_cal.kg * gravity_acc;
wii.factory_cal.signal = [wii_csv.data(1,4);...
                          wii_csv.data(1,5);...
                          wii_csv.data(1,6)];


%%%%%%%% REMOVING DUPLICATES FROM WII DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sometimes Wii program might record multiple samples with same timestamp.
% These duplicates create problems in the subsequent interpolation,
% because the time array is not strictly monotonic anymore (which is a
% requirement for the interpolation function).

% Find duplicates in Wii timestamps:
wii.raw.duplicates_id = find(diff(wii.raw.time)==0); 

if ~isempty(wii.raw.duplicates_id)
    fprintf('Removing %d duplicates from Wii data...\n\n', length(wii.raw.duplicates_id));
    % Remove duplicates from Wii time array and correspondent signals entries:
    wii.raw.time(wii.raw.duplicates_id) = []; 
    wii.raw.signal(wii.raw.duplicates_id) = [];
end


%%%%%%%%%%%%% TRIMMING RAW WII DATA AFTER PEAK %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BEFORE filtering and interpolating, we need to trim Wii data after the
% peak, otherwise we end up smoothing too much the peak itself.
% According to my preliminary results, the unwanted smoothing effect can
% be around 0.5% of the peak value.
[wii.raw.max, wii.raw.max_id] = max(wii.raw.signal);
% Just copy the time and signal array, from start until the peak:
wii.raw.signal_trim = wii.raw.signal(1:wii.raw.max_id);
wii.raw.time_trim = wii.raw.time(1:wii.raw.max_id);

fprintf('Trimming %d Wii samples after peak...\n\n',...
    length(wii.raw.time)-length(wii.raw.time_trim));


%%%%%%%%%%%%%%% FILTERING OF WII DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Filtering Wii signal using a moving average, zero-phase filter...\n')
fprintf('\tFilter frame size: %d.\n\n', wii.filt.frame_size);

% Coefficients of the moving average filter:
wii.filt.coeff = 1 / wii.filt.frame_size * ones(wii.filt.frame_size, 1);
% Apply the zero-phase filter:
wii.filt.signal = filtfilt(wii.filt.coeff, 1, wii.raw.signal_trim);

% Just copy the time array:
wii.filt.time = wii.raw.time_trim;


%%%%%%%%%%%%%% INTERPOLATION OF WII DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Since Wii has an irregular acquisition rate (~100 Hz), we interpolate
% its data using Instron timestamps as input:
if wii.raw.rate > instron.rate
    fprintf('Downsampling Wii signal to Instron acquisition rate (%4.4f samples/s).\n\n', instron.rate);
elseif wii.raw.rate == instron.rate
    fprintf('Wii has same acquisition rate of Instron (%4.4f samples/s), interpolating anyway.\n\n', instron.rate);
else
    fprintf('Upsampling Wii signal to Instron acquisition rate (%4.4f samples/s).\n\n', instron.rate);
end

% Since our "synchronization point" will be the signal peak, we want the 
% corresponding instant to be present into the new time array. Thus, since
% the signal has been trimmed at the peak, we build the new time array
% from the end time instead that from the start time:
wii.interp.time = flip(wii.filt.time(end) : -1/instron.rate : wii.filt.time(1))';

% Interpolate Wii raw data at Instron's data rate, using splines. This is
% not the case because we specified the same time range of the raw data,
% but values outside the range might be extrapolated.
wii.interp.signal = interp1(wii.filt.time, wii.filt.signal, wii.interp.time,...
    'spline', 'extrap');


%%%%%%%%%%%%%%%%%% SYNCHRONIZE THE TWO SIGNALS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Syncing Wii data with Instron load signal...\n')
% Find the peak load and its position in both signals:
[instron.zeroed.max, instron.zeroed.max_id] = max(instron.zeroed.load);

% Find the times corresponding to peak loads:
instron.zeroed.t_max = instron.zeroed.time(instron.zeroed.max_id);
wii.interp.t_max = wii.interp.time(end);

% Calculate the time shift of Wii peak with respect to Instron peak:
wii.interp.shift = wii.interp.t_max - instron.zeroed.t_max;
% Shift the Wii timestamps to sync with Instron load:
wii.sync.time = wii.interp.time - wii.interp.shift;
fprintf('\tTime shift subtracted to Wii time array: %.4f s.\n', wii.interp.shift);

% Just copy the interpolated signal values:
wii.sync.signal = wii.interp.signal;

%%%%%%%%%% FURTHER TRIM DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Trim Instron data after the peak:
instron.trim.time = instron.zeroed.time(1:instron.zeroed.max_id);
instron.trim.load = instron.zeroed.load(1:instron.zeroed.max_id);

% Now that both signals are synced and trimmed after the peak, which one is
% longer?
[len, device] = max([length(instron.trim.time), length(wii.sync.time)]);

if device==1
    fprintf('\tInstron data is longer, trimming it at the beginning...\n\n');
    
    % Wii signal length in samples:
    wii.sync.length = length(wii.sync.time);
    
    % Trim Instron zeroed data: remember that Matlab is 1-based and not
    % zero-based.
    instron.trim.time = instron.zeroed.time(end-wii.sync.length+1 : end);
    instron.trim.load = instron.zeroed.load(end-wii.sync.length+1 : end);
    
    % Just copy Wii data into the trim struct:
    wii.trim.time = wii.sync.time;
    wii.trim.signal = wii.sync.signal;
        
elseif device==2
    fprintf('\tWii data is longer, trimming it at the beginning...\n\n');
    % Wii signal length in samples:
    instron.trim.length = length(instron.trim.time);
    
    % Trim Wii data: remember that Matlab is 1-based and not
    % zero-based.
    wii.trim.time = wii.sync.time(end-instron.trim.length+1 : end);
    wii.trim.signal = wii.sync.signal(end-instron.trim.length+1 : end);
        
else
    fprintf(['\tAcquisitions started at the same time,'...
        'no need to trim at the beginning.\n\n']);
end


%%%%%%%%%%%% CALIBRATION CURVE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The calibration curve is a function like:
% y = p(x)
% where:
% x = signal from the Wii Balance Board, synced with Instron and trimmed;
% y = the corresponding load values in N from the Instron machine.
% This function is obtained as a polynomial (p) fitting all the pairs of
% points Wii-Instron.

fprintf('Determination of the calibration curve using polynomial curve fitting...\n')
fprintf('\tPolynomial order: %.4f\n', wii.custom_cal.poly_ord)

% We obtain the calibration curve using the synced and trimmed signals:
% p = polyfit(x,y,ord)
[wii.custom_cal.p, wii.custom_cal.S] = polyfit(wii.trim.signal,...
    instron.trim.load, wii.custom_cal.poly_ord);

fprintf('\tCalibration curve coefficients (in descending power order):\n\t' );
fprintf('%.4f ', wii.custom_cal.p)
fprintf('\n')

fprintf('\tNorm of the residuals of the polyfit: %4.4f\n\n', wii.custom_cal.S.normr) 

% Calculation of the fit errors.
% Evaluate the polynomial using Wii signal as input:
wii.custom_cal.polyval = polyval(wii.custom_cal.p, wii.trim.signal);
wii.custom_cal.err = wii.custom_cal.polyval - instron.trim.load;


%%%%%%% TARE: REMOVAL OF THE WEIGHT OF THE TOP PART OF THE WII BOARD %%%%%%
fprintf('Tare weight of the board: %4.4f N\n', wii.tare_N);
fprintf('\tSubtracting 1/4 of the tare weight from the constant term of the calibration curve...\n');

% Copy the custom calibration curve into a new array:
wii.custom_cal_tare.p = wii.custom_cal.p;
% Subtract 1/4 of the tare weight from the constant term of the polynomial:
wii.custom_cal_tare.p(end) = wii.custom_cal_tare.p(end) - wii.tare_N / 4;

fprintf('\tCalibration curve coefficients after TARE (in descending power order):\n\t' );
fprintf('%.4f ', wii.custom_cal_tare.p)
fprintf('\n')

% Calculation of the fit errors (after tare).
% Evaluate the polynomial after tare using Wii signal as input:
wii.custom_cal_tare.polyval = polyval(wii.custom_cal_tare.p, wii.trim.signal);
wii.custom_cal_tare.err = wii.custom_cal_tare.polyval - instron.trim.load;


%%%%%% EVALUATION OF THE FACTORY CALIBRATION CURVE %%%%%%%%%%%%%%%%%%%%%%%%
% To quantify the error between the experimental data and the factory
% calibration curve, we need to evaluate the latter across the range of
% values obtained during the test:
[wii.factory_cal.p_low, wii.factory_cal.S_low] =...
    polyfit(wii.factory_cal.signal(1:2), wii.factory_cal.N(1:2), 1);
[wii.factory_cal.p_high, wii.factory_cal.S_high] =...
    polyfit(wii.factory_cal.signal(2:3), wii.factory_cal.N(2:3), 1);

% Calculation of the factory calibration erros.
% Evaluate the polynomial using Wii signal as input:
wii.factory_cal.polyval_low = polyval(wii.factory_cal.p_low, wii.trim.signal);
wii.factory_cal.polyval_high = polyval(wii.factory_cal.p_high, wii.trim.signal);
wii.factory_cal.err_low = wii.factory_cal.polyval_low - instron.trim.load;
wii.factory_cal.err_high = wii.factory_cal.polyval_high - instron.trim.load;


%%%%%%%%%%%%%%%%%%%%%%%%%% PLOTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure('Name', sprintf(['Wii ', wii.serial, ', sensor ', wii.sensor]),...
    'Units', 'Normalized',...
    'OuterPosition', [0 0 1 1])

subplot(2,2,1)
plot(instron.raw.time, instron.raw.load, 'Color', 'k', 'linestyle', '-')
hold on
plot(instron.zeroed.time, instron.zeroed.load, 'k', 'linewidth', 2)
plot(instron.raw.time(instron.raw.first_contact_id),...
    instron.raw.load(instron.raw.first_contact_id),...
    'ro', 'linewidth', 2)
h_ref_preload = refline(0,instron.preload);
set(h_ref_preload, 'Color', light_grey, 'linestyle', '--')
legend('Instron raw (N)',...
    'Instron zeroed (N)',...
    'First contact',...
    sprintf('Instron preload (%.2f N)',instron.preload),...
    'location', 'NorthWest')
title('Instron preload', 'fontweight', 'bold')
xlabel('Time (s)')
ylabel('Compression force (N)')

subplot(2,2,2)
plot(wii.raw.time, wii.raw.signal, 'color', light_grey, 'linestyle', ':')
hold on
plot(wii.raw.time_trim, wii.raw.signal_trim, 'color', light_grey)
plot(wii.filt.time, wii.filt.signal, 'k:')
plot(wii.interp.time, wii.interp.signal, 'Color', 'r', 'linewidth', 2)
title(sprintf('Interpolation and filtering of Wii data (%s, %s, %s)',...
    wii.serial, wii.sensor, wii.trial_date),...
    'fontweight', 'bold')
xlabel('Time (s)')
ylabel('Wii signal (?)')
legend('Wii raw (?)',...
        'Wii raw trim (?)',...
        'Wii filt (?)',...
        'Wii interp (?)',...
        'location', 'NorthWest')

subplot(2,2,3)
plot(instron.zeroed.time, instron.zeroed.load, 'k', 'linewidth', 1)
hold on
plot(instron.trim.time, instron.trim.load, 'Color', dark_grey, 'linestyle', '--', 'linewidth', 3)
plot(wii.sync.time, wii.sync.signal, 'Color', dark_grey, 'linestyle', '-', 'linewidth', 1)
plot(wii.trim.time, wii.trim.signal, 'r-.', 'linewidth', 3)
title('Trimming of Wii data', 'fontweight', 'bold')
xlabel('Time (s)')
legend('Instron zeroed (N)',...
    'Instron zeroed trim (N)',...
    'Wii sync (?)',...
    'Wii sync trim (?)',...
    'location', 'NorthWest')

subplot(2,2,4)
plot(wii.trim.signal, instron.trim.load, 'color', dark_grey, 'marker','o')
hold on
plot(wii.factory_cal.signal, wii.factory_cal.N, 'k', 'linewidth', 1.3,...
    'linestyle', '--', 'marker', 'o')
plot(wii.trim.signal, wii.custom_cal.polyval, 'r', 'linewidth', 2)
plot(wii.trim.signal, wii.custom_cal_tare.polyval, 'r:', 'linewidth', 1.3)
title('Wii calibration curve based on synced and trimmed data', 'fontweight', 'bold')
xlabel('Wii signal (?)')
ylabel('Compression force (N)')
legend('Wii sync trim vs Instron zeroed trim',...
    'Factory calibration curve',...    
    ['Custom calibration curve, order ', sprintf('%d',wii.custom_cal.poly_ord)],...
    ['Custom calibration curve after tare, order ', sprintf('%d',wii.custom_cal.poly_ord)],...
    'location', 'NorthWest')


figure('Name','Polyfit residuals')
plot(wii.trim.signal, wii.factory_cal.err_low, 'color', dark_grey, 'linewidth', 1)
hold on
plot(wii.trim.signal, wii.factory_cal.err_high, 'k', 'linewidth', 1)
plot(wii.trim.signal, wii.custom_cal.err, 'r', 'linewidth', 2)
plot(wii.trim.signal, wii.custom_cal_tare.err, 'r:', 'linewidth', 1)
title(sprintf('Polyfit residuals (%s, %s, %s)',...
    wii.serial, wii.sensor, wii.trial_date), 'fontweight', 'bold')
xlabel('Wii signal (?)')
ylabel('Force (N)')
legend('Factory cal low range',...
    'Factory cal high range',...
    ['Custom calibration, order ', sprintf('%d', wii.custom_cal.poly_ord)],...
    ['Custom calibration after tare, order ', sprintf('%d', wii.custom_cal.poly_ord)],...
    'location', 'best')


% Reset current path to the previous one:
cd(oldPath)
