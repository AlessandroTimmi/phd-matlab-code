function gamma_correction()
% Practicing with gamma correction
% � February 2016 Alessandro Timmi
% 
% Amplification affects the slope of the intensity histogram:
% - 0 < a < 1 decreases the slope;
% - a > 1 increases the slope.
% Gamma affects the concavity of the histogram:
% - 0 < gamma < 1: concavity is downwards;
% - gamma > 1: concavity is upwards.

clc
close all

ushort_max = 65535;


a = 2;
gamma1 = 0.99;
gamma2 = 1.01;
% q for the linear correction is expressed as a fraction of ushort_max:
q = 0;


% Input image, represented as an array of intensity values. I chose 8000
% because it seemed to be the max value in a sample (ushort) IR image from
% Kinect v2.
%p = linspace(0, ushort_max, 100)';
x = linspace(0, 8000, 100)';
% Brightest pixel in the source image:
source_max = max(x);

% Scale the image to 1-255 format:
y_s = scale_to_byte(x, source_max);


%% Test different corrections
% Apply amplification and gamma correction to original image:
y_gamma1 = apply_gamma_correction(x, a, gamma1);
% Scale corrected image:
y_gamma1_s = scale_to_byte(y_gamma1, source_max);

% Apply amplification and gamma correction to original image:
y_gamma2 = apply_gamma_correction(x, a, gamma2);
% Scale corrected image:
y_gamma2_s = scale_to_byte(y_gamma2, source_max);

% Apply linear correction:
y_lin = a * x + q * ushort_max;
% Scale corrected image:
y_lin_s = scale_to_byte(y_lin, source_max);


%% Plots
% Original scale:
figure('Name', 'Image corrections')


h_ax_orig = subplot(2,1,1);
plot(x, x, 'linewidth', 1.5)
hold on
%plot(p_gamma1)
%plot(p_gamma2)
plot(x, y_lin, 'linewidth', 1.5)

hr = refline(0, ushort_max);
set(hr, 'color', 'k', 'linestyle', '--')

% legend('Original image',...
%     sprintf('Corrected: A = %0.2f, gamma = %0.2f', a, gamma1),...
%     sprintf('Corrected: A = %0.2f, gamma = %0.2f', a, gamma2),...
%     sprintf('Corrected: A = %0.2f, q = %0.2f', a, q),...
%     'location', 'best')
legend('original',...
    sprintf('corrected: a = %0.2f, q = %0.2f * ushortMax', a, q),...
    'location', 'best')
h_ax_orig.YTick = [0, ushort_max];
h_ax_orig.YAxis.Exponent = 0;
xlabel('Original intensity (#)')
ylabel('Output (ushort)')
title('Pixles in original scale (ushort)')



% Byte scale:
h_ax_byte = subplot(2,1,2);
plot(x, y_s, 'linewidth', 1.5)
hold on
% plot(y_gamma1_s)
% plot(y_gamma2_s)
plot(x, y_lin_s, 'linewidth', 1.5)
hr = refline(0, 255);
set(hr, 'color', 'k', 'linestyle', '--')

% legend('Original image',...
%     sprintf('Corrected: A = %0.2f, gamma = %0.2f', a, gamma1),...
%     sprintf('Corrected: A = %0.2f, gamma = %0.2f', a, gamma2),...
%         sprintf('Corrected: A = %0.2f, q = %0.2f', a, q),...
%     'location', 'best')
legend('original',...
    sprintf('corrected: a = %0.2f, q = %0.2f * ushortMax', a, q),...
    'location', 'best')
h_ax_byte.YTick = [0, 255];
h_ax_byte.YAxis.Exponent = 0;
xlabel('Original intensity (#)')
ylabel('Output (byte)')
title('Pixels in byte scale (0,255)')


end

function p_gamma = apply_gamma_correction(p, a, gamma)
% Apply gamma and amplification (a) correction to all pixels:
p_gamma = a * p.^gamma;
end

function ps = scale_to_byte(p, sourceMax)
range_byte = 255;
% Scale an array of intensity values to the range of a byte (0-255,
% integer). Since we want to preserve the amplification of the values, we
% normalize with respect to the source max value, instead of normalizing
% with respect to the current max value after correction. For the same
% reason, all pixels amplified over the byte threshold will be set to 255
% using the min() operation.
ps = max(0, min(255, round(p * range_byte / sourceMax)));
end