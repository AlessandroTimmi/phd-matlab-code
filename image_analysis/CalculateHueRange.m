function CalculateHueRange()
% Calculate the hue range which will be used to filter an HSV image.
%
% Input:
%   h = the measured hue of a pixel, corresponding to the centre of the
%       hue range.
%   tol = half the width of the desired hue range.
% Output:
%   hueRanges = the range(s) to be used as hue filter. In case of red hue,
%   the hue range crosses the limits of the domain. In this
%   case, the hue range must be represented using two intervals.
%
% NOTE: starting from EMGU 2.4.0, the bug in the method CvInvoke.InRangeS()
% was fixed, i.e. both the lower and upper boundaries passed as input
% arguments to this method are now inclusive.
%
% � May 2015 Alessandro Timmi

close all
clear
clc

% Input values:
h = 178;
tol = 5;


%% Constants
% Hue domain limits (exclusive as in EMGU CV):
HUE_MIN = 0.0;
HUE_MAX = 180.0;
HUE_DOMAIN = (HUE_MIN : 0.1 : HUE_MAX)'; % column vector

% Colours for plot:
GREEN_LIGHT = [0.85, 1, 0.85];


%% Hue range functions
[hLower, hUpper] = HueFunctions(HUE_DOMAIN, tol, HUE_MAX);

% Plot the hue range functions:
titleString = sprintf('Hue range(s) for hue = %0.1f and tolerance = %0.1f degrees', h, tol);
figure('Name', titleString)

axes1 = subplot(2,1,1);
area(axes1, [HUE_MIN, HUE_MAX], [HUE_MAX, HUE_MAX],...
    'facecolor', GREEN_LIGHT, 'edgecolor', GREEN_LIGHT );
hold on
plot(axes1, HUE_DOMAIN, hLower(:,1), 'b')
plot(axes1, HUE_DOMAIN, hLower(:,2), 'b--')
plot(axes1, HUE_DOMAIN, hUpper(:,1), 'r')
plot(axes1, HUE_DOMAIN, hUpper(:,2), 'r--')
xlabel('Measured hue (h) [\circ]')
ylabel('Hue range(s) [\circ]')
set(axes1, 'YTick', 0:60:180);
set(axes1, 'XTick', 0:20:180);
title(titleString, 'fontweight', 'bold')


%% Hue range
fprintf('Calculate the hue range(s), given a measured hue and a tolerance\n\n');
fprintf('Measured hue = %0.1f degrees\n', h);
fprintf('Tolerance = %0.1f degrees\n\n', tol);

% Check that the input hue is inside the domain [0, 180]:
assert(h >= HUE_MIN && h <= HUE_MAX,...
    'Measured hue (%0.1f degrees) is OUTSIDE the hue domain: [%d, %d] degrees.',...
    h, HUE_MIN, HUE_MAX)

% Among the 4 possible value for the hue range(s), reject those outside the
% hue domain:
fprintf('Possible values for the hue range(s):\n');
[lowerHuePair, upperHuePair] = HueFunctions(h, tol, HUE_MAX);
fprintf('lowerHuePair = (%0.1f, %0.1f) degrees\n', lowerHuePair');
fprintf('upperHuePair = (%0.1f, %0.1f) degrees\n\n', upperHuePair');

fprintf('Discard values outside the hue domain.\n');
lowerHue = lowerHuePair(lowerHuePair >= HUE_MIN & lowerHuePair <= HUE_MAX);
upperHue = upperHuePair(upperHuePair >= HUE_MIN & upperHuePair <= HUE_MAX);


% There are ambiguities at h = tol and h = HUE_MAX - tol, which are
% resolved here:
if length(lowerHue) > 1    
    fprintf('Removing ambiguitiy in lower hue: (%0.1f, %0.1f) degrees: selecting min value.\n', lowerHuePair');
    lowerHue = min(lowerHue);
end

if length(upperHue) > 1
    fprintf('Removing ambiguitiy in upper hue: (%0.1f, %0.1f) degrees: selecting max value.\n', upperHuePair');
    upperHue = max(upperHue);
end
fprintf('lowerHue = %0.1f degrees\n', lowerHue);
fprintf('upperHue = %0.1f degrees\n\n', upperHue);


%% Determine the hue range(s), depending on the relative magnitude of the
% two boundaries just found:
axes2 = subplot(2,1,2);

if lowerHue < upperHue
    hueRanges = [lowerHue, upperHue];
    fprintf('Calculated hue range: %0.1f <= h <= %0.1f degrees.\n\n', hueRanges');
    
    width = hueRanges(1,2) - hueRanges(1,1);
    
    % Plot the range:
    area(axes2, hueRanges, [1,1])
    hold on
    
    % Add the range to the hue range functions plot:
    plot(axes1, [h,h], hueRanges, 'm', 'linewidth', 5)
    
else
    fprintf('NOTE: because lowerHue > upperHue, the measured hue is reddish (or you set a huge tolerance).\n');
    fprintf('The hue range will be split in correspondence of the 0/180 degrees limit. All good!\n\n');
    hueRanges = [HUE_MIN, upperHue;...
        lowerHue, HUE_MAX];
    fprintf('Calculated hue ranges: %0.1f <= h <= %0.1f and %0.1f <= h <= %0.1f degrees.\n\n', hueRanges');
    
    width = hueRanges(1,2) - hueRanges(1,1) + hueRanges(2,2) - hueRanges(2,1);
    
    % Plot the ranges:
    area(axes2, hueRanges(1,:), [1,1])
    hold on
    area(axes2, hueRanges(2,:), [1,1])
    
    % Add the ranges hue range functions plot:
    plot(axes1, [h,h], hueRanges(1,:), 'm', 'linewidth', 5)
    plot(axes1, [h,h], hueRanges(2,:), 'm', 'linewidth', 5)
end

fprintf('Total range width = %0.1f degrees.\n', width);

plot(axes2, [h,h], [0,1], 'r', 'linewidth',3);
xlim(axes2, [HUE_MIN, HUE_MAX]);
ylim(axes2, [0, 1]);
set(axes2, 'XTick', 0:10:180);
set(axes2, 'YTick', []);
xlabel(axes2, 'Hue [\circ]')

legend(axes1, 'Hue domain',...
    'h - tol', 'h - tol + HUE MAX',...
    'h + tol', 'h + tol - HUE MAX',...
    'Hue range(s)',...
    'location','northeastoutside')

end



function [hLower, hUpper] = HueFunctions(h, tol, HUE_MAX)
% Hue range functions, based on the measured hue and its tolerance.
%
% Input:
%   h = input hue.
%   tol = half the width of the hue range.
%   HUE_MAX = upper limit of the hue domain.
%
% Output:
%   hLower = output of the two functions representing the lower boundaries
%            of the hue range.
%   hUpper = output of the two functions representing the upper boundaries
%            of the hue range.
%
% � May 2015 Alessandro Timmi


hLower = [h - tol, h - tol + HUE_MAX];
hUpper = [h + tol, h + tol - HUE_MAX];
end