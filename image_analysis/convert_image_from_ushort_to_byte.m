% Conversion of IR bounding box (BB) from ushort to byte format.
% The aim of this conversion is to transform the ushort greyscale image
% into a format (byte) which is usable for OpenCV image processing.
% This conversion could be performed directly, matching the ushort range
% (0, 65535) to the byte range (0, 255). However, in this way we would
% waste part of the (alredy small) byte range to describe intensity values
% which are not of interest for our analysis, which is aimed at detecting
% circles using Hough transform).
% For this reason, given the current BB in the IR image, we retrieve the
% darkest and brightest pixel, identified by the lowest and highest
% intensity value. Then we use this as our origin intensity range, to be
% converted in byte format.
% While this approach is correct and works in normal data capture, it
% generates some issues when we use our custom marker supports (rigs).
% Indeed, in this case, the coloured marker has 2 reflective markers close
% to it. These reflective markers, when visible into the IR BB, generate
% estremely high intensity values. Similarly, the markers are supported by
% a 3D-printed black support: its very low reflectivity generates very low
% intensity values. These values affect our image conversion algorithm,
% because they artificially increase the extension of our original image
% range.
% To resolve this issue, we applied percentiles (10th and 90th) to
% calculated the lower and upper threshold to compress the image.
%
% � February 2016 Alessandro Timmi

clear
clc
close all

ushort_max = 65535;
byte_max = 255;
% This threshold limits the upper bound of the allowed intensity values for
% the original (ushort) image. Intensities above this value will be
% considered due to reflective markers and will be ignored in the
% calculation of the image range.
upper_intensity_threshold = 6000;

fprintf('Converting image from ushort to byte format...\n')
fprintf('� February 2016 Alessandro Timmi\n\n')


%% Read CSV files containing intensity values:
folder = 'D:\Repos\kinedge\gui\Assemblies\DebugImages\';
files = dir([folder, '*.csv']);

for i=1:length(files)
    csv(i).data_ushort = importdata([folder, files(i).name])';
end

% Concatenate data from all .csv files into a single array:
all_data_ushort = vertcat(csv(:).data_ushort);


%% Convert image from ushort to byte:
lowerIntensityThreshold = zeros(length(files), 1);
upperIntensityThreshold = zeros(length(files), 1);
for i=1:length(files)
    % Determine the lower threshold as the 10th percentile of the intensity
    % values:
    lowerIntensityThreshold(i, 1) = prctile(csv(i).data_ushort, 10);    
    % Determine the upper threshold as the 90th percentile of the intensity
    % values:
    upperIntensityThreshold(i, 1) = prctile(csv(i).data_ushort, 90);
    
    % Convert from ushort to byte using the custom intensity range:    
    csv(i).data_byte = max(0, min(255,...
        (csv(i).data_ushort - lowerIntensityThreshold(i,1)) * 255 /...
        (upperIntensityThreshold(i,1) - lowerIntensityThreshold(i,1))));
    
    % To plot the thresholds over the data we need to generate arrays
    % of the same length of the data themselves:
    csv(i).upper_threshold = ones(size(csv(i).data_ushort)) * upperIntensityThreshold(i,1); 
    csv(i).lower_threshold = ones(size(csv(i).data_ushort)) * lowerIntensityThreshold(i,1); 
end

% Concatenate converted data into a single array:
all_data_byte = vertcat(csv(:).data_byte);


%% Plot original image intensity values (ushort range):
figure('Name', 'Converting image from ushort to byte format')

h_ax_ushort = subplot(2,2,1);
plot(all_data_ushort, 'linestyle', 'none', 'marker', '.', 'markersize', 8)
hold on
plot(vertcat(csv(:).lower_threshold), 'linewidth', 1.5, 'linestyle', '-.', 'color', 'r')
plot(vertcat(csv(:).upper_threshold), 'linewidth', 1.5, 'linestyle', '-.', 'color', 'r')
h_ax_ushort.YTick = [0, upper_intensity_threshold, ushort_max];
h_ax_ushort.YAxis.Exponent = 0;
title('Intensity values (ushort range)')
xlabel('Pixels [#]')
ylabel('Output image [ushort]')


% Histogram of intensities before conversion (ushort range):
title_hist_ushort = 'Histogram of intensity values (ushort range)';
h_ax_hist = subplot(2,2,2);
histogram(all_data_ushort, 100);
title(title_hist_ushort)
ylabel('Occurrencies [#]')
xlabel('Intensities [ushort range]')
h_ax_hist.XTick = [[0, 1, 2, 3, 4, 5, 6] * 10^4, ushort_max];
h_ax_hist.XAxis.Exponent = 0;
h_ax_hist.XAxis.TickLabelRotation = 25;


% Plot image intensity values after conversion to byte depth:
h_ax_byte = subplot(2,2,3);
plot(all_data_byte, 'linestyle', 'none', 'marker', '.', 'markersize', 8)
h_ax_byte.YTick = [0, byte_max];
h_ax_byte.YAxis.Exponent = 0;
xlabel('Pixels [#]')
ylabel('Output image [byte]')
title('Intensity values after conversion to byte range')
h2 = refline(0, byte_max);
set(h2, 'linewidth', 1.5, 'linestyle', '-.', 'color', 'r')
h_zero_byte = refline(0, 0);
set(h_zero_byte, 'linewidth', 1.5, 'linestyle', '-.', 'color', 'r')
ylim([-25, 280])


% Histogram of intensities after conversion to byte depth:
title_hist_byte = 'Histogram of intensity values after conversion to byte range';
h_ax_hist = subplot(2,2,4);
histogram(all_data_byte, 100);
title(title_hist_byte)
ylabel('Occurrencies [#]')
xlabel('Intensities [byte range]')
h_ax_hist.XTick = [0, 50, 100, 150, byte_max];
h_ax_hist.XAxis.Exponent = 0;
h_ax_hist.XAxis.TickLabelRotation = 25;


