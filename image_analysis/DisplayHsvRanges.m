% This script plots the HSV ranges of the coloured markers. The aim is to
% visualize which colours may interfere with each other.
%
% � October 2015 Alessandro Timmi

clear
clc
close all

fprintf('Representation of HSV ranges used for coloured markers detection\n')
fprintf('� October 2015 Alessandro Timmi\n\n')

%% Read XML file containing attributs of coloured markers:
xmlFilename = 'C:\TEST_FOLDER\CustomMarkers_combination_v1.6_and_sonja.xml';
%xmlFilename = 'D:\Repos\kinedge\gui\Assemblies\MarkerSetup\CustomMarkers_single_leg_v1.6.xml';


s = ReadCustomMarkersXmlFile(xmlFilename);

fprintf('Total number of markers: %d\n', length(s));
fprintf('Marker tags:\n');
fprintf('\t%s\n', s(:).tag);
fprintf('\n')


%% Plot marker HSV ranges defined in the XML file:
myTitle = 'Marker HSV ranges';
figure('Name', myTitle, 'Position', [230 100 1000 750]);
hold on;

edgeAlpha = 0.7;
edgeWidth = 1.2;

textYPosition = 0.25;

axHue = subplot(3,1,1);
axSat = subplot(3,1,2);
axVal = subplot(3,1,3);

xlabel(axHue, 'Hue (degrees)')
xlabel(axSat, 'Saturation')
xlabel(axVal, 'Value')

xlim(axHue, [0, 180])
xlim(axSat, [0, 256])
xlim(axVal, [0, 256])

axSat.XTick(end) = 256;
axVal.XTick(end) = 256;

axHue.YTick = [];
axSat.YTick = [];
axVal.YTick = [];

title(axHue, myTitle)

for i = 1:length(s)
    
    % Convert current marker colour from HSV to RGB, for plotting. For
    % representation purposes, the mean values between the upper and lower
    % HSV limits are used. This approach is consistent with our colour
    % tracking algorithm, because the HSV ranges are always centred around
    % the detected values.
    s(i).rgb = hsv2rgb(...
        mean(s(i).hmin, s(i).hmax) / 180,...
        mean(s(i).smin, s(i).smax) / 256,...
        mean(s(i).vmin, s(i).vmax) / 256);
    
    if strfind(s(i).tag, '_')
        % If this colour was used in the SLS markerset, fill the rectangle
        % with the colour:
        hueAlpha = 0.7;
        
        % Remove the anatomical landmark from the marker name:
        cellTag = strsplit(char(s(i).tag), '_');
        s(i).tag = cellTag{2};
        
    else
        % Otherwise plot only the coloured edge of the rectangle:
        hueAlpha = 0;
    end
    
    
    %% Draw the HUE rectangle:
    % Dimensions of the rectangular patch. NOTICE: we use patch() instead
    % of rectangle, because the latter doesn't have any property to set
    % its transparency.
    subplot(axHue)
    y = [0, 0, 1, 1];
    
    if s(i).hmin > s(i).hmax
        % The red hue generally crosses the boundary (0/180) and the
        % corresponding hue limits are inverted: (hmin > hmax). Therefore,
        % this range must be split in two sub-ranges: [0 - hmax] and
        % [hmin - 180]:
        x1 = [0, s(i).hmax, s(i).hmax, 0];
        x2 = [s(i).hmin, 180, 180, s(i).hmin];
        
        patch(x1, y, s(i).rgb, 'facealpha', hueAlpha, 'EdgeColor', s(i).rgb, 'EdgeAlpha', edgeAlpha, 'LineWidth', edgeWidth);
        patch(x2, y, s(i).rgb, 'facealpha', hueAlpha, 'EdgeColor', s(i).rgb, 'EdgeAlpha', edgeAlpha, 'LineWidth', edgeWidth);
        
        % Write the name of the marker into the rectangle:
        text(mean(x1), textYPosition, char(s(i).tag), 'Rotation', 90, 'interpreter', 'none');
        text(mean(x2), textYPosition, char(s(i).tag), 'Rotation', 90, 'interpreter', 'none');
        
    else
        x = [s(i).hmin, s(i).hmax, s(i).hmax, s(i).hmin];
        patch(x, y, s(i).rgb, 'facealpha', hueAlpha, 'EdgeColor', s(i).rgb, 'EdgeAlpha', edgeAlpha, 'LineWidth', edgeWidth);
        
        % Write the name of the marker into the rectangle:
        text(mean(x), textYPosition, char(s(i).tag), 'Rotation', 90, 'interpreter', 'none');
    end
    
    
    %% Because saturation and value should be as high as possible for all
    % marker colours, their ranges will largely overlap. For clarity, the
    % corresponding rectangles are plotted without colour (i.e., transparent).
    % Draw the SATURATION rectangle:
    x = [s(i).smin, s(i).smax, s(i).smax, s(i).smin];
    y = [0, 0, 1, 1];
    
    subplot(axSat)
    patch(x, y, s(i).rgb, 'facealpha', 0, 'EdgeColor', s(i).rgb, 'EdgeAlpha', edgeAlpha, 'LineWidth', edgeWidth);
    
    % Write the name of the marker into the rectangle:
    text(mean(x), textYPosition, char(s(i).tag), 'Rotation', 90, 'interpreter', 'none');
    
    
    %% Draw the VALUE rectangle:
    x = [s(i).vmin, s(i).vmax, s(i).vmax, s(i).vmin];
    y = [0, 0, 1, 1];
    
    subplot(axVal)
    %patch(x, y, s(i).rgb, 'facealpha', myAlpha);
    patch(x, y, s(i).rgb, 'facealpha', 0, 'EdgeColor', s(i).rgb, 'EdgeAlpha', edgeAlpha, 'LineWidth', edgeWidth);
    
    % Write the name of the marker into the rectangle:
    text(mean(x), textYPosition, char(s(i).tag), 'Rotation', 90, 'interpreter', 'none');
end

% Because Kinect v2 RGB camera perceived violet as blue, an emtpy area will
% be plotted in the corresponding hue range
subplot(axHue)
myViolet = [170, 0, 255]/255;
x = [130, 150, 150, 130];
patch(x, y, s(i).rgb, 'facealpha', hueAlpha, 'EdgeColor', myViolet, 'EdgeAlpha', edgeAlpha, 'LineWidth', edgeWidth, 'LineStyle', '--');

% Write the name of the marker into the rectangle:
text(mean(x), textYPosition, 'Violet hues', 'Rotation', 90, 'interpreter', 'none');


