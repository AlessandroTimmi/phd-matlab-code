clear
clc
close all

colors = {'r.','g.','m.','b.','y.','c.'};

img.orig = imread('C:\Users\Alex\Desktop\test-perim.bmp');



figure

subplot(2,2,1)
imshow(img.orig)
title('Original grayscale')

subplot(2,2,2)
% Convert the image from grayscale to BW:
otsu_threshold = graythresh(img.orig);
img.bw = im2bw(img.orig, otsu_threshold);
imshow(img.bw)

% Find and plot boundaries:
[B,L,N,A] = bwboundaries(img.bw);
hold on
if ~isempty(B)    
    for i=1:length(B)
        boundary = B{i,1};
        plot(boundary(:,2), boundary(:,1), colors{i})
    end
end
title('BW and boundaries')


%% Invert BW image:
subplot(2,2,3)
img.inverse = imcomplement(img.bw);
imshow(img.inverse);
title('Inverse')

subplot(2,2,4)
imshow(img.inverse);
hold on
[B2,L2,N2,A2] = bwboundaries(img.inverse);

if ~isempty(B2)    
    for i=1:(length(B2))
        boundary = B2{i,1};
        plot(boundary(:,2), boundary(:,1), colors{i})
    end
end
title('Inverse and boundaries')



%% Overlay boundaries from both images (BW and inverse BW)
figure
subplot(2,2,1)
imshow(img.bw)
hold on
boundary1 = B{1};
plot(boundary1(:,2), boundary1(:,1), 'r.')
boundary2 = B{2};
plot(boundary2(:,2), boundary2(:,1), 'g.')
boundary3 = B2{3};
plot(boundary2(:,2), boundary2(:,1), 'm.')


subplot(2,2,2)
se = strel('line',100,100);
img.dilate = imdilate(img.bw,se);
imshow(img.dilate);
title('Dilated')