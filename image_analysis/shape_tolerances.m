% Shape tolerance equations for marker detection from 2D RGB data.
% � May 2015 Alessandro Timmi

% The shape is tested in different ways:
% 1) number of vertices must be > 6;
% 2) abs(1 - width/height) <= shape tolerance;
% 3) abs(1 - Area/pi * r^2) <= shape tolerance.

% In this program, only equations 2 and 3 will be taken into account,
% because equation 1 is managed by the visual library and there is nothing
% to set, except for the number of vertices.

% Notice: since we are dealing with images, all values are in pixels.
close all
clear all
clc


% Ratio width/height of the bounding box:
w_h = 0:0.01:2;
% Equation 2:
tol2 = abs(1 - w_h);

figure('Name', 'Tolerance equation 2')
plot(w_h, tol2, 'linewidth',2)
line([1 1], [min(tol2) tol2(end)], 'linewidth',2, 'color','r', 'linestyle','--')
xlabel('width / height')
ylabel('Flattening tolerance')
title('Tolerance equation 2: flattening', 'fontweight', 'bold')


% Area ratio: area of the detected shape divided by the area of a circle
% with radius equal to half the width of the bounding box:
A_Acircle = 0:0.01:4;
% Equation 3:
tol3 = abs(1 - A_Acircle);

figure('Name', 'Tolerance equation 3')
plot(A_Acircle, tol3, 'linewidth',2)
line([1 1], [min(tol3) tol3(end)], 'linewidth',2, 'color','r', 'linestyle','--')
xlabel('Area / Area of a circle with r = w/2')
ylabel('Area tolerance')
title('Tolerance equation 3: area', 'fontweight', 'bold')


% Simplified area tolerance: the shape is always an ellipse.
% In this case we simplify the problem, assuming that
% the detected marker shape is always an ellipse, with dimensions
% spanning the range w/h defined above for the flattening tolerance. 
% Doing simple substitutions using the ellipse and circle areas, we get
% the equation below.

% Ratio height/width of the bounding box:
h_w = 0:0.01:4;
% Simplified equation 3, under the assumption of marker shape = ellipse:
tol3_ell = abs(1 - h_w);
% Plot:
figure('Name', 'Tolerance equation 3'' ellipse')
plot(h_w, tol3_ell, 'linewidth',2)
line([1 1], [min(tol3_ell) tol3_ell(end)], 'linewidth',2, 'color','r', 'linestyle','--')
xlabel('height / width')
ylabel('Area tolerance')
title('Tolerance equation 3'': area (assumption of ellipse)', 'fontweight', 'bold')


