clear
close all
clc

% Aspect ratio for the surf axes:
surf_daspect = [30 30 1];
noise_level = 0.1;

%% Load image:
% [img.fname, img.pname] = uigetfile({'*.bmp'; '*.png'; '*.jpg'}, 'Select an image');
% if ~img.fname
%     fprintf('No file selected.\n')
%     return
% end


% img.filename = fullfile(img.pname, img.fname);

img.filename = 'D:\Repos\kinedge\gui\Assemblies\DebugImages\10581179439_12_magentaIntensityDepth.bmp';

img.orig = imread(img.filename);
fprintf('Loaded image:\n\t%s\n\n', img.filename);
fprintf('Image size: %d x %d px.\n\n', size(img.orig));

% Display the image:
figure('Name', 'Original depth image', 'NumberTitle', 'off');
imshow(img.orig);
title('Original depth image')
hold on


%% Draw a circle surrounding the coloured marker in the IR stream. This
% simulates the circle detected by Hough transform:
fprintf('Waiting for user to draw a circle around the marker...\n');
circle.object = imellipse;
% Create a mask with 1s inside the circle and 0s everywhere else:
circle.mask = createMask(circle.object);

% Determine some properties of the circle:
circle.stats = regionprops(circle.mask, 'BoundingBox');
% Get bounding box of the circle:
bbox.rect = circle.stats.BoundingBox;
% Store bounding box sizes in more memorable variables:
bbox.x = bbox.rect(1);
bbox.y = bbox.rect(2);
bbox.w = bbox.rect(3);
bbox.h = bbox.rect(4);
fprintf('Bounding box dimensions:\n');
fprintf('\tw = %d\n\th = %d.\n', bbox.w, bbox.h);
% Draw bounding box on the image:
rectangle('Position', bbox.rect, 'EdgeColor', 'm');


%% Crop and translate coordinate system from image to bounding box
% Crop image using bounding box:
img.crop = imcrop(img.orig, bbox.rect);
% Crop mask using bounding box:
circle.mask_crop = imcrop(circle.mask, bbox.rect);


%% Mask and display the original cropped image, as a reference:

% Convert the image to double, to add support for NaNs and avoid multiple
% conversions later:
img.crop = im2double(img.crop);

figure('Name', 'Masked image')
subplot(2,1,1)
% Copy the original cropped image:
img.crop_mask = img.crop;
% Apply mask (circle):
img.crop_mask(~circle.mask_crop) = 0;
% Convert all zeroes to NaNs:
img.crop_mask(~img.crop_mask) = NaN;
imshow(img.crop_mask)
title('Masked image: all zeroes converted to NaNs')

subplot(2,1,2)
surf(img.crop_mask)
xlabel('x')
ylabel('y')
zlabel('z')
title('Surface of the masked image')


%% Process the cropped image, showing the intermediate steps:
figure('Name', 'Image processing steps and corresponding surfaces')

subplot(2,3,1)
imshow(img.crop)
title('Cropped image')

% Add noise to simulate the effect of speed on the depth map.
% NOTE: since the imnoise() function converts NaNs to 1s, we need to apply
% the noise now, before converting all zeroes to NaNs.
img.noise = imnoise(img.crop, 'speckle', noise_level);
subplot(2,3,2)
imshow(img.noise)
title('Added multiplicative noise')

% Apply mask (the selected circle):
img.noise_mask = img.noise;
img.noise_mask(~circle.mask_crop) = 0;
% Convert all zeroes to NaNs:
img.noise_mask(~img.noise_mask) = NaN;
subplot(2,3,3)
imshow(img.noise_mask);
title('Masked noisy image: all zeroes converted to NaNs')


subplot(2,3,4)
surf(img.crop);
xlabel('x')
ylabel('y')
zlabel('z')
title('Surface of the cropped image')

subplot(2,3,5)
surf(img.noise);
xlabel('x')
ylabel('y')
zlabel('z')
title('Surface of the noisy image')

subplot(2,3,6)
surf(img.noise_mask);
xlabel('x')
ylabel('y')
zlabel('z')
title('Surface of the masked noisy image')


%% Estimate circle position ON the depth map, using only valid depth
% pixels contained in the Hough circle.

% Circle position on the image plane. MATLAB is one-based, hence we need
% to round towards positive infinity:
circle.X = ceil(bbox.w / 2);
circle.Y = ceil(bbox.h / 2);

% Reference Z, calculated as the depth of the centre of the Hough circle
% in the noisy image. This is the depth we would get if we don't apply any
% sort of spatial filtering on the depth map:
circle.Z_default = img.noise(circle.Y, circle.X);

% "Flatten" the masked image from 2D to 1D array, ignoring NaNs:
img.flat = img.noise_mask(~isnan(img.noise_mask));

variance = var(img.flat);
sd = std(img.flat);
fprintf('Variance of the masked noisy image: %0.3f\n', variance)
fprintf('Standard deviation of the masked noisy image: %0.3f\n', sd)

% Estimate circle Z coordinate using median of valid depth pixels:
circle.Z_median = median(img.flat);

% Alternative Z of the circle position, calculated using mean of depth
% pixels:
circle.Z_mean = mean(img.flat);


%% Visualize the depth map before and after noise, and show the estimated 
% circle centre:
title_circle_centre = 'Estimated depth of the circle centre';
figure('Name', title_circle_centre)
mesh(img.crop_mask, 'FaceAlpha', 0, 'EdgeAlpha', 1, 'EdgeColor', 'k');
hold on
surf(img.noise_mask, 'FaceAlpha', 0.25, 'EdgeAlpha', 0.0);
scatter3(circle.X, circle.Y, circle.Z_default, 100, 'ro', 'MarkerFaceColor', 'r')
scatter3(circle.X, circle.Y, circle.Z_median, 100, 'go', 'MarkerFaceColor', 'g')
scatter3(circle.X, circle.Y, circle.Z_mean, 100, 'bo', 'MarkerFaceColor', 'b')
xlabel('x')
ylabel('y')
zlabel('z')
title(title_circle_centre)
legend('Orig depth map', 'Noisy depth map', 'Z default', 'Z median', 'Z mean')




