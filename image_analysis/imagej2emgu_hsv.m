function [hsv_emgu] = imagej2emgu_hsv(hsv_imagej)
% Convert HSV parameter from ImageJ to EMGU CV convention.
% � June 2015 Alessandro Timmi


hsv_emgu = [hsv_imagej(1) * 180/254, hsv_imagej(2), hsv_imagej(3) * 255/254];


