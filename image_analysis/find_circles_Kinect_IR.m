% function find_circles_Kinect_IR(varargin)
% % Function to find imperfect circles in Kinect IR images.
% % The circular Hough transform (CHT) is applied to a sub-image, obtained
% % as bounding box of the the coloured blob of the marker. The algorithm
% % is optimized to give higher scores to circles located close to the edges
% % of the bounding box, simulating the effect of motion blur on coloured
% % blobs.
% %
% % Input:
% %   debug_mode = (optional parameter, default = false) if true, shows
% %                 some extra info for debug purposes.
% %
% %
% %
% % � November 2015 Alessandro Timmi
%
%
% %% Default input values:
% default.debug_mode = false;
%
% % Setup input parser:
% p = inputParser;
% p.CaseSensitive = false;
%
% % Add optional input arguments in the form of name-value pairs:
% addParameter(p, 'debug_mode', default.debug_mode, @islogical);
%
% % Parse input arguments:
% parse(p, varargin{:});
%
% % Copy input arguments into more memorable variables:
% debug_mode = p.Results.debug_mode;


%% TODO:
% - Dilate blob to ensure the marker is within its contour.
% - add another sub-score: when the circle has the centre in the blob but
%   the area partially outside, 

clc
clear
close all
debug_mode = true;


%%
fprintf('Find imperfect circles in Kinect IR images\n')
fprintf('� November 2015 Alessandro Timmi\n\n')

% Hough transform sensitivity: the higher, the more circles will be found,
% but the more false positive (default is 0.85).
hough_sensitivity = 0.90;
fprintf('Hough sensitivity (0 = less circles, 1 = more false circles, 0.85 = default): %0.2f.\n\n', hough_sensitivity);

% For this session, suppress the warnings about the radius length and range.
% They will be re-enabled when restarting Matlab.
warning('off', 'images:imfindcircles:warnForSmallRadius');
warning('off', 'images:imfindcircles:warnForLargeRadiusRange');


%% Load image:
[img.fname, img.pname] = uigetfile({'*.png'; '*.jpg'; '*.bmp'}, 'Select an image');
if ~img.fname
    fprintf('No file selected.\n')
    return
end

img.filename = fullfile(img.pname, img.fname);
img.orig = imread(img.filename);
fprintf('Loaded image:\n\t%s\n\n', img.filename);
fprintf('Image size: %d x %d px.\n\n', size(img.orig));

figure('Name', 'Original image', 'NumberTitle', 'off');
imshow(img.orig);
title('Original image')
ax_orig = gca;
hold on

% BLURRING IS NOT NECESSARY, as the IR images from Kinect v2 are
% not very noisy and have low resolution. Otherwise we could have used a
% gaussian blur kernel (see imgaussfilt).


%% Draw a blob, simulating the motion-blurred area of the coloured marker:
fprintf('Waiting for user to draw a blob around a marker...\n');
blob.object = imfreehand;
blob.contour = getPosition(blob.object);
blob.mask = createMask(blob.object);

% Determine properties of blob(s) (there may be more than one):
blob.stats = regionprops(blob.mask, 'BoundingBox', 'Area',...
    'MajorAxisLength', 'MinorAxisLength');

% Find bounding box of largest blob:
[~, blob.id] = max([blob.stats.Area]);
bbox.rect = blob.stats(blob.id).BoundingBox;
% Store bounding box sizes in more memorable variables:
bbox.x = bbox.rect(1);
bbox.y = bbox.rect(2);
bbox.w = bbox.rect(3);
bbox.h = bbox.rect(4);
fprintf('Bounding box dimensions:\n');
fprintf('\tw = %d\n\th = %d.\n', bbox.w, bbox.h);
% Draw largest bounding box:
rectangle('Position', bbox.rect, 'EdgeColor', 'm');
if debug_mode
    % Draw discarded bounding boxes too:
    for bb=1:length(blob.stats)
        if bb==blob.id
            continue
        end
        rectangle('Position', blob.stats(bb).BoundingBox, 'EdgeColor', 'r');
    end
end

% Crop image using bounding box:
img.crop = imcrop(img.orig, bbox.rect);

% Translate blob contour from image origin to bounding box origin:
blob.contour_bbox(:,1) = blob.contour(:,1) - bbox.x;
blob.contour_bbox(:,2) = blob.contour(:,2) - bbox.y;

% Crop mask using bounding box:
blob.mask_crop = imcrop(blob.mask, bbox.rect);

fprintf('Blob axes:\n');
fprintf('\tMajor axis length = %0.2f\n\tMinor axis length = %0.2f.\n',...
    blob.stats(blob.id).MajorAxisLength,...
    blob.stats(blob.id).MinorAxisLength);
% Set the max radius as half the blob minor axis, rounded towards +inf:
rmax = ceil(blob.stats(blob.id).MinorAxisLength / 2);
% Set the rmin as a fraction of rmax. If < 2, take 2 px:
rmin = max(floor(rmax / 2.9), 2);

fprintf('Radius range (based on minor axis):\n');
fprintf('\tRadius max (rmax): %d\n', rmax);
fprintf('\tRadius min (rmin): %d\n', rmin);
fprintf('\n');


%% Test preprocessing 1: Canny edge detector.
figure('Name', 'Find circles', 'NumberTitle', 'off');

% Calculate the threshold for Canny edge detector using only those pixels
% within the blob.
% NOTE: we cannot calculate the threshold from the masked image, otherwise
% the large amount of black pixels (zero values) would affect the
% calculation of the threshold. Hence, we take only those pixels of the
% bounding box which are within the blob:
[~, thresh_blob] = edge(img.crop(blob.mask_crop), 'Canny');
% Apply Canny edge detector to the entire bounding box using the calculated
% threshold:
img.crop_canny = edge(img.crop, 'Canny', thresh_blob);

ax1 = subplot(2,2,1);
imshow(img.crop_canny, 'Parent', ax1);
% Draw blob in the bounding box:
patch(blob.contour_bbox(:,1), blob.contour_bbox(:,2), 'c',...
    'FaceAlpha', 0.05, 'EdgeColor', 'b', 'Parent', ax1)
title('Test pre-processing: Canny edges');


%% Test preprocessing 2: Otsu's threshold.
% Calculate Otsu's threshold inside bounding box, within the blob.
% NOTE: we cannot calculate the threshold from the masked image, otherwise
% the large amount of black pixels (zero values) would affect the
% calculation of the threshold. Hence, we take only those pixels of the
% bounding box which are within the blob:
[otsu.threshold, otsu.em] = graythresh(img.crop(blob.mask_crop));

% Convert crop image to binary image, using the calculated threshold.
img.crop_otsu = im2bw(img.crop, otsu.threshold);

ax2 = subplot(2,2,3);
imshow(img.crop_otsu, 'Parent', ax2)
% Draw blob in the bounding box:
patch(blob.contour_bbox(:,1), blob.contour_bbox(:,2), 'c',...
    'FaceAlpha', 0.05, 'EdgeColor', 'b', 'Parent', ax2)
title('Test pre-processing: Otsu''s threshold')


%% Find circles
ax3 = subplot(2,2,[2,4]);
imshow(img.crop, 'Parent', ax3)
hold on
% Draw blob in the bounding box:
patch(blob.contour_bbox(:,1), blob.contour_bbox(:,2), 'c',...
    'FaceAlpha', 0.05, 'EdgeColor', 'b')
title({'Circles detection: CHT', 'Low ID number = high roundness', 'Green thick edge = highest score'})

% Find circles using circular Hough transform (CHT). The method "TwoStage"
% is based on Yuen et al. (1990), which should be the same algorithm
% implemented in OpenCV.
% Since the function imfindcircles() internally thresholds the image using
% Otsu's method, passing an already pre-processed image is counter
% productive because there is too much loss of information.
% Moreover, passing an image with a mask applied creates fake circles if
% the mask itself has a round shape. Hence, we pass the original image,
% just cropped: we will take the mask into account when calculating scores
% for circles.
[circles.centres, circles.radii, circles.metric] = imfindcircles(...
    img.crop,  [rmin, rmax],...
    'method', 'TwoStage', 'Sensitivity', hough_sensitivity);

circles.n = size(circles.centres, 1);

if circles.n
    fprintf('Number of found circles: %d\n', circles.n);
    
    % Give a score to found circles, based on several parameters:
    scores = rank_circles(circles, bbox, blob.contour_bbox,...
        'debug_mode', debug_mode);
    
    % Draw circles located outside the blob (score = 0):
    viscircles(ax3, circles.centres(~scores,:), circles.radii(~scores),...
        'EdgeColor', 'r', 'linewidth', 0.8, 'linestyle', '-');
    
    % Draw circles located within the blob (score > 0)
    viscircles(ax3, circles.centres(scores>0,:), circles.radii(scores>0),...
        'EdgeColor', 'g', 'linewidth', 0.8, 'linestyle', '-');
    
    % Find circle with best score:
    [~, best_circle_id] = max(scores);
    % Highlight circle with best score:
    viscircles(ax3, circles.centres(best_circle_id,:), circles.radii(best_circle_id),...
        'Color', 'g', 'linewidth', 2, 'linestyle', '-');
    
    for i=1:circles.n
        % Display ID number for each circle in the image:
        text(circles.centres(i,1), circles.centres(i,2),...
            sprintf('%d',i), 'Color', 'w', 'Parent', ax3)
    end
    
else
    fprintf('No circles found.\n');
end




%% Find contours using image thresholded with Otsu's algorithm
figure('Name', 'Find contours')

c1 = subplot(2,2,1);
imshow(img.crop, 'Parent', c1)
title('Original')

c2 = subplot(2,2,3);
imshow(img.crop_otsu, 'Parent', c2)
title('Otsu''s threshold')


bw = img.crop_otsu;

% % remove all object containing fewer than 30 pixels
% bw = bwareaopen(bw,30);

% % fill a gap in the pen's cap
% se = strel('disk',2);
% bw = imopen(bw,se);

% % fill any holes, so that regionprops can be used to estimate
% % the area enclosed by each of the boundaries
% bw = imfill(bw,'holes');

c3 = subplot(2,2,[2 4]);
imshow(bw, 'Parent', c3)
title('Metrics closer to 1 = more round');


[B,L] = bwboundaries(bw,'noholes');

% Display the label matrix and draw each boundary
imshow(label2rgb(L, @jet, [.5 .5 .5]), 'Parent', c3)
hold on
for k = 1:length(B)
    boundary = B{k};
    plot(boundary(:,2), boundary(:,1), 'w', 'LineWidth', 2)
end


stats = regionprops(L,'Area','Centroid');

threshold = 0.94;

% loop over the boundaries
for k = 1:length(B)
    
    % obtain (X,Y) boundary coordinates corresponding to label 'k'
    boundary = B{k};
    
    % compute a simple estimate of the object's perimeter
    delta_sq = diff(boundary).^2;
    perimeter = sum(sqrt(sum(delta_sq,2)));
    
    % obtain the area calculation corresponding to label 'k'
    area = stats(k).Area;
    
    % compute the roundness metric
    metric = 4*pi*area/perimeter^2;
    
    % display the results
    metric_string = sprintf('%2.2f',metric);
    
    % mark objects above the threshold with a black circle
    if metric > threshold
        centroid = stats(k).Centroid;
        plot(centroid(1),centroid(2),'ko');
        text(centroid(1),centroid(2), metric_string,'Color','k',...
            'FontSize',14,'FontWeight','bold');
        
    end
    
    
end

