function markersStruct = ReadCustomMarkersXmlFile(xmlFilename)
% Read coloured markers properties from an XML file.
% Input:
%   xmlFilename = full filename of the XML file containing marker
%                 attributes.
% Output:
%   markersStruct = array of structs, containing attribute names and values
%                 from the XML file for each marker.
%
% � February 2017 Alessandro Timmi


DOMnode = xmlread(xmlFilename);
% Print xml on screen:
% xmlwrite(DOMnode)

% Get all markers nodes:
markersNodes = DOMnode.getElementsByTagName('ColorMarker');

markersStruct = struct;

% Loop on nodes, which are zero-based:
for i = 0 : markersNodes.getLength - 1
    
    % Get the node corresponding to the current marker:
    thisMarkerNode = markersNodes.item(i);
    
    % Get all attributes for this marker:
    markerAttributes = thisMarkerNode.getAttributes();
    
    % Loop on attributes of current marker:
    for j = 0 : markerAttributes.getLength - 1
        
        attribute = markerAttributes.item(j);
               
        attributeName = attribute.getNodeName();
        attributeValue = attribute.getNodeValue();
        
        % Convert attribute to double, except for "tag" and "enabled":
        if ~strcmpi(attributeName, {'tag', 'enabled'})
            attributeValue = str2double(attributeValue);
        end
        
        % The attribute "enabled" must be converted to a logical:
        if strcmpi(attributeName, 'enabled')
            attributeValue = strcmpi(attributeValue, 'true');
        end
        
        % Store current attribute name and value into the struct:
        markersStruct(i+1).(sprintf('%s', attributeName)) = attributeValue;
        
    end
end