function [k_norm, k_norm_diff] = norm_ker(k)
% Normalization of a convolution kernel: the sum of all the elements has to
% be 1 to preserve the image brightness.
%
% INPUT:
%   k = a kernel matrix.
% OUTPUT
%   k_norm = the normalized kernel matrix.
%   k_norm_diff = the difference between the central element
%   and the element with the smallest absolute value of the normalized kernel.
%
% � April 2015 Alessandro Timmi

% Preallocate a matrix of the same size of the kernel:
k_norm = zeros(size(k));
% Calculate the sum of the elements of the kernel:
sum_k = sum(sum(k)); % notice: faster than sum(k(:))

for r=1:size(k,1)
    for c=1:size(k,2)
        k_norm(r,c) = k(r,c)/sum_k;  
    end
end

% Calculate the indices of the centre of the kernel:
kernel_centre_pos = ceil(size(k)/2);
k_norm_diff = k_norm(kernel_centre_pos(1),kernel_centre_pos(2)) - min(min(abs(k_norm)));
