function total_scores = rank_circles(circles, bbox, blob_contour, varargin)
% Rank circles found by circular Hough transform, bases on several
% parameters:
% - closeness to bounding box edges: the closer a circle is to the
%   bounding box edges, the higher its score;
% - Hough relative accumulator: this is a measurement of the roundness of
%   the detected circle. The higher the accumulator value, the higher the
%   score attributed to a circle.
% - Position with respect to the blob: since markers cannot be outside the
%   blob, circles outside it are given a zero total score.
%
% Input:
%   circles = struct containing the following fields: centres, radii,
%             metric, which are the output of "imfindcircles()".
%   bbox = a crop rectangle in the image space It's a struct containing
%          the fields: rect, x, y, w, and h.
%   blob_contour = Circles outside this contour will be given a zero score.
%   debug_mode = (optional parameter, default = false) if true, shows
%                 some extra info for debug purposes.
%
% Output:
%   total_scores = an array with a total score for each circle. Total
%           scores take into account several other scores. Total scores
%           are sorted in the same way as the input arrays in "circles".
%
%
% � November 2015 Alessandro Timmi


%% TODO:
% - add more parameters to the score, to make it more accurate.
% - add more checks into the input parser for the structs, e.g. check the
%   individual fields.
% - at the moment, the distance score is not taking into account the case
%   when d_i (the generic distance) is zero. In this case, for that centre,
%   we will have d_inv/max(d_inv) = Inf/Inf = Nan (indeterminate form),
%   while for all the other cenres we will have score = 0.
%   Hence, in this case, the score of the centre with distance = 0 should
%   be automatically set to 1.


%% Default input values:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'circles', @isstruct);
addRequired(p, 'bbox', @isstruct);
addRequired(p, 'blob_contour',...
    @(x) validateattributes(x, {'numeric'}, {'ncols', 2}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, circles, bbox, blob_contour, varargin{:});

% Copy input arguments into more memorable variables:
circles = p.Results.circles;
bbox = p.Results.bbox;
debug_mode = p.Results.debug_mode;
blob_contour = p.Results.blob_contour;


%%
fprintf('Calculating scores for circles...\n')
if debug_mode
    fprintf('� November 2015 Alessandro Timmi\n')
end

% Number of circles:
n = size(circles.centres, 1);



%% Roundness score:
% This score is calculated as the normalized accumulator provided by Hough
% transform:
roundness_score = circles.metric / max(circles.metric);



%% Distance from edges score:
% Preallocate array of distances from edges:
d = zeros(n, 1);

for i=1:n
    xc = circles.centres(i,1);
    yc = circles.centres(i,2);
    
    % Calculate the shortest distance of the centre from the edges of the
    % bounding box:
    d(i,1) = min([xc, bbox.w - xc, yc, bbox.h - yc]);
end

% Since closer circles deserve higher scores, we calculate the inverse of
% the distances:
d_inv = 1 ./ d;
% Normalize the inverse of the distances with respect to their max, to get
% values between 0 and 1: these will be our distance scores:
d_scores = d_inv / max(d_inv);


%% Blob score:
% This score is binary: 1 for centres within the blob, 0 for those outside.
blob_score = inpolygon(circles.centres(:,1), circles.centres(:,2),...
    blob_contour(:,1), blob_contour(:,2));


%% Total score, can vary between 0 and 1:
total_scores = roundness_score .* d_scores .* blob_score;

if debug_mode
    fprintf('#%d: dist score %0.2f, roundness %0.2f, blob score %d, total score %0.2f\n',...
        [(1:n)', d_scores(:), roundness_score(:), blob_score(:), total_scores(:)]');
end


end




