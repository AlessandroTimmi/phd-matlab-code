function [s1_sync, s2_sync, shift, mean_sd] = SyncStructs(s1, s2,...
    fields_to_sync_1, varargin)
% This function synchronizes and trims to the same length two mocap trials
% using cross-covariance and returns the shift value between the trials.
% The optimal shift which ensures the best synchronization is calculated
% iteratively, minimising the mean of the standard deviations of the
% differences between pairs of signals.
%
% � October 2014 Alessandro Timmi, edited in September 2016.
%
% Input:
% s1, s2 = two structs containing as fields the signals to be synced. One
%       of the fields in each struct must contain the timestamps and be
%       called "time".
% fields_to_sync_1 = cell array with the name(s) of the field(s) to be used
%       for syncronization. For .mot files, they must be joint angles
%       (e.g. {'knee_angle_r'}). For .trc files, they must be marker names
%       (e.g. {'RASI'}). If multiple markers are defined, synchronization
%       will be based on all of them.
% fields_to_sync_2 = (optional parameter, default = fields_to_sync_1) if
%       the two structs have fields with different labels, we need to
%       specify the field(s) for sync in the second struct correspondent
%       to that in the first struct. The lenght of the two cell array must
%       be the same.
% coords_to_sync = (optional parameter, default = 'X') cell array with
%       coordinates to be used for synchronization. If multiple coordinates
%       are defined, synchronization will be based on all of them. For .mot
%       files, the default value can be used, because each joint angle is
%       a single coordinate.
% maxlag = (optional parameter, default = []) maximum lag, expressed in
%       number of frames. If specified, the returned cross-correlation
%       sequence is calculated only in the range [-maxlag maxlag], instead
%       of across the entire length of the longer signal.
%
% Output:
% s1_sync, s2_sync = the two structs, synced and trimmed at the same
%       length.
% shift = If shift > 0, s1 was moved backwards. If shift < 0, s2 was moved
%       backwards.
% mean_sd = mean of the standard deviations of the differences between
%       pairs of signals. Used as objective function to be minimized via
%       synchronization. Also useful to monitor the results of
%       synchronization of a set of trials.
%
% ASSUMPTIONS:
% - the two signals represent the SAME ACTUAL QUANTITY (or trial, etc.);
% - the two signals have SAME FRAMERATE (or have been interpolated to have
%   same framerate);
% - if the two signals have different durations (numbers of frames), the
%   SHORTEST CONTAINS THE ENTIRE TRIAL. Therefore, after syncing, the extra
%   duration of the longest signal can be discarded without any significant
%   loss of information;
%   2) Moreover, we might further trim the trials by a length equal to
%   abs(shift), assuming we don't cause any loss of significant
%   information. See below for detailed information on how signals are
%   trimmed.
%
% ALGORITHM:
% The number of cases we can have depends on the combinations of different
% lengths ("L" from now on) and different shifts ("s" from now on) between
% the two signals:
%   L1 < = > L2;
%   s < = > 0.
% Thus we have 3^2 = 9 possible cases.
%
% In order to reduce the number of cases, we determine which signal is
% shorter (f) and which is longer (g) using the min() function. Therefore:
%   Lf <= Lg.
% Then we pass the shorter signal as first argument and the longer as
% second argument to the cross-covariance function. In this way we have
% removed the 3 cases involving L1 > L2. Obviously, if signals are swapped
% for synchronization, then they are returned in their original order
% before output and the sign of the shift is changed as well.
%
% The remaining 6 cases can be implemented as follows:
%
% 1) s < 0, Lf <= Lg
% Circshift g backwards and trim both signals at:
%   min(Lf, Lg - abs(s))
% from the start.
%
% 2) s > 0, Lf <= Lg
% Circshift f backwards (to avoid more complex trim at the start of the
% signals) and trim both signals at:
%   Lf - s
% from the start.
%
% 3) s = 0, Lf <= Lg
% No circshift required. If Lf < Lg, trim g at:
%   Lf
% from the start.


%% Default input values:
default.fields_to_sync_2 = fields_to_sync_1;
default.coords_to_sync = {'X'};
default.maxlag = [];

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 's1', @isstruct)
addRequired(p, 's2', @isstruct)
addRequired(p, 'fields_to_sync_1',...
    @(x) validateattributes(x, {'cell'}, {'vector', 'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'fields_to_sync_2', default.fields_to_sync_2,...
    @(x) validateattributes(x, {'cell'}, {'vector', 'nonempty'}));
addParameter(p, 'coords_to_sync', default.coords_to_sync,...
    @(x) validateattributes(x, {'cell'}, {'vector', 'nonempty'}));
addParameter(p, 'maxlag', default.maxlag,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));

% Parse input arguments:
parse(p, s1, s2, fields_to_sync_1, varargin{:});

% Copy input arguments into more memorable variables:
s1 = p.Results.s1;
s2 = p.Results.s2;
fields_to_sync_1 = p.Results.fields_to_sync_1;

fields_to_sync_2 = p.Results.fields_to_sync_2;
coords_to_sync = p.Results.coords_to_sync;
maxlag = p.Results.maxlag;


clear varargin default p


%%
fprintf('Starting sync and trim procedure...\n');

% Check that fields_to_sync_1 and fields_to_sync_2 have same length:
if ~isequal(length(fields_to_sync_1), length(fields_to_sync_1))
    error('fields_to_sync_1 and fields_to_sync_2 have different length.')
end

% NOTE: In min() function, if there are several identical minimum values,
% the index of the first one found is returned (which in this case is 1).
[frames_short, shortest] = min([length(s1.time), length(s2.time)]);

% Detect the short and long trials in terms of number of frames:
if shortest==1
    s_short = s1;
    s_long = s2;
    fields_to_sync_short = fields_to_sync_1;
    fields_to_sync_long = fields_to_sync_2;
    
elseif shortest==2
    s_short = s2;
    s_long = s1;
    fields_to_sync_short = fields_to_sync_2;
    fields_to_sync_long = fields_to_sync_1;
    
else
    error('Problem while detecting the shortest signal');
end

% We need the length of the long dataset too:
frames_long = length(s_long.time);

% Convert coordinates to sync from char to corresponding numbers:
coords_to_sync_num = coord2num(coords_to_sync);


%% Synchronization of the long dataset relative to the short:
fprintf('Synchronization based on the following fields:\n')
for i=1:length(fields_to_sync_1)
    fprintf('\t"%s" and "%s"\n', fields_to_sync_1{i}, fields_to_sync_2{i});
end
fprintf('and on the following columns:\n')
for i=1:length(coords_to_sync_num)
    fprintf('\t%d\n', coords_to_sync_num{i})
end

% If not passed as input argument, set maxlag to its default value
% reported in Matlab implementation.
% We need to do this, otherwise we should write the cross-covariance
% command twice (with and without maxlag as optional argument).
% According to Matlab help for the xcov() function, the default value for
% maxlag is equal to N - 1, where N is the length of the longer signal.
% In this way, the lag range is 2N - 1:
if isempty(maxlag)
    maxlag = frames_long - 1;
end

% Pre-set this variable which will be used below:
mean_sd = Inf;

fprintf('List of calculated shifts:\n')
for i = 1:length(fields_to_sync_1)
    for j = 1:length(coords_to_sync)
        
        % Cross-covariance for current pair of signals:
        [cc, lags] = xcov(...
            s_short.markers.(fields_to_sync_short{i})(:, coords_to_sync_num{j}),...
            s_long.markers.(fields_to_sync_long{i})(:, coords_to_sync_num{j}),...
            maxlag);
        
        % Find index corresponding to maximum cross-covariance:
        [~, max_cc_index] = max(cc);
        
        % Get the lag corresponding to max cross-covariance. This will be
        % the SHIFT for the current pair of signals (temporary shift):
        shift_temp = lags(max_cc_index);
        fprintf('Shift from %s %s: %d.\n', fields_to_sync_short{i},...
            coords_to_sync{j}, shift_temp);
        
        % Sync and trim the two structs using the temporary shift value,
        % returning them in the order in which they were input:
        [s1_sync_temp, s2_sync_temp] = ShiftAndTrim(...
            s_short, s_long, shift_temp,...
            frames_short, frames_long, shortest);
        
        % To determine if this is the "best" shift, we need
        % to verify if it minimizes the standard deviation (SD) of the
        % differences between pairs of signals. Indeed, a pure sync error
        % affects only the SD, not the mean (or constant bias) of the
        % differences between two signals (see test_sync_vs_agreement.m).
        % Therefore, we calculate the SD of the differences for each pair
        % of signals to be synced.
        % Preallocate an array for the SD of each pair of signals:
        sd_collection = zeros(length(fields_to_sync_1), length(coords_to_sync));
        for k = 1:length(fields_to_sync_1)
            % Remember that std() operates along the columns, returning a row
            % vector.
            sd_collection(k,:) = std(s1_sync_temp.markers.(fields_to_sync_1{k}) -...
                s2_sync_temp.markers.(fields_to_sync_2{k}));
        end
        % Calculate the mean of all the SD...
        mean_sd_temp = mean(sd_collection(:));
        % ... and compare it to the reference one:
        if mean_sd_temp < mean_sd
            % If it is smaller, it becomes the new reference:
            mean_sd = mean_sd_temp;
            % and the corrspondent temporary shift is stored:
            shift = shift_temp;
        end
    end
end

% Sync and trim the two trials using the best shift found. Now we also get
% as output the shift with the correct sign, taking into account whether or
% not the two trials were swapped due to their lenghts:
[s1_sync, s2_sync, shift] = ShiftAndTrim(s_short, s_long,...
    shift, frames_short, frames_long, shortest);

end


function [s1_sync, s2_sync, shift] = ShiftAndTrim(f, g, s, Lf, Lg, shortest)

if s < 0
    % CircShift g backwards:
    g.markers = structfun(@(y) ( circshift(y, s, 1) ),...
        g.markers, 'UniformOutput', false);
    
    % Determine new trial length:
    newLength = min(Lf, Lg - abs(s));
    
    % Trim both trials at the new length from the start:
    f.markers = structfun(@(y) ( y(1:newLength, :) ),...
        f.markers, 'UniformOutput', false);
    g.markers = structfun(@(y) ( y(1:newLength, :) ),...
        g.markers, 'UniformOutput', false);
    
    f.time = f.time(1:newLength);
    g.time = g.time(1:newLength);    
    
elseif s > 0
    % CircShift f backwards:
    f.markers = structfun(@(y) ( circshift(y, -s, 1) ),...
        f.markers, 'UniformOutput', false);

    % Trim both trials at Lf - s from the start:
    newLength = Lf - s;
    
    f.markers = structfun(@(y) ( y(1:newLength, :) ),...
        f.markers, 'UniformOutput', false);
    g.markers = structfun(@(y) ( y(1:newLength, :) ),...
        g.markers, 'UniformOutput', false);
    
    f.time = f.time(1:newLength);
    g.time = g.time(1:newLength);    
    
elseif s == 0
    fprintf('Signals are already synced (shift = 0)')
    
    if Lf < Lg        
        fprintf('.\n\n')
        
        % Trim g at Lf from the start:
        newLength = Lf;
        g.markers = structfun(@(y) ( y(1:newLength, :) ),...
            g.markers, 'UniformOutput', false);
        
        g.time = g.time(1:newLength);                
        
    else
        fprintf(' and have same length.\n\n')
    end    
    
else
    error('Error during the detection of the shift')
end


% Return the structs in the same order as they were input and the shift
% with the same sign:
if shortest == 1
    s1_sync = f;
    s2_sync = g;
    shift = s;
else
    % Trials were swapped, swap them back:
    s1_sync = g;
    s2_sync = f;
    % Since we have just swapped the trials, we also need to change the
    % sign of the shift before returning it:
    shift = -s;
end

% Update structs info for .trc files:
% This "if" is just a way to check whether this is a .trc or .mot struct,
% because .mot files don't need this step.
if isfield(s1_sync, 'NumMarkers')
    s1_sync = UpdateTrcStructInfo(s1_sync);
    s2_sync = UpdateTrcStructInfo(s2_sync);
end


end