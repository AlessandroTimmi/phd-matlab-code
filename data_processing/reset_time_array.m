function new_time_array = reset_time_array(time_array, varargin)
% This function resets a time array to a desired start time.
% Sometimes motion capture trials may be trimmed at the beginning, leaving
% a start time different from zero.
% However, when different files must be analysed together, it might be
% useful to have a common start time.
%
% Input:
%   time_array = an array containing the timestamps, in row or column
%                format. It can have constant of variable timestep.
%   start_time = (optional, default = 0) the new desired start time to be
%                set on the time array.
% log_message = (optional parameter, default = 'Resetting time array')
%               string with custom message for the log.
%   debug_mode = (optional parameter, default = false). If true, displays
%                some extra info for debug purposes.
%
% Output:
%   new_time_array = the time array reset at the desired start time.
%
% � November 2014 Alessandro Timmi


% Default input values:
default.start_time = 0;
default.log_message = 'Resetting timestamps array';
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add input arguments:
addRequired(p, 'time_array', @isvector);
addParameter(p, 'start_time', default.start_time, @isnumeric);
addParameter(p, 'log_message', default.log_message,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, time_array, varargin{:});

% Copy input arguments into more memorable variables:
time_array = p.Results.time_array;
start_time = p.Results.start_time;
log_message = p.Results.log_message;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
if debug_mode
    fprintf('� November 2014 Alessandro Timmi.\n\n')
    fprintf('[Debug] First timestamp: %g s.\n', time_array(1))
end

%% If the first element of the time array is equal to the requested start
% time, return the array as it is:
if isequal(time_array(1), start_time)
    if debug_mode
        fprintf('[Debug] The first timestamp is equal to the requested start time:\n')
        fprintf('\treturning input array as it is.\n')
    end
    new_time_array = time_array;    
else
    %% Reset the time array to zero, subtracting the first timestamp and
    % adding the desired start time to all its elements:    
    fprintf('%s to requested start time: %g s.\n', log_message, start_time)
    new_time_array = time_array - time_array(1) + start_time;
end








