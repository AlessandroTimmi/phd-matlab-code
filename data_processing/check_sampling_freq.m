function [diff_t, mean_diff_t, std_dev_diff_t, avg_freq_t] = check_sampling_freq(t, varargin)
% This function shows the sampling frequency of an array calculating the
% differences between adjacent elements of the array, plotting them and
% calculating the mean and standard deviation of these differences.
%
% Input:
%   t = an array of timestamps.
%   debug_mode = (optional parameter, default = false) if true, displays
%                some extra info for debug purposes.
%
% Output:
%   diff_t = an array containing the differences between adjacent elements
%            of t;
%   mean_diff_t = the mean of t;
%   std_dev_t = the standard deviation of t;
%   avg_freq_t = the average sampling frequency of t in Hz, rounded to the
%                nearest integer.
%
% � November 2014 Alessandro Timmi.

% TODO:
% - Implement varargout and put avg_freq_t as first output argument.


%% Check input:
narginchk(1, inf);

% Default input values:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 't', @isvector);

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, t, varargin{:});

% Copy input arguments into more memorable variables:
t = p.Results.t;
debug_mode = p.Results.debug_mode;


%%
diff_t = diff(t);
mean_diff_t = mean(diff_t);
std_dev_diff_t = std(diff_t);
avg_freq_t = round(1 / mean_diff_t);

light_gray = [0.5 0.5 0.5];

if debug_mode
    fprintf('[Debug] Mean interval between timestamps = %.4f s;\n', mean_diff_t);
    fprintf('[Debug] Standard deviation of timestamps (S) = %.4f s;\n', std_dev_diff_t);
    fprintf('[Debug] Average sampling frequency (rounded) = %.1f Hz.\n', avg_freq_t);
    
    fprintf('[Debug] See debug plot of sampling frequency.\n')
    
    % The range containing 95% of the differences is delimted by +/- 2 standard
    % deviations around the mean of the differences:
    upper_limit = mean_diff_t + 2 * std_dev_diff_t;
    lower_limit = mean_diff_t - 2 * std_dev_diff_t;
    
    % Plot results:
    
    figure('Name', 'Frames and timestamps')
    subplot(2,1,1)
    plot(1:length(t), t, '.')
    xlabel('Frames [#]')
    ylabel('Timestamps [s]')
    title('Frames vs corresponding timestamps')
    
    subplot(2,1,2)
    plot(t(2:end), diff(t), '.')
    mean_line = refline(0, mean_diff_t);
    set(mean_line, 'Color', light_gray)
    up_limit_line = refline(0, upper_limit);
    low_limit_line = refline(0, lower_limit);
    set(up_limit_line, 'Color', light_gray, 'linestyle','--')
    set(low_limit_line, 'Color', light_gray, 'linestyle','--')
    xlabel('Timestamps [s]')
    ylabel('\DeltaT [s]')
    title('Differences between adjacent timestamps', 'fontweight','bold')
    legend('\DeltaT [s]', 'Mean', '+/- 2S',...
        'Location', 'NorthEast')
    % Rescale axes limits automatically, just in case the reference lines
    % are outside the previously calculated limits:
    axis auto
end

