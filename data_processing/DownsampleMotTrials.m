function motProcessedFilenames = DownsampleMotTrials(motFilenames, varargin)
% Downsample .mot trials.
%
% Input:
%   motFilenames = row cell array containing full filenames of the .mot
%                  files to be downsampled.
%   targetFramerate = (optional, default = 30) sampling frequency to which
%                  trials must be converted (fps). It must be a positive
%                  integer.
%
% Output:
%   motProcessedFilenames = row cell array containing full filenames of
%                  the downsampled .mot trials.
%
% � August 2016 Alessandro Timmi.


%% Default input values:
default.targetFramerate = 30;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'motFilenames',...
    @(x) validateattributes(x, {'cell'}, {'nonempty', 'row'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'targetFramerate', default.targetFramerate,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'positive', 'integer'}));

% Parse input arguments:
parse(p, motFilenames, varargin{:});

% Copy input arguments into more memorable variables:
motFilenames = p.Results.motFilenames;
targetFramerate = p.Results.targetFramerate;

clear varargin default p


%% Preallocate a cell array for the output filenames:
motProcessedFilenames = cell(size(motFilenames));

for i=1:numel(motFilenames)
    % Load the file to be processed and store its content into a struct:
    s = LoadMotFile(...
        'filename', motFilenames{i},...
        'logMessage', sprintf('Loading file %d of %d:\n', i, numel(motFilenames)));
    
    % Copy raw struct, skipping time and coordinates:
    sDownsampled = rmfield(s, {'time', 'coords'});
    
    
    %% Downsampling
    % Calculate sampling frequency of current trial:
    [~, ~, ~, avgFreqT] = check_sampling_freq(s.time);
    
    % Downsampling factor: it must be a positive integer (this check is
    % performed by Matlab downsample() function).
    downsamplingFactor = avgFreqT / targetFramerate;
    
    fprintf('\nDownsampling data by %g / %g = %g.\n', avgFreqT,...
        targetFramerate, downsamplingFactor);
    
    % Downsampling the timestamps array:
    sDownsampled.time = downsample(s.time, downsamplingFactor);
    
    % Downsampling data:
    coordNames = fieldnames(s.coords);
    for m=1:length(coordNames)        
        sDownsampled.coords.(coordNames{m}) =...
            downsample(s.coords.(coordNames{m}), downsamplingFactor);        
    end    
    
    
    %% Generate the destination filename:
    [pathstr, fname, ext] = fileparts(motFilenames{i});
    motProcessedFilenames{i} = fullfile(pathstr,...
        [fname, '_downsampled', ext] );
    
    % Write the output file:
    WriteMotFile(sDownsampled, 'destinationFilename', motProcessedFilenames{i});
end

end

