function trc_processed_filenames = DownsampleTrcTrials(trc_filenames, varargin)
% Downsample .trc trials.
%
% Input:
%   trc_filenames = row cell array containing full filenames of the .trc
%                   files to be downsampled.
%   target_framerate = (optional, default = 30) sampling frequency to which
%                   trials must be converted (fps). It must be a positive
%                   integer.
%   debug_mode    = (optional parameter, default = false) if true, shows
%                   some extra info for debug purposes.
%
% Output:
%   trc_processed_filenames = row cell array containing full filenames of
%                   the downsampled .trc trials.
%
% � April 2016 Alessandro Timmi.


%% Default input values:
default.debug_mode = false;
default.target_framerate = 30;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'trc_filenames',...
    @(x) validateattributes(x, {'cell'}, {'nonempty', 'row'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'target_framerate', default.target_framerate,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'positive', 'integer'}));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, trc_filenames, varargin{:});

% Copy input arguments into more memorable variables:
trc_filenames = p.Results.trc_filenames;
target_framerate = p.Results.target_framerate;
debug_mode = p.Results.debug_mode;

clear varargin default p


%% Preallocate a cell array for the output filenames:
trc_processed_filenames = cell(size(trc_filenames));

for i=1:numel(trc_filenames)
    % Load the file to be processed and store its content into a struct:
    s = LoadTrcFile(...
        'filename', trc_filenames{i},...
        'logMessage', sprintf('Loading file %d of %d:\n', i, numel(trc_filenames)));  
    
    % Copy raw struct, skipping time and marker coordinates:
    s_downsampled = rmfield(s, {'time', 'markers'});
    
    
    %% Downsampling
    % Calculate sampling frequency of current trial:
    [~, ~, ~, avg_freq_t] = check_sampling_freq(s.time);
        
    % Downsampling factor: it must be a positive integer (this check is
    % performed by Matlab downsample() function).
    downsampling_factor = avg_freq_t / target_framerate;
    
    fprintf('\nDownsampling data by %g / %g = %g.\n',...
        avg_freq_t, target_framerate, downsampling_factor);
    
    % Downsampling the timestamps array:
    s_downsampled.time = downsample(s.time, downsampling_factor);
    
    % Downsampling marker data:
    marker_names = fieldnames(s.markers);
    for m=1:length(marker_names)
        for c=1:3          
            s_downsampled.markers.(marker_names{m})(:,c) =...
                downsample(s.markers.(marker_names{m})(:,c),...
                downsampling_factor);
        end
    end
    
    % Update info in the struct, calculating them from data:  
    s_downsampled = UpdateTrcStructInfo(s_downsampled, 'debug_mode',...
        debug_mode);
    
    
    %% Generate the destination filename:
    [pathstr, fname, ext] = fileparts(trc_filenames{i});
    trc_processed_filenames{i} = fullfile(pathstr,...
        [fname, '_downsampled', ext] );
    
    % Write the output file:
    WriteTrcFile(s_downsampled,...
        'destinationFilename', trc_processed_filenames{i});
end

end

