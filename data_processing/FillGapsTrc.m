function s = FillGapsTrc(s, maxGapSize)
% Fill gaps (if present) in the input .trc struct.
%
% Input:
%   s = struct containing .trc data.
%   maxGapSize = max size of gaps (in frames) to be filled. Gaps larger
%              than this will be filled with "Nan" and a warning message
%              will be printed on screen.
%
% Output:
%   s = struct containing .trc data with gaps filled.
%
% � November 2014 Alessandro Timmi


%% TODO:
% - currently all fields of the struct are filled: however, we should avoid
%   doing this for "frames" and "time" fields. We can use the function
%   "IsFiltrable" to detect these fields.


%% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 's', @isstruct);
addRequired(p, 'maxGapSize',...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'positive', 'integer'}));

% Parse input arguments:
parse(p, s, maxGapSize);

% Copy input arguments into more memorable variables:
s = p.Results.s;
maxGapSize = p.Results.maxGapSize;

clear varargin default p


%%
fprintf('Filling gaps in marker coordinates...\n');

% Fill the gaps if present, using spline interpolation. Gaps larger
% than max_gap_size are filled with Nan by default. OpenSim
% seems to accept Nan without problems, hence I prefer Nan to empty
% positions, because it is easier to determine the length of a
% vector containing Nan compared to an empty vector.
fprintf('Max size of gaps to be filled: %d frames.\n', maxGapSize)

% Extrapolating, in case of big gaps, would return bad results. Since 
% the spline interpolation requires a form of extrapolation, we could use
% Nan as default value to fill the gaps at the extremities of the arrays.
% However, since we usually have small gaps, it is better to extrapolate.
% Otherwise, subsequent filtering will increase the size of the gaps at
% the extremities, making the problem much worse. Hence, we leave the
% default behaviour of the spline interpolation, which is to extrapolate.

markerNames = fieldnames(s.markers);

for m = 1 : length(markerNames)
    for c = 1:3
        s.markers.(markerNames{m})(:,c) =...
            interp1gap(s.markers.(markerNames{m})(:,c),...
            'spline', maxGapSize);
    end
end


end