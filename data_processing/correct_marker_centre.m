function s2 = correct_marker_centre(s, sph_marker_names, diameter, varargin)
%CORRECT_MARKER_CENTRE Get marker centre from 2.5D depth data.
%   Being a 2.5D depth camera, Kinect is able to map surfaces in front of
%   it. Hence, to track the centre of a spherical marker knowing the centre
%   of its surface as it appears from Kinect point of view, we need to
%   extend the position vector by a quantity equal to the marker radius.
%
%   This function takes a list of selected markers names and their
%   diameter. Then applies a correction to their 3D coordinates, returning
%   a struct with corrected coordinates for the selected markers, leaving
%   other points untouched.
%
%   Input:
%       s                = struct containing trial data.
%       sph_marker_names = char (if single marker) or cell array of
%                        strings containing the names of the markers to be
%                        corrected (not case sensitive). Some of the
%                        points contained into the .trc are Kinect skeleton
%                        joints. We only want to correct the coordinates of
%                        the spherical markers contained in this cell array.
%       diameter         = the diameter of the markers (m). Markers are
%                        commonly described by their diameter, rather than
%                        their radius. To ensure the proper unit of
%                        measurement is used, currently the program accepts
%                        markers within 0.005<= diameter <= 0.05 m.
%       debug_mode       = (optional parameter, default = false) if true
%                        shows some extra info for debug purposes.
%
%   Output:
%       s2        = struct containing coordinates after correction has been
%                  applied to selected markers.
%
%   � July 2015 Alessandro Timmi

%% TODO:
% - Modify this function so that we don't need select the spherical
% markers. It should be able to understand that all labels that are not
% skeleton joints (or COM) are coloured markers.


%% Default input values:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input argument:
addRequired(p, 's', @isstruct);
addRequired(p, 'sph_marker_names',...
    @(x) validateattributes(x, {'char', 'cell'}, {'nonempty'}));
% Marker diameter must be expresed in m: to ensure the proper unit is used,
% a limited range is allowed for the diameter.
addRequired(p, 'diameter', @(x) validateattributes(x, {'numeric'},...
    {'scalar', 'positive','>=', 0.005, '<=', 0.05}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, s, sph_marker_names, diameter, varargin{:});

% Copy input arguments into more memorable variables:
s = p.Results.s;
sph_marker_names = p.Results.sph_marker_names;
diameter = p.Results.diameter;
debug_mode = p.Results.debug_mode;


%%
fprintf('Correcting spherical markers coordinates, accounting for their radius...\n');
if debug_mode
    fprintf('� July 2015 Alessandro Timmi.\n')
end

fprintf('Known marker diameter: %g m.\n', diameter);
% Marker radius (m):
radius = diameter / 2;
fprintf('Radius: %g m.\n', radius);



%% Preallocate an array to store the original coordinates of markers to be
% corrected:
coords = zeros(s.NumFrames, length(sph_marker_names) * 3);

% Copy coordinate of markers to be corrected from the input struct to the
% preallocated array:
for i=1:length(sph_marker_names)
    coords(:, (i-1)*3+(1:3)) = s.markers.(sph_marker_names{i});
end


%% Apply the radius correction according to this formula:
% rC = rP * (1 + rm / ||rP||)
% where:
% rC = vector position of the marker centre.
% rP = vector position of the marker surface as tracked from Kinect.
% rm = marker radius (known).

% Preallocate the array of transformed coordinates:
coords_corrected = zeros(size(coords));

% For each frame, apply the coordinate transformation to each marker:
for f=1:s.NumFrames
    % Reshape the current frame row as a 3 x n_marker matrix:
    rP = reshape(coords(f,:), 3, []);
    % Praeallocate an array for the coordinates of the centres at the
    % current frame:
    rC = zeros(size(rP));
    % Correct each marker at the current frame:
    for v=1:length(sph_marker_names)
        rC(:,v) = rP(:,v) * (1 + radius / norm(rP(:,v)));
    end
    
    % Reshape back to row format:
    coords_corrected(f,:) = reshape(rC, 1, []);
end

% Copy the original struct in the destination one:
s2 = s;

% Overwrite the relevant columns of the output struct using the corrected
% marker coordinates:
for i=1:length(sph_marker_names)
    s2.markers.(sph_marker_names{i}) = coords_corrected(:, (i-1)*3+(1:3));
end


%% Plot just the first corrected marker, for debug purposes:
if debug_mode   
    fprintf('[Debug] See debug plots for marker radius correction.\n')
    figure('Name', '[Debug] Original and corrected 3D trajectories for first marker')
    plot3(s.markers.(sph_marker_names{1})(:,1),...
        s.markers.(sph_marker_names{1})(:,2),...
        s.markers.(sph_marker_names{1})(:,3), 'b')
    hold on  
    plot3(s2.markers.(sph_marker_names{1})(:,1),...
        s2.markers.(sph_marker_names{1})(:,2),...
        s2.markers.(sph_marker_names{1})(:,3), 'g')
    
      % Kinect coordinate system:
    plot3(0,0,0, 'ks', 'MarkerSize', 15, 'MarkerFaceColor', 'k');
    plot3([0;0.5],[0;0],[0;0], 'r', 'linewidth', 3);
    hold on
    plot3([0;0],[0;0.5],[0;0], 'g', 'linewidth', 3);
    plot3([0;0],[0;0],[0;0.5], 'b', 'linewidth', 3);
    
    legend('Marker surface (P)', 'Marker centre (C)')
    xlabel('X [m]')
    ylabel('Y [m]');
    zlabel('Z [m]');
    title('Original and corrected marker 3D trajectory',...
        'Fontweight', 'bold');
    view([-180,-60])
    axis equal
    
    
    
    figure('Name', '[Debug] Original and corrected coordinates for first marker')
    subplot(3,1,1)
    plot(s.markers.(sph_marker_names{1})(:,1), 'b')
    hold on
    plot(s2.markers.(sph_marker_names{1})(:,1), 'g')
    ylabel('X (m)')
    legend('Marker surface (P)', 'Marker centre (C)','location', 'best')
    title('Original and corrected coordinates for first marker', 'Fontweight', 'bold')
    subplot(3,1,2)
    plot(s.markers.(sph_marker_names{1})(:,2), 'b')
    hold on
    plot(s2.markers.(sph_marker_names{1})(:,2), 'g')
    ylabel('Y (m)')
    subplot(3,1,3)
    plot(s.markers.(sph_marker_names{1})(:,3), 'b')
    hold on
    plot(s2.markers.(sph_marker_names{1})(:,3), 'g')
    ylabel('Z (m)')
    
    
    % Distance between original and corrected marker coordinates.
    % it should be constant and equal to the radius:
    difference = zeros(length(s.time), 1);
    for j=1:s.NumFrames
        vec_before = s.markers.(sph_marker_names{1})(j, :);
        
        vec_after = s2.markers.(sph_marker_names{1})(j, :);
        
        difference(j) = norm(vec_after - vec_before);
    end
    
    figure('Name', '[Debug] Distance between original and corrected coordinates for first marker')
    plot(s.time, difference)
    hold on
    h_radius = refline(0, radius);
    set(h_radius, 'Color', 'g', 'linestyle', '--', 'linewidth', 1.5)
    legend('Dist. after - before correction','Marker radius')
    ylabel('Distance (m)')
    xlabel('Time [s]')
    title('Distance between original and corrected coordinates for first marker',...
        'Fontweight', 'bold')
end
end

