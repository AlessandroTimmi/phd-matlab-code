function trc_processed_filenames = MidpointPipeline(trc_filenames, varargin)
% Obtains the coordinates of the midpoint between two markers. Markers with
% same initial 3 letters are automatically identified as pairs and their
% midpoint is calculated as mean of the coordinates between them. The name
% of the midpoint is obtained as the first 3 characters of the names of the
% corresponding markers.
%
% Input:
%   trc_filenames = cell array with filenames of the .trc files containing
%                 paired markers data, from which we need to calculate
%                 the midpoints.
%   debug_mode    = (optional parameter, default = false) if true, shows
%                 some extra info for debug purposes.
%
% Output:
%   trc_processed_filenames = row cell array with the full filenames of
%                 the resulting .trc files, containing midpoints coordinates. 
%
% � October 2015 Alessandro Timmi.

%% TODO
% - improve marker pairing method, to make it more robust. The subfix
% should be passed as input argument by the user and removed before looking
% for pairs.

%% Default input values:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'trc_filenames', @(x) validateattributes(x, {'cell'}, {'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, trc_filenames, varargin{:});

% Copy input arguments into more memorable variables:
trc_filenames = p.Results.trc_filenames;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
fprintf('Pipeline to obtain the coordinates of the midpoints between pairs of markers.\n');
if debug_mode
    fprintf('� October 2015 Alessandro Timmi.\n\n')
end


%% Preallocate a cell array for the filenames of the cleaned files:
trc_processed_filenames = cell(1, numel(trc_filenames));

% Process selected .trc file(s):
for i=1:numel(trc_filenames)
    % Load the .trc file to be processed and store its content into a
    % struct:
    s = LoadTrcFile(...
        'filename', trc_filenames{i},...
        'logMessage', sprintf('Loading file %d of %d:\n', i, numel(trc_filenames)));
    
    % Copy the struct except the marker coordinates:
    s_midpoint = rmfield(s, 'markers');
    
    
    %% Find marker pairs, ignoring the last character of their names:
    marker_names = fieldnames(s.markers);
    [paired_markers, pair_names] = find_marker_pairs(marker_names,...
        'debug_mode', debug_mode);
    
    % Calculate the coordinates of the points in the middle of each pair:
    for p=1:length(pair_names)
        for c=1:3
            s_midpoint.markers.(pair_names{p})(:,c) =...
                mean([...
                s.markers.(paired_markers{p,1})(:,c),...
                s.markers.(paired_markers{p,2})(:,c)],...
                2);
        end
    end
    
    % Update the info in the new struct, based on the new marker data:
    s_midpoint = UpdateTrcStructInfo(s_midpoint);
    
    
    %% Generate the destination file name(s):
    [pathstr, fname, ext] = fileparts(trc_filenames{i});    
    trc_processed_filenames{i} = fullfile(pathstr,...
        [fname, '_midpoints', ext] );
    
    % Write the resulting .trc into an ASCII file, using names of
    % pairs instead of markers:
    WriteTrcFile(s_midpoint,...
        'destinationFilename', trc_processed_filenames{i});
end

end

