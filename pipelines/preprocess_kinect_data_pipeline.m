function [trc_processed_cal_filenames, trc_processed_rig_filenames,...
    trc_processed_trial_filenames] = preprocess_kinect_data_pipeline(varargin)
% Pre-process Kinect .trc files, to make them ready for further analyses
% (e.g. OpenSim, Bland-Altman, etc.).
%
% Input:
%   trc_cal_filenames = (optional parameter, default = {false}) row cell
%                       array of strings with the filenames of .trc
%                       calibration files to be processed. If the first
%                       element is false, the processing of this type of
%                       trials will be skipped.
%   cal_markerset_name = (optional parameter, default = 'Ov_X1_X2_Y1')
%                       name of the markerset used for calibration.
%   marker_cal_diameter = (optional parameter, default = 0.020) diameter of
%                       the markers used for calibration in [m].
%
%   trc_rig_filenames = (optional parameter, default = {false}) row cell
%                       array of strings with the filenames of .trc
%                       rig files to be processed for translation vector.
%                       If the first element is false, the processing of
%                       this type of trials will be skipped.
%   rig_markerset_name = (optional parameter, default = see below) name of
%                      the markerset containing rig data for translation
%                      vector.
%   marker_rig_diameter = (optional parameter, default = 0.038) diameter of
%                       the markers used for calibration in [m].
%
%   trc_trial_filenames = (optional parameter, default = {false}) row cell
%                       array of strings with the filenames of .trc
%                       trial files to be processed. If the first element
%                       is false, the processing of this type of trials
%                       will be skipped.
%   trial_markerset_name = (optional parameter, default =
%                       'Agreement_coloured_markerset') name of the
%                       markerset used for trials.
%   sph_trial_markerset_name = (optional parameter, default =
%                       'Agreement_coloured_markerset') name of the
%                       spherical markers in the trials to be corrected
%                       using their radius.
%   marker_trial_diameter = (optional parameter, default = 0.038) diameter
%                       of the markers used for trials in [m].
%   max_gap_size = (optional, default = 10) Max size of gaps (in frames)
%                   which will be filled. Gaps larger than this will be
%                   filled with "Nan" and a warning message will be
%                   printed on screen.
%   filterType  = (optional, default = 'none') one of the following strings:
%                   'none', 'butter', 'sgolay', 'median'.
%   filterType2  = (optional, default = 'none') second pass of filtering,
%                   see above.
%   butterOrder  = (optional, default = 0) order of Butterworth filter.
%   butterCutoff = (optional, default = 0) cutoff frequency for
%                   Butterworth filter.
%   medianSpan   = (optional, default = 0) span of median filter.
%   sgolayOrder  = (optional, default = 0) order of Savitzky-Golay filter.
%   sgolaySpan   = (optional, default = 0) span of Savitzky-Golay filter.
%   transform_kinect_to_vicon = (optional parameter, default = false) if
%                   true, transforms coordinates from Kinect to Vicon
%                   reference frame, using the calibration trial for pose
%                   estimation.
%   trcReferenceFrame = (optional parameter, default = '') .trc file
%                containing coordinates of Vicon reference frame, recorded
%                by Kinect. If the string is empty, a dialog will open to
%                select a .trc file.
%   rotate_vicon_to_opensim = (optional parameter, default = false) if
%                   true, rotates coordinates from Vicon to OpenSim
%                   reference frame.
%
%   debug_mode    = (optional parameter, default = false). If true, shows
%                   extra info for debug purposes.
%
% Output:
%   trc_processed_cal_filenames = row cell array containing full
%           filenames of processed calibration files.
%   trc_processed_rig_filenames = row cell array containing full
%           filenames of processed translation rig files.
%   trc_processed_trial_filenames = row cell array contining full
%           filenames of processed trials.
%
% � September 2015 Alessandro Timmi

%% TODO:
% - when calibration object is chosen, automatically set the proper marker
%   radius (currently it is a parameter in the input parser below).


%% Default input values:
% For calibration trials:
default.trc_cal_filenames = {false};
default.cal_markerset_name = 'Ov_X1_X2_Y1';
default.marker_cal_diameter = 0.020; % [m]

% For "translation_rig.trc" trials, used to determine the translation
% vector d_vk from Vicon to Kinect origin:
default.trc_rig_filenames = {false};
default.rig_markerset_name = 'Agreement_translation_coloured';
default.marker_rig_diameter = 0.038; % [m]

% For actual trials:
default.trc_trial_filenames = {false};
default.trial_markerset_name =      'Agreement_dynamic_coloured';
default.sph_trial_markerset_name =  'Agreement_dynamic_coloured';
default.marker_trial_diameter = 0.038; % [m]
default.max_gap_size = 10;
expectedFilters = {'none', 'butter', 'sgolay', 'median'};
default.filterType = 'none';
default.filterType2 = 'none';
default.butterOrder = 0;   % Butterworth filter order
default.butterCutoff = 0;  % cutoff freq. for lowpass Butter. filter (Hz)
default.medianSpan = 0;    % (samples)
default.sgolayOrder = 0;   % Savitzky-Golay filter order
default.sgolaySpan = 0;    % (samples)
default.transform_kinect_to_vicon = false;
default.trcReferenceFrame = '';
default.rotate_vicon_to_opensim = false;

default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'trc_cal_filenames', default.trc_cal_filenames,...
    @(x) validateattributes(x, {'cell'}, {'row'}));
addParameter(p, 'cal_markerset_name', default.cal_markerset_name,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'marker_cal_diameter', default.marker_cal_diameter,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative'}));

addParameter(p, 'trc_rig_filenames', default.trc_rig_filenames,...
    @(x) validateattributes(x, {'cell'}, {'row'}));
addParameter(p, 'rig_markerset_name', default.rig_markerset_name,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'marker_rig_diameter', default.marker_rig_diameter,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative'}));

addParameter(p, 'trc_trial_filenames', default.trc_trial_filenames,...
    @(x) validateattributes(x, {'cell'}, {'row'}));
addParameter(p, 'trial_markerset_name', default.trial_markerset_name,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'sph_trial_markerset_name', default.sph_trial_markerset_name,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'marker_trial_diameter', default.marker_trial_diameter,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative'}));
addParameter(p, 'max_gap_size', default.max_gap_size,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative', 'integer'}));
addParameter(p, 'filterType', default.filterType,...
    @(x) any(validatestring(x, expectedFilters)));
addParameter(p, 'filterType2', default.filterType2,...
    @(x) any(validatestring(x, expectedFilters)));
addParameter(p, 'butterOrder', default.butterOrder,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));
addParameter(p, 'butterCutoff', default.butterCutoff,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));
addParameter(p, 'medianSpan', default.medianSpan,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));
addParameter(p, 'sgolayOrder', default.sgolayOrder,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));
addParameter(p, 'sgolaySpan', default.sgolaySpan,...
    @(x) validateattributes(x, {'numeric'}, {'scalar'}));
addParameter(p, 'transform_kinect_to_vicon',...
    default.transform_kinect_to_vicon, @islogical);
addParameter(p, 'trcReferenceFrame', default.trcReferenceFrame,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'rotate_vicon_to_opensim',...
    default.rotate_vicon_to_opensim, @islogical);
addParameter(p, 'debug_mode',...
    default.debug_mode, @islogical);

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
trc_cal_filenames = p.Results.trc_cal_filenames;
cal_markerset_name = p.Results.cal_markerset_name;
marker_cal_diameter = p.Results.marker_cal_diameter;

trc_rig_filenames = p.Results.trc_rig_filenames;
rig_markerset_name = p.Results.rig_markerset_name;
marker_rig_diameter = p.Results.marker_rig_diameter;

trc_trial_filenames = p.Results.trc_trial_filenames;
trial_markerset_name = p.Results.trial_markerset_name;
sph_trial_markerset_name = p.Results.sph_trial_markerset_name;
marker_trial_diameter = p.Results.marker_trial_diameter;
max_gap_size = p.Results.max_gap_size;
filterType = p.Results.filterType;
filterType2 = p.Results.filterType2;
butterOrder = p.Results.butterOrder;
butterCutoff = p.Results.butterCutoff;
medianSpan = p.Results.medianSpan;
sgolayOrder = p.Results.sgolayOrder;
sgolaySpan = p.Results.sgolaySpan;
transform_kinect_to_vicon = p.Results.transform_kinect_to_vicon;
trcReferenceFrame = p.Results.trcReferenceFrame;
rotate_vicon_to_opensim = p.Results.rotate_vicon_to_opensim;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
fprintf('Pipeline to pre-process Kinect .trc files.\n')
fprintf('� September 2015 Alessandro Timmi.\n\n')



%% Pre-process calibration files
% Preallocate a cell array for the filenames of the output files.
% Even if we skip this step, we need to define this variable, because it is
% an output argument:
trc_processed_cal_filenames = cell(size(trc_cal_filenames));

if ~trc_cal_filenames{1}
    % Skip processing of calibration files:
    fprintf('No calibration files selected.\n\n')
    
else
    fprintf('Pre-processing calibration files...\n')
    
    % Load markerset to be used by centre correction function:
    cal_marker_names = markerset(cal_markerset_name);
    fprintf('Selected markerset name: %s.\n', cal_markerset_name);
    fprintf('Included markers:\n');
    fprintf('\t%s\n', cal_marker_names{:});
    
    % Process selected .trc file(s):
    for i=1:numel(trc_cal_filenames)
        % Generate the destination file name(s).
        [pathstr, fname, ext] = fileparts(trc_cal_filenames{i});
        trc_processed_cal_filenames{i} = fullfile(pathstr,...
            [fname, '_pre-processed', ext]);
        
        % Load the .trc file to be processed and store its content into a
        % struct:
        s = LoadTrcFile(...
            'filename', trc_cal_filenames{i},...
            'logMessage', sprintf('Loading file %d of %d:\n', i,...
            numel(trc_cal_filenames)));
        
        % Only process markers of interest, removing the others (e.g.
        % skeleton joints). NOTE: in marker names, characters after the
        % underscore are ignored.
        s = select_markers(s,...
            'selected_marker_names', cal_marker_names,...
            'debug_mode', debug_mode);
        
        % Apply marker centre correction and get cleaned marker names
        % (removing colours names):
        s = correct_marker_centre(...
            s,...
            cal_marker_names,...
            marker_cal_diameter,...
            'debug_mode', debug_mode);
        
        % Fill gaps in calibration data:
        s = FillGapsTrc(s, max_gap_size);
        
        % Filter calibration data. The median filter is applied here to
        % make the calibration coordinates more stable, improving
        % calibration results:
        s = FilterTrc(s, 'median',...
            'medianSpan', 15);
        
        % Write the resulting .trc file, using cleaned markers names:
        WriteTrcFile(s, 'destinationFilename', trc_processed_cal_filenames{i});
    end
    
    fprintf('End of calibration files pre-processing.\n\n')
end


%% Pre-process calibration files containing 3D-printed rig data used to
% determine the translation vector d_vk from Vicon to Kinect origin.

% Preallocate a cell array for the filenames of the output files.
% Even if we skip this step, we need to define this variable, because it is
% an output argument:
trc_processed_rig_filenames = cell(size(trc_rig_filenames));

if ~trc_rig_filenames{1}
    % Skip processing of rig files:
    fprintf('No rig files selected.\n\n')
    
else
    fprintf('Pre-processing rig files...\n')
    
    % Load markerset to be used by centre correction function:
    rig_marker_names = markerset(rig_markerset_name);
    fprintf('Selected markerset name: %s.\n', rig_markerset_name);
    fprintf('Included markers:\n');
    fprintf('\t%s\n', rig_marker_names{:});
    
    % Process selected .trc file(s):
    for i=1:numel(trc_rig_filenames)
        % Generate the destination file name(s):
        [pathstr, fname, ext] = fileparts(trc_rig_filenames{i});
        trc_processed_rig_filenames{i} = fullfile(pathstr,...
            [fname, '_pre-processed', ext]);
        
        % Load the .trc file to be processed and store its content into a
        % struct:
        s = LoadTrcFile(...
            'filename', trc_rig_filenames{i},...
            'logMessage', sprintf('Loading file %d of %d:\n', i,...
            numel(trc_rig_filenames)));
        
        % Only process markers of interest, removing the others. NOTE: in
        % marker names, characters after the underscore are ignored.
        s = select_markers(s,...
            'selected_marker_names', rig_marker_names,...
            'debug_mode', debug_mode);
        
        % Apply marker centre correction and get cleaned marker names
        % (removing colours names):
        s = correct_marker_centre(...
            s,...
            rig_marker_names,...
            marker_rig_diameter,...
            'debug_mode', debug_mode);
        
        % Fill gaps in rig data:
        s = FillGapsTrc(s, max_gap_size);
        
        % Filter rig data. The median filter is applied here to make rig
        % coordinates more stable, improving calibration results:
        s = FilterTrc(s, 'median',...
            'medianSpan', 15);
        
        % Write the resulting .trc file, using cleaned markers names:
        WriteTrcFile(s, 'destinationFilename', trc_processed_rig_filenames{i});
        
    end
    
    fprintf('End of rig files pre-processing.\n\n')
end


%% Pre-process trial files:
% Preallocate a cell array for the filenames of the output files.
% Even if we skip this step, we need to define this variable, because it is
% an output argument:
trc_processed_trial_filenames = cell(size(trc_trial_filenames));

if ~trc_trial_filenames{1}
    % Skip processing of trial files:
    fprintf('No trial files selected.\n\n')
    
else
    fprintf('Pre-processing trial files...\n')
    
    % Load markerset to be used by centre correction function:
    trial_marker_names = markerset(trial_markerset_name);
    fprintf('Selected markerset name: %s.\n', trial_markerset_name);
    fprintf('Included markers:\n');
    fprintf('\t%s\n', trial_marker_names{:});
    
    sph_trial_marker_names = markerset(sph_trial_markerset_name);
    fprintf('Spherical markerset name: %s.\n', sph_trial_markerset_name);
    fprintf('Spherical markers:\n');
    fprintf('\t%s\n', sph_trial_marker_names{:});
    
    % Process selected .trc file(s):
    for i=1:numel(trc_trial_filenames)
        % Generate the destination file name(s):
        [pathstr, fname, ext] = fileparts(trc_trial_filenames{i});
        trc_processed_trial_filenames{i} = fullfile(pathstr,...
            [fname, '_pre-processed', ext]);
        
        % Load the .trc file to be processed and store its content into a
        % struct:
        s = LoadTrcFile(...
            'filename', trc_trial_filenames{i},...
            'logMessage', sprintf('Loading file %d of %d:\n', i,...
            numel(trc_trial_filenames)));
        
        % Only process markers of interest, removing the others. NOTE: in
        % marker names, characters after the underscore are ignored.
        s = select_markers(s,...
            'selected_marker_names', trial_marker_names,...
            'debug_mode', debug_mode);
        
        % Apply marker centre correction and get cleaned marker names
        % (removing colours names):
        s = correct_marker_centre(...
            s,...
            sph_trial_marker_names,...
            marker_trial_diameter,...
            'debug_mode', debug_mode);
        
        % Fill gaps:
        s = FillGapsTrc(s, max_gap_size);
        
        % Filter trial data:
        if strcmpi(filterType, 'none')
            fprintf('First pass of filtering not requested.\n')
        else
            s = FilterTrc(s, filterType,...
                'butterOrder', butterOrder,...
                'butterCutoff', butterCutoff,...
                'medianSpan', medianSpan,...
                'sgolayOrder', sgolayOrder,...
                'sgolaySpan', sgolaySpan);
        end
        
        % Second pass of filtering, if requested:
        if strcmpi(filterType2, 'none')
            fprintf('Second pass of filtering not requested.\n')
        else
            s = FilterTrc(s, filterType2,...
                'butterOrder', butterOrder,...
                'butterCutoff', butterCutoff,...
                'medianSpan', medianSpan,...
                'sgolayOrder', sgolayOrder,...
                'sgolaySpan', sgolaySpan);
        end
        
        if transform_kinect_to_vicon
            % Transform coordinates of current trial from Kinect to Vicon
            % reference system:
            s = transform_kinect_coordinates_to_vicon(...
                s,...
                'calibration_object', 'L-frame_adjusted',...
                'trcReferenceFrame', trcReferenceFrame,...
                'debug_mode', debug_mode);
        end
        
        if rotate_vicon_to_opensim
            % Rotate coordinates from Vicon to OpenSim reference system:
            s = ViconToOpenSimRotation(s);
        end
        
        % Write the resulting .trc file, using cleaned markers names:
        WriteTrcFile(s, 'destinationFilename', trc_processed_trial_filenames{i});
        
    end
    
    fprintf('End of trial files pre-processing.\n\n')
end


fprintf('End of the pipeline.\n\n')

end
