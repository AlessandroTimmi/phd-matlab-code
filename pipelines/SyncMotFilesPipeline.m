function [viconSyncFilenames, kinectSyncFilenames, meanSdStorage,...
    viconGrfSyncFilenames] = SyncMotFilesPipeline(viconFilenames,...
    kinectFilenames, fieldsToAnalyse, trialRegularExpression, varargin)
% Interp and sync .mot files from Vicon and Kinect for the ACL study,
% to prepare them for statistical analysis. If available, Vicon GRF
% data (.mot) will be synced using the same shift determined for IK data.
%
% Input:
%   viconFilenames = row cell array containing full filenames of trials
%                   from Vicon.
%   kinectFilenames = row cell array containing full filenames of trials
%                   from Kinect.
%   fieldsToAnalyse = cell array with names of the fields to be analysed.
%                   They must be joint angles names (e.g. 'knee_angle').
%                   It is assumed that fields to be analysed have same
%                   names in both Vicon and Kinect data. The order of the
%                   fields in the two datasets is irrelevant.
%   trialRegularExpression = case insensitive regular expression used to
%                   compare the names of current pair of Vicon and Kinect
%                   trials, to see if their subject number, trial type and
%                   trial number match. It needs to contain the following
%                   named tokens: subjectNumber, trialType, trialNumber.
%
%   maxlagSeconds = (optional parameter, default = 2) maximum time lag
%                   (in s) between Kinect and Vicon data. Decimal numbers
%                   are allowed. This parameter is used to constrain the
%                   synchronization problem. Since the lag between Vicon
%                   and Kinect trials is usually not larger than 2 s, this
%                   is a reasonable default value.
%
% Output:
%   viconSyncFilenames = row cell array of filenames of the synced
%                   Vicon trials.
%   kinectSyncFilenames = row cell array of filenames of the synced
%                   Kinect trials.
%   meanSdStorage = array containing the mean standard deviation of the
%                   differences between pairs of signals obtained from
%                   synchronization, for each pair of trials.
%   viconGrfSyncFilenames = row cell array of filenames of the synced Vicon
%                   GRF data. For those trials which don't have GRF data,
%                   an empty cell is returned in this array.
%
% � August 2016 Alessandro Timmi


%% Default input values:
default.maxlagSeconds = 2;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'viconFilenames',...
    @(x) validateattributes(x,{'cell'}, {'nonempty', 'row'}));
addRequired(p, 'kinectFilenames',...
    @(x) validateattributes(x,{'cell'}, {'nonempty', 'row'}));
addRequired(p, 'fieldsToAnalyse',...
    @(x) validateattributes(x, {'cell'}, {'vector', 'nonempty'}));
addRequired(p, 'trialRegularExpression',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'maxlagSeconds', default.maxlagSeconds,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative'}));

% Parse input arguments:
parse(p, viconFilenames, kinectFilenames, fieldsToAnalyse,...
    trialRegularExpression, varargin{:});

% Copy input arguments into more memorable variables:
viconFilenames = p.Results.viconFilenames;
kinectFilenames = p.Results.kinectFilenames;
fieldsToAnalyse = p.Results.fieldsToAnalyse;
trialRegularExpression = p.Results.trialRegularExpression;

maxlagSeconds = p.Results.maxlagSeconds;

clear varargin default p


%%
fprintf('Interpolate and sync pairs of Vicon and Kinect .mot trials\n');


%% Check if the same number of trials has been selected for both Vicon and
% Kinect:
assert(isequal(numel(viconFilenames), numel(kinectFilenames)),...
    'Different number of trials selected for Vicon (%d) and Kinect (%d).',...
    numel(viconFilenames), numel(kinectFilenames));

fprintf('Number of trials selected for each system: %d.\n', numel(viconFilenames));


%% Preallocate cell arrays for the filenames of the synced files:
viconSyncFilenames = cell(size(viconFilenames));
kinectSyncFilenames = cell(size(viconFilenames));
viconGrfSyncFilenames = cell(size(viconFilenames));

% Preallocate array to store the mean standard deviation of the differences
% between pairs of signals obtained from synchronization:
meanSdStorage = zeros(1, length(viconFilenames));

for f = 1:numel(viconFilenames)
    fprintf('\nPair of trials number %d of %d.\n', f, numel(viconFilenames))
    
    % Load .mot files containing joint angles:
    viconRaw = LoadMotFile(...
        'filename', viconFilenames{f},...
        'logMessage', sprintf('Loading Vicon ik.mot file...\n'));
    
    kinectRaw = LoadMotFile(...
        'filename', kinectFilenames{f},...
        'logMessage', sprintf('Loading Kinect ik.mot file...\n'));
    
    
    %% Compare names of current pair of Vicon and Kinect trials using
    % case insensitive regular expressions, to see if subject number,
    % trial type and trial number match:
    CompareTrialsFilenames(viconFilenames{f}, kinectFilenames{f},...
        trialRegularExpression);
    
    
    %% IK results files may have different fields (coordinates) due to the
    % use of different .osim models for analysing Vicon and Kinect data.
    % For this reason, here we don't compare the list of coordinates
    % between Vicon and Kinect data, but we only check for the existence of
    % the coordinates to be used for synchronization:
    viconFieldnames = fieldnames(viconRaw.coords);
    kinectFieldnames = fieldnames(kinectRaw.coords);
    
    assert(all(ismember(fieldsToAnalyse, viconFieldnames)),...
        'One or more requested fields are missing in Vicon coordinates');
    assert(all(ismember(fieldsToAnalyse, kinectFieldnames)),...
        'One or more requested fields are missing in Kinect coordinates');
    
    
    %% RESET TIME ARRAYS
    % Kinect time array usually starts from zero, while Vicon time array from
    % 0.083 (which is 1/120 s). Moreover, sometimes trials might have been trimmed
    % into OpenSim, leaving a time array which starts from different time
    % values. In order to correctly synchronize two different trials, we need
    % to ensure that their starting timestamps are the same:
    viconRaw.time = reset_time_array(viconRaw.time,...
        'log_message', 'Resetting Vicon timestamps array');
    
    kinectRaw.time = reset_time_array(kinectRaw.time,...
        'log_message', 'Resetting Kinect timestamps array');
    
    
    %% INTERPOLATION OF VICON DATA
    % A higher framerate allows a finer synchronization between Vicon and
    % Kinect data. For this reason and after some testing, I decided to
    % increase Vicon framerate to 480 fps.
    
    % Copy Vicon raw struct into the interpolated one, skipping
    % time and coordinates:
    viconInterp = rmfield(viconRaw, {'time', 'coords'});
    
    % Manually set the new data rate to the desired value:
    targetDataRate = 480;
    
    % New time array for Vicon, interpolated from 120 to the new requested rate:
    viconInterp.time = (viconRaw.time(1) : 1/targetDataRate : viconRaw.time(end))';
    
    fprintf('\nInterpolating Vicon data at %g fps...\n\n', targetDataRate);
    
    % Interpolate each field of Vicon's struct using splines. This
    % is not the case (because we specified the same time range of the raw
    % data), but values outside the range might be extrapolated.
    viconInterp.coords = structfun(@(y)...
        ( interp1(viconRaw.time, y, viconInterp.time, 'spline', 'extrap') ),...
        viconRaw.coords, 'UniformOutput', false);
    
    
    %% INTERPOLATION OF KINECT DATA TO MATCH VICON FRAMERATE
    % Copy the Kinect raw struct into the interpolated one, skipping
    % time and marker coordinates:
    kinectInterp = rmfield(kinectRaw, {'time', 'coords'});
    
    % New time array for Kinect, interpolated at Vicon framerate:
    kinectInterp.time = (kinectRaw.time(1) : 1/targetDataRate : kinectRaw.time(end))';
    
    fprintf('\nInterpolating Kinect data at %g fps...\n\n', targetDataRate);
    
    % Interpolate each field of Kinect's struct at Vicon's framerate,
    % using splines. This is not the case (because we specified the same time
    % range of the raw data) but values outside the range might be extrapolated.
    kinectInterp.coords = structfun(@(y)...
        ( interp1(kinectRaw.time, y, kinectInterp.time, 'spline', 'extrap') ),...
        kinectRaw.coords, 'UniformOutput', false);
    
    
    %% SYNC THE TRIALS
    % Convert max lag from s to frames. We round because frames must always
    % be integers:
    maxlag = round(maxlagSeconds * targetDataRate);
    fprintf('Max lag was set to %0.1f s (%d frames).\n', maxlagSeconds, maxlag);
    
    % Rename "coords" into "markers", because the sync function was
    % developed for structs containing .trc data (marker coordinates):
    viconInterp = RenameStructField(viconInterp, 'coords', 'markers');
    kinectInterp = RenameStructField(kinectInterp, 'coords', 'markers');
    
    % NOTE: the order in which Vicon and Kinect trials are passed here
    % matters for the synchronization of the GRF (see below).
    % If shift > 0, Vicon data were shifted backwards. If shift < 0, Kinect
    % data were shifted backwards.
    [viconSync, kinectSync, shift, meanSdStorage(f)] =...
        SyncStructs(viconInterp, kinectInterp,...
        fieldsToAnalyse,...
        'maxlag', maxlag);
    
    fprintf('Shift: %g frames.\n', shift)
    
    % Restore original field names:
    viconSync = RenameStructField(viconSync, 'markers', 'coords');
    kinectSync = RenameStructField(kinectSync, 'markers', 'coords');
    
    
    %% SAVE SYNCED TRIALS DATA INTO .MOT FILES
    fprintf('Saving synced data into .mot files...\n');
    
    % Generate the destination file name(s):
    [viconPathstr, viconFname, viconExt] = fileparts(viconFilenames{f});
    viconSyncFilenames{f} = fullfile(viconPathstr,...
        [viconFname, '_sync', viconExt] );
    
    [kinectPathstr, kinectFname, kinectExt] = fileparts(kinectFilenames{f});
    kinectSyncFilenames{f} = fullfile(kinectPathstr,...
        [kinectFname, '_sync', kinectExt] );
    
    WriteMotFile(viconSync,...
        'destinationFilename', viconSyncFilenames{f});
    WriteMotFile(kinectSync,...
        'destinationFilename', kinectSyncFilenames{f});
    
    fprintf('Vicon:\t%s\n', viconSyncFilenames{f});
    fprintf('Kinect:\t%s\n', kinectSyncFilenames{f});
    
    
    %% Sync ground reaction forces (GRF) data from Vicon (if available),
    % using the shift determined for joint angles. In this case:
    % - a positive shift means the Vicon trial was moved backwards,
    % - a negative shift means the Kinect trial was moved backwards.
    % NOTE: AMTI force plates at CHESM lab have a sampling frequency of
    % 2400 Hz.
    viconInputGrfFilename = fullfile(viconPathstr,...
        [viconFname(1:end-2), 'grf', viconExt]);
    
    % If the current Vicon trial has a corresponding GRF file:
    if isequal(exist(viconInputGrfFilename, 'file'), 2)
        
        % Load the .mot file containing GRF:
        viconGrf = LoadMotFile('filename', viconInputGrfFilename);
        
        % Reset time array of GRF data, to ensure it starts from 0 instead
        % of 0.0083 s. It is important to apply this step here for
        % consistency with IK results, otherwise we introduce a sync error in
        % the GRF data.
        viconGrf.time = reset_time_array(viconGrf.time,...
            'log_message', 'Resetting Vicon GRF timestamps array');
        
        % Get average GRF sampling frequency for current trial:
        [~, ~, ~, grfRate] = check_sampling_freq(viconGrf.time);
        fprintf('GRF sampling rate: %0.1f\n', grfRate)
        
        % If Vicon IK results were shifted, then shift Vicon GRF too:
        if shift > 0
            % Convert the shift from the interpolated framerate used for
            % IK results to the framerate of the force plates:
            shiftGrf = round(shift / targetDataRate * grfRate);
            % Vicon marker coordintes were shifted back, so we need to do
            % the same for Vicon GRF:
            viconGrf.coords = structfun(@(y) ( circshift(y, -shiftGrf, 1) ),...
                viconGrf.coords, 'UniformOutput', false);
        end               
        
        % Check that Kinect and Vicon synced IK results have same length:
        assert(isequal(length(kinectSync.time), length(viconSync.time)),...
            'Kinect and Vicon IK results have different length')
        % Convert trial length from marker coordinates framerate to GRF
        % framerate:        
        newGrfLength = round(length(viconSync.time) / targetDataRate * grfRate);
        % Trim GRF data to match the length of IK results:
        viconGrf.coords = structfun(@(y) ( y(1:newGrfLength) ),...
            viconGrf.coords, 'UniformOutput', false);        
        viconGrf.time = viconGrf.time(1:newGrfLength);  
        
        % Save the synced Vicon GRF into a .mot file:
        viconGrfSyncFilenames{f} = fullfile(viconPathstr,...
            [viconFname(1:end-2), 'grf_sync', viconExt]);
        
        WriteMotFile(viconGrf,...
            'destinationFilename', viconGrfSyncFilenames{f});
        
    else
        fprintf('GRF data not available for current Vicon trial.\n')
    end
    
    
    
    
end


end