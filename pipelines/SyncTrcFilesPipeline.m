function [vicon_sync_filenames, kinect_sync_filenames] =...
    SyncTrcFilesPipeline(vicon_filenames, kinect_filenames,...
    fields_to_analyse, trialRegularExpression, varargin)
% Interp and sync .trc files from Vicon and Kinect.
%
% Input:
%   vicon_filenames = row cell array containing full filenames of trials
%                   from Vicon.
%   kinect_filenames = row cell array containing full filenames of trials
%                   from Kinect.
%   fields_to_analyse = cell array with names of the fields to be analysed.
%                   They must be marker names (e.g. 'RASI'). It is assumed
%                   that fields to be analysed have same names in both
%                   Vicon and Kinect data. The order of the fields in the
%                   two datasets is irrelevant.
%   trialRegularExpression = regular expression used to compare the names
%                   of current pair of Vicon and Kinect trials, to see if
%                   their subject number, trial type and trial number match.
%
%   coords_to_sync = (optional parameter, default = {'X', 'Y', 'Z'}) cell
%                   array with coordinates to be used for synchronization.
%                   If multiple coordinates are defined, synchronization
%                   will be based on all of them.
%   maxlag_s        = (optional parameter, default = 2) maximum time lag
%                   (in s) between Kinect and Vicon data. Decimal numbers
%                   are allowed. This parameter is used to constrain the
%                   synchronization problem. Since the lag between Vicon
%                   and Kinect trials is usually not larger than 2 s, this
%                   is a reasonable default value.
%   debug_mode =    (optional parameter, default = true). If true, displays
%                   some extra info for debug purposes.
%
% Output:
%   vicon_sync_filenames = row cell array of filenames of the synced
%                   Vicon trials.
%   kinect_sync_filenames = row cell array of filenames of the synced
%                   Kinect trials.
%
%
% � June 2016 Alessandro Timmi


%% Default input values:
default.coords_to_sync = {'X', 'Y', 'Z'};
default.maxlag_s = 2;
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'vicon_filenames',...
    @(x) validateattributes(x,{'cell'}, {'nonempty', 'row'}));
addRequired(p, 'kinect_filenames',...
    @(x) validateattributes(x,{'cell'}, {'nonempty', 'row'}));
addRequired(p, 'fields_to_analyse',...
    @(x) validateattributes(x, {'cell'}, {'vector', 'nonempty'}));
addRequired(p, 'trialRegularExpression',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'coords_to_sync', default.coords_to_sync,...
    @(x) validateattributes(x, {'cell'}, {'vector', 'nonempty'}));
addParameter(p, 'maxlag_s', default.maxlag_s,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative'}));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, vicon_filenames, kinect_filenames, fields_to_analyse,...
    trialRegularExpression, varargin{:});

% Copy input arguments into more memorable variables:
vicon_filenames = p.Results.vicon_filenames;
kinect_filenames = p.Results.kinect_filenames;
fields_to_analyse = p.Results.fields_to_analyse;
trialRegularExpression = p.Results.trialRegularExpression;

coords_to_sync = p.Results.coords_to_sync;
maxlag_s = p.Results.maxlag_s;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
fprintf('Interpolate and sync pairs of Vicon and Kinect .trc trials\n');


%% Check if the same number of trials has been selected for both Vicon and
% Kinect:
assert(isequal(numel(vicon_filenames), numel(kinect_filenames)),...
    'Different number of trials selected for Vicon (%d) and Kinect (%d).',...
    numel(vicon_filenames), numel(kinect_filenames));

fprintf('Number of trials selected for each system: %d.\n', numel(vicon_filenames));


%% Preallocate cell arrays for the filenames of the synced files:
vicon_sync_filenames = cell(size(vicon_filenames));
kinect_sync_filenames = vicon_sync_filenames;


%% PRE-PROCESS EACH PAIR OF TRIALS:
for f=1:numel(vicon_filenames)
    fprintf('\nPair of trials number %d of %d.\n', f, numel(vicon_filenames))
    
    % Load trials:
    vicon_raw = LoadTrcFile(...
        'filename', vicon_filenames{f},...
        'logMessage', sprintf('Loading Vicon .trc file...\n'));
    
    kinect_raw = LoadTrcFile(...
        'filename', kinect_filenames{f},...
        'logMessage', sprintf('Loading Kinect .trc file...\n'));
    
    
    %% Compare names of current pair of Vicon and Kinect trials using
    % regular expressions, to see if subject number, trial type and trial
    % number match:
    vicon_trial_name = regexp(vicon_filenames{f}, trialRegularExpression, 'match');
    kinect_trial_name = regexp(kinect_filenames{f}, trialRegularExpression, 'match');
    
    assert(isequal(vicon_trial_name{1}, kinect_trial_name{1}),...
        'Strings mismatch:\nVicon:\t\t%s\nKinect:\t\t%s',...
        vicon_trial_name{1}, kinect_trial_name{1});
    
    
    %% Check field names and number of coordinates for current trials:
    if f == 1
        % Read field names from current trials, sorting them in
        % alphabetical order:
        vicon_sorted_fieldnames = sort(fieldnames(vicon_raw.markers));
        kinect_sorted_fieldnames = sort(fieldnames(kinect_raw.markers));
        
        % Check that Vicon and Kinect trials have same field names. The
        % order doesn't matter:
        if isequal(vicon_sorted_fieldnames, kinect_sorted_fieldnames)
            sorted_fieldnames = vicon_sorted_fieldnames;
        else
            error('Vicon and Kinect marker names don''t match for this trial.')
        end
    else
        % Compare field names of current trials with those from previous
        % trials, to ensure consistency:
        assert(isequal(sort(fieldnames(vicon_raw.markers)), sorted_fieldnames),...
            'Field names from Vicon trial number %d don''t match those from previous trials.', f);
        assert(isequal(sort(fieldnames(kinect_raw.markers)), sorted_fieldnames),...
            'Field names from Kinect trial number %d don''t match those from previous trials.', f);
    end
    
    % Number of coordinates per field. Markers have 3 coords: X, Y and Z.
    % Here we assume that all fields in a trial have the same number of
    % coordinates, so we check only the first field for each trial vs the
    % expected number of columns:
    n_cols = 3;
    n_cols_vicon = size(vicon_raw.markers.(vicon_sorted_fieldnames{1}), 2);
    n_cols_kinect = size(kinect_raw.markers.(kinect_sorted_fieldnames{1}), 2);
    
    assert(isequal(n_cols, n_cols_vicon),...
        'Wrong number of coordinates for Vicon fields: expected %d, found %d.', n_cols, n_cols_vicon);
    assert(isequal(n_cols, n_cols_kinect),...
        'Wrong number of coordinates for Kinect fields: expected %d, found %d.', n_cols, n_cols_kinect);
    
    
    %% RESET TIME ARRAYS
    % Kinect time array usually starts from zero, while Vicon time array from
    % 0.083 (which is 1/120 s). Moreover, sometimes trials might have been trimmed
    % into OpenSim, leaving a time array which starts from different time
    % values. In order to correctly synchronize two different trials, we need
    % to ensure that their starting timestamps are the same:
    vicon_raw.time = reset_time_array(vicon_raw.time,...
        'log_message', 'Resetting Vicon timestamps array',...
        'debug_mode', debug_mode);
    
    kinect_raw.time = reset_time_array(kinect_raw.time,...
        'log_message', 'Resetting Kinect timestamps array',...
        'debug_mode', debug_mode);
    
    
    %% INTERPOLATION OF VICON DATA
    % A higher framerate allows a finer synchronization between Vicon and
    % Kinect data. For this reason and after some testing, I decided to
    % increase Vicon framerate to 480 fps.
    
    % Copy Vicon raw struct into the interpolated one, skipping
    % time and marker coordinates:
    vicon_interp = rmfield(vicon_raw, {'time', 'markers'});
    
    % Manually set the new data rate to the desired value:
    vicon_interp.DataRate = 480;
    
    % New time array for Vicon, interpolated from 120 to the new requested rate:
    vicon_interp.time = (vicon_raw.time(1) : 1/vicon_interp.DataRate : vicon_raw.time(end))';
    
    fprintf('\nInterpolating Vicon data at %g fps...\n\n', vicon_interp.DataRate);
    
    % Interpolate each field of Vicon's struct using splines. This
    % is not the case (because we specified the same time range of the raw
    % data), but values outside the range might be extrapolated.
    vicon_interp.markers = structfun(@(y)...
        ( interp1(vicon_raw.time, y, vicon_interp.time, 'spline', 'extrap') ),...
        vicon_raw.markers, 'UniformOutput', false);
    
    % Update info in Vicon interpolated struct, calculating them from data:
    vicon_interp = UpdateTrcStructInfo(vicon_interp, 'debug_mode', debug_mode);
    
    
    %% INTERPOLATION OF KINECT DATA TO MATCH VICON FRAMERATE
    % Copy the Kinect raw struct into the interpolated one, skipping
    % time and marker coordinates:
    kinect_interp = rmfield(kinect_raw, {'time', 'markers'});
    
    % New time array for Kinect, interpolated at Vicon framerate:
    kinect_interp.time = (kinect_raw.time(1) : 1/vicon_interp.DataRate : kinect_raw.time(end))';
    
    fprintf('\nInterpolating Kinect data at %g fps...\n\n', vicon_interp.DataRate);
    
    % Interpolate each field of Kinect's struct at Vicon's framerate,
    % using splines. This is not the case (because we specified the same time
    % range of the raw data) but values outside the range might be extrapolated.
    kinect_interp.markers = structfun(@(y)...
        ( interp1(kinect_raw.time, y, kinect_interp.time, 'spline', 'extrap') ),...
        kinect_raw.markers, 'UniformOutput', false);
    
    % Update info in Kinect interpolated struct, calculating them from data:
    kinect_interp = UpdateTrcStructInfo(kinect_interp, 'debug_mode', debug_mode);
    
    
    %% SYNC THE TRIALS
    % Convert max lag from s to frames. We round because frames must always
    % be integers.
    maxlag = round(maxlag_s * vicon_interp.DataRate);
    fprintf('Max lag was set to %0.1f s (%d frames).\n', maxlag_s, maxlag);
    
    [vicon_sync, kinect_sync, shift] =...
        SyncStructs(vicon_interp, kinect_interp,...
        fields_to_analyse,...
        'coords_to_sync', coords_to_sync,...
        'maxlag', maxlag);
    
    fprintf('Shift: %g frames.\n', shift)
    
    
    %% SAVE SYNCED TRIALS DATA INTO .TRC FILES
    fprintf('Saving synced data into .trc files...\n');
    
    % Generate the destination file name(s):
    [vicon_pathstr, vicon_fname, vicon_ext] = fileparts(vicon_filenames{f});
    vicon_sync_filenames{f} = fullfile(vicon_pathstr,...
        [vicon_fname, '_sync', vicon_ext] );
    
    [kinect_pathstr, kinect_fname, kinect_ext] = fileparts(kinect_filenames{f});
    kinect_sync_filenames{f} = fullfile(kinect_pathstr,...
        [kinect_fname, '_sync', kinect_ext] );
    
    WriteTrcFile(vicon_sync,...
        'destinationFilename', vicon_sync_filenames{f});
    WriteTrcFile(kinect_sync,...
        'destinationFilename', kinect_sync_filenames{f});
    
    fprintf('Vicon:\t%s\n', vicon_sync_filenames{f});
    fprintf('Kinect:\t%s\n', kinect_sync_filenames{f});
    
end


end