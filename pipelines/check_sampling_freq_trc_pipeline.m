% Load any number of .trc files and check their sampling frequency in
% batch.
% � January 2016 Alessandro Timmi


% TODO:
% - ask to save the report in a txt file
% - add support for .mot files
% - add input parser



debug_mode = false;

%%
fprintf('Checking sampling frequency...\n')
if debug_mode
    fprintf('[Debug] � January 2016 Alessandro Timmi.\n');
end
fprintf('\n')

[kinect_fname, kinect_path] = uigetfile({...
    '*.trc', 'Trace files'},...
    'Select .trc files to be checked',...
    'MultiSelect', 'on');
kinect_filenames = fullfile(kinect_path, kinect_fname);

% If a single file is selected, the output of the dialog is a string. We
% want a cell array in any case (single or multiple selection):
if ~iscell(kinect_filenames)
    kinect_filenames = {kinect_filenames};
end

for f=1:numel(kinect_filenames)
    s = LoadTrcFile(...
        'filename', kinect_filenames{f},...
        'logMessage', sprintf('Loaded file %d of %d.\n', f, numel(kinect_filenames)),...
        'dialogTitle', sprintf('Select Kinect .trc file\n'));
       
    check_sampling_freq(s.time, 'debug_mode', true);
    fprintf('\n')
end