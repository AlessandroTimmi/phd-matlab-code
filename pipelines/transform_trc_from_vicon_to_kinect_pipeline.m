function trc_transformed_trial_filenames =...
    transform_trc_from_vicon_to_kinect_pipeline(varargin)
% Apply coordinate transformation from Vicon to Kinect reference frame.
%
% Input:
%   trc_orientation_filename = (optional parameter, default = '') filename
%                of the .trc trial containing coordinates of reflective
%                markers attached on Kinect top surface, used for
%                estimating Kinect orientation in Vicon space. If the
%                string is empty, a dialog will open to select a .trc file.
%	trc_rig_vicon_filename = (optional parameter, default = '') filename
%                of the .trc trial containing coordinates of a 3D printed
%                rig in Vicon space. This point will be used to determine
%                the translation vector from Vicon to Kinect origin. If
%                the string is empty, a dialog will open to select a .trc
%                file. 
%	trc_rig_kinect_filename = (optional parameter, default = '') filename
%                of .trc trial containing coordinates of a 3D printed rig
%                in Kinect space. This point will be used to determine the
%                translation vector from Vicon to Kinect origin. If the
%                string is empty, a dialog will open to select a .trc file.
%   trc_trial_filenames = (optional parameter, default = {}) row cell array
%                containing full filenames of the .trc file(s) to be
%                transformed.
%   debug_mode = (optional parameter, default = false) if true, shows some
%                extra info for debug purposes.
%
% Output:
%   trc_transformed_trial_filenames = row cell array containing the full
%                filenames of the transformed trials.
%
% � October 2015 Alessandro Timmi.


%% Default input values:
default.trc_orientation_filename = '';
default.trc_rig_vicon_filename = '';
default.trc_rig_kinect_filename = '';
default.trc_trial_filenames = {};
default.debug_mode = false;


% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'trc_orientation_filename', default.trc_orientation_filename,...
    @(x)validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'trc_rig_vicon_filename', default.trc_rig_vicon_filename,...
    @(x)validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'trc_rig_kinect_filename', default.trc_rig_kinect_filename,...
    @(x)validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'trc_trial_filenames', default.trc_trial_filenames,...
    @(x)validateattributes(x, {'cell'}, {'nonempty', 'row'}));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
trc_orientation_filename = p.Results.trc_orientation_filename;
trc_rig_vicon_filename = p.Results.trc_rig_vicon_filename;
trc_rig_kinect_filename = p.Results.trc_rig_kinect_filename;
trc_trial_filenames = p.Results.trc_trial_filenames;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
fprintf('Pipeline to transform coordinates from Vicon to Kinect reference frame\n');
if debug_mode
    fprintf('� October 2015 Alessandro Timmi.\n\n');
end



%% Preallocate a cell array for the filenames of the transformed files:
trc_transformed_trial_filenames = cell(size(trc_trial_filenames));


% Process selected .trc file(s):
for i=1:numel(trc_trial_filenames)
    % Load the .trc file to be processed and store its content into a
    % struct:
    s = LoadTrcFile(...
        'filename', trc_trial_filenames{i},...
        'logMessage', sprintf('Loading file %d of %d:\n', i, numel(trc_trial_filenames)));
    
    
    
    % Transforming coordinates of current trial from Vicon to Kinect
    % reference system (alternative method using L-frame):
    %         if i==1
    %             % For the first trial, ask which file to use for calibration:
    %             [s, calibration_object, trc.calibration_trial] =...
    %                 transform_vicon_coordinates_to_kinect(...
    %                 s,...
    %                 'debug_mode', debug_mode);
    %         else
    %             % For the following trials, re-use the same calibration
    %             % file selected for the first iteration:
    %             s = transform_vicon_coordinates_to_kinect(...
    %                 s,...
    %                 'calibration_object', calibration_object,...
    %                 'trcReferenceFrame', trc.calibration_trial,...
    %                 'debug_mode', debug_mode);
    %         end
    
    
    if i==1
        % For the first trial, ask which files to use for calibration:
        [s, trc_orientation_filename, trc_rig_vicon_filename,...
            trc_rig_kinect_filename] = transform_vicon_coords_to_kinect_with_refl_markers(s,...
            'orientation_trc', trc_orientation_filename,...
            'rig_vicon_trc', trc_rig_vicon_filename,...
            'rig_kinect_trc', trc_rig_kinect_filename,...
            'debug_mode', debug_mode);
    else
        % For the following trials, re-use the same calibration
        % files selected for the first iteration:
        s = transform_vicon_coords_to_kinect_with_refl_markers(s,...
            'orientation_trc', trc_orientation_filename,...
            'rig_vicon_trc', trc_rig_vicon_filename,...
            'rig_kinect_trc', trc_rig_kinect_filename,...
            'debug_mode', debug_mode);
    end
    
    % Generate the destination file name(s):
    [pathstr, fname, ext] = fileparts(trc_trial_filenames{i});   
    trc_transformed_trial_filenames{i} = fullfile(pathstr,...
        [fname, '_transformed', ext] );
    
    % Write the resulting .trc into an ASCII file:
    WriteTrcFile(s, 'destinationFilename', trc_transformed_trial_filenames{i});
end

end



