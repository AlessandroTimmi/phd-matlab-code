function StopProgramExecution()
% This is the easiest way to stop the execution of a Matlab program:
% http://au.mathworks.com/matlabcentral/newsreader/view_thread/246447
%
% � August 2016 Alessandro Timmi

error('Not really an error, just User stopping the execution :)');