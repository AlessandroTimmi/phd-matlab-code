%ANTHROPOMETRY15 Funzione per il calcolo delle caratteristiche
%   antropometriche relative ai 15 corpi rigidi tramite i quali �
%   schematizzato il corpo umano, il cui movimento � acquisito tramite il
%   software Optitrack Arena ed i dati sono esportati in formato C3D.
%
%   MASSE, LUNGHEZZE, POSIZIONI DEI BARICENTRI (COM - center of mass) e
%   MOMENTI PRINCIPALI DI INERZIA dei segmenti articolari calcolate secondo
%   lo studio di Shan e Bohn (2003) relativo a maschi tedeschi
%   (tipo umano Caucasico);
%
% TO-DO AGGIUNGERE TIPO UMANO ASIATICO (Cinese)
%
%   I riferimenti locali sono stati scelti per praticit� allo stesso modo
%   in cui sono definiti nello studio di Shan-Bohn: questo permette
%   l'adozione dei valori tabellati per i momenti di inerzia senza
%   ulteriori elaborazioni. In particolare:
%
%   - l'ORIGINE di ogni riferimento locale � supposta coincidente col
%   BARICENTRO del rispettivo segmento articolare, a sua volta 
%   determinato come CENTROIDE (baricentro) DEL SISTEMA DI PUNTI (marker)
%   che lo individua;
%
%   - l'ORIENTAMENTO degli assi dei riferimenti locali � parallelo ed
%   equiverso rispetto a quello degli assi assoluti, quando l'atleta sia
%   disposto secondo la i-pose.
%
%   Dato il particolare orientamento degli assi locali rispetto ai
%   segmenti articolari risultante da questa convenzione (vedi figura
%   i-pose), si pu�, con buona approssimazione, assumere i riferimenti
%   locali scelti come SISTEMI PRINCIPALI DI INERZIA degli arti e,
%   conseguentemente, i momenti di inerzia calcolati come MOMENTI
%   PRINCIPALI DI INERZIA dei vari segmenti articolari.
%
%
%   Si considerano UT e MT come un corpo unico (2-CHEST); la massa e la
%   lunghezza del corpo risultante sono pari alla somma delle stesse
%   caratteristiche dei due componenti. Poich� i momenti di inerzia sono
%   grandezze additive, per calcolare quelli del corpo composto sistima la
%   posizione del baricentro di quest'ultimo; poi si calcolano i momenti di
%   inerzia principali dei componenti e le grandezze md^2
%   (secondo il teorema di Huygens-Steiner); infine si somma il tutto.
%
%   ID dei corpi rigidi del manichino da 34 marker di Arena:
%   1) Hip
%   2) Chest
%   3) Head
%   4) LUArm
%   5) LHand
%   6) RUArm
%   7) RHand
%   8) LThigh
%   9) LShin
%   10) LFoot
%   11) RThigh
%   12) RShin
%   13) RFoot
%   14) LForearm
%   15) RForearm
%
%
% Input:
%   w       =   massa corporea complessiva dell'atleta [kg];
%   h       =   altezza [cm];
%   g       =   sesso (m/f);
%   nRB     =   n� corpi rigidi costituenti il manichino di Arena (15)
%
% Output:
%   m(nRB)      =   massa [kg] di ogni corpo rigido;
%   J(3,3,nRB)  =   matrice 3x3 diagonale contenente i momenti principali
%                   di inerzia [kg*m^2] di ogni corpo rigido relativi al
%                   riferimento locale centrato nel baricentro.
%
%
% Written by Alessandro Timmi


function[m,J]=anthropometry15(w,h,g,nRB)

% Scelta dei coefficienti di Shan-Bohn in base al sesso dell'atleta
% analizzato:
switch g
    case 'm'
        % Array dei coefficienti di regressione relativi a maschi tedeschi
        % per il calcolo delle MASSE [kg] dei vari segmenti articolari:
        b_m_Germans=[-7.75 0.0586 0.0497;       % 1) Head
            -4.13 0.175 0.0248;                 % 2) UT
            11.7 0.120 -0.0633;                 % 3) MT
            13.1 0.162 -0.0873;                 % 4) LT
            1.18 0.182 -0.0259;                 % 5) Thigh
            -3.53 0.0306 0.0268;                % 6) Shank
            -2.25 0.0010 0.0182;                % 7) Foot
            -0.896 0.0252 0.0051;               % 8) UA
            -0.731 0.0047 0.0084;               % 9) FA
            -0.325 -0.0016 0.0051];             % 10) Hand
        
        % Array dei coefficienti di regressione relativi a maschi tedeschi
        % per il calcolo delle LUNGHEZZE [cm] dei vari segmenti articolari:
        b_L_Germans=[1.95 0.0535 0.105;         % 1) Head
            -30.4 -0.0156 0.324;                % 2) UT
            -1.71 -0.0794 0.138;                % 3) MT
            26.4 0.0473 -0.0311;                % 4) LT
            4.26 -0.0183 0.240;                 % 5) Thigh
            -16.0 0.0218 0.321;                 % 6) Shank
            3.80 0.0130 0.119;                  % 7) Foot
            -15.0 0.0120 0.229;                 % 8) UA
            0.143 -0.0281 0.161;                % 9) FA
            -3.70 0.0036 0.131];                % 10) Hand
        
        % Array dei coefficienti di regressione relativi a maschi tedeschi
        % per il calcolo dei MOMENTI PRINCIPALI DI INERZIA [kg cm^2] dei
        % vari segmenti articolari (NOTA: suppongo che Izy sia un refuso e
        % lo considero Iz, come da nota "Abbreviations" della Tabella 6):
        b_Ix_Germans=[-1450 6.29 7.36;          % 1) Head
            -3034 40.1 10.5;                    % 2) UT
            1471 22.7 -12.4;                    % 3) MT
            1532 30.1 -15.8;                    % 4) LT
            -1884 41.4 3.20;                    % 5) Thigh
            -1504 5.34 8.76;                    % 6) Shank
            -23.1 0.0030 0.155;                 % 7) Foot
            -509 1.68 2.82;                     % 8) UA
            -160 -0.0113 1.26;                  % 9) FA
            -20.3 -0.0284 0.158];               % 10) Hand
        
        b_Iy_Germans=[-1632 6.06 8.86;          % 1) Head
            -2321 17.7 10.7;                    % 2) UT
            440 9.93 -3.89;                     % 3) MT
            788 23.6 -10.3;                     % 4) LT
            -2028 41.2 4.09;                    % 5) Thigh
            -1389 4.45 8.19;                    % 6) Shank
            -105 0.116 0.703;                   % 7) Foot
            -471 1.85 2.55;                     % 8) UA
            -158 -0.0327 1.23;                  % 9) FA
            -21.3 -0.0219 0.168];               % 10) Hand
        
        b_Iz_Germans=[-498 3.67 2.40;           % 1) Head
            16.7 45.8 -10.1;                    % 2) UT
            2535 31.2 -21.8;                    % 3) MT
            1639 34.2 -18.3;                    % 4) LT
            282 13.8 -5.46;                     % 5) Thigh
            -167 1.73 0.707;                    % 6) Shank
            -100 0.119 0.674;                   % 7) Foot
            26.0 0.711 -0.309;                  % 8) UA
            -11.4 0.0844 0.0708;                % 9) FA
            -5.11 -0.0084 0.0467];              % 10) Hand
        
        % Array delle posizioni dei BARICENTRI (COM, Center Of Mass) dei
        % segmenti articolari calcolate per maschi tedeschi.
        % Posizione X del centro di massa espressa in percentuale sulla
        % lunghezza dell'arto a partire dall'alto:
        COM_percent=[40.2;                      % 1) Head
            55.8;                               % 2) UT
            48.9;                               % 3) MT
            42.6;                               % 4) LT
            32.2;                               % 5) Thigh
            46.4;                               % 6) Shank
            45.1;                               % 7) Foot
            44.9;                               % 8) UA
            43.1;                               % 9) FA
            39.8];                              % 10) Hand
        
    case 'f'
        % Array dei coefficienti di regressione relativi a femmine tedesche per
        % il calcolo delle MASSE [kg] dei vari segmenti articolari:
        b_m_Germans=[-2.95 0.0359 0.0322;       % 1) Head
            25.5 0.228 -0.160;                  % 2) UT
            -1.45 0.0975 0.0176;                % 3) MT
            1.10 0.104 -0.0027;                 % 4) LT
            -10.9 0.213 0.0380;                 % 5) Thigh
            -0.563 0.0191 0.0141;               % 6) Shank
            -1.27 0.0045 0.0104;                % 7) Foot
            3.05 0.0184 -0.0164;                % 8) UA
            -0.481 0.0087 0.0043;               % 9) FA
            -1.13 0.0031 0.0074];               % 10 )Hand
        
        % Array dei coefficienti di regressione relativi a femmine tedesche
        % per il calcolo delle LUNGHEZZE [cm] dei vari segmenti articolari:
        b_L_Germans=[-8.95 -0.0057 0.202;       % 1) Head
            13.0 0.175 0.0310;                  % 2) UT
            -2.52 -0.0459 0.116;                % 3) MT
            21.4 0.0146 -0.0050;                % 4) LT
            -26.8 -0.0725 0.436;                % 5) Thigh
            -7.21 -0.0618 0.308;                % 6) Shank
            7.39 0.0311 0.0867;                 % 7) Foot
            2.44 -0.0169 0.146;                 % 8) UA
            -8.57 0.0494 0.180;                 % 9) FA
            -8.96 0.0057 0.163];                % 10) Hand
        
        % Array dei coefficienti di regressione relativi a femmine tedesche
        % per il calcolo dei MOMENTI PRINCIPALI DI INERZIA [kg cm^2] dei
        % vari segmenti articolari (suppongo che Izy sia un refuso e lo
        % considero Iz, come da nota "Abbreviations" della Tabella 6):
        b_Ix_Germans=[-768 2.44 5.20;           % 1) Head
            5096 51.1 -39.3;                    % 2) UT
            -1042 14.5 3.70;                    % 3) MT
            -356 12.4 1.31;                     % 4) LT
            -6984 40.6 34.5;                    % 5) Thigh
            -703 2.47 5.32;                     % 6) Shank
            -12.1 0.030 0.0781;                 % 7) Foot
            43.8 1.15 -0.202;                   % 8) UA
            -121 0.622 0.710;                   % 9) FA
            -16.3 0.0409 0.102];                % 10) Hand
        
        b_Iy_Germans=[-952 2.70 6.50;           % 1) Head
            2303 34.0 -20.2;                    % 2) UT
            -319 6.40 1.08;                     % 3) MT
            -129 11.0 -0.79;                    % 4) LT
            -6342 35.5 32.5;                    % 5) Thigh
            -690 1.86 5.22;                     % 6) Shank
            -78.1 0.181 0.496;                  % 7) Foot
            46.1 1.11 -0.190;                   % 8) UA
            -122 0.598 0.714;                   % 9) FA
            -17.6 0.0436 0.113];                % 10) Hand
        
        b_Iz_Germans=[-540 1.43 3.583;          % 1) Head
            5072 37.7 -36.5;                    % 2) UT
            -716 18.4 0.301;                    % 3) MT
            -314 17.5 -0.862;                   % 4) LT
            -755 14.3 0.766;                    % 5) Thigh
            -21.5 1.03 0.147;                   % 6) Shank
            -73.8 0.194 0.465;                  % 7) Foot
            69.6 0.384 -0.475;                  % 8) UA
            -4.09 0.0841 0.0176;                % 9) FA
            -2.27 0.0148 0.0157];               % 10) Hand
        
        % Array delle posizioni dei BARICENTRI (COM, Center Of Mass) dei
        % segmenti articolari calcolate per femmine tedesche.
        % Posizione X del centro di massa espressa in percentuale sulla
        % lunghezza dell'arto a partire dall'alto:
        COM_percent=[42.1;                      % 1) Head
            60.9;                               % 2) UT
            50.9;                               % 3) MT
            42.1;                               % 4) LT
            30.3;                               % 5) Thigh
            46.3;                               % 6) Shank
            46.1;                               % 7) Foot
            45.2;                               % 8) UA
            42.9;                               % 9) FA
            38.8];                              % 10) Hand
    otherwise
        errordlg('Wrong gender input. Gender must be "m" or "f"','Gender Error');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calcolo delle caratteristiche antropometriche secondo la suddivisione del
% corpo umano operata nello studio di Shan e Bohn.
% FORMULA per il calcolo del generico dato antropometrico y:
% y=b0+b1*BM+b2*BH (tutto in [kg] e [cm])

% ID dei segmenti articolari usati nello studio di Shan e Bohn:
%   1)  Head
%   2)  UT
%   3)  MT 
%   4)  LT
%   5)  Thigh
%   6)  Shank
%   7)  Foot
%   8)  UA
%   9)  FA
%   10) Hand


% MASSA di ogni segmento articolare (si ottiene un vettore colonna):
m_individual=b_m_Germans(:,1)+b_m_Germans(:,2)*w+b_m_Germans(:,3)*h;


% LUNGHEZZA di ogni segmento articolare (si ottiene un vettore colonna).
% Per ottenere le lunghezze in [m], necessarie per il calcolo
% dell'energia cinetica in [J], moltiplico tutto per 0.01 perch� la
% formula di Shan-Bohn restituisce lunghezze in [cm];
L_individual=0.01*(b_L_Germans(:,1)+b_L_Germans(:,2)*w+b_L_Germans(:,3)*h);


% MOMENTI PRINCIPALI DI INERZIA di ogni segmento articolare:
% J_individual=[Jx(Head)     Jy(Head)    Jz(Head);
%               Jx(UT)       Jy(UT)      Jz(UT);
%               ...
%               Jx(Hand)     Jy(Hand)    Jz(Hand)]
% Poich� le formule di Shan e Bohn sono in [kg cm^2], moltiplico per
% 0.0001 per riportare in [kg m^2].
J_individual(:,1)=0.0001*(b_Ix_Germans(:,1)+b_Ix_Germans(:,2)*w+b_Ix_Germans(:,3)*h);
J_individual(:,2)=0.0001*(b_Iy_Germans(:,1)+b_Iy_Germans(:,2)*w+b_Iy_Germans(:,3)*h);
J_individual(:,3)=0.0001*(b_Iz_Germans(:,1)+b_Iz_Germans(:,2)*w+b_Iz_Germans(:,3)*h);


% DISTANZA del baricentro di ogni segmento a partire dall'alto (COM_top) e
% dal basso (COM_bot) (ottengo due vettori colonna):
COM_top=0.01*L_individual.*COM_percent;
COM_bot=L_individual-COM_top;

% Per applicare il teorema di Huygens-Steiner occorre conoscere la distanza
% fra il centro di massa di ogni componente e quello del corpo risultante:
%   d_UT   =   distanza G(UT) - G(CHEST),
%   d_MT   =   distanza G(CHEST) - G(MT).
% Ipotizzando che il baricentro di CHEST sia collocato alla met� di
% L(UT)+L(MT) si pu� scrivere:
d_UT=(L_individual(2)+L_individual(3))/2-COM_top(2);
d_MT=(L_individual(2)+L_individual(3))/2-COM_bot(3);

% Calcolo momenti principali di inerzia del corpo composto CHEST=UT+MT:
Jx_CHEST=J_individual(2,1)+m_individual(2)*d_UT^2+J_individual(3,1)+m_individual(3)*d_MT^2;
Jy_CHEST=J_individual(2,2)+m_individual(2)*d_UT^2+J_individual(3,2)+m_individual(3)*d_MT^2;
Jz_CHEST=J_individual(2,3)+J_individual(3,3);


% Scrittura matrice dei momenti principali di inerzia per ogni corpo
% rigido del manichino di Arena, espressi nel riferimento locale:
J(:,:,1)=diag([J_individual(4,1),J_individual(4,2),J_individual(4,3)]);     % 1) Hip
J(:,:,2)=diag([Jx_CHEST,Jy_CHEST,Jz_CHEST]);                                % 2) Chest
J(:,:,3)=diag([J_individual(1,1),J_individual(1,2),J_individual(1,3)]);     % 3) Head
J(:,:,4)=diag([J_individual(8,1),J_individual(8,2),J_individual(8,3)]);     % 4) LUArm
J(:,:,5)=diag([J_individual(10,1),J_individual(10,2),J_individual(10,3)]);  % 5) LHand
J(:,:,6)=J(:,:,4);                                                          % 6) RUArm
J(:,:,7)=J(:,:,5);                                                          % 7) RHand
J(:,:,8)=diag([J_individual(5,1),J_individual(5,2),J_individual(5,3)]);     % 8) LThigh
J(:,:,9)=diag([J_individual(6,1),J_individual(6,2),J_individual(6,3)]);     % 9) LShin
J(:,:,10)=diag([J_individual(7,1),J_individual(7,2),J_individual(7,3)]);    % 10) LFoot
J(:,:,11)=J(:,:,8);                                                         % 11) RThigh
J(:,:,12)=J(:,:,9);                                                         % 12) RShin
J(:,:,13)=J(:,:,10);                                                        % 13) RFoot
J(:,:,14)=diag([J_individual(9,1),J_individual(9,2),J_individual(9,3)]);    % 14) LForearm
J(:,:,15)=J(:,:,14);                                                        % 15) RForearm


% Masse dei segmenti articolari costituenti il manichino di Arena composto
% da 15 corpi;
m=[m_individual(4),...                      % 1) Hip
    m_individual(2)+m_individual(3),...     % 2) Chest
    m_individual(1),...                     % 3) Head
    m_individual(8),...                     % 4) LUArm
    m_individual(10),...                    % 5) LHand
    m_individual(8),...                     % 6) RUArm
    m_individual(10),...                    % 7) RHand
    m_individual(5),...                     % 8) LThigh
    m_individual(6),...                     % 9) LShin
    m_individual(7),...                     % 10) LFoot
    m_individual(5),...                     % 11) RThigh
    m_individual(6),...                     % 12) RShin
    m_individual(7),...                     % 13) RFoot
    m_individual(9),...                     % 14) LForearm
    m_individual(9)];                       % 15) RForearm




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Verifica liceit� posizionamento baricentro del corpo composto:
% Calcolo posizione baricentro Head_+_Torso a partire da estremit� superiore testa:
% COM_top_HeadTorso=(m1*x1+m2*x2+m3*x3+m4*x4)/(m1+m2+m3+m4)
COM_top_CHEST=(...
    m_individual(2)*COM_top(2)+...
    m_individual(3)*(L_individual(2)+COM_top(3)))/...
    (m_individual(2)+m_individual(3));
% si osserva dai risultati che la x del baricentro del corpo CHEST si trova
% approssimativamente...

