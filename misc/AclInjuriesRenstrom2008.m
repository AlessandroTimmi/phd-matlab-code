% Graph obtained from data reported in Renstrom et al. 2008 on ACL
% injuries. Data were originally reported in the Norwegian National Knee
% Ligament Registry.
clc
clear
close all

% Each measurement is taken in the middle of each age interval:
age = 12.5 : 5 : 69;

men = [17
345
364
357
365
286
210
101
39
11
4
2
];

women = [62
533
211
188
164
188
118
58
39
11
4
3
];


figure
plot(age,men, 'b.-.', 'MarkerSize',16)
hold on
plot(age,women,'m.-', 'MarkerSize',16)

ax = gca;
ax.XTick = age;
ax.XTickLabels = {'10-14','15-19','20-24','25-29','30-34','35-39','40-44',...
    '45-49','50-54','55-59','60-64','65-69'};
ax.XTickLabelRotation = 45;

ylabel('Number of cases')
xlabel('Age (years)')
legend('Men', 'Women')
