function [sh, leg_fraction] = step_height(BM, BH)
% Calculation of the step height for drop vertical jump tasks (DVJ) using
% potential energy as parameter to make the task difficulty constant.
%
% INPUT
% BM = body mass of the subject in kg;
% BH = body height of the subject in m;
% 
% OUTPUT
% step_height = height of the step in m
% leg_fraction = percent of the leg length represented by the step height

% Constants:
% From Shan-Bohn's study, the leg length in girls is defined as 

%anthropometry15(w,h,g,nRB)

STD_LEG_PERCENT_OF_HEIGHT = 0.55;
STD_BH = 1.50;
STD_BM = 45;
STD_LEG_FRACTION = 0.3; % according to Adam and Tim

STD_POTENTIAL_ENERGY = STD_BM * 9.81 * STD_BH * STD_LEG_PERCENT_OF_HEIGHT * STD_LEG_FRACTION;

sh = STD_POTENTIAL_ENERGY/