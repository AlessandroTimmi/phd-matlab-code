% Evaluation of the accuracy of Kinect v2 for distance measurement.
% Kinect is placed in front of a planar target and measured values from
% Kinect are compared with true distances. This test is useful to
% analyse the "wiggling" error.
%
% Plots in the papers have been digitized using:
% WebPlotDigitizer (http://arohatgi.info/WebPlotDigitizer/)
%
% Data from:
%  E. Lachat, H. Macher, M.-A. Mittet, T. Landes, and P. Grussenmeyer,
% "First Experiences With Kinect V2 Sensor for Close Range 3D Modelling",
% ISPRS - Int. Arch. Photogramm. Remote Sens. Spat. Inf. Sci., vol. XL5/W4,
% no. February, pp. 93100, 2015.
%
%  A. Corti, S. Giancola, G. Mainetti, and R. Sala, A metrological
% characterization of the Kinect V2 time-of-flight camera, Rob. Auton.
% Syst., pp. 68, 2015.
% Instead of reporting absolute distances, they positioned their
% planar target at 0.8 m from Kinect and set that distance as zero (see the
% setup explained in paragraph 4.1 and confirmation in their email).
% Looking at the plot, I have the feeling that the dataset in Corti et al.
% (2015) has been reported with the wrong y sign, i.e. it is
% actually (Kinect displacement - true displacement) instead of
% (true displacement - Kinect displacement).
%
%  Sarbolandi H., Lefloch D, Kolb A et al., "Kinect range sensing:
% structured light versus time-of-flight Kinect", Journal of Computer
% Vision and Image Understanding (2015).
%
%  Fursattel, P. et al., 2015. A Comparative Error Analysis of Current
% Time-of-Flight Sensors. IEEE Transactions on Computational Imaging.
%
%  Pagliari, D. & Pinto, L., 2015. Calibration of Kinect for Xbox One and
% Comparison between the Two Generations of Microsoft Sensors. Sensors,
% 15(11), pp.2756927589.
%
%  Timmi A., "Accuracy and repeatability of Kinect 2 for distance
% measurement", essay for the course of Statistics for research workers
% (MAST90007), The University of Melbourne, Melbourne, Australia, 2014.
%
%
% İ August 2015 Alessandro Timmi

clear
close all
clc

%% Data from Lachat et al 2015, fig. 11.
% x = true distance [m],
% y = true distance - Kinect measurement [cm].
Lachat.raw = [  0.80, 2.39;...
    1.00, 2.52;...
    1.25, 2.76;...
    1.50, 2.54;...
    1.75, 2.50;...
    2.00, 2.08;...
    2.25, 1.87;...
    2.50, 2.25;...
    2.75, 2.26;...
    3.00, 2.37;...
    3.25, 2.25;...
    3.50, 1.85;...
    3.75, 1.62;...
    4.00, 1.48;...
    4.25, 1.80;...
    4.50, 2.11;...
    4.75, 1.53;...
    5.00, 1.01;...
    5.25, 0.59;...
    5.50, 0.58;...
    5.75, 0.65;...
    6.00, 0.70];

% Copy data in more memorable variables:
Lachat.x = Lachat.raw(:,1);
% Change the sign of y, to match the convention used in other studies
% (which is also more reasonable and the same used by Bland and Altman).
Lachat.y = -Lachat.raw(:,2);

% Adjust error data, subtracting the constant bias (systematic offset)
% due to the distance between depth sensor and fixing screw of Kinect.
% As suggested by the authors, we subtract the error measured at the
% nearest range (0.8 m from the wall):
Lachat.yAdj = Lachat.y - Lachat.y(1);

% New x array to be used for the spline interpolation, with higher
% resolution:
Lachat.xSpline = Lachat.x(1) : 0.05 : Lachat.x(end);
% Interpolate original data using a spline:
Lachat.ySpline = interp1(Lachat.x, Lachat.y, Lachat.xSpline, 'spline');
% Interpolate adjusted data using a spline:
Lachat.yAdjSpline = interp1(Lachat.x, Lachat.yAdj, Lachat.xSpline, 'spline');


%% Data from Corti et al. 2015, fig 8a.
% x = true displacement [mm],
% y = true displacement - Kinect displacement [mm]. (?)
Corti.raw = [2.7, -0.5
    19.4, -1.1
    36.8, -1.8
    63.6, -2.1
    105.2, -1.3
    131.7, -0.4
    172.6, 0.6
    219.1, -0.4
    234.5, -1.4
    254.1, -2.5
    275.2, -3.8
    301.3, -5.0
    346.9, -6.2
    403.6, -6.9
    465.8, -7.4
    509.7, -6.8
    574.6, -5.9
    629.1, -7.0
    656.2, -8.0
    696.5, -8.7
    735.4, -7.9
    754.0, -6.7
    776.8, -5.5
    799.9, -4.4
    838.5, -3.7
    868.1, -4.2
    942.9, -5.6
    1011.3, -4.1
    1045.5, -3.0
    1078.1, -1.8
    1107.4, -0.7
    1131.8, 0.5
    1153.0, 1.6
    1174.1, 2.7
    1208.2, 4.1
    1240.4, 4.6
    1289.7, 3.8
    1335.1, 3.1
    1380.8, 4.0
    1412.3, 5.2
    1450.6, 6.0
    1487.1, 5.3
    1506.6, 4.1
    1522.6, 2.9
    1538.9, 1.8
    1556.8, 0.6
    1578.0, -0.6
    1608.9, -1.8
    1631.4, -2.3
    1664.3, -2.9
    1695.9, -3.4
    1750.6, -3.9
    1798.4, -3.6
    1851.6, -3.2
    1889.5, -3.9
    1916.7, -4.9
    1938.9, -5.7
    1981.8, -6.6
    2026.2, -5.4
    2045.6, -4.4
    2074.2, -3.1
    2110.9, -2.5
    2151.5, -3.4
    2187.0, -4.5
    2226.9, -5.1
    2269.2, -4.3
    2296.4, -3.3
    2322.2, -2.2
    2348.2, -1.1
    2374.3, 0.1
    2397.1, 1.2
    2416.6, 2.4
    2436.2, 3.6
    2455.7, 4.8
    2476.9, 6.0
    2509.5, 7.3
    2531.1, 7.7
    2577.8, 7.9
    2640.3, 9.4
    2657.6, 10.6
    2675.6, 11.8
    2693.5, 13.0
    2717.2, 14.2
    2750.5, 15.0
    2790.2, 14.2
    2817.2, 13.0
    2849.8, 11.8
    2884.2, 11.2
    2919.8, 11.2
    2967.7, 11.3
    3020.8, 11.1
    3089.2, 11.0
    3125.0, 10.8
    3171.1, 9.4
    3190.4, 8.2
    3210.0, 7.1
    3235.2, 5.7
    3274.6, 4.8
    3332.2, 5.9
    3372.8, 6.5
    3411.9, 5.6
    3431.1, 4.7
    3453.9, 3.6
    3461.3, 3.0
    ];

% Copy data in more memorable variables, converting x to [m] and y to [cm].
Corti.x = Corti.raw(:,1) / 1000;
% Comparing this dataset with other studies, it seem they reported the
% differences with the wrong sign, so differences are actually:
% (Kinect displacements - true displacements)
Corti.y = Corti.raw(:,2) / 10;

% Adjust true distance, adding the distance at the first station (0.8 m,
% see paragraph 4.1 and their email).
Corti.xAdj = Corti.x + 0.8;

% New x array to be used for the spline interpolation, with higher
% resolution:
Corti.xAdjSpline = Corti.xAdj(1) : 0.05 : Corti.xAdj(end);

% Interpolate data using a spline:
Corti.ySpline = interp1(Corti.xAdj, Corti.y, Corti.xAdjSpline, 'spline');


%% Data from Sarbolandi et al. (2015), fig 15, top righ plot, blue line.
% This dataset doesn't require any adjustement in terms of bias,
% because the position of the origin was estimated usign photometric
% techniques.
% x = true distance [mm],
% y = Kinect measurement - true distance [mm].
Sarbolandi.raw = [496, -6;
    538, -5;
    573, -4;
    617, -3;
    667, -2;
    718, -0;
    766, -1;
    801, -2;
    843, -4;
    889, -4;
    927, -2;
    977, -1;
    1027, -1;
    1056, -2;
    1094, -5;
    1136, -6;
    1190, -8;
    1236, -8;
    1295, -7;
    1349, -5;
    1395, -4;
    1446, -6;
    1500, -7;
    1554, -5;
    1592, -2;
    1638, -2;
    1713, -3;
    1785, -1;
    1851, 3;
    1923, 6;
    1964, 7;
    2006, 9;
    2036, 9;
    2061, 9;
    2115, 7;
    2157, 8;
    2199, 10;
    2262, 10;
    2295, 9;
    2314, 7;
    2349, 6;
    2400, 4;
    2458, 5;
    2500, 5;
    2554, 6;
    2636, 8;
    2680, 6;
    2722, 5;
    2772, 6;
    2814, 8;
    2897, 9;
    2956, 7;
    3002, 9;
    3036, 12;
    3073, 13;
    3119, 14;
    3169, 15;
    3215, 17;
    3262, 18;
    3308, 18;
    3358, 18;
    3412, 22;
    3479, 26;
    3538, 26;
    3579, 25;
    3619, 24;
    3659, 26;
    3676, 25;
    3701, 26;
    3743, 27;
    3814, 28;
    3868, 29;
    3918, 28;
    3952, 25;
    3998, 25];

% Copy data in more memorable variables, converting x to [m] and y to [cm].
Sarbolandi.x = Sarbolandi.raw(:,1) / 1000;
Sarbolandi.y = Sarbolandi.raw(:,2) / 10;


%% Data from Fursattel et al. (2015)
% x = true distance [m].
% Although in the paper it is not specified, comparing these data with the
% others we concluded that errors are expressed as:
% y = true distance  Kinect measurement [m].
Fursattel.raw = [0.807, -0.006
    0.816, -0.007
    0.828, -0.006
    0.837, -0.007
    0.846, -0.006
    0.857, -0.007
    0.866, -0.006
    0.875, -0.006
    0.887, -0.005
    0.899, -0.005
    0.908, -0.004
    0.928, -0.004
    0.949, -0.005
    0.967, -0.007
    0.996, -0.010
    1.020, -0.011
    1.035, -0.011
    1.079, -0.011
    1.117, -0.009
    1.129, -0.009
    1.137, -0.007
    1.147, -0.008
    1.156, -0.006
    1.169, -0.006
    1.176, -0.005
    1.200, -0.005
    1.218, -0.004
    1.241, -0.004
    1.262, -0.004
    1.292, -0.002
    1.309, -0.001
    1.330, -0.001
    1.371, -0.001
    1.395, -0.001
    1.422, -0.001
    1.441, -0.003
    1.475, -0.002
    1.501, -0.003
    1.519, -0.002
    1.560, -0.002
    1.578, -0.003
    1.590, -0.005
    1.602, -0.004
    1.611, -0.007
    1.620, -0.006
    1.631, -0.008
    1.649, -0.009
    1.661, -0.008
    1.679, -0.008
    1.695, -0.008
    1.723, -0.007
    1.751, -0.007
    1.788, -0.006
    1.812, -0.006
    1.829, -0.007
    1.841, -0.008
    1.862, -0.009
    1.881, -0.010
    1.890, -0.009
    1.902, -0.010
    1.942, -0.013
    1.971, -0.014
    2.001, -0.016
    2.033, -0.017
    2.072, -0.019
    2.103, -0.018
    2.131, -0.017
    2.146, -0.016
    2.193, -0.016
    2.224, -0.016
    2.252, -0.016
    2.281, -0.019
    2.296, -0.018
    2.323, -0.018
    2.343, -0.017
    2.355, -0.016
    2.382, -0.016
    2.408, -0.015
    2.429, -0.014
    2.479, -0.013
    2.522, -0.015
    2.544, -0.014
    2.568, -0.015
    2.586, -0.015
    2.603, -0.014
    2.621, -0.014
    2.642, -0.014
    2.671, -0.017
    2.692, -0.016
    2.702, -0.014
    2.716, -0.013
    2.733, -0.014
    2.754, -0.012
    2.784, -0.011
    2.792, -0.010
    2.815, -0.011
    2.846, -0.014
    2.855, -0.012
    2.883, -0.015
    2.896, -0.014
    2.914, -0.015
    2.951, -0.017
    ];

% Copy data in more memorable variables:
Fursattel.x = Fursattel.raw(:,1);
% To match our convention, we change the sign of y and convert it from
% [m] to [cm]. Moreover, to remove the constant bias, we subtract the
% deviation obtained at 0.8 m of true distance:
Fursattel.y = -(Fursattel.raw(:,2) - Fursattel.raw(1,2)) * 100;


%% Data from Pagliari and Pinto (2015), fig. 9, red dots.
% x = true distance [m].
% In the study it is not specified, but plotting the data it seems that the
% convention used for reporting the deviations is:
% y = Kinect measurement - true distance [m].
Pagliari.raw = [0.794, 0.015
    0.906, 0.011
    1.077, 0.013
    1.212, 0.008
    1.459, 0.010
    1.600, 0.010
    1.855, 0.012
    2.134, 0.021
    2.473, 0.016
    2.783, 0.013
    3.094, 0.013
    3.382, 0.021
    3.683, 0.024
    3.976, 0.023
    ];

% Copy data in more memorable variables:
Pagliari.x = Pagliari.raw(:,1);
% To match our convention, we convert y from [m] to [cm]. Moreover, to
% remove the constant bias, we subtract the deviation obtained at 0.8 m of
% true distance:
Pagliari.y = (Pagliari.raw(:,2) - Pagliari.raw(1,2)) * 100;


%% Data from my essay for the course: Statistics for research workers 2014
% (MAST90007).
% True distance obtained from a measurement tape (mm):
Timmi.tape = [3000
    2975
    2950
    2925
    2900
    2875
    2850
    2825
    2800
    2775
    2750
    2725
    2700
    2675
    2650
    2625
    2600
    2575
    2550
    2525
    2500
    2475
    2450
    2425
    2400
    2375
    2350
    2325
    2300
    2275
    2250
    2225
    2200
    2175
    2150
    2125
    2100
    2075
    2050
    2025
    2000
    1975
    1950
    1925
    1900
    1875
    1850
    1825
    1800
    1775
    1750
    1725
    1700
    1675
    1650
    1625
    1600
    1575
    1550
    1525
    1500
    1475
    1450
    1425
    1400
    1375
    1350
    1325
    1300
    1275
    1250
    1225
    1200
    1175
    1150
    1125
    1100
    1075
    1050
    1025
    1000
    975
    950
    925
    900
    875
    850
    825
    800
    775
    750
    725
    700
    675
    650
    625
    600
    ];

% Distance measured from a pixel from Kinect v2 [mm]. Each value is
% obtained as mean across 30 consecutive frames.
Timmi.D_time1 = [3003
    2981
    2958
    2932
    2907
    2881
    2855
    2830
    2812
    2784
    2759
    2736
    2710
    2687
    2661
    2635
    2610
    2585
    2560
    2536
    2511
    2485
    2461
    2438
    2414
    2391
    2368
    2345
    2320
    2295
    2270
    2244
    2217
    2192
    2168
    2144
    2120
    2094
    2069
    2043
    2018
    1992
    1965
    1939
    1914
    1888
    1862
    1835
    1809
    1784
    1761
    1735
    1711
    1686
    1660
    1634
    1608
    1584
    1558
    1533
    1510
    1486
    1461
    1434
    1410
    1385
    1359
    1334
    1309
    1283
    1259
    1233
    1209
    1186
    1163
    1138
    1114
    1091
    1067
    1041
    1017
    990
    963
    938
    913
    890
    866
    842
    817
    791
    765
    740
    714
    690
    664
    639
    612
    ];


% Calculate the differences (Kinect measurement - true value), then convert
% them to [cm]:
Timmi.y = (Timmi.D_time1 - Timmi.tape) / 10;

% Adjust the differences, removing the bias estimated as difference when
% the true distance is 0.8 m:
Timmi.yAdj = Timmi.y - Timmi.y(89);

% Convert the true value in [m]:
Timmi.tape = Timmi.tape / 1000;


%% Plot data:
mytitle = 'Accuracy of Kinect v2 for distance measurement';
figure('Name', mytitle)

hold on
% Plot adjusted interpolated data from Lachat et al. (2015):
plot(Lachat.xSpline, Lachat.yAdjSpline, 'Color', 'r', 'linestyle', '-',...
    'linewidth', 1.5)

% Plot adjusted interpolated data from Corti et al. (2015):
plot(Corti.xAdjSpline, Corti.ySpline, 'Color', [0 0.8 0], 'linestyle', '-',...
    'linewidth', 1.5)

% Plot interpolated data from Sarbolandi et al. (2015):
plot(Sarbolandi.x, Sarbolandi.y, 'Color', [1 0.65 0], 'linestyle', '-',...
    'linewidth', 1.5)

% Plot from Fursattel et al. (2015):
plot(Fursattel.x, Fursattel.y, 'Color', 'c', 'linestyle', '-',...
    'linewidth', 1.5)

% Plot from Pagliari and Pinto (2015):
plot(Pagliari.x, Pagliari.y, 'Color', 'm', 'linestyle', '-',...
    'linewidth', 1.5)

% Plot from my essay for Statistics for research workers (2014):
plot(Timmi.tape, Timmi.yAdj, 'Color', 'b', 'linestyle', '-',...
    'linewidth', 1.5)


% Plot limits of the reliable distance range:
ref_line_vertical_range = 2 * ylim;
h_low_dis = line([0.8 0.8], ref_line_vertical_range);
%h_upp_dist = line([4.5 4.5], ref_line_vertical_range);
set(h_low_dis, 'linestyle', '-.', 'linewidth', 0.8)
%set(h_upp_dist, 'linestyle', '-.')

% Manually set the limits of the plot:
xlim([0 6.3])
ylim([-1.8 3.2])

ax = gca;
grid(ax, 'on');
ax.XTick = [0 0.8 2 3 4 5 6];
ylabel('Kinect v2 - true distance [cm]')
xlabel('True distance [m]')
title(mytitle,...
    'fontweight', 'bold')
legend('Lachat et al. (2015) (sign, bias)',...
    'Corti et al. (2015) (shift)',...
    'Sarbolandi et al. (2015)',...
    'Fursattel et al. (2015) (sign, bias)',...
    'Pagliari and Pinto (2015) (bias)',...
    'Timmi (2014) (bias)',...
    'location', 'NorthWest')







