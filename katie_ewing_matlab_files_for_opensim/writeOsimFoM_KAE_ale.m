function writeOsimFoM_KAE_ale(varargin)
%% SCALE THE MAXIMUM ISOMETRIC FORCE IN OPENSIM MODEL
% By Prasanna Sritharan, December 2012
%
% The xml_read and xml_write functions do not read and write OpenSim model
% files correctly. This is probably due to the complicated nature of
% OpenSim XML files and some limitations with the functions themselves.
%
% This is a workaround. It scales the default FoM by a desired scale
% factor by parsing the file line by line.
%
% Assumption: all muscles need to be scaled by the same factor.
%
% modelname: name of the model file, excluding .osim extension
% modelpath: full OSIM file path
% scalefactor: scaling factor for FoM
%
% EXAMPLE (KATIE): writeOsimFoM_KAE('subject_simbody', 'C:\MyOpenSim3\Subject_1\SCALE', 2)
%
% Edited by Alessandro Timmi (2015)

%% TODO
% - update the model name into the .osim file, appending the same string used
% for the updated filename.
% - add info during processing, using fprintf().




%% Default parameters
default.filenames = '';
default.scalefactor = 3;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments:
addOptional(p, 'filenames', default.filenames,...
            @(x) validateattributes(x,{'char'}, {'nonempty'})); 
addOptional(p, 'scalefactor', default.scalefactor,...
            @(x) validateattributes(x,{'numeric'}, {'scalar'}));
       
% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
filenames = p.Results.filenames;
scalefactor = p.Results.scalefactor;

%% Load model file:
if isempty(filenames)
    [fnames, paths] = uigetfile({'*.osim'},...
        'Select the model file(s) (*.osim)', 'MultiSelect', 'on');
    filenames = fullfile(paths, fnames);
end

if ~iscell(filenames)
    filenames = {filenames};
end


%% Regular expression pattern matching:
FoMTag = '(\s*<max_isometric_force>\s*)(\d+|\d+.\d+)(\s*</max_isometric_force>\s*)';
ForceSetTag = '\s*<ForceSet(?:\s*|\s*name="\w*")>\s*';

for i=1:numel(filenames)
    % Open files for reading and writing:
    fin = fopen(filenames{i}, 'r');
    
    [path, fname, ext] = fileparts(filenames{i});
    filename_out = sprintf('%s%s%s_StrongX%d%s',...
        path, filesep, fname, scalefactor, ext);
    fout = fopen(filename_out, 'w');

    % Lock selected joints (?)
    while ~feof(fin)

        % parse file line by line
        s = fgetl(fin);

        % check if muscles definitions reached
        if ~isempty(regexp(s, ForceSetTag))

                % write the current line to file
                fprintf(fout,'%s\n',s);

                c = [];
                while isempty(strfind(c,'</ForceSet>'))

                    % continue to parse file line by line
                    s = fgetl(fin);

                    % NOTE: this code just looks for max_isometric_force tags
                    % and applies scale the factors indiscriminately. If you
                    % only want to scale certain muscles you'll need to do
                    % more rigorous pattern matching.

                    % find the definition for FoM and replace the existing default value with new one
                    fomtoks = regexp(s,FoMTag,'tokens');
                    if ~isempty(fomtoks)
                        newFoM = scalefactor * str2num(fomtoks{1}{2});                    
                        s = [fomtoks{1}{1} num2str(newFoM) fomtoks{1}{3}];
                    end

                    % write the current line to file
                    fprintf(fout,'%s\n',s);  

                    c = s;
                end

        % write all other lines directly to output file        
        else
            fprintf(fout,'%s\n',s);         
        end

    end


    % close model files
    fclose(fin);
    fclose(fout);


end

end