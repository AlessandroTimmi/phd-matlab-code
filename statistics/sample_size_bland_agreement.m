% Sample size calculation for agreement study, according to Martin Bland
% (2004):
% https://wwwusers.
% york.ac.uk/~mb55/meas/sizemeth.htm
% 
% For the standard deviation of Kinect depth map, see:
% - Lachat, E. et al., 2015. Assessment and calibration of a RGB-D camera
% (Kinect v2 Sensor) towards a potential use for close-range 3D modeling.
% Remote Sensing, 7(10), pp.13070�13097.
% - Lachat, E. et al., 2015. First Experiences With Kinect V2 Sensor for
% Close Range 3D Modelling. ISPRS - International Archives of the
% Photogrammetry, Remote Sensing and Spatial Information Sciences,
% XL-5/W4(February), pp.93�100.
% - Corti, A. et al., 2015. A metrological characterization of the Kinect
% V2 time-of-flight camera. Robotics and Autonomous Systems, pp.6�8. 
%
% � February 2016 Alessandro Timmi 

close all
clear
clc

fprintf('Sample size calculation according to M. Bland (2004):\n')
fprintf('york.ac.uk/~mb55/meas/sizemeth.htm\n')
fprintf('� February 2016 Alessandro Timmi\n\n')


%% Data
% Kinect v2 depth map standard deviation:
s = 5;
fprintf('Standard deviation of Kinect v2 depth map from literature: %0.1f mm.\n\n', s)
% Range for possible number of participants:
subjects = 1:20;

% Assumptions:
fps = 30;
duration = 15;
trials = 1;

fprintf('Assuming:\n')
fprintf('- %d samples/s,\n', fps)
fprintf('- %d s/trial,\n', duration)
fprintf('- %d trial(s)/subject.\n', trials)


%% Calculations:
% Samples (number of Kinect frames):
f = fps * duration * trials * subjects;

% Range containing a possible sample size, expressed as total number of
% frames recorded by Kinect:
x = 1000:100:60000;
% Half size of the confidence interval (expressed in the same unit of the
% standard deviation):
ci = 1.96 * sqrt(3 ./ x) * s;


%% Plots
figure('Name', 'Sample size calculation')
subplot(2,1,1)
plot(subjects, f)
xlabel('Subjects (#)')
ylabel('Samples (frames)')
title({'Sample size calculation, assuming:';...
    sprintf('- %d samples/s,', fps);...
    sprintf('- %d s/trial', duration);...
    sprintf('- %d trial(s)/subject', trials) })

subplot(2,1,2)
plot(x, ci)
xlabel('Samples (frames)')
ylabel('Half CI of the LOA (mm)')
