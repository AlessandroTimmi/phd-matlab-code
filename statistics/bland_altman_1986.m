function [bias, lloa, uloa] = bland_altman_1986(...
    old_method, new_method, label_title, label_var, label_unit, varargin)
% Bland-Altman analysis of agreement based on their paper on The Lancet
% (1986) and the following for the regression based method (on Statistical
% Methods in Medical Resarch - 1999). Input data must be already synced.
%
% Input:
%   old_method = column array containing data measured using the old method.
%   new_method = column array containing data measured using the new method.
%   label_title = string to be used as title for the plots.
%   label_var = string to be used as variable name.
%   label_unit = string with the unit of measurement.
%
%   hax_hist = (optional parameter, default = []) handle to the axes
%           containing the histogram of the differences between new and
%           old method.
%   hax_ba = (optional parameter, default = []) handle to the axes
%           containing the Bland-Altman plot.
%   analysis_type = (optional parameter, default = 'standard') A string
%            indicating which kind of Bland-Altman analysis will be
%            performed: 'standard', 'log-transformed' or
%            'regression-transformed'.
%   yDecimals = (optional parameter, default = 3) number of decimal places
%            to be shown on the Y axis of the Bland-Altman plot. It must
%            be a non-negative scalar.
%   debug_mode = (optional parameter, default = false) if true shows
%           shows some extra info for debug purposes.
%
% Output:
%   bias = systematic error (also called bias), calculated as the mean
%          of the differences between the two methods.
%   lloa = lower limit of agreement, calculated as bias - 1.96 sd.
%   uloa = lower limit of agreement, calculated as bias + 1.96 sd.
%
% � October 2014 Alessandro Timmi


%% Default input values:
default.hax_hist = [];
default.hax_ba = [];
default.analysis_type = 'standard';
expected_analysis_types = {'standard', 'regression_transformed', 'log_transformed'};
default.yDecimals = 3;
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input argument:
addRequired(p, 'old_method',...
    @(x) validateattributes(x, {'numeric'}, {'column'}));
addRequired(p, 'new_method',...
    @(x) validateattributes(x, {'numeric'}, {'column'}));
addRequired(p, 'label_title',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'label_var',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addRequired(p, 'label_unit',...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'hax_hist', default.hax_hist,...
    @(x) validateattributes(x, {'matlab.graphics.axis.Axes'}, {}));
addParameter(p, 'hax_ba', default.hax_ba,...
    @(x) validateattributes(x, {'matlab.graphics.axis.Axes'}, {}));
addParameter(p, 'analysis_type', default.analysis_type,...
    @(x) any(validatestring(x, expected_analysis_types)));
addParameter(p, 'yDecimals', default.yDecimals,...
    @(x) validateattributes(x, {'numeric'}, {'scalar', 'nonnegative'}));
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, old_method, new_method, label_title, label_var, label_unit, varargin{:});

% Copy input arguments into more memorable variables:
old_method = p.Results.old_method;
new_method = p.Results.new_method;
label_title = p.Results.label_title;
label_var = p.Results.label_var;
label_unit = p.Results.label_unit;

hax_hist = p.Results.hax_hist;
hax_ba = p.Results.hax_ba;
analysis_type = p.Results.analysis_type;
yDecimals = p.Results.yDecimals;
debug_mode = p.Results.debug_mode;

clear varargin default p


%% BLAND-ALTMAN ANALYSIS
if debug_mode
    fprintf('\nBland-Altman analysis of agreement\n');
    fprintf('� October 2014 Alessandro Timmi.\n\n')
    fprintf('Analysis type: %s.\n', analysis_type);
end

fprintf('%s %s\n', label_title, label_var)

sample_size = length(new_method);
fprintf('Sample size (number of measurements recorded with the new method): n = %d samples.\n',...
    sample_size)

% t-value for a two-sided test with n-1 degrees of freedom (n = sample
% size) and level of significance alpha = 0.05. In their paper Bland and
% Altman imply the use of a two-sided test, so we need to convert the
% probability from one-sided (95%) to two-sided.
% For larger sample sizes, this t-value tends to 1.96, because Student's
% t distribution tends to the Normal distribution.
alpha = 0.05;
prob = 1 - alpha / 2;
dof = sample_size - 1;
t_value = tinv(prob, dof);
fprintf('T-value (alpha = %0.3f, 2-sided, dof = %d): %4.4f.\n',...
    alpha, dof, t_value);


switch analysis_type
    case 'standard'
        % On the y-axes we plot the differences between the 2 devices (NEW - OLD):
        dif = new_method - old_method;
        
        % The mean of the differences is also referred to as the "bias":
        bias = mean(dif);
        sd = std(dif);
        
        % We might test for the presence of a bias (mean value of the difference
        % differs significantly from 0) using a one-sample t-test:
        % [h,p] = ttest(dif)
        % If there is a consistent bias, we can adjust for it by subtracting
        % the mean of the differences from the new method (or, as in this
        % case, from the difference: new_method - old_method).
        
        % The 95% limits of agreement are defined as (mean � 1.96 * sd).
        % It is where the 95% of differences are expected to lie (if the
        % assumption of Normal distribution for the differences is true).
        lloa = bias - 1.96 * sd;
        uloa = bias + 1.96 * sd;
        
        % On the x-axes we plot the mean between the two methods,
        % because it is the best estimate of the true value:
        avg = mean([new_method, old_method], 2);
        
        
        %% CONFIDENCE INTERVALS (CI) FOR THE LIMITS OF AGREEMENT
        % CI tell how precise our estimates are in representing the
        % values of the whole population, provided the differences
        % follow a distribution which is approximatively Normal.
        fprintf('Parameter = Estimate (95%% CI):\n')
        
        % Standard error of the mean difference:
        se_dif = sqrt(sd^2 / sample_size);
        
        % Standard error of the limits of agreement.
        % NOTICE: there is a (not straightforward) derivation to
        % obtain the standard error below (and in particular
        % the "3" in it). See derivation provided by Prof Ian Gordon.
        se_loa = sqrt(3 * sd^2 / sample_size);
        
        % Margin of error for the bias (= half the width of the confidence
        % interval):
        moe_bias = t_value * se_dif;
        
        % 95% CI for the mean difference (or bias):
        ci_bias = bias + [-moe_bias, moe_bias];
        fprintf('Bias = %4.4f (%4.4f, %4.4f) %s.\n',...
            bias, ci_bias', label_unit)
        
        % Margin of error for the limits of agreement:
        moe_loa = t_value * se_loa;
        
        % 95% CI for the limits of agreement:
        ci_lloa = lloa + [-moe_loa, moe_loa];
        ci_uloa = uloa + [-moe_loa, moe_loa];
        fprintf('Lower LOA = %4.4f (%4.4f, %4.4f) %s.\n',...
            lloa, ci_lloa', label_unit)
        fprintf('Upper LOA = %4.4f (%4.4f, %4.4f) %s.\n',...
            uloa, ci_uloa', label_unit)
        
        % The width of the limits of agreement (upper limit - lower limit)
        % is not mentioned in Bland-Altman papers, but it conveniently
        % conveys the level of disagreement using a single number.
        % Moreover, it is mentioned in Myles and Cui (2007).
        width_loa = uloa - lloa;
        fprintf('Width of the limits of agreement: %.4f %s.\n',...
            width_loa, label_unit)
        
        fprintf('MOE bias = %4.4f %s.\n', moe_bias, label_unit)
        fprintf('MOE LOA = %4.4f %s.\n\n', moe_loa, label_unit)
        
        
    case 'log_transformed'
        % TODO: fix and complete with 95% CI.
        % If the variability of the differences increases as the magnitude of
        % measurements increases, we could ignore this relationship between
        % difference and magnitude, but the limits of agreement will be wider
        % apart for small magnitudes and narrow for large magnitudes. We can
        % try to remove this relationship log-transforming our data before the
        % analysis, and then back-transform.
        dif = log(new_method) - log(old_method);
        bias = mean(dif);
        sd = std(dif);
        lloa = bias - 1.96 * sd;
        uloa = bias + 1.96 * sd;
        avg = mean([log(new_method), log(old_method)], 2);
        
        
    case 'regression_transformed'
        % TODO: fix and complete with 95% CI.
        dif = new_method - old_method;
        avg = mean([new_method, old_method], 2);
        
        % According to (Bland-Altman, 1999), we try to remove the
        % relationship between differences and average using a linear
        % regression of our data:
        [p] = polyfit(avg, dif, 1);
        
        % (D^) evaluate the fitting of the differences vs the average:
        bias = polyval(p, avg);
        
        % Calculate the residuals of this fitting:
        resid_dif = dif - bias;
        
        % Regress the absolute values of the residuals:
        [p_resid] = polyfit(avg, abs(resid_dif), 1);
        
        % (R^) evaluate the fitting of the residuals:
        fit_resid = polyval(p_resid, avg);
        
        % The 95% limits of agreement are obtained as:
        % lloa = D^ - 1.96 * sqrt(pi/2) * R^
        % uloa = D^ + 1.96 * sqrt(pi/2) * R^
        lloa = bias - 1.96 * sqrt(pi/2) * fit_resid;
        uloa = bias + 1.96 * sqrt(pi/2) * fit_resid;
        
        fprintf('TODO: print regression equations with values.\n');
end


%% Plot settings
% RGB colours:
ps.DARK_GRAY = [0.6, 0.6, 0.6];
ps.MEDIUM_GRAY = [0.73, 0.73, 0.73];
ps.LIGHT_GRAY = [0.85, 0.85, 0.85];

ps.AVG_LABEL = {'Average(Kinect2, Vicon)'; sprintf('%s [%s]', label_var, label_unit)};
ps.DIF_LABEL = {'Kinect2 - Vicon';         sprintf('%s [%s]', label_var, label_unit)};

ps.TITLE_NAMES = {'interpreter', 'fontweight', 'fontsize'};
ps.TITLE_VALUES = {'none', 'bold', 14};

ps.AXIS_NAMES = {'Ygrid'};
ps.AXIS_VALUES = {'on'};

ps.LABEL_NAMES = {'interpreter', 'fontweight', 'fontsize'};
ps.LABEL_VALUES = {'none', 'normal', 12};

ps.REFLINE_NAMES = {'Color', 'linestyle', 'linewidth'};
ps.REFLINE_VALUES_DASH = {'k', '--', 1.5};
ps.REFLINE_VALUES_SOLID = {'k', '-', 1.5};

ps.AXIS_EXP = 0;
% The output of this string is '%,.3f' (replace 3 with the value of
% yDecimals):
ps.TICK_LABEL_FORMAT = sprintf('%%,.%df', yDecimals);

ps.PATCH_FACE_ALPHA = 0.5;
ps.PATCH_EDGE_COLOR = 'none';


%% Histogram of the differences, to visually check if they are Normally
% distributed:
str_hist_title = sprintf('Histogram - %s', label_title);

if isempty(hax_hist)
    figure('Name', str_hist_title)
    hax_hist = gca;
end

% Plot the histogram:
histogram(hax_hist, dif);

% Set histogram properties:
set(hax_hist, ps.AXIS_NAMES, ps.AXIS_VALUES)
xlabel(hax_hist, ps.DIF_LABEL, ps.LABEL_NAMES, ps.LABEL_VALUES)
ylabel(hax_hist, 'Occurrencies', ps.LABEL_NAMES, ps.LABEL_VALUES)
title_hist = title(hax_hist, str_hist_title);
set(title_hist, ps.TITLE_NAMES, ps.TITLE_VALUES)
hax_hist.XAxis.Exponent = ps.AXIS_EXP;


%% Bland-Altman plots:
str_ba_title = sprintf('%s', label_title);

if isempty(hax_ba)
    figure('Name', str_ba_title)
    hax_ba = gca;
end

% Need to specify which axes object we want to hold, otherwise it will not 
% hold the correct one:
hold(hax_ba, 'on')

plot(hax_ba, avg, dif, 'Marker', '.', 'Color', ps.MEDIUM_GRAY,...
    'MarkerSize', 6, 'linestyle', 'none')

% Set custom axes limits, because MATLAB sometimes leaves some
% useless empty space around data, depending on the format of the
% computer screen and on the current size of the figure:
hax_ba.XLim = custom_axis_margins(avg, 0.08);
hax_ba.YLim = custom_axis_margins([lloa, uloa], 0.3);

set(hax_ba, ps.AXIS_NAMES, ps.AXIS_VALUES)
hax_ba.YAxis.Exponent = ps.AXIS_EXP;
hax_ba.YAxis.TickLabelFormat = ps.TICK_LABEL_FORMAT;

% Set axes labels:
my_ylabel = ylabel(hax_ba, ps.DIF_LABEL);
my_xlabel = xlabel(hax_ba, ps.AVG_LABEL);

set(my_ylabel, ps.LABEL_NAMES, ps.LABEL_VALUES);
set(my_xlabel, ps.LABEL_NAMES, ps.LABEL_VALUES);

% Set title:
title_ba = title(hax_ba, str_ba_title);
set(title_ba, ps.TITLE_NAMES, ps.TITLE_VALUES);

switch analysis_type
    case 'standard'
        %% Plot 95% Confidence intervals (CI) as shaded areas:
        delta_X_array = [hax_ba.XLim(1), hax_ba.XLim(2), hax_ba.XLim(2), hax_ba.XLim(1)];
        
        patch_ci_bias = fill(hax_ba, delta_X_array,...
            [ci_bias(2), ci_bias(2), ci_bias(1), ci_bias(1)],...
            ps.DARK_GRAY);

        patch_ci_bias.FaceAlpha =  ps.PATCH_FACE_ALPHA;
        patch_ci_bias.EdgeColor =  ps.PATCH_EDGE_COLOR;
        
        patch_ci_lloa = fill(hax_ba, delta_X_array,...
            [ci_lloa(2), ci_lloa(2), ci_lloa(1), ci_lloa(1)],...
            ps.DARK_GRAY);
        
        patch_ci_lloa.FaceAlpha =  ps.PATCH_FACE_ALPHA;
        patch_ci_lloa.EdgeColor =  ps.PATCH_EDGE_COLOR;
        
        patch_ci_uloa = fill(hax_ba, delta_X_array,...
            [ci_uloa(2), ci_uloa(2), ci_uloa(1), ci_uloa(1)],...
            ps.DARK_GRAY);    
        
        patch_ci_uloa.FaceAlpha =  ps.PATCH_FACE_ALPHA;
        patch_ci_uloa.EdgeColor =  ps.PATCH_EDGE_COLOR;
        
        % Plot limits of agreement:
        ref_mean = refline(hax_ba, 0, bias);
        ref_loa_low = refline(hax_ba, 0, lloa);
        ref_loa_high = refline(hax_ba, 0, uloa);
        
        set(ref_mean, ps.REFLINE_NAMES, ps.REFLINE_VALUES_DASH)
        set(ref_loa_low, ps.REFLINE_NAMES, ps.REFLINE_VALUES_SOLID)
        set(ref_loa_high, ps.REFLINE_NAMES, ps.REFLINE_VALUES_SOLID)
        
        % Set tick values for the Y axis:
        hax_ba.YTick = [lloa, bias, uloa];
             
        
    case 'log_transformed'
        % TODO: fix and complete with 95% CI.
        
        % Limits of agreement:
        ref_mean = refline(0, bias);
        ref_loa_low = refline(0, lloa);
        ref_loa_high = refline(0, uloa);
        
        set(ref_mean, ps.REFLINE_NAMES, ps.REFLINE_VALUES_DASH)
        set(ref_loa_low, ps.REFLINE_NAMES, ps.REFLINE_VALUES_SOLID)
        set(ref_loa_high, ps.REFLINE_NAMES, ps.REFLINE_VALUES_SOLID)

        
    case 'regression_transformed'
        % TODO: fix and complete with 95% CI.
        
        % Plot the linear regression:
        h_regr = plot(avg, fit_dif);
        set(h_regr, ps.REFLINE_NAMES, ps.REFLINE_VALUES_DASH)
        
        % Plot the limits of agreement calculated from the fit:
        h_low_loa = plot(avg, fit_low_loa);
        set(h_low_loa, ps.REFLINE_NAMES, ps.REFLINE_VALUES_SOLID);
        h_upp_loa = plot(avg, fit_upp_loa);
        set(h_upp_loa, ps.REFLINE_NAMES, ps.REFLINE_VALUES_SOLID);
        
end





%% TODO: REPEATABILITY


%% TODO: CONFIDENCE INTERVALS FOR THE REPEATABILITY


end
