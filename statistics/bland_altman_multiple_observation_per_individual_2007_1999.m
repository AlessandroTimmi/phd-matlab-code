clc
close all
clear


%TODO: use function custom_axis_margins.m in the plot below.

fprintf('Bland-Altman analysis of agreement with multiple observations per individual (Bland and Altman, 2007, 1999)\n')
fprintf('Method where the true value varies.\n')
fprintf('Units are in %%.\n')
fprintf('NOTICE: there seem to be some errors in the 2007 paper:\n')
fprintf('- Table 1. See Bland and Altman (1999) for the correct table.\n')
fprintf('- Figure 3, the Y axis actually reports the mean of the differences per subject. See Figure 10 (1999).\n')
fprintf('- Figure 5, the Y values of the points labelled 1 and 4 are swapped.\n')
fprintf('� March 2016 Alessandro Timmi\n\n')

%% Data from Bland-Altman (2007)
% Cardiac ejection fraction (%) by two methods, radionuclide
% vetriculography (RV) and impedance cardiography (IC) for 12 subjects:
RV = [7.83; 7.42; 7.89; 7.12; 7.88;...
    6.16; 7.26; 6.71; 6.54;...
    4.75; 5.24; 4.86; 4.78; 6.05; 5.42;...
    4.21; 3.61; 3.72; 3.87; 3.92;...
    3.13; 2.98; 2.85; 3.17; 3.09; 3.12;...
    5.92; 6.42; 5.92; 6.27;...
    7.13; 6.62; 6.58; 6.93;...
    4.54; 4.81; 5.11; 5.29; 5.39; 5.57;...
    4.48; 4.92; 3.97;...
    4.22; 4.65; 4.74; 4.44; 4.50;...
    6.78; 6.07; 6.52; 6.42; 6.41; 5.76;...
    5.06; 4.72; 4.90; 4.80; 4.90; 5.10];

IC = [6.57; 5.62; 6.90; 6.57; 6.35;...
    4.06; 4.29; 4.26; 4.09;...
    4.71; 5.50; 5.08; 5.02; 6.01; 5.67;...
    4.14; 4.20; 4.61; 4.68; 5.04;...
    3.03; 2.86; 2.77; 2.46; 2.32; 2.43;...
    5.90; 5.81; 5.70; 5.76;...
    5.09; 4.63; 4.61; 5.09;...
    4.72; 4.61; 4.36; 4.20; 4.36; 4.20;...
    3.17; 3.12; 2.96;...
    4.35; 4.62; 3.16; 3.53; 3.53;...
    7.20; 6.09; 7.00; 7.10; 7.40; 6.80;...
    4.50; 4.20; 3.80; 3.80; 4.20; 4.50];

% NOTICE: in the 2007 paper this array is wrong, use the one reported in
% the paper from 1999.
groups = [1,1,1,1,1,...
    2,2,2,2,...
    3,3,3,3,3,3,...
    4,4,4,4,4,...
    5,5,5,5,5,5,...
    6,6,6,6,...
    7,7,7,7,...
    8,8,8,8,8,8,...
    9,9,9,...
    10,10,10,10,10,...
    11,11,11,11,11,11,...
    12,12,12,12,12,12]';

title1 = 'Figure 1. Scatter plot';
figure('Name', title1, 'NumberTitle', 'off')
plot(IC, RV, 'o')
xlabel('Ejection fraction (IC) [%]')
ylabel('Ejection fraction (RV) [%]')
title(title1)


%% Plot difference against the average for the two methods, to check if
% the agreement is the same across the range of measurements.
dif.all = RV - IC;
avg.all = mean([RV, IC], 2);

title2 = 'Figure 2. Scatter plot of difference against average of the two methods';
figure('Name', title2, 'NumberTitle', 'off')
plot(avg.all, dif.all, 'o')
ylabel('Difference, RV - IC [%]')
xlabel('Average of RV and IC [%]')
title(title2)
h_zero = refline(0,0);
set(h_zero, 'linestyle', '--', 'color', 'k')


%% One-way ANOVA:
[p, tbl, dif_stats] = anova1(dif.all, groups);
dif.mean_per_subject = dif_stats.means';
% Number of measurements per subject:
m = dif_stats.n';


%% Checking the assumption that the within-subjects variance is constant.
% Plot within-subject SD of the differences against within-subject
% average measurement.
% NOTICE: in the paper the Y axis is said to report the SD of the
% differences for each subject, but it actually reports the mean of the
% differences for each subject.

avg.per_subject = grpstats(avg.all, groups, {'mean'});
figure('Name', 'Figure 3 Scatter plot of SD vs mean per subject',...
    'NumberTitle', 'off')

% This plot is incorrectly reported in the 2007 paper. It is also reported
% (correctly) as Figure 10 in the 1999 paper. Notice that the "difference
% between the means for the two methods" (1999 paper, Y axis) is
% equivalent to the mean of the differences between the two methods (Y
% axis here).
subplot(2,1,1)
scatter(avg.per_subject, dif.mean_per_subject, 10 * m, 'r')
ylabel({'Within-subject mean of differences'; 'RV - IC [%]'})
xlabel('Within-subject average of RV and IC [%]')
title('Figure 3 (2007 paper)')

% Plot as it should be:
dif.sd_per_subject = grpstats(dif.all, groups, {'std'});
subplot(2,1,2)
scatter(avg.per_subject, dif.sd_per_subject, 10 * m, 'b')
ylabel({'Within-subject SD of differences'; 'RV - IC [%]'})
xlabel('Within-subject average of RV and IC [%]')
title('Figure 3 (revised)')


%% Estimate variances
variance_within = tbl{3,4};

% Number of subjects:
n = length(dif_stats.gnames);
divisor = (sum(m)^2 - sum(m.^2)) / ((n-1) * sum(m));

variance_between = (tbl{2,4} - variance_within) / divisor;
total_variance = variance_within + variance_between;
sd = sqrt(total_variance);

% Bias calculated according to the 2007 paper
bias = mean(dif.all);
% Bias calculated according to the 1999 paper (the two versions are equivalent):
bias_1999 = sum(m .* dif.mean_per_subject) / sum(m);

upper_loa = bias + 1.96 * sd;
lower_loa = bias - 1.96 * sd; 

fprintf('Variance within: %0.8f\n', variance_within)
fprintf('Divisor: %0.8f\n', divisor)
fprintf('Variance between: %0.8f\n', variance_between)
fprintf('Total variance: %0.8f\n', total_variance)
fprintf('Standard deviation: %0.8f %%\n', sd)
fprintf('Bias (2007): %0.8f %%\n', bias)
fprintf('Bias (1999): %0.8f %%\n', bias_1999)
fprintf('Upper LoA: %0.8f %%\n', upper_loa)
fprintf('Lower LoA: %0.8f %%\n', lower_loa)


title4 = 'Figure 4. Bland-Altman plot';
figure('Name', title4, 'NumberTitle', 'off')
plot(avg.all, dif.all, 'o')
h_upp = refline(0, upper_loa);
h_bias = refline(0, bias);
h_low = refline(0, lower_loa);
xlabel('Average of RV and IC [%]')
ylabel('Difference, RV - IC [%]')
title(title4)


%% Checking the assumption that the repeated differences for a single
% subject are independent.
% NOTICE: in the 2007 paper, the Y values for points labelled 1 and 4 look
% swapped by mistake.
title5 = ('Figure 5. Difference against order of measurements');
figure('Name', title5, 'NumberTitle', 'off')
hold on
end_point = 0;
for i=1:n    
    start_point = end_point + 1;
    end_point = end_point + m(i);
    text(start_point:end_point, dif.all(start_point:end_point),...
        sprintf('%d', i))    
end
h5 = gca;
delta_xlim = length(dif.all);
delta_ylim = abs((max(dif.all) - min(dif.all)));
margin = 0.1;
h5.XLim = [0 - margin * delta_xlim, length(dif.all) + margin * delta_xlim];
h5.YLim = [min(dif.all) - margin * delta_ylim, max(dif.all) + margin * delta_ylim];
title(title5)
xlabel('Order in which measurements were recorded')
ylabel('Difference, RV - IC [%]')
