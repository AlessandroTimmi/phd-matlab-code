function treadmill_speeds = speeds_treadmill_study(trial_types, varargin)
% Return the treadmill speeds corresponding to input trial types used for
% the Treadmill study.
%
% Input:
%   trial_types = row cell array of strings containing types of trials
%                 captured during the Treadmill study. See below for
%                 accepted strings.
%   debug_mode  = (optional parameter, default = false). If true, displays
%                 some extra info for debug purposes.
%
% Output:
%   treadmill_speeds = treadmill speeds corresponding to input trial types.
%
% � July 2016 Alessandro Timmi


%% Default input values:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'trial_types',...
    @(x) validateattributes(x, {'cell'}, {'row'}));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, trial_types, varargin{:});

% Copy input arguments into more memorable variables:
trial_types = p.Results.trial_types;
debug_mode = p.Results.debug_mode;

clear varargin default p


%% Return treadmill speeds corresponding to input trial types:
all_trial_types = {'gait_slow', 'gait_fast', 'run_slow', 'run_fast'};
all_treadmill_speeds = [0.83, 1.31, 2, 2.5];

treadmill_speeds = zeros(size(trial_types));

for i=1:length(trial_types)
    index = strcmpi(trial_types{i}, all_trial_types);
    
    if any(index)
        treadmill_speeds(i) = all_treadmill_speeds(index);
    else
        treadmill_speeds(i) = NaN;
        warning('"%s" is not a recognized trial type of the Treadmill study.', trial_types{i})
    end
end


end