function BlandAltman1986KneeAdductionAngleTreadmillStudy(varargin)
% Bland-Altman (The Lancet - 1986 version) analysis of agreement
% on the dataset of the treadmill study. The agreement of the knee
% adduction angle is assessed along the entire trial and in correspondence
% of specific gait events. The dataset must be already synced.
%
% Our study design doesn't match the one described in BA's 1986 paper.
% Therefore, performing the analysis of agreement without taking into
% account this aspect is incorrect. However, it is a good "benchmark" to
% have a first impression on the agreement between Kinect and Vicon.
%
% Input:
%   trialTypes = (optional parameter, default = see below) cell array of
%                strings containing types of trials to be loaded. See
%                ListTrialTypeAgreementStudy.m for accepted strings.
%   rootFolder = (optional parameter, default = '') folder containing all
%                trials to be loaded. The expected folder structure is
%                shown in list_trials_in_all_subjects.m. If empty, a dialog
%                window will open to select a root folder.
%
% � February 2017 Alessandro Timmi


%% Default input values:
default.trialTypes = {'gait_slow', 'gait_fast', 'run_slow', 'run_fast'};
default.rootFolder = 'C:\TEST_FOLDER\5 - Ready for statistical analysis (Bland-Altman analyses included)';

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'trialTypes', default.trialTypes,...
    @(x) validateattributes(x, {'cell'}, {'nonempty'}));
addParameter(p, 'rootFolder', default.rootFolder,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
trialTypes = p.Results.trialTypes;
rootFolder = p.Results.rootFolder;

clear varargin default p


%% Select root folder
if isempty(rootFolder)
    rootFolder = uigetdir('', 'Select root folder containing Kinect and Vicon data');
    if isequal(rootFolder, 0)
        return
    end
end

% Enable logging:
LogFile(fullfile(rootFolder, 'Bland-Altman1986KneeAdductionAngleTreadmillStudy.log'));

fprintf('Bland-Altman analysis of agreement (1986 version) for the treadmill study (knee adduction angle)\n')

subjectsPrefix = 'TML';
% Preallocate storage arrays for biases and limits of agreement:
analysisTypes = {'', ' at FS', ' at TO'};
biasStorage = zeros(length(trialTypes), length(analysisTypes));
lloaStorage = zeros(size(biasStorage));
uloaStorage = zeros(size(biasStorage));

% Create emtpy structs which will contain the mean interval (in frames)
% between consecutive gait events for each trial:
meanIntervalsFS = struct;
meanIntervalsTO = struct;

% Analyse data by trial type:
for t = 1:length(trialTypes)
    
    % List trials of a specific type for both Kinect and Vicon:
    [kinectTrials.(trialTypes{t}), viconTrials.(trialTypes{t})] =...
        ListTrialTypeAgreementStudy(trialTypes{t},...
        'rootFolder', rootFolder);
    
    % Load trials of current type into arrays of structs:
    kinectArrayOfStructs = LoadTrcToArrayOfStructs(kinectTrials.(trialTypes{t}),...
        subjectsPrefix);
    viconArrayOfStructs = LoadTrcToArrayOfStructs(viconTrials.(trialTypes{t}),...
        subjectsPrefix);
    
    % Check correspondence of structs between the two devices:
    CompareArraysOfStructsTreadmillStudy(kinectArrayOfStructs, viconArrayOfStructs);
    
    % Detect gait events and calculate knee flexion angle for trials of current type:
    [kinectArrayOfStructs, viconArrayOfStructs] =...
        GetKneeFlexionAngleAndGaitEvents(kinectArrayOfStructs, viconArrayOfStructs);
    
    % Preallocate arrays of mean intervals between gait events:
    meanIntervalsFS.(trialTypes{t}) = zeros(length(viconTrials.(trialTypes{t})), 1);
    meanIntervalsTO.(trialTypes{t}) = zeros(length(viconTrials.(trialTypes{t})), 1);
    
    for f = 1:length(viconTrials.(trialTypes{t}))
        % Mean interval in frames between consecutive FS events for
        % current trial:
        meanIntervalsFS.(trialTypes{t})(f,1) = mean(diff(viconArrayOfStructs(f).locsFS));
        % Mean interval in frames between consecutive TO events for
        % current trial:
        meanIntervalsTO.(trialTypes{t})(f,1) = mean(diff(viconArrayOfStructs(f).locsTO));
    end
    
    
    %% Calculate knee adduction angle for trial of current type:
    [kinectArrayOfStructs, viconArrayOfStructs] =...
        GetKneeAdductionAngle(kinectArrayOfStructs, viconArrayOfStructs);
    
    
    %% Bland-Altman analysis of agreement (1986 version) on knee adduction angle
    % NOTE: we cannot concatenate all quantities in one go as we do for the
    % marker coordinates, because here phi, phiFS and phiTO arrays
    % are non-standard fields for a .trc struct.
    % Concatenate knee adduction angle from trials of current type:
    viconPhi = vertcat(viconArrayOfStructs(:).phi);
    kinectPhi = vertcat(kinectArrayOfStructs(:).phi);
    
    viconPhiFS = vertcat(viconArrayOfStructs(:).phiFS);
    viconPhiTO = vertcat(viconArrayOfStructs(:).phiTO);
    kinectPhiFS = vertcat(kinectArrayOfStructs(:).phiFS);
    kinectPhiTO = vertcat(kinectArrayOfStructs(:).phiTO);
    
    % Figures handles for Bland-Altman analysis:
    figure('Name', sprintf('Histograms of the differences - %s', trialTypes{t}));
    % Get the subplot handles for histograms:
    haxHistogramPhi = subplot(3, 1, 1);
    haxHistogramPhiFS = subplot(3, 1, 2);
    haxHistogramPhiTO = subplot(3, 1, 3);
    
    figure('Name', sprintf('Bland-Altman plots - %s', trialTypes{t}));
    % Get the subplot handles for Bland-Altman plots:
    haxBaPhi = subplot(3, 1, 1);
    haxBaPhiFS = subplot(3, 1, 2);
    haxBaPhiTO = subplot(3, 1, 3);
    
    % Perform Bland-Altman analysis of agreement on current type of trials
    % and plot the results:
    [bias.phi, lloa.phi, uloa.phi] =...
        bland_altman_1986(...
        viconPhi,...
        kinectPhi,...
        sprintf('%s - Knee adduction', trialTypes{t}),...
        'Knee adduction',...
        '�',...
        'hax_hist', haxHistogramPhi,...
        'hax_ba', haxBaPhi);
    
    [bias.phiFS, lloa.phiFS, uloa.phiFS] =...
        bland_altman_1986(...
        viconPhiFS,...
        kinectPhiFS,...
        sprintf('%s - Knee adduction at FS', trialTypes{t}),...
        'Knee adduction at FS',...
        '�',...
        'hax_hist', haxHistogramPhiFS,...
        'hax_ba', haxBaPhiFS);
    
    [bias.phiTO, lloa.phiTO, uloa.phiTO] =...
        bland_altman_1986(...
        viconPhiTO,...
        kinectPhiTO,...
        sprintf('%s - Knee adduction at TO', trialTypes{t}),...
        'Knee adduction at TO',...
        '�',...
        'hax_hist', haxHistogramPhiTO,...
        'hax_ba', haxBaPhiTO);
    
    
    %% Store results of analysis of agreement for summary plot vs all
    % treadmill speeds (i.e. all trial types):
    biasStorage(t,:) = [bias.phi, bias.phiFS, bias.phiTO];
    lloaStorage(t,:) = [lloa.phi, lloa.phiFS, lloa.phiTO];
    uloaStorage(t,:) = [uloa.phi, uloa.phiFS, uloa.phiTO];
    
end


%% Plot summarizing BA results for the knee adduction angle vs treadmill speed
treadmillSpeeds = speeds_treadmill_study(trialTypes);
plotMarkerSize = 16;

lowerBound = min(lloaStorage(:));
upperBound = max(uloaStorage(:));

figure('Name', 'Bland-Altman (1986) vs treadmill speed - Knee adduction angle',...
    'Position', [100, 100, 1100, 350])


for a = 1:length(analysisTypes)
    hax = subplot(1,3,a);
    hold(hax, 'on');
    
    plot(hax, treadmillSpeeds, biasStorage(:,a),...
        'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', ':');
    
    plot(hax, treadmillSpeeds, lloaStorage(:,a),...
        'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', '--');
    
    plot(hax, treadmillSpeeds, uloaStorage(:,a),...
        'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', '--');
    
    hax.XTick = treadmillSpeeds;
    hax.XLim = custom_axis_margins(treadmillSpeeds, 0.15);
    hax.YGrid = 'on';
    hax.YAxis.Exponent = 0;
    hax.YLim = custom_axis_margins([lowerBound, upperBound], 0.1);
    
    xlabel(hax, 'Treadmill speed [m/s]')
    ylabel(hax, {'KinEdge - Vicon'; sprintf('Knee adduction angle%s [�]',...
        analysisTypes{a})})
    title(hax, sprintf('Knee adduction angle%s', analysisTypes{a}))
    
    if a == 1
        legend(hax, 'Bias', 'LOA', 'location', 'best' )
    end
    
end


%% Analyse intervals (in frames) between gait events, calculating grand
% mean and standard deviation across all trials of the same type.
% Probably an analysis of variance (ANOVA) would be more correct here, as
% the standard deviation should take into account the variance within and
% between groups.
fprintf('Intervals in frames between gait events, reported as: mean (standard deviation):\n')
fprintf('NOTE: an analysis of variance (ANOVA) would be more correct here.\n')
for t=1:length(trialTypes)
    
    grandMeanIntervalsFS.(trialTypes{t}) = mean(meanIntervalsFS.(trialTypes{t}));
    sdMeanIntervalsFS.(trialTypes{t}) = std(meanIntervalsFS.(trialTypes{t}));
    
    grandMeanIntervalsTO.(trialTypes{t}) = mean(meanIntervalsTO.(trialTypes{t}));
    sdMeanIntervalsTO.(trialTypes{t}) = std(meanIntervalsTO.(trialTypes{t}));
    
    
    fprintf('Interval between FS for "%s" trials: %0.1f (%0.1f) frames.\n',...
        trialTypes{t}, grandMeanIntervalsFS.(trialTypes{t}), sdMeanIntervalsFS.(trialTypes{t}))
    
    fprintf('Interval between TO for "%s" trials: %0.1f (%0.1f) frames.\n',...
        trialTypes{t}, grandMeanIntervalsTO.(trialTypes{t}), sdMeanIntervalsTO.(trialTypes{t}))
    
end

% Disable logging:
LogFile('off');

end
