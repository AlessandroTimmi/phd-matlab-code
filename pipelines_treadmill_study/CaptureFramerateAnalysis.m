function CaptureFramerateAnalysis(varargin)
% Analysis of capture framerate sustained by KinEdge during recording of
% Treadmill Study dataset. Results of this analysis are used for the
% Methodological Study. Kinect theoretical sampling rate is 30 fps.
% The aim of this analysis is to verify if Kinect was capturing data at
% its full potential or if it was slowed down (and how much), i.e. due to
% full blocking (generation 2) Garbage Collection (GC) operations.
%
% Raw KinEdge data from Treadmill Study trials are used for this analysis.
%
% In this analysis we don't need to group by trial type, as the
% speed of the treadmill doesn't affect Kinect capture framerate. However,
% we analyse the capture framerate over the duration of the trials (which
% are supposed to last for 15 s) to see if the accumulation of object in
% memory affects the capture framerate.
%
% Input:
%   rootFolder = (optional parameter, default = '') folder containing all
%                trials to be loaded. The expected folder structure is
%                shown in list_trials_in_all_subjects.m. If empty, a dialog
%                window will open to select a root folder.
%  simplifiedHistograms = (optional paramter, default = false) if true,
%               show only 2 histograms instead of 3 and hide mean and
%               standard deviation of capture framerate.
%
% � November 2016 Alessandro Timmi


%% Default input values:
default.rootFolder = 'C:\TEST_FOLDER\Raw data for methodological paper';
default.simplifiedHistograms = true;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'rootFolder', default.rootFolder,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'simplifiedHistograms', default.simplifiedHistograms,...
    @islogical);

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
rootFolder = p.Results.rootFolder;
simplifiedHistograms = p.Results.simplifiedHistograms;

clear varargin default p


%% Select root folder
if isempty(rootFolder)
    rootFolder = uigetdir('', 'Select root folder containing Kinect data');
    if isequal(rootFolder, 0)
        return
    end
end

fprintf('Analysis of KinEdge capture framerate for the Treadmill Study\n')


%% Load data
% Subfolders for different conditions are not required for
% this study, because trial names are already explicative of the treadmill
% speed, which is the only changing condition.
conditionsFolders = {''};
subjectsPrefix = 'TML';
trialsExt = '.trc';

% List all Kinect trials in all subjects subfolders:
allKinectTrialsFilenames = list_trials_in_all_subjects(rootFolder,...
    subjectsPrefix, conditionsFolders, 'Kinect', trialsExt);

% Load all trials into an array of structs:
kinectArrayOfStructs = LoadTrcToArrayOfStructs(allKinectTrialsFilenames,...
    subjectsPrefix);


%% Preallocate arrays, using resampled length:
newLength = 3000;
captureRateMatrix = nan(length(kinectArrayOfStructs), newLength);
% Since the array of differences is one sample shorter compared to the
% input array, we start the timestamps from 1/30 s instead of 0 s:
newTimeArray = linspace(1/30, 15, newLength);

% Max error allowed for timestamp around 15 s of capture. We will discard
% trials with error larger than this, because resampling would generate
% larger error:
endTimeTolerance = 2/30;

for i = 1:length(kinectArrayOfStructs)
    
    % Find frame closest to 15 s of capture:
    [endTimeError, endIndex] = min(abs(kinectArrayOfStructs(i).time - 15));
    fprintf('Trial = %d, second timestamp = %0.4f s, end index = %d, end time error = %0.4f s.\n',...
        i, kinectArrayOfStructs(i).time(2), endIndex, endTimeError)
    if endTimeError > endTimeTolerance
        fprintf('Skipping trial %d because the error on the end time is > %0.4f s.\n',...
            i, endTimeTolerance)
        continue
    end
    
    % From the current array of timestamps, take only the range between
    % 0 and 15 s:
    timeArray = kinectArrayOfStructs(i).time(1:endIndex);
    
    % Array of differences between consecutive timestamps. NOTE: this is 1
    % sample shorter than the input timeArray.
    diffTimeArray = diff(timeArray);
    
    % Array of instantaneous capture framerate at each frame:
    captureRate = 1 ./ diffTimeArray;
    
    % Resample capture framerate array: remember that differences (and
    % consequently the capture framerate array) start from the second frame.
    % NOTICE: in some cases, there will be NaN at the beginning and/or end
    % of the interpolated array, because we set interpolation to
    % "not extrapolate" and not all trials range exactly between 1/30 and
    % 15 s.
    captureRateInterp = interp1(timeArray(2:end), captureRate, newTimeArray);
    
    % Store into the matrix.
    captureRateMatrix(i,:) = captureRateInterp;
    
    %     figure
    %     plot(timeArray(2:end), captureRate)
    %     hold on
    %     plot(newTimeArray, captureRateInterp)
    %     legend('raw','interp')
end


%% Histogram of capture framerate to check the distribution of data at
% different points during capture time:
if simplifiedHistograms
    trialPercentages = [0.3, 0.9];
else
    trialPercentages = [0.3, 0.6, 0.9];
end
trialPercentagesIndices = round(trialPercentages * newLength);
% Since Kinect captures at about 30 fps, 15 frames correspond to about half
% a second of capture. We need to resample them to the resampling lenght:
halfSecondOfCaptureResampled = round(newLength / 450 * 15);
matlabOrange = [0.8500    0.3250    0.0980];

figure('Name', 'Histograms of capture framerate')
for i = 1 : length(trialPercentagesIndices)
    
    subplot(1, length(trialPercentagesIndices), i)
    
    % Load one second worth of data, centred in correspondence of the
    % current index:
    columnsSpan = trialPercentagesIndices(i) - halfSecondOfCaptureResampled : trialPercentagesIndices(i) + halfSecondOfCaptureResampled;
    oneSecondOfData = captureRateMatrix(:, columnsSpan);
    
    % Calculate some statistical parameters, to see how they fit on the
    % histogram:
    if ~simplifiedHistograms
        meanOneSecond = mean(oneSecondOfData(:), 'omitnan');
        sdOneSecond = std(oneSecondOfData(:), 'omitnan');
        meanMinusSdOneSecond = meanOneSecond - sdOneSecond;
        meanPlusSdOneSecond = meanOneSecond + sdOneSecond;
    end
    medianOneSecond = median(oneSecondOfData(:), 'omitnan');
    % The function quantile() omits NaNs by default:
    firstQuartileOneSecond = quantile(oneSecondOfData(:), 0.25);
    thirdQuartileOneSecond = quantile(oneSecondOfData(:), 0.75);
    
    % Plot the histogram:
    histogram(oneSecondOfData, 'EdgeColor', 'none');
    yLimits = ylim;
    hold on
    
    % Plot statistical parameters:
    if ~simplifiedHistograms
        hMeanSD = fill([meanMinusSdOneSecond, meanPlusSdOneSecond,...
            meanPlusSdOneSecond, meanMinusSdOneSecond],...
            [yLimits(2), yLimits(2), 0, 0],...
            matlabOrange, 'FaceAlpha', 0.20, 'EdgeColor', matlabOrange);
        
        hMean = plot([meanOneSecond, meanOneSecond], [0, yLimits(2)],...
            '--', 'Color', matlabOrange, 'linewidth', 1.5);
    end
    
    hIQR = fill([firstQuartileOneSecond, thirdQuartileOneSecond,...
        thirdQuartileOneSecond, firstQuartileOneSecond],...
        [yLimits(2), yLimits(2), 0, 0],...
        'k', 'FaceAlpha', 0.20, 'EdgeColor', 'k');
    
    hMedian = plot([medianOneSecond, medianOneSecond], [0, yLimits(2)],...
        '--', 'Color', 'k', 'linewidth', 1.5);
    
    ylim(yLimits)
    if simplifiedHistograms
        xlim(custom_axis_margins([min(oneSecondOfData), max(oneSecondOfData)], 0.05))
    else
        xlim(custom_axis_margins([meanOneSecond - sdOneSecond, meanOneSecond + sdOneSecond], 0.3))
    end
    xlabel('Capture framerate [fps]')
    
    if i == 1
        ylabel('Occurrencies [#]')
        
        if simplifiedHistograms
            legend([hMedian, hIQR], {'Median', 'IQR'},...
                'location', 'northwest')
        else
            legend([hMean, hMeanSD, hMedian, hIQR], {'Mean', 'Mean\pmSD',...
                'Median', 'IQR'},...
                'location', 'northwest')
        end
    end
    
    title(sprintf('%d%% of trial time', trialPercentages(i) * 100))
end


%% Calculate statistical parameters in correspondence of each (resampled)
% frame, across the entire trials duration.

% Mean and standard deviation in correspondence of each frame,
% ignoring NaNs:
meanCaptureRate = mean(captureRateMatrix, 1, 'omitnan');
sdCaptureRate = std(captureRateMatrix, 'omitnan');

% Mean +/- sd in correspondence of each frame:
meanPlusSd = meanCaptureRate + sdCaptureRate;
meanMinusSd = meanCaptureRate - sdCaptureRate;

% Quartiles of the capture framerate over time:
medianCaptureRate = median(captureRateMatrix, 1, 'omitnan');
firstQuartileCaptureRate = quantile(captureRateMatrix, 0.25, 1);
thirdQuartileCaptureRate = quantile(captureRateMatrix, 0.75, 1);


%% Plot statistical results over capture time.
% Plot mean +/- SD:
myTitle = 'Capture framerate - Mean\pmSD';
figure('Name', myTitle)
hMeanSD = fill([newTimeArray, fliplr(newTimeArray)],...
    [meanPlusSd, fliplr(meanMinusSd)], matlabOrange,...
    'FaceAlpha', 0.35, 'EdgeColor', 'none');
hold on
hMean = plot(newTimeArray, meanCaptureRate, '-', 'Color', matlabOrange);
xlim([0, 15])
ax = gca;
ax.YGrid = 'on';
title(myTitle)
xlabel('Capture time (s)')
ylabel('Capture framerate (fps)')
legend([hMean, hMeanSD], {'Mean', 'Mean\pmSD'}, 'location', 'northwest')


% Plot mean and SD separately
myTitle2 = 'Capture framerate - Mean and SD';
figure('Name', myTitle2)

subplot(2,1,1)
plot(newTimeArray, meanCaptureRate, 'Color', matlabOrange)
xlim([0, 15])
ax = gca;
ax.YGrid = 'on';
title(myTitle2)
xlabel('Capture time (s)')
ylabel('Mean capture framerate (fps)')

subplot(2,1,2)
plot(newTimeArray, sdCaptureRate, 'Color', matlabOrange)
xlim([0, 15])
ax = gca;
ax.YGrid = 'on';
xlabel('Capture time (s)')
ylabel('SD of capture framerate (fps)')


% Plot quartiles:
myTitle3 = 'Capture framerate - Quartiles';
figure('Name', myTitle3)
hFill = fill([newTimeArray, fliplr(newTimeArray)],...
    [thirdQuartileCaptureRate, fliplr(firstQuartileCaptureRate)], 'k',...
    'FaceAlpha', 0.35, 'EdgeColor', 'none');
hold on
hMedian = plot(newTimeArray, medianCaptureRate, '-', 'Color', 'k');
xlim([0, 15])
ax = gca;
ax.YGrid = 'on';
title(myTitle3)
xlabel('Capture time (s)')
ylabel('Capture framerate (fps)')
legend([hMedian, hFill], {'Median', 'Interquartile range'}, 'location', 'northwest')

end
