function output_trials = SelectAndStorePreprocessedTreadmillTrials(kinect_trc,...
    vicon_trc, new_root_folder, varargin)
% Copy only one .trc file per type (the one with smaller trial number) to
% a new root folder, preserving the original root folder structure. All
% subfixes following the trial number will be removed in copied files.
% For example:
%   C:\Root_folder\Kinect\SUB01\gait_slow_01_midpoint_sync.trc
% will be copied to the following destination:
%   C:\NEW_root_folder\Kinect\SUB01\gait_slow_01.trc
% and:
%   C:\Root_folder\Kinect\SUB01\gait_slow_02_midpoint_sync.trc
% will be skipped.
%
% Input:
%   kinect_trc  = row cell array containing full filenames of Kinect trials.
%   vicon_trc   = row cell array containing full filenames of Vicon trials.
%   new_root_folder = folder in which all Kinect and Vicon trials will be
%                 stored, preserving the original root folder structure.
%   debug_mode  = (optional parameter, default = false). If true, displays
%                 some extra info for debug purposes.
%
% Output:
%   output_trials = row cell array with full filenames of all trials.
%
% � June 2016 Alessandro Timmi

%% Default input values:
default.debug_mode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'kinect_trc',...
    @(x) validateattributes(x, {'cell'}, {'nonempty', 'row'}));
addRequired(p, 'vicon_trc',...
    @(x) validateattributes(x, {'cell'}, {'nonempty', 'row'}));
addRequired(p, 'new_root_folder', @ischar);

% Add optional input arguments:
addParameter(p, 'debug_mode', default.debug_mode, @islogical);

% Parse input arguments:
parse(p, kinect_trc, vicon_trc, new_root_folder, varargin{:});

% Copy input arguments into more memorable variables:
kinect_trc = p.Results.kinect_trc;
vicon_trc = p.Results.vicon_trc;
debug_mode = p.Results.debug_mode;

clear varargin default p


%%
fprintf('Storing final pre-processed trials in the following new root folder:\n')
fprintf('\t%s\n', new_root_folder);


%% Horizontally concatenate the two cell arrays containing Kinect and Vicon
% trials names and sort them, just for safety:
input_trials = sort(horzcat(kinect_trc, vicon_trc));

% Preallocate a cell array to store all destination filenames:
output_trials = cell(size(input_trials));

old_matched_expression = {''};

for f=1:length(input_trials)   
    % This expression specifies that the string we are interested in:
    %   - begins with 1 or more alpha-numeric characters (\w+)
    %   - followed by slash (\\)
    %   - followed by "TML"
    %   - followed by 2 numeric digits (\d\d)
    %   - followed by slash (\\)
    %   - followed by 1 or more alpha-numeric characters (\w+)
    %   - ends with 2 numeric digits (\d\d)
    expression = '\w+\\TML\d\d\\\w+\d\d';
    
    % Find string matching the expression in current trial filename:
    matched_expression = regexp(input_trials{f}, expression, 'match');
    
    fprintf('Old matched expression: %s\n', old_matched_expression{1})
    fprintf('Current matched expression: %s\n', matched_expression{1})
    
    if isequal(old_matched_expression{1}(1:end-2), matched_expression{1}(1:end-2))
        % This means that we just found another trial of the same device,
        % subject and type. Move to the next one.
        fprintf('Skip current trial, because we already stored one for this device, subject and type.\n\n')
        continue
    end
    
    % Load the .trc file to be copied and store its content into a struct:
    s = LoadTrcFile(...
        'filename', input_trials{f},...
        'logMessage', sprintf('Loading file %d of %d:\n', f, numel(input_trials)));
    
    % Assemble destination filename:
    output_trials{f} = fullfile(new_root_folder, [matched_expression{1}, '.trc']);
    fprintf('Destination filename:\n%s\n', output_trials{f});
    
    % If the destination folder doesn't exist, make it:
    output_path = fileparts(output_trials{f});
    if ~isequal(exist(output_path, 'dir'), 7)
        mkdir(output_path);
    end
    
    % Write the .trc file (updating its header) to the new destination:
    WriteTrcFile(s, 'destinationFilename', output_trials{f});
    
    % Store current matched_expression for next iteration:
    old_matched_expression = matched_expression;
    
end