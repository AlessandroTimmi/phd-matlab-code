% Concatenate marker coordinates from all trials of the Treadmill Study
% and store them into a .tsv file for each trial type, according to Prof
% Ian Gordon's template, to facilitate the statistical analysis.
% Data are BALANCED (same number of samples is used for each trial type).
%
% � October 2016 Alessandro Timmi
clc
clear

rootFolder = 'C:\TEST_FOLDER\5 - Ready for statistical analysis (Bland-Altman analyses included)';


%% Select root folder containing all treadmill trials ready for
% statistical analysis:
if isempty(rootFolder)
    rootFolder = uigetdir('', 'Select root folder containing Kinect and Vicon data');
    if isequal(rootFolder, 0)
        return
    end
end


%% Subfolders for different conditions are not required for
% this study, because trial names are already explicative of the treadmill
% speed, which is the only changing condition.
conditionsFolders = {''};
subjectsPrefix = 'TML';
trialTypes = {'gait_slow', 'gait_fast', 'run_slow', 'run_fast'};

for t = 1:length(trialTypes)
    
    fprintf(['Concatenating and storing marker coordinates from "%s" trials of the Treadmill Study '...
    'into a single large file...\n'], trialTypes{t})
    
    % List trials in all subjects subfolders for current trial type:
    [kinectFilenames, viconFilenames] = ListTrialTypeAgreementStudy(...
        trialTypes{t}, 'rootFolder', rootFolder);
        
    % Store trials into an array of structs (one for each device):
    kinectArrayOfStructs = LoadTrcToArrayOfStructs(kinectFilenames, subjectsPrefix);
    viconArrayOfStructs = LoadTrcToArrayOfStructs(viconFilenames, subjectsPrefix);
    
    % Check that the two arrays of structs have matching characteristics:
    CompareArraysOfStructsTreadmillStudy(kinectArrayOfStructs, viconArrayOfStructs)
    
    
    %% Write arrays of structs into a single long text file:
    treadmillStudyConcatenatedDataFilename = fullfile(rootFolder,...
        sprintf('%s - Balanced marker coordinates.tsv', trialTypes{t}));
    
    WriteArraysOfStructsToTsv(kinectArrayOfStructs, viconArrayOfStructs,...
        'destFilename', treadmillStudyConcatenatedDataFilename,...
        'balanced', true);
    
end

