function BlandAltman1986MarkerCoordinatesTreadmillStudy(varargin)
% Bland-Altman (The Lancet - 1986 version) analysis of agreement
% on the dataset of the treadmill study. The agreement of the linear marker
% coordinates (X, Y and Z) is analysed. The dataset must be already synced
% and downsampled to Kinect native framerate.
%
% Our study design doesn't match the one described in Bland and Altmans's
% 1986 paper. Therefore, performing the analysis of agreement without
% taking into account this aspect is incorrect. However, it is a good
% "benchmark" to have a first impression on the agreement between Kinect
% and Vicon.
%
% Input:
%   trialTypes = (optional parameter, default = see below) cell array of
%                strings containing types of trials to be loaded. See
%                ListTrialTypeAgreementStudy.m for accepted strings.
%   markerNames = (optional parameter, default = see below) names of the
%                markers used in the treadmill study.
%   rootFolder = (optional parameter, default = see below) folder
%                containing all trials to be loaded. The expected folder
%                structure is reported in list_trials_in_all_subjects.m. If
%                empty, a dialog window will open to select a root folder.
%
% � June 2016 Alessandro Timmi


%% Default input values:
default.trialTypes = {'gait_slow', 'gait_fast', 'run_slow', 'run_fast'};
default.markerNames = markerset('Agreement_dynamic_coloured');
default.rootFolder = 'C:\TEST_FOLDER\5 - Ready for statistical analysis (Bland-Altman analyses included)';

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'trialTypes', default.trialTypes,...
    @(x) validateattributes(x, {'cell'}, {'nonempty'}));
addParameter(p, 'markerNames', default.markerNames,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'rootFolder', default.rootFolder,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
trialTypes = p.Results.trialTypes;
markerNames = p.Results.markerNames;
rootFolder = p.Results.rootFolder;

clear varargin default p


%% Select root folder
if isempty(rootFolder)
    rootFolder = uigetdir('', 'Select root folder containing Kinect and Vicon data');
    if isequal(rootFolder, 0)
        return
    end
end

% Enable logging:
LogFile(fullfile(rootFolder, 'Bland-Altman1986MarkerCoordinatesTreadmillStudy.log'));

fprintf('Bland-Altman analysis of agreement (1986 version) for the treadmill study (linear coordinates)\n')

subjectsPrefix = 'TML';
% Number of coords (X, Y, Z) for each marker:
nCoords = 3;
% Preallocate storage arrays for biases and limits of agreement:
biasStorage = zeros(nCoords, length(markerNames), length(trialTypes));
lloaStorage = zeros(size(biasStorage));
uloaStorage = zeros(size(biasStorage));

% Since we want subplots to advance column-wise instead of row-wise (which
% is the default in MATLAB), we need to create custom subplot indices:
spIDs = columnwise_subplots_ids(numel(markerNames), nCoords);

% Analyse data by trial type:
for t = 1:length(trialTypes)
    
    % List trials of a specific type for both Kinect and Vicon:
    [kinectFilenames, viconFilenames] =...
        ListTrialTypeAgreementStudy(trialTypes{t},...
        'rootFolder', rootFolder);
    
    % Load trials into arrays of structs:
    kinectArrayOfStructs = LoadTrcToArrayOfStructs(kinectFilenames, subjectsPrefix);
    viconArrayOfStructs = LoadTrcToArrayOfStructs(viconFilenames, subjectsPrefix);
    
    % Check correspondence of structs between the two devices:
    CompareArraysOfStructsTreadmillStudy(kinectArrayOfStructs, viconArrayOfStructs);
    
    % Concatenate each array of structs into a single struct:
    kinectConcatenated = ConcatenateArrayOfTrcStructs(kinectArrayOfStructs);
    viconConcatenated = ConcatenateArrayOfTrcStructs(viconArrayOfStructs);
    
    % Check that both concatenated structs have same length:
    CompareLengthConcatenatedTrcStructs(kinectConcatenated, viconConcatenated);
    
    
    % Generate figures handles outside the "for" loop, to avoid having
    % multiple figures:
    hfigHistogram = figure('Name', sprintf('Histograms of the differences - %s',...
        trialTypes{t}));
    hfigBA = figure('Name', sprintf('Bland-Altman plots - %s',...
        trialTypes{t}));
    
    for m = 1:length(markerNames)
        for c = 1:nCoords
            % Make histogram figure current:
            figure(hfigHistogram)
            % Get the handle of the current subplot position:
            hax_hist = subplot(nCoords, length(markerNames), spIDs(c, m));
            % Make the Bland-Altman figure current:
            figure(hfigBA)
            % Get the handle of the current subplot position:
            hax_ba = subplot(nCoords, length(markerNames), spIDs(c, m));
            
            % Perform Bland-Altman analysis of agreement for current marker
            % coordinate of current trial type:
            [biasStorage(c,m,t), lloaStorage(c,m,t), uloaStorage(c,m,t)] =...
                bland_altman_1986(...
                viconConcatenated.markers.(markerNames{m})(:, c),...
                kinectConcatenated.markers.(markerNames{m})(:, c),...
                markerNames{m},...
                num2coord(c),...
                'm',...
                'hax_hist', hax_hist,...
                'hax_ba', hax_ba);
        end
    end
    
end


%% Plot results of the Bland-Altman analysis vs treadmill speed (trial type):
treadmillSpeeds = speeds_treadmill_study(trialTypes);
titleBAvsSpeed = 'Bland-Altman analysis of agreement (1986 version) vs treadmill speed';
figure('Name', titleBAvsSpeed)
plotMarkerSize = 16;

lowerBound = min(lloaStorage(:));
upperBound = max(uloaStorage(:));

for c = 1:nCoords
    for m = 1:numel(markerNames)
        hax = subplot(nCoords, numel(markerNames), spIDs(c, m));
        hold(hax, 'on');
        
        plot(hax, treadmillSpeeds, squeeze(biasStorage(c,m,:)),...
            'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', ':');
        
        plot(hax, treadmillSpeeds, squeeze(lloaStorage(c,m,:)),...
            'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', '--');
        
        plot(hax, treadmillSpeeds, squeeze(uloaStorage(c,m,:)),...
            'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', '--');
        
        hax.XTick = treadmillSpeeds;
        hax.XLim = custom_axis_margins(treadmillSpeeds, 0.15);
        hax.YGrid = 'on';
        hax.YAxis.Exponent = 0;
        hax.YLim = custom_axis_margins([lowerBound, upperBound], 0.03);
        
        xlabel(hax, 'Treadmill speed [m/s]')
        ylabel(hax, {'Kinect - Vicon'; sprintf('%s [m]', num2coord(c))})
        if m == 1 && c == 1
            legend(hax, 'Bias', 'Limits of agreement', 'location', 'best' )
        end
        
        if m == 2 && c == 1
            title(hax, {titleBAvsSpeed; markerNames{m}})
        else
            title(markerNames{m});
        end
        
    end
end


%% Plot summarizing all results, using the ANK marker as worst case scenario.
AnkleMarkerNumber = 3;
titleAnkle = sprintf('Bland-Altman (1986) vs treadmill speed - %s marker',...
    markerNames{AnkleMarkerNumber});

figure('Name', titleAnkle)

for c = 1:nCoords
    
    hax = subplot(1, AnkleMarkerNumber, c);
    hold(hax, 'on')
    
    plot(hax, treadmillSpeeds, squeeze(biasStorage(c, AnkleMarkerNumber, :)),...
        'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', ':');
    
    plot(hax, treadmillSpeeds, squeeze(lloaStorage(c, AnkleMarkerNumber, :)),...
        'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', '--');
    
    plot(hax, treadmillSpeeds, squeeze(uloaStorage(c, AnkleMarkerNumber, :)),...
        'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', '--');
    
    hax.XTick = treadmillSpeeds;
    hax.XLim = custom_axis_margins(treadmillSpeeds, 0.15);
    hax.YGrid = 'on';
    hax.YAxis.Exponent = 0;
    hax.YLim = custom_axis_margins([lowerBound, upperBound], 0.03);
    
    xlabel(hax, 'Treadmill speed [m/s]')
    ylabel(hax, {'Kinect - Vicon'; sprintf('%s [m]', num2coord(c))})
    
    if c == 2
        title(hax, titleAnkle)
    end
    
    if c == 1
        legend(hax, 'Bias', 'LOA', 'location', 'best' )
    end
end

% Disable logging:
LogFile('off');

end