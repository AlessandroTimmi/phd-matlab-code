% Compare results of the Treadmill Study obtained from two different
% Bland-Altman analyses of agreement on marker coordinates:
% - "naive": ignoring the structure of the data and applying the simple
%   analysis suggested in Bland and Altman's paper on The Lancet (1986).
% - "Gordon": taking into account the structure of the data via a random
%   effect model, which includes within subjects and between-subjects
%   variation and autocorrelation.
% NOTE: "balanced" data were used for each analysis, which means all trials
% (for a specific treadmill speed) were cropped at the same length.
% NOTE: UNITS are in mm here, as opposed to my usual convention of using m.
%
% � September 2016 Alessandro Timmi

clc
%close all
clear

%% Input parameters:
% True to compare "Naive" and "Gordon" methods, false to show only "Gordon"
% method:
compareMethods = false;

% Reduce the number of labels if the plot must be used in a presentation:
presentationPlot = false;

if presentationPlot
    myFontSize = 13;
else
    % Default Matlab font size:
    myFontSize = 11;
end

%% Input data:
rootFolder = 'C:\TEST_FOLDER\5 - Ready for statistical analysis (Bland-Altman analyses included)\Bland-Altman analysis (Gordon version)';
%rootFolder = 'C:\TEST_FOLDER\Treadmill study';

%resultsFilename = fullfile(rootFolder,...
%    'Bland-Altman analysis (Gordon version)\adjusted limits and margins of error 16-09-22_AT.xlsx');
resultsFilename = fullfile(rootFolder,...
    'adjusted limits and margins of error 16-09-22_AT.xlsx');
trialTypes = {'gait_slow', 'gait_fast', 'run_slow', 'run_fast'};
markerNames = markerset('Agreement_dynamic_coloured');

% Number of coords (X, Y, Z) for each marker:
nCoords = 3;

% Preallocate storage arrays for biases and limits of agreement:
gordonBiasStorage = zeros(nCoords, length(markerNames), length(trialTypes));
gordonLloaStorage = zeros(size(gordonBiasStorage));
gordonUloaStorage = zeros(size(gordonBiasStorage));

naiveBiasStorage = zeros(size(gordonBiasStorage));
naiveLloaStorage = zeros(size(gordonBiasStorage));
naiveUloaStorage = zeros(size(gordonBiasStorage));

% Since we want subplots to advance column-wise instead of row-wise (which
% is the default in MATLAB), we need to create custom subplot indices:
spIDs = columnwise_subplots_ids(numel(markerNames), nCoords);


%% Read results from Gordon's spreadsheet:
gordonGaitFastBias = xlsread(resultsFilename, 'D3:D11');
gordonGaitFastLoa = xlsread(resultsFilename, 'K3:L11');
naiveGaitFastBias = xlsread(resultsFilename, 'P3:P11');
naiveGaitFastLoa = xlsread(resultsFilename, 'R3:S11');

gordonGaitSlowBias = xlsread(resultsFilename, 'D15:D23');
gordonGaitSlowLoa = xlsread(resultsFilename, 'K15:L23');
naiveGaitSlowBias = xlsread(resultsFilename, 'P15:P23');
naiveGaitSlowLoa = xlsread(resultsFilename, 'R15:S23');

gordonRunFastBias = xlsread(resultsFilename, 'D25:D33');
gordonRunFastLoa = xlsread(resultsFilename, 'K25:L33');
naiveRunFastBias = xlsread(resultsFilename, 'P25:P33');
naiveRunFastLoa = xlsread(resultsFilename, 'R25:S33');

gordonRunSlowBias = xlsread(resultsFilename, 'D37:D45');
gordonRunSlowLoa = xlsread(resultsFilename, 'K37:L45');
naiveRunSlowBias = xlsread(resultsFilename, 'P37:P45');
naiveRunSlowLoa = xlsread(resultsFilename, 'R37:S45');

% Copy results into the storage arrays:
newSize = [nCoords, length(markerNames)];
naiveBiasStorage(:,:,1) = reshape(naiveGaitSlowBias, newSize);
naiveBiasStorage(:,:,2) = reshape(naiveGaitFastBias, newSize);
naiveBiasStorage(:,:,3) = reshape(naiveRunSlowBias, newSize);
naiveBiasStorage(:,:,4) = reshape(naiveRunFastBias, newSize);

naiveLloaStorage(:,:,1) = reshape(naiveGaitSlowLoa(:,1), newSize);
naiveLloaStorage(:,:,2) = reshape(naiveGaitFastLoa(:,1), newSize);
naiveLloaStorage(:,:,3) = reshape(naiveRunSlowLoa(:,1), newSize);
naiveLloaStorage(:,:,4) = reshape(naiveRunFastLoa(:,1), newSize);

naiveUloaStorage(:,:,1) = reshape(naiveGaitSlowLoa(:,2), newSize);
naiveUloaStorage(:,:,2) = reshape(naiveGaitFastLoa(:,2), newSize);
naiveUloaStorage(:,:,3) = reshape(naiveRunSlowLoa(:,2), newSize);
naiveUloaStorage(:,:,4) = reshape(naiveRunFastLoa(:,2), newSize);


gordonBiasStorage(:,:,1) = reshape(gordonGaitSlowBias, newSize);
gordonBiasStorage(:,:,2) = reshape(gordonGaitFastBias, newSize);
gordonBiasStorage(:,:,3) = reshape(gordonRunSlowBias, newSize);
gordonBiasStorage(:,:,4) = reshape(gordonRunFastBias, newSize);

gordonLloaStorage(:,:,1) = reshape(gordonGaitSlowLoa(:,1), newSize);
gordonLloaStorage(:,:,2) = reshape(gordonGaitFastLoa(:,1), newSize);
gordonLloaStorage(:,:,3) = reshape(gordonRunSlowLoa(:,1), newSize);
gordonLloaStorage(:,:,4) = reshape(gordonRunFastLoa(:,1), newSize);

gordonUloaStorage(:,:,1) = reshape(gordonGaitSlowLoa(:,2), newSize);
gordonUloaStorage(:,:,2) = reshape(gordonGaitFastLoa(:,2), newSize);
gordonUloaStorage(:,:,3) = reshape(gordonRunSlowLoa(:,2), newSize);
gordonUloaStorage(:,:,4) = reshape(gordonRunFastLoa(:,2), newSize);


%% Plot results of the Bland-Altman analysis vs treadmill speed:
treadmillSpeeds = speeds_treadmill_study(trialTypes);
if compareMethods
    titleBAvsSpeed = 'Bland-Altman analyses comparison - "Naive" vs "Gordon"';
else
    titleBAvsSpeed = 'Bland-Altman analysis "Gordon"';
end
figure('Name', titleBAvsSpeed, 'Position', [100, 100, 900, 800])
plotMarkerSizeNaive = 20;
plotMarkerSize = 16;

lowerBound = min(gordonLloaStorage(:));
upperBound = max(gordonUloaStorage(:));

for c = 1:nCoords
    for m = 1:numel(markerNames)
        hax = subplot(nCoords, numel(markerNames), spIDs(c, m));
        hold(hax, 'on');
        
        if compareMethods
            hNaiveBias = plot(hax, treadmillSpeeds, squeeze(naiveBiasStorage(c,m,:)),...
                'r', 'Marker', '.', 'MarkerSize', plotMarkerSizeNaive, 'linestyle', '--');
            
            plot(hax, treadmillSpeeds, squeeze(naiveLloaStorage(c,m,:)),...
                'r', 'Marker', '.', 'MarkerSize', plotMarkerSizeNaive, 'linestyle', '-');
            
            plot(hax, treadmillSpeeds, squeeze(naiveUloaStorage(c,m,:)),...
                'r', 'Marker', '.', 'MarkerSize', plotMarkerSizeNaive, 'linestyle', '-');
        end
        
        hGordonBias = plot(hax, treadmillSpeeds, squeeze(gordonBiasStorage(c,m,:)),...
            'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', ':');
        
        hGordonLoa = plot(hax, treadmillSpeeds, squeeze(gordonLloaStorage(c,m,:)),...
            'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', '--');
        
        plot(hax, treadmillSpeeds, squeeze(gordonUloaStorage(c,m,:)),...
            'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', '--');
        
        hax.XTick = treadmillSpeeds;
        hax.XLim = custom_axis_margins(treadmillSpeeds, 0.15);
        hax.YGrid = 'on';
        hax.YAxis.Exponent = 0;
        hax.YLim = custom_axis_margins([lowerBound, upperBound], 0.03);
        
        if presentationPlot
            hax.XAxis.TickLabelFormat = '%.1f';
        end
        
        if ~presentationPlot || c == 3
            xlabel(hax, 'Treadmill speed [m/s]')
        end
        
        if ~presentationPlot || m == 1
            ylabel(hax, {'KinEdge - Vicon'; sprintf('%s [mm]', num2coord(c))})
            if m == 1 && c == 1
                if compareMethods
                    legend(hax,...
                        [hNaiveBias, hGordonBias], {'Bias and LOA "Naive"', 'Bias and LOA "Gordon"'},...
                        'location', 'NorthEast' )
                else
                    legend(hax,...
                        [hGordonBias, hGordonLoa], {'Bias', 'LOA'},...
                        'location', 'NorthEast' )
                end
            end
        end
        
        if ~presentationPlot || c==1
            title(hax, markerNames{m})
        end
        
        
    end
end

if presentationPlot
    fig = gcf;
    set(findall(fig, '-property', 'FontSize'), 'FontSize', myFontSize)
end