function [kinectTrials, viconTrials] =...
    ListTrialTypeAgreementStudy(trialType, varargin)
% List full filenames of a specific type of trials of the agreement study,
% given a root folder and a string identifying the trial type.
%
% Input:
%   trialType  = string identifying the trial type. See below for the
%                 allowed strings.
%   rootFolder = (optional parameter, default = '') folder containing all
%                 trials to be loaded. The expected folder structure is
%                 shown in list_trials_in_all_subjects.m.
%   debugMode  = (optional parameter, default = false). If true, displays
%                 some extra info for debug purposes.
%
% Output:
%   kinectTrials = row cell array containing full filenames of Kinect
%           trials matching the specified type.
%   viconTrials = row cell array containing full filenames of Vicon
%           trials matching the specified type.
%
% � June 2016 Alessandro Timmi


%% Default input values:
expectedTrialTypes = {'gait_slow', 'gait_fast', 'run_slow', 'run_fast'};
default.rootFolder = '';
default.debugMode = false;

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add required input arguments:
addRequired(p, 'trialType',...
    @(x) any(validatestring(x, expectedTrialTypes)));

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'rootFolder', default.rootFolder,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));
addParameter(p, 'debugMode', default.debugMode, @islogical);

% Parse input arguments:
parse(p, trialType, varargin{:});

% Copy input arguments into more memorable variables:
trialType = p.Results.trialType;
rootFolder = p.Results.rootFolder;
debugMode = p.Results.debugMode;

clear varargin default p


%%
if isempty(rootFolder)
    rootFolder = uigetdir('', 'Select root folder containing Kinect and Vicon data');
    if isequal(rootFolder, 0)
        return
    end
end


%% Subfolders for different conditions are not required for
% this study, because trial names are already explicative of the treadmill
% speed, which is the only changing condition.
conditionsFolders = {''};
subjectsPrefix = 'TML';
trialsExt = '.trc';

% List all Kinect trials in all subjects subfolders:
allKinectTrialsFilenames = list_trials_in_all_subjects(rootFolder,...
    subjectsPrefix, conditionsFolders, 'Kinect', trialsExt);

% List all Vicon trials in all subjects subfolders:
allViconTrialsFilenames = list_trials_in_all_subjects(rootFolder,...
    subjectsPrefix, conditionsFolders, 'Vicon', trialsExt);


%% Find trials matching specified type:
% Look for the specified string in all trials filenames. The returned cell
% array has the same dimensions of the input one. When the string is found,
% the corresponding cell contains the starting position of the string in
% the filename. When the string is not found, the corresponding cell is empty.
stringPositionsKinect = strfind(allKinectTrialsFilenames, trialType);
stringPositionsVicon = strfind(allViconTrialsFilenames, trialType);

% Array of logicals: 1 when the string is found, 0 otherwise:
booleanKinect = ~cellfun('isempty', stringPositionsKinect);
booleanVicon = ~cellfun('isempty', stringPositionsVicon);

% Cell arrays containing only trials of the specified type:
kinectTrials = allKinectTrialsFilenames(booleanKinect);
viconTrials = allViconTrialsFilenames(booleanVicon);

if debugMode
    fprintf('\n[Debug] Kinect trials of type "%s":\n', trialType)
    for f=1:length(kinectTrials)
        fprintf('%s\n', kinectTrials{f})
    end
    
    fprintf('\n[Debug] Vicon trials of type "%s":\n', trialType)
    for f=1:length(viconTrials)
        fprintf('%s\n', viconTrials{f})
    end
end



end




