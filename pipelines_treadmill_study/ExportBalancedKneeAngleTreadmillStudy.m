% Concatenate knee flexion angle calculated from all trials of the
% Treadmill Study and store it into a .tsv file for each trial type,
% according to Prof Ian Gordon's template, to facilitate the statistical
% analysis.
% Data are BALANCED (same number of samples is used for each trial type).
%
% � October 2016 Alessandro Timmi
clc
clear

rootFolder = 'C:\TEST_FOLDER\5 - Ready for statistical analysis (Bland-Altman analyses included)';


%% Select root folder containing all treadmill trials ready for
% statistical analysis:
if isempty(rootFolder)
    rootFolder = uigetdir('', 'Select root folder containing Kinect and Vicon data');
    if isequal(rootFolder, 0)
        return
    end
end


%% Subfolders for different conditions are not required for
% this study, because trial names are already explicative of the treadmill
% speed, which is the only changing condition.
conditionsFolders = {''};
subjectsPrefix = 'TML';
trialTypes = {'gait_slow', 'gait_fast', 'run_slow', 'run_fast'};

for t = 1:length(trialTypes)
    
    fprintf(['Concatenating and storing knee angle data from "%s" trials of the Treadmill Study '...
        'into a single large file...\n'], trialTypes{t})
    
    % List trials in all subjects subfolders for current trial type:
    [kinectFilenames, viconFilenames] = ListTrialTypeAgreementStudy(...
        trialTypes{t}, 'rootFolder', rootFolder);
    
    % Store trials into an array of structs (one for each device):
    viconArrayOfStructs = LoadTrcToArrayOfStructs(viconFilenames, subjectsPrefix);
    kinectArrayOfStructs =  LoadTrcToArrayOfStructs(kinectFilenames, subjectsPrefix);
    
    % Check that the two arrays of structs have matching characteristics:
    CompareArraysOfStructsTreadmillStudy(kinectArrayOfStructs, viconArrayOfStructs)
    
    % Detect gait events and calculate knee flexion angle:
    [kinectArrayOfStructs, viconArrayOfStructs] = GetKneeFlexionAngleAndGaitEvents(...
        kinectArrayOfStructs, viconArrayOfStructs);
    
    
    %% Write THETA from arrays of structs into a single long text file:
    destinationFilename = fullfile(rootFolder,...
        sprintf('%s - Balanced knee angles.tsv', trialTypes{t}));
    
    WriteKneeAnglesArrayOfStructsToTsv(kinectArrayOfStructs, viconArrayOfStructs,...
        {'theta'},...
        'destFilename', destinationFilename,...
        'balanced', true);
    
    
    %% Write THETA FS from arrays of structs into a single long text file:
    destinationFilename = fullfile(rootFolder,...
        sprintf('%s - Balanced knee angles FS.tsv', trialTypes{t}));
    
    WriteGaitEventsArrayOfStructsToTsv(kinectArrayOfStructs, viconArrayOfStructs,...
        'thetaFS',...
        'locsFS',...
        'destFilename', destinationFilename,...
        'balanced', false);
    
    
    %% Write THETA TO from arrays of structs into a single long text file:
    destinationFilename = fullfile(rootFolder,...
        sprintf('%s - Balanced knee angles TO.tsv', trialTypes{t}));
    
    WriteGaitEventsArrayOfStructsToTsv(kinectArrayOfStructs, viconArrayOfStructs,...
        'thetaTO',...
        'locsTO',...
        'destFilename', destinationFilename,...
        'balanced', false);
    
end
