function CompareArraysOfStructsTreadmillStudy(kinectArrayOfStructs, viconArrayOfStructs)
% Perform some correspondence checks on two arrays of structs for the
% Treadmill Study.
%
% Input:
%   kinectArrayOfStructs = array of structs containing .trc data collected
%               using Kinect.
%   viconArrayOfStructs = array of structs containing .trc data collected
%               using Vicon.
%
% � September 2016 Alessandro Timmi


% Check that both arrays of structs have same number of trials:
assert(isequal(length(kinectArrayOfStructs), length(viconArrayOfStructs) ),...
    'The two arrays of structs have different lengths')


for f = 1:length(kinectArrayOfStructs)
    % Check correspondence of filenames (name and extension) between both
    % arrays of structs:
    assert(strcmpi(...
        kinectArrayOfStructs(f).filename, viconArrayOfStructs(f).filename),...
        'Trials %d of %d have different filenames.', f, length(kinectArrayOfStructs))
    
    % Check correspondence of frames number:
    assert(isequal(...
        kinectArrayOfStructs(f).NumFrames, viconArrayOfStructs(f).NumFrames),...
        'Trials %d of %d have different frames numbers', f, length(kinectArrayOfStructs))
end

% Check that both arrays of structs have same marker names. Since we check
% within the same array of struct at creation time, we can just compare the
% first struct from both arrays:
assert(isequal(...
    sort(fieldnames(kinectArrayOfStructs(1).markers)),...
    sort(fieldnames(viconArrayOfStructs(1).markers)) ),...
    'The two arrays of structs have different marker names.')

end