function [kinectArrayOfStructs, viconArrayOfStructs] =...
    GetGaitEventsAndSpatiotemporalParameters(kinectArrayOfStructs, viconArrayOfStructs)
% Detect gait events and calculate spatiotemporal parameters from data
% collected during the Treadmill Study. Input arrays of structs are
% returned with results added as new fields.

% NOTE: we don't calculate "swing time" because, for both Vicon and
% Kinect dataset, we use events locations detected from Vicon data.
% Consequently, since the two datasets are synced, the swing times would
% be identical and the analysis of agreement would return 0 s as bias and
% (0,0) s as limits of agreement.
% An alternative would be to detect gait events separately for Vicon and
% Kinect, but that would be a less reliable approach, due to Kinect data
% being more noisy. This would lead to (rare) different number of gait
% events between the two datasets, generating large disagreement in the
% BA analysis, which however would not reflect the actual agreement in
% correspondence of those events.
% Since generally gait events are detected by an external and accurate
% device (typically a force plate), it makes sense in this instance to use
% data from Vicon, which is the most accurate between the two analysed
% systems.
%
% Input:
%   kinectArrayOfStructs = array of structs containing .trc data from the
%           Treadmill study, collected using Kinect.
%   viconArrayOfStructs = array of structs containing .trc data from the
%           Treadmill study, collected using Vicon.
% Output:
%   kinectArrayOfStructs = same as input, but with added fields:
%            swingLength.
%   viconArrayOfStructs = same as input, but with added fields:
%            swingLength.
%
% � November 2016 Alessandro Timmi

% Loop on trials:
for t = 1:length(viconArrayOfStructs)
    
    % Gait events detection from each struct: foot strike (FS) and toe
    % off (TO). Since data have been interpolated at the same framerate,
    % synced and trimmed, we can detect gait events USING JUST VICON DATA,
    % which are supposed to be more accurate than those from Kinect.
    [locsFS, locsTO] = detect_gait_events_simple(viconArrayOfStructs(t));    
    
    % Preallocate arrays of swing length:
    viconArrayOfStructs(t).swingLength = zeros(length(locsTO), 1);
    kinectArrayOfStructs(t).swingLength = zeros(size(viconArrayOfStructs(t).swingLength));
        
    % Loop on frames and calculate swing length:
    for f = 1:length(locsTO)
        
        % Step length:
        viconArrayOfStructs(t).swingLength(f) =...
            norm(...
            viconArrayOfStructs(t).markers.ANK(locsFS(f+1),:) -...
            viconArrayOfStructs(t).markers.ANK(locsTO(f),:));
        
        kinectArrayOfStructs(t).swingLength(f) =...
            norm(...
            kinectArrayOfStructs(t).markers.ANK(locsFS(f+1),:) -...
            kinectArrayOfStructs(t).markers.ANK(locsTO(f),:));
        
    end
    
end

end