function BlandAltman1986Spatiotemporal(varargin)
% Bland-Altman (The Lancet - 1986 version) analysis of agreement
% on the dataset of the Treadmill Study (but used for the Methodological
% Study). The agreement between KinEdge and Vicon is assessed for some
% spatiotemporal parameters:
% - swing length.
% The dataset must be already synced.
%
% Our study design doesn't match the one described in BA's 1986 paper.
% Therefore, performing the analysis of agreement without taking into
% account this aspect is incorrect. However, it is a good "benchmark" to
% have a first impression on the agreement between Kinect and Vicon.
% Moreover, given the results provided by Prof Gordon using the correct
% approach, it seems that the error introduced by the incorrect approach is
% negligible.
%
% Input:
%   trialTypes = (optional parameter, default = see below) cell array of
%                strings containing types of trials to be loaded. See
%                ListTrialTypeAgreementStudy.m for accepted strings.
%   rootFolder = (optional parameter, default = '') folder containing all
%                trials to be loaded. The expected folder structure is
%                shown in list_trials_in_all_subjects.m. If empty, a dialog
%                window will open to select a root folder.
%
% � November 2016 Alessandro Timmi


%% Default input values:
default.trialTypes = {'gait_slow', 'gait_fast', 'run_slow', 'run_fast'};
default.rootFolder = 'C:\TEST_FOLDER\5 - Ready for statistical analysis (Bland-Altman analyses included)';

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'trialTypes', default.trialTypes,...
    @(x) validateattributes(x, {'cell'}, {'nonempty'}));
addParameter(p, 'rootFolder', default.rootFolder,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
trialTypes = p.Results.trialTypes;
rootFolder = p.Results.rootFolder;

clear varargin default p


%% Select root folder
if isempty(rootFolder)
    rootFolder = uigetdir('', 'Select root folder containing Kinect and Vicon data');
    if isequal(rootFolder, 0)
        return
    end
end

% Enable logging:
LogFile(fullfile(rootFolder, 'Bland-Altman1986SpatioTemporal.log'));

fprintf('Bland-Altman analysis of agreement (1986 version) for the Treadmill Study (spatiotemporal parameters)\n')

subjectsPrefix = 'TML';
% Preallocate storage arrays for biases and limits of agreement:
analysisTypes = {'Swing length'};
analysisUnits = {'mm'};

biasStorage = zeros(length(trialTypes), length(analysisTypes));
lloaStorage = zeros(size(biasStorage));
uloaStorage = zeros(size(biasStorage));


% Analyse data by trial type:
for t = 1:length(trialTypes)
    
    % List trials of a specific type for both Kinect and Vicon:
    [kinectTrials.(trialTypes{t}), viconTrials.(trialTypes{t})] =...
        ListTrialTypeAgreementStudy(trialTypes{t},...
        'rootFolder', rootFolder);
    
    % Load trials of current type into arrays of structs:
    kinectArrayOfStructs = LoadTrcToArrayOfStructs(kinectTrials.(trialTypes{t}),...
        subjectsPrefix);
    viconArrayOfStructs = LoadTrcToArrayOfStructs(viconTrials.(trialTypes{t}),...
        subjectsPrefix);
    
    % Check correspondence of structs between the two devices:
    CompareArraysOfStructsTreadmillStudy(kinectArrayOfStructs, viconArrayOfStructs);
    
    
    % Detect gait events and calculate spatiotemporal parameters for trials of current type:
    [kinectArrayOfStructs, viconArrayOfStructs] =...
        GetGaitEventsAndSpatiotemporalParameters(kinectArrayOfStructs, viconArrayOfStructs);
    
    
    %% Bland-Altman analysis of agreement (1986 version)
    % NOTE: we cannot concatenate all quantities in one go as we do for the
    % marker coordinates, because here the swingLength array is a 
    % non-standard fields for a .trc struct.
    % Concatenate swingLength from trials of current type and convert from
    % m to mm for ease of interpretation:
    viconSwingLength = vertcat(viconArrayOfStructs(:).swingLength) * 1000;
    kinectSwingLength = vertcat(kinectArrayOfStructs(:).swingLength) * 1000;
    
    % Figures handles for Bland-Altman analysis:
    figure('Name', sprintf('Histograms of the differences - %s', trialTypes{t}));
    % Get the subplot handles for histograms:
    haxHistogramSwingLength = subplot(length(analysisTypes), 1, 1);

    figure('Name', sprintf('Bland-Altman plots - %s', trialTypes{t}));
    % Get the subplot handles for Bland-Altman plots:
    haxBaSwingLength = subplot(length(analysisTypes), 1, 1);
    
    
    % Perform Bland-Altman analysis of agreement on current type of trials
    % and plot the results:
    [bias.swingLength, lloa.swingLength, uloa.swingLength] =...
        bland_altman_1986(...
        viconSwingLength,...
        kinectSwingLength,...
        sprintf('%s - %s', trialTypes{t}, analysisTypes{1}),...
        analysisTypes{1},...
        analysisUnits{1},...
        'hax_hist', haxHistogramSwingLength,...
        'hax_ba', haxBaSwingLength);
    
        
    %% Store results of analysis of agreement for summary plot vs all
    % treadmill speeds (i.e. all trial types):
    biasStorage(t,:) = [bias.swingLength];
    lloaStorage(t,:) = [lloa.swingLength];
    uloaStorage(t,:) = [uloa.swingLength];
    
end


%% Plot summarizing BA results for spatiotemporal parameters vs treadmill speed
treadmillSpeeds = speeds_treadmill_study(trialTypes);
plotMarkerSize = 16;

figure('Name', 'Bland-Altman (1986) vs treadmill speed - Spatiotemporal parameters')

for a = 1:length(analysisTypes)
    hax = subplot(1, length(analysisTypes), a);
    hold(hax, 'on');
    
    plot(hax, treadmillSpeeds, biasStorage(:,a),...
        'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', ':');
    
    plot(hax, treadmillSpeeds, lloaStorage(:,a),...
        'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', '--');
    
    plot(hax, treadmillSpeeds, uloaStorage(:,a),...
        'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', '--');
    
    hax.XTick = treadmillSpeeds;
    hax.XLim = custom_axis_margins(treadmillSpeeds, 0.15);
    hax.YGrid = 'on';
    hax.YAxis.Exponent = 0;
    lowerBound = min(lloaStorage(:, a));
    upperBound = max(uloaStorage(:, a));
    hax.YLim = custom_axis_margins([lowerBound, upperBound], 0.1);
    
    xlabel(hax, 'Treadmill speed [m/s]')
    ylabel(hax, {'KinEdge - Vicon'; sprintf('%s [%s]',...
        analysisTypes{a}, analysisUnits{a})})
    title(hax, sprintf('%s', analysisTypes{a}))
    
    if a==1
        legend(hax, 'Bias', 'LOA', 'location', 'NorthWest' )
    end
    
end

% Disable logging:
LogFile('off');

end
