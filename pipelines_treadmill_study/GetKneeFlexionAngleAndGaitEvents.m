function [kinectArrayOfStructs, viconArrayOfStructs] =...
    GetKneeFlexionAngleAndGaitEvents(kinectArrayOfStructs, viconArrayOfStructs)
% Detect gait events and calculate knee flexion angle from data collected
% during the Treadmill Study. Input arrays of structs are returned with
% results added as new fields.
%
% NOTE: we cannot find gait events in trials AFTER concatenating
% them in a single long trial. Indeed, in this way we would create
% artificial signal peaks at the junction between consecutive
% trials, thus affecting the detection of gait events. For this
% reason, we first detect gait events, store them into the current
% trials structs, and only then we proceed with concatenation.
%
% Input:
%   kinectArrayOfStructs = array of structs containing .trc data from the
%           Treadmill study, collected using Kinect.
%   viconArrayOfStructs = array of structs containing .trc data from the
%           Treadmill study, collected using Vicon.
% Output:
%   kinectArrayOfStructs = same as input, but with added fields:
%            theta, thetaFS, thetaTO.
%   viconArrayOfStructs = same as input, but with added fields:
%            locFS, locsTO, theta, thetaFS, thetaTO.
%
% � October 2016 Alessandro Timmi


for f = 1:length(viconArrayOfStructs)
    
    % Calculate knee flexion angle (theta) for Vicon and Kinect:
    viconArrayOfStructs(f).theta =...
        KneeFlexionAngle2dTreadmillStudy(viconArrayOfStructs(f));
    kinectArrayOfStructs(f).theta =...
        KneeFlexionAngle2dTreadmillStudy(kinectArrayOfStructs(f));
    
    % Gait events detection from each struct: foot strike (FS) and toe
    % off (TO). Since data have been interpolated at the same framerate,
    % synced and trimmed, we can detect gait events USING JUST VICON DATA,
    % which are supposed to be more accurate than those from Kinect.
    % Detected events will be stored as new fields into the Vicon structs:
    [viconArrayOfStructs(f).locsFS, viconArrayOfStructs(f).locsTO] =...
        detect_gait_events_simple(viconArrayOfStructs(f));       
    
    % Find knee flexion angle in correspondence of detected gait events
    % for both Vicon and Kinect structs:
    viconArrayOfStructs(f).thetaFS =...
        viconArrayOfStructs(f).theta(viconArrayOfStructs(f).locsFS);
    viconArrayOfStructs(f).thetaTO =...
        viconArrayOfStructs(f).theta(viconArrayOfStructs(f).locsTO);
    
    kinectArrayOfStructs(f).thetaFS =...
        kinectArrayOfStructs(f).theta(viconArrayOfStructs(f).locsFS);
    kinectArrayOfStructs(f).thetaTO =...
        kinectArrayOfStructs(f).theta(viconArrayOfStructs(f).locsTO);
    
end

end