% Compare results of the Treadmill Study obtained from two different
% Bland-Altman analyses of agreement on knee flexion angle:
% - "naive": ignoring the structure of the data and applying the simple
%   analysis suggested in Bland and Altman's paper on The Lancet (1986).
% - "Gordon": taking into account the structure of the data via a random
%   effect model, which includes within subjects and between-subjects
%   variation and autocorrelation.
% NOTE: "balanced" data were used for each analysis, which means all trials
% (for a specific treadmill speed) were cropped at the same length.
%
% � October 2016 Alessandro Timmi

clc
%close all
clear

% True to compare "Naive" and "Gordon" methods, false to show only "Gordon"
% method:
compareMethods = false;

%% Input data:
rootFolder =...
    ['C:\TEST_FOLDER\5 - Ready for statistical analysis (Bland-Altman analyses included)\'...
    'Bland-Altman analysis (Gordon version)'];

resultsFilename = fullfile(rootFolder,...
    'adjusted limits and margins of error knee flexion angles 16-10-28_AT.xlsx');
trialTypes = {'gait_slow', 'gait_fast', 'run_slow', 'run_fast'};
analysisTypes = {'', ' at FS', ' at TO'};

% Number of coords (Knee angle only) for each marker:
nCoords = 1;

% Preallocate storage arrays for biases and limits of agreement:
gordonBiasStorage = zeros(nCoords, length(analysisTypes), length(trialTypes));
gordonLloaStorage = zeros(size(gordonBiasStorage));
gordonUloaStorage = zeros(size(gordonBiasStorage));

naiveBiasStorage = zeros(size(gordonBiasStorage));
naiveLloaStorage = zeros(size(gordonBiasStorage));
naiveUloaStorage = zeros(size(gordonBiasStorage));

% Since we want subplots to advance column-wise instead of row-wise (which
% is the default in MATLAB), we need to create custom subplot indices:
spIDs = columnwise_subplots_ids(numel(analysisTypes), nCoords);


%% Read results from Gordon's spreadsheet. Also, circshift all arrays by
% one position along the first dimension, to have the following order of analysis:
% "all", "FS", "TO".
gordonGaitFastBias = circshift(xlsread(resultsFilename, 'C3:C5'), 1, 1);
gordonGaitFastLoa = circshift(xlsread(resultsFilename, 'J3:K5'), 1, 1);
naiveGaitFastBias = circshift(xlsread(resultsFilename, 'O3:O5'), 1, 1);
naiveGaitFastLoa = circshift(xlsread(resultsFilename, 'Q3:R5'), 1, 1);

gordonGaitSlowBias = circshift(xlsread(resultsFilename, 'C7:C9'), 1, 1);
gordonGaitSlowLoa = circshift(xlsread(resultsFilename, 'J7:K9'), 1, 1);
naiveGaitSlowBias = circshift(xlsread(resultsFilename, 'O7:O9'), 1, 1);
naiveGaitSlowLoa = circshift(xlsread(resultsFilename, 'Q7:R9'), 1, 1);

gordonRunFastBias = circshift(xlsread(resultsFilename, 'C11:C13'), 1, 1);
gordonRunFastLoa = circshift(xlsread(resultsFilename, 'J11:K13'), 1, 1);
naiveRunFastBias = circshift(xlsread(resultsFilename, 'O11:O13'), 1, 1);
naiveRunFastLoa = circshift(xlsread(resultsFilename, 'Q11:R13'), 1, 1);

gordonRunSlowBias = circshift(xlsread(resultsFilename, 'C15:C17'), 1, 1);
gordonRunSlowLoa = circshift(xlsread(resultsFilename, 'J15:K17'), 1, 1);
naiveRunSlowBias = circshift(xlsread(resultsFilename, 'O15:O17'), 1, 1);
naiveRunSlowLoa = circshift(xlsread(resultsFilename, 'Q15:R17'), 1, 1);

% Copy results into the storage arrays:
newSize = [nCoords, length(analysisTypes)];
naiveBiasStorage(:,:,1) = reshape(naiveGaitSlowBias, newSize);
naiveBiasStorage(:,:,2) = reshape(naiveGaitFastBias, newSize);
naiveBiasStorage(:,:,3) = reshape(naiveRunSlowBias, newSize);
naiveBiasStorage(:,:,4) = reshape(naiveRunFastBias, newSize);

naiveLloaStorage(:,:,1) = reshape(naiveGaitSlowLoa(:,1), newSize);
naiveLloaStorage(:,:,2) = reshape(naiveGaitFastLoa(:,1), newSize);
naiveLloaStorage(:,:,3) = reshape(naiveRunSlowLoa(:,1), newSize);
naiveLloaStorage(:,:,4) = reshape(naiveRunFastLoa(:,1), newSize);

naiveUloaStorage(:,:,1) = reshape(naiveGaitSlowLoa(:,2), newSize);
naiveUloaStorage(:,:,2) = reshape(naiveGaitFastLoa(:,2), newSize);
naiveUloaStorage(:,:,3) = reshape(naiveRunSlowLoa(:,2), newSize);
naiveUloaStorage(:,:,4) = reshape(naiveRunFastLoa(:,2), newSize);


gordonBiasStorage(:,:,1) = reshape(gordonGaitSlowBias, newSize);
gordonBiasStorage(:,:,2) = reshape(gordonGaitFastBias, newSize);
gordonBiasStorage(:,:,3) = reshape(gordonRunSlowBias, newSize);
gordonBiasStorage(:,:,4) = reshape(gordonRunFastBias, newSize);

gordonLloaStorage(:,:,1) = reshape(gordonGaitSlowLoa(:,1), newSize);
gordonLloaStorage(:,:,2) = reshape(gordonGaitFastLoa(:,1), newSize);
gordonLloaStorage(:,:,3) = reshape(gordonRunSlowLoa(:,1), newSize);
gordonLloaStorage(:,:,4) = reshape(gordonRunFastLoa(:,1), newSize);

gordonUloaStorage(:,:,1) = reshape(gordonGaitSlowLoa(:,2), newSize);
gordonUloaStorage(:,:,2) = reshape(gordonGaitFastLoa(:,2), newSize);
gordonUloaStorage(:,:,3) = reshape(gordonRunSlowLoa(:,2), newSize);
gordonUloaStorage(:,:,4) = reshape(gordonRunFastLoa(:,2), newSize);


%% Plot results of the Bland-Altman analysis vs treadmill speed:
treadmillSpeeds = speeds_treadmill_study(trialTypes);
if compareMethods
    titleBAvsSpeed = 'Bland-Altman analyses comparison - "Naive" vs "Gordon"';
else
    titleBAvsSpeed = 'Bland-Altman analysis "Gordon"';
end
figure('Name', titleBAvsSpeed, 'Position', [100, 100, 1100, 350])
plotMarkerSizeNaive = 20;
plotMarkerSize = 16;

lowerBound = min(gordonLloaStorage(:));
upperBound = max(gordonUloaStorage(:));

for c = 1:nCoords
    for m = 1:numel(analysisTypes)
        hax = subplot(nCoords, numel(analysisTypes), spIDs(c, m));
        hold(hax, 'on');
        
        if compareMethods
            hNaiveBias = plot(hax, treadmillSpeeds, squeeze(naiveBiasStorage(c,m,:)),...
                'r', 'Marker', '.', 'MarkerSize', plotMarkerSizeNaive, 'linestyle', '--');
            
            plot(hax, treadmillSpeeds, squeeze(naiveLloaStorage(c,m,:)),...
                'r', 'Marker', '.', 'MarkerSize', plotMarkerSizeNaive, 'linestyle', '-');
            
            plot(hax, treadmillSpeeds, squeeze(naiveUloaStorage(c,m,:)),...
                'r', 'Marker', '.', 'MarkerSize', plotMarkerSizeNaive, 'linestyle', '-');
        end
        
        hGordonBias = plot(hax, treadmillSpeeds, squeeze(gordonBiasStorage(c,m,:)),...
            'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', ':');
        
        hGordonLoa = plot(hax, treadmillSpeeds, squeeze(gordonLloaStorage(c,m,:)),...
            'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', '--');
        
        plot(hax, treadmillSpeeds, squeeze(gordonUloaStorage(c,m,:)),...
            'k', 'Marker', '.', 'MarkerSize', plotMarkerSize, 'linestyle', '--');
        
        hax.XTick = treadmillSpeeds;
        hax.XLim = custom_axis_margins(treadmillSpeeds, 0.15);
        hax.YGrid = 'on';
        hax.YAxis.Exponent = 0;
        hax.YLim = custom_axis_margins([lowerBound, upperBound], 0.03);
        
        xlabel(hax, 'Treadmill speed [m/s]')
        ylabel(hax, {'KinEdge - Vicon'; ['Knee flexion angle', analysisTypes{m}, ' [�]']})
        if m==1 && c==1
            if compareMethods
                legend(hax,...
                    [hNaiveBias, hGordonBias], {'Bias and LOA "Naive"', 'Bias and LOA "Gordon"'},...
                    'location', 'Best' )
            else
                legend(hax,...
                    [hGordonBias, hGordonLoa], {'Bias', 'LOA'},...
                    'location', 'Best' )
            end
        end
        
        title(hax, ['Knee flexion angle', analysisTypes{m}])      
        
    end
end