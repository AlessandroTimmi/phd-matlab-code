function PreprocessDataForTreadmillStudy()
% Process Kinect and Vicon data for the agreement study on the treadmill,
% preparing them to be analysed by the Bland-Altman pipeline.
% The only input requested by the user is selecting a root folder,
% containing all Kinect and Vicon trials, plus the required calibration
% trials.
%
% See root_folder_lister_agreement_study.m to know how the root folder
% must be structured.
%
% � November 2015 Alessandro Timmi

% Struct containing Kinect trials filenames:
kinect = struct;
% Struct containing Vicon trials filenames:
vicon = struct;


%% Read all required filenames from the root folder containing all Kinect
% and Vicon data from a treadmill session for multiple subjects:
[kinect.rig_trc, vicon.kinect_cal_c3d,...
    kinect.trials_trc, vicon.trials_c3d, root_folder] =...
    root_folder_lister_agreement_study('TML');


%% Process Kinect data
[~, kinect.rig_preproc_trc, kinect.trials_preproc_trc] =...
    preprocess_kinect_data_pipeline(...
    'trc_cal_filenames', {false},...
    'trc_rig_filenames', kinect.rig_trc,...
    'trc_trial_filenames', kinect.trials_trc,...
    'debug_mode', false);


%% Process Vicon data
% Extract calibration trial, to determine Kinect ORIENTATION in Vicon space:
[~, vicon.kinect_cal_trc] = ExtractC3D(vicon.kinect_cal_c3d,...
    'markersetName', 'Agreement_orientation_reflective',...
    'trimTrial', false,...
    'rotateViconToOpenSim', false,...
    'destinationFilename', 'Kinect_orientation');

% Extract calibration trial, to determine the TRANSLATION vector from Vicon
% to Kinect origin:
[~, vicon.rig_trc] = ExtractC3D(vicon.kinect_cal_c3d,...
    'markersetName', 'Agreement_translation_reflective',...
    'trimTrial', false,...
    'rotateViconToOpenSim', false,...
    'destinationFilename', 'translation_rig');

% Calculate midpoints of Vicon translation_rig trial: 
vicon.rig_mid_trc = MidpointPipeline(vicon.rig_trc);

% Extract Vicon trials:
[~, vicon.trials_trc] = ExtractC3D(vicon.trials_c3d,...
    'markersetName', 'Agreement_dynamic_reflective',...
    'trimTrial', false,...
    'rotateViconToOpenSim', false);

% Calculate midpoints of trials in which 3D-printed rigs were used:
vicon.trials_mid_trc = MidpointPipeline(vicon.trials_trc);

% Transform trials from Vicon to Kinect coordinate system, using
% calibration files (orientation and translation):
vicon.trials_mid_transf_trc = transform_trc_from_vicon_to_kinect_pipeline(...
    'trc_orientation_filename', vicon.kinect_cal_trc{1},...
    'trc_rig_vicon_filename', vicon.rig_mid_trc{1},...
    'trc_rig_kinect_filename', kinect.rig_preproc_trc{1},...
    'trc_trial_filenames', vicon.trials_mid_trc);


%% Interp and sync Kinect and Vicon trials

% Check names of current pair of Vicon and Kinect trials, to see if
% they match (in subject number, trial type and trial number).
% Sample filenames:
% C:\Treadmill_22June2016\Kinect\TML14\gait_fast_01.trc
% C:\Treadmill_22June2016\Vicon\TML14\gait_fast_01.c3d

% This expression specifies that the string we are interested in:
%   - begins with "TML"
%   - followed by 2 numeric digits (\d\d)
%   - followed by a slash (\\)
%   - followed by 1 or more alpha-numeric characters (\w+)
%   - ends with 2 numeric digits (\d\d)
trialRegularExpression = 'TML\d\d\\\w+\d\d';

% Interp and sync trials:
[vicon.trials_mid_transf_sync_trc, kinect.trials_preproc_sync_trc] =...
    SyncTrcFilesPipeline(vicon.trials_mid_transf_trc,...
    kinect.trials_preproc_trc, markerset('Agreement_dynamic_coloured'),...
    trialRegularExpression);
            
                 
%% Downsample Kinect and Vicon trials:
kinect.trials_preproc_sync_down_trc = DownsampleTrcTrials(kinect.trials_preproc_sync_trc);    
vicon.trials_mid_transf_sync_down_trc = DownsampleTrcTrials(vicon.trials_mid_transf_sync_trc);


%% Create a separate folder with only 1 trial per type, preserving the
% original folder structure:
new_root_folder = fullfile(root_folder, 'Pre-processed');
SelectAndStorePreprocessedTreadmillTrials(kinect.trials_preproc_sync_down_trc,...
    vicon.trials_mid_transf_sync_down_trc, new_root_folder);

end