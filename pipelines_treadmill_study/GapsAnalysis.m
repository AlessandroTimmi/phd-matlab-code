function GapsAnalysis(varargin)
% Quantification of gaps in KinEdge data captured during the Treadmill
% Study. Results of this analysis are used for the Methodological paper.
% The aim of this analysis is to verify how many gaps there are in KinEdge
% data and if there is any correlation with treadmill speed and marker
% anatomical position.
%
% Raw KinEdge data from Treadmill Study trials are used for this analysis.
%
% Input:
%   trialTypes = (optional parameter, default = see below) cell array of
%                strings containing types of trials to be loaded. See
%                ListTrialTypeAgreementStudy.m for accepted strings.
%   rootFolder = (optional parameter, default = '') folder containing all
%                trials to be loaded. The expected folder structure is
%                shown in list_trials_in_all_subjects.m. If empty, a dialog
%                window will open to select a root folder.
%
% � November 2016 Alessandro Timmi


%% Default input values:
default.trialTypes = {'gait_slow', 'gait_fast', 'run_slow', 'run_fast'};
default.rootFolder = 'C:\TEST_FOLDER\Raw data for methodological paper';

% Setup input parser:
p = inputParser;
p.CaseSensitive = false;

% Add optional input arguments in the form of name-value pairs:
addParameter(p, 'trialTypes', default.trialTypes,...
    @(x) validateattributes(x, {'cell'}, {'nonempty'}));
addParameter(p, 'rootFolder', default.rootFolder,...
    @(x) validateattributes(x, {'char'}, {'nonempty'}));

% Parse input arguments:
parse(p, varargin{:});

% Copy input arguments into more memorable variables:
trialTypes = p.Results.trialTypes;
rootFolder = p.Results.rootFolder;

clear varargin default p


%% Select root folder
if isempty(rootFolder)
    rootFolder = uigetdir('', 'Select root folder containing Kinect data');
    if isequal(rootFolder, 0)
        return
    end
end

fprintf('Gaps analysis of KinEdge data from the Treadmill Study\n')

subjectsPrefix = 'TML';
nSubjects = 20;
markerNames = markerset('Agreement_dynamic_coloured');


%% Analyse data:
% Preallocate a matrix to store number of gaps per trial: we use NaNs so
% that we can skip missing trials in the rest of the analysis.
gapsPerTrial = nan(nSubjects, length(markerNames), length(trialTypes));
% Preallocate a matrix to store the number of frames of each trial:
nFramesPerTrial = nan(nSubjects, length(trialTypes));

% Analyse data by trial type:
for t = 1:length(trialTypes)
    
    % List trials of a specific type for both Kinect and Vicon:
    [kinectTrials.(trialTypes{t}), ~] =...
        ListTrialTypeAgreementStudy(trialTypes{t}, 'rootFolder', rootFolder);
    
    % Load trials of current type into arrays of structs:
    kinectArrayOfStructs = LoadTrcToArrayOfStructs(kinectTrials.(trialTypes{t}),...
        subjectsPrefix);
    
    % Gaps are stored as NaNs in data. When a gap is present, it
    % all 3 coordinates (X, Y, Z) of a marker are missing, so we just
    % analyse the X coordinate of each marker:
    for s = 1:length(kinectArrayOfStructs)
        for m = 1:length(markerNames)
            gapsPerTrial(s, m, t) = sum(isnan(...
                kinectArrayOfStructs(s).markers.(markerNames{m})(:,1)));
        end
        
        % Store number of frames of current trial:
        nFramesPerTrial(s,t) = kinectArrayOfStructs(s).NumFrames;
    end
end


%% Histogram of gaps from all trials, grouped by marker and treadmill
% speed:
myTitleHist = 'Histograms of gaps';
figure('Name', myTitleHist)
matlabOrange = [0.8500    0.3250    0.0980];

for t = 1:length(trialTypes)
    for m = 1:length(markerNames)
        
        subplot(length(trialTypes), length(markerNames),...
            m + length(markerNames) * (t-1))
        
        
        dataHistogram = gapsPerTrial(:,m,t);
        histogram(dataHistogram, 'EdgeColor', 'none')
        
        if t == length(trialTypes)
            xlabel('Gaps [#]')
        end
        if m == 1
            ylabel('Occurrencies [#]')
        end
        if m == 2 && t == 1
            title({myTitleHist; sprintf('%s %s', markerNames{m}, trialTypes{t})},...
                'Interpreter', 'none')
        else
            title(sprintf('%s %s', markerNames{m}, trialTypes{t}),...
                'Interpreter', 'none')
        end
        
        
        meanHistogram = mean(dataHistogram, 'omitnan');
        sdHistogram = std(dataHistogram, 'omitnan');
        meanMinusSdHistogram = meanHistogram - sdHistogram;
        meanPlusSdHistogram = meanHistogram + sdHistogram;
        
        medianHistogram = median(dataHistogram, 'omitnan');
        % The function quantile() omits NaNs by default:
        firstQuartileHistogram = quantile(dataHistogram, 0.25);
        thirdQuartileHistogram = quantile(dataHistogram, 0.75);
        
        % Plot the histogram:

        yLimits = ylim;
        hold on
        
        % Plot statistical parameters:
        hMeanSD = fill([meanMinusSdHistogram, meanPlusSdHistogram, meanPlusSdHistogram, meanMinusSdHistogram],...
            [yLimits(2), yLimits(2), 0, 0], matlabOrange, 'FaceAlpha', 0.20, 'EdgeColor', matlabOrange);
        hMean = plot([meanHistogram, meanHistogram], [0, yLimits(2)], '--', 'Color', matlabOrange, 'linewidth', 1.5);
        
        hIQR = fill([firstQuartileHistogram, thirdQuartileHistogram, thirdQuartileHistogram, firstQuartileHistogram],...
            [yLimits(2), yLimits(2), 0, 0], 'k', 'FaceAlpha', 0.20, 'EdgeColor', 'k');
        hMedian = plot([medianHistogram, medianHistogram], [0, yLimits(2)], '--', 'Color', 'k', 'linewidth', 1.5);
        
        ylim(yLimits)
        %xlim(custom_axis_margins([meanHistogram - sdHistogram, meanHistogram + sdHistogram], 0.3))
        xlabel('capture framerate [fps]')
        if m == 1 && t == 1
            ylabel('Occurrencies [#]')
            
            legend([hMean, hMeanSD, hMedian, hIQR], {'Mean', 'Mean\pmSD',...
                'Median', 'IQR'},...
                'location', 'northwest')
        end

    end
end


%% Since gaps have a very right-skewed distribution, the standard deviation
% is grossly inflated and is not a good measure of variability to use.
% http://www.bmj.com/about-bmj/resources-readers/publications/statistics-square-one/2-mean-and-standard-deviation
% Moreover, the number of gaps is zero in most conditions and very low in
% the remaining ones. For this reason, a statistical analysis is
% superfluous.
% We prefer to plot the sum of gaps vs treadmill speeds, grouping by
% marker anatomical position. The sum of gaps is reported as percentage of
% total frames.
sumGaps = sum(gapsPerTrial, 'omitnan');
sumGapsSqueezed = squeeze(sumGaps);

% Calculate total number of frames for each treadmill speed:
sumNumFrames = sum(nFramesPerTrial, 'omitnan');
sumNumFramesRepeated = repmat(sumNumFrames, [3,1]);

% Calculate percentage of gaps out of total frames for each marker and
% treadmill speed:
sumGapsSqueezedPercent = sumGapsSqueezed ./ sumNumFramesRepeated * 100;

% Load treadmill speeds, used for plot:
treadmillSpeeds = speeds_treadmill_study(trialTypes);


%% Plot results:
myTitle = 'Gaps analysis';
figure('Name', myTitle)
ax = gca;
hold(ax, 'on')
ax.XLim = custom_axis_margins(treadmillSpeeds, 0.15);
ax.YLim = custom_axis_margins([min(sumGapsSqueezedPercent), max(sumGapsSqueezedPercent)], 0.15);
ax.XTick = treadmillSpeeds;
ax.YGrid = 'on';
hRefline = refline(0,0);
set(hRefline, 'color', 'k', 'linestyle', '-')

plotMarkerSizes = [40,30,20];
hPlots = zeros(1, length(markerNames));
for m = 1:length(markerNames)
    hPlots(m) = plot(treadmillSpeeds, sumGapsSqueezedPercent(m,:),...
        '.', 'markersize', plotMarkerSizes(m));
end

legend(hPlots, markerNames, 'location', 'best')
title(myTitle)
xlabel('Treadmill speed [m/s]')
ylabel('Gaps / Total frames [%]')


end
